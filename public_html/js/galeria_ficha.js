$(document).ready(function() {

    var btn_proximo = ".btn_proximo_ficha";
    var btn_anterior = ".btn_anterior_ficha";
    var listar = "#galeria_ficha";
    var bg_anterior = "url(img/slide_esquerda.jpg) no-repeat center";
    var bg_proximo = "url(img/slide_direita.jpg) no-repeat center";
    var tamanhobox = 183;
    var quantidadeitensexibido = 5;

    var quantidade = ($(listar).find(".image").size());
    ;
    var larguratotal = quantidade * tamanhobox;

    $(listar).css("width", larguratotal);

    quantidade = quantidade - quantidadeitensexibido;
    totalx = quantidade * -tamanhobox;
    var executando = false;


    $(btn_proximo).live('click', function() {
        if (executando) {
            return false;
        }

        executando = true;
        var deslocamento_left = $(listar).css('left');

        var andamento_maximo = totalx; // + "px";;
        andamento_maximo = parseInt(andamento_maximo);

        var deslocamento_ja_feito = $(listar).css('left');
        deslocamento_ja_feito = deslocamento_ja_feito.replace("px", "");
        if (deslocamento_ja_feito == "auto") {
            deslocamento_ja_feito = 0;
        }
        deslocamento_ja_feito = parseInt(deslocamento_ja_feito);

        if (deslocamento_ja_feito > andamento_maximo) {
            $(btn_proximo).css('background', bg_proximo);
            $(btn_anterior).css('background', bg_anterior);
            $(listar).stop().animate({"left": "-=" + tamanhobox}, 300, function() {
                executando = false;
            });
        } else {
            $(btn_proximo).css('background', "#FFF");
            executando = false;
            return false;
        }
    });


    $(btn_anterior).live('click', function() {
        if (executando) {
            return false;
        }
        executando = true;

        var deslocamento_left = $(listar).css('left');
        deslocamento_left = deslocamento_left.replace("px", "");

        if (deslocamento_left == "auto") {
            deslocamento_left = 0;
        }

        deslocamento_left = parseInt(deslocamento_left);


        if (deslocamento_left < 0) {
            $(btn_proximo).css('background', bg_proximo);
            $(btn_anterior).css('background', bg_anterior);
            $(listar).stop().animate({"left": "+=" + tamanhobox}, 300, function() {
                executando = false;
            });
            return false;
        } else {
            $(btn_anterior).css('background', "#FFF");

            executando = false;
            return false;

        }

    });
});