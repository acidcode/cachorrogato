/*
 * 
 * @returns {Boolean}
 */
function validarStep1CadastroEmpresa() {
    $('.alertaerro').hide('slow');
    $('#dia_atividade').removeAttr('style');
    $('#setor_atividade').removeAttr('style');
    $('#formulariodecadastro label').removeAttr('style');
    var error = 0;
    //<!--Primeiro bloco do formulário-->
    if ($('#vc_nome_empresa').val() == '') {
        $('#label_vc_nome_empresa').css('border', '1px solid #ff0000');
        error++;
    }

    if ($('#vc_acesso_email').val() == '') {
        $('#label_vc_acesso_email').css('border', '1px solid #ff0000');
        error++;
    }

    if ($('#vc_acesso_email_confirmar').val() == '') {
        $('#label_vc_acesso_email_confirmar').css('border', '1px solid #ff0000');
        error++;
    }

    if (!verificaEmail()) {
        error++;
    }

    if ($('#vc_acesso_senha').val() == '') {
        $('#label_vc_acesso_senha').css('border', '1px solid #ff0000');
        error++;
    }

    if ($('#vc_acesso_senha_confirmar').val() == '') {
        $('#label_vc_acesso_senha_confirmar').css('border', '1px solid #ff0000');
        error++;
    }

    if (!verificaSenha()) {
        error++;
    }
//<!--/Primeiro bloco do formulário-->
//<!--Descreva sua empresa-->
    /*if ($('#vc_descricao_empresa').val() == '') {
     $('#vc_descricao_empresa').css('border', '1px solid #ff0000');
     error++;
     }*/
//<!--/Descreva sua empresa-->
//<!--Endereço-->
    if ($('#it_cep').val() == '') {
        $('#label_it_cep').css('border', '1px solid #ff0000');
        error++;
    }
    if ($('#vc_estado').val() == '') {
        $('#label_vc_estado').css('border', '1px solid #ff0000');
        error++;
    }
    if ($('#vc_cidade').val() == '') {
        $('#label_vc_cidade').css('border', '1px solid #ff0000');
        error++;
    }
    if ($('#vc_endereco').val() == '') {
        $('#label_vc_endereco').css('border', '1px solid #ff0000');
        error++;
    }
    if ($('#vc_numero').val() == '') {
        $('#label_vc_numero').css('border', '1px solid #ff0000');
        error++;
    }
//<!--/Endereço-->
//<!--Dados do contato-->
    if ($('#vc_pessoa_contato').val() == '') {
        $('#label_vc_pessoa_contato').css('border', '1px solid #ff0000');
        error++;
    }

    if ($('#vc_nome_proprietario').val() == '' && !$('#bl_dono').is(':checked')) {
        $('#label_vc_nome_proprietario').css('border', '1px solid #ff0000');
        error++;
    }

    if ($('#vc_contato_email').val() == '') {
        $('#label_vc_contato_email').css('border', '1px solid #ff0000');
        error++;
    }

    if ($('#it_telefone').val() == '') {
        $('#label_it_telefone').css('border', '1px solid #ff0000');
        error++;
    }

    if ($('#vc_email_contato_empresa').val() == '') {
        $('#label_vc_email_contato_empresa').css('border', '1px solid #ff0000');
        error++;
    }

    if (!$('.dia_atividade').is(':checked')) {
        $('#dia_atividade').css('border', '1px solid #ff0000');
        error++;
    }

    if (!$('#bl_periodo_de_atividade').is(':checked')) {
        if ($('#tm_periodo_de_atividade_inicio').val() == '') {
            $('#label_tm_periodo_de_atividade_inicio').css('border', '1px solid #ff0000');
            error++;
        }

        if ($('#tm_periodo_de_atividade_termino').val() == '') {
            $('#label_tm_periodo_de_atividade_termino').css('border', '1px solid #ff0000');
            error++;
        }
    }

    if (!$('.setor_atividade').is(':checked')) {
        $('#setor_atividade').css('border', '1px solid #ff0000');
        error++;
    }
//<!--/Dados do contato-->

    if (error > 0) {
        $('.alertaerro').show('fast');
        return false;
    } else {
        $('.alertaerro').hide('slow');

        var vc_dias_atividade = Array();

        $('.dia_atividade').each(function() {
            if ($(this).is(':checked')) {
                vc_dias_atividade.push($(this).attr('value'));
            }
        });

        $.ajax({
            url: "/frontend/acesso-empresa/cadastro/",
            type: "POST",
            data: ({
                vc_nome_empresa: $('#vc_nome_empresa').val(),
                vc_acesso_email: $('#vc_acesso_email').val(),
                vc_acesso_senha: $('#vc_acesso_senha').val(),
                vc_descricao_empresa: $('#vc_descricao_empresa').val(),
                it_cep: $('#it_cep').val(),
                vc_estado: $('#vc_estado').val(),
                vc_cidade: $('#vc_cidade').val(),
                vc_endereco: $('#vc_endereco').val(),
                vc_numero: $('#vc_numero').val(),
                vc_complemento: $('#vc_complemento').val(),
                vc_pessoa_contato: $('#vc_pessoa_contato').val(),
                vc_nome_proprietario: $('#vc_nome_proprietario').val(),
                bl_dono: $('#bl_dono').val(),
                vc_contato_email: $('#vc_contato_email').val(),
                it_telefone: $('#it_telefone').val(),
                it_celular: $('#it_celular').val(),
                vc_email_contato_empresa: $('#vc_email_contato_empresa').val(),
                vc_pagina_web: $('#vc_pagina_web').val(),
                vc_dias_atividade: vc_dias_atividade,
                tm_periodo_de_atividade_inicio: $('#tm_periodo_de_atividade_inicio').val(),
                tm_periodo_de_atividade_termino: $('#tm_periodo_de_atividade_termino').val(),
                bl_periodo_de_atividade: $('#bl_periodo_de_atividade').val(),
                id_setor_de_atividade: getItens('setor_atividade'),
                bl_ativo: $('#bl_ativo').val(),
                ajax: true
            }),
            dataType: 'json',
            success: function(data) {
                if (data) {
                    $.cookie(data.nome, data.data, {expires: data.expira, path: data.caminho, domain: data.dominio});
                    window.location = '/frontend/acesso-empresa/galeria/';
                } else {

                }

            }
        });

        return false;
    }

}

/**
 * 
 * @returns {undefined}
 */
function verificaDonoEmpresa() {
    if ($('#bl_dono').val() == 'sim') {
        $('#bl_dono').val('nao');
        $('#vc_nome_proprietario').attr('disabled', 'disabled');
        $('#vc_nome_proprietario').val('');
    } else {
        $('#bl_dono').val('sim');
        $('#vc_nome_proprietario').removeAttr('disabled');
    }
}

/**
 * @param {json} json Json com os ids das perguntas obrigatórias
 * @returns {undefined}
 */
function enviarFormularioQuestionario(json) {
    $('#formulariodecadastro').submit();
}

/**
 * 
 * @returns {undefined}
 */
function enviarFoto() {
    $('#form-upload-foto').submit();
}

/**
 * 
 * @param {int} foto
 * @returns {void}
 */
function deletarFotoEmpresa(foto) {
    $.ajax({
        url: "/frontend/acesso-empresa/deletar-foto",
        type: "POST",
        data: ({
            id_foto: foto
        }),
        dataType: "json",
        success: function(data) {
            alert('Ocorreu um erro ao excluir a foto desejada, tente novamente mais tarde');
        }
    });
    $('#foto' + foto).fadeOut('hide');
}

/**
 * 
 * @returns {void}
 */
function validarStep2CadastroEmpresa() {
    $.ajax({
        url: "/frontend/acesso-empresa/verifica-total-fotos",
        type: "POST",
        data: ({
            ajax: true
        }),
        dataType: "html",
        success: function(data) {
            if (data) {
                window.location = '/frontend/acesso-empresa/questionario/';
            } else {
                alert('Envie no mínimo 8 fotos');
            }
        }
    });
}

/**
 * 
 * @returns {Boolean}
 */
function verificaEmail() {
    $.ajax({
        url: "/frontend/acesso-empresa/verifica-email",
        type: "POST",
        data: ({
            vc_acesso_email: $('#vc_acesso_email').val()
        }),
        dataType: "json",
        success: function(data) {
            if (!data) {
                if ($('#alert_email').length > 0) {
                    $('#alert_email').remove();
                }
                return true;
            } else {
                if ($('#alert_email').length == 0 && $('#vc_acesso_email').val() != '' && $('vc_acesso_email_confirmar').val() != '') {
                    $('#label_vc_acesso_email_confirmar').after('<span id="alert_email" class="msgerro">E-mail desejado ja está em uso no nosso sistema</span>');
                }
                if ($('#alerta_email_diferente').length > 0) {
                    $('#alerta_email_diferente').remove();
                }
                $('#vc_acesso_email').val('');
                $("#vc_acesso_email_confirmar").val('');
                return false;
            }
        }
    });

    if ($('#vc_acesso_email').val() != $('#vc_acesso_email_confirmar').val()) {
        $('#label_vc_acesso_email').css('border', '1px solid #ff0000');
        $('#label_vc_acesso_email_confirmar').css('border', '1px solid #ff0000');

        if ($('#alerta_email_diferente').length == 0) {
            $('#label_vc_acesso_email_confirmar').after('<span id="alerta_email_diferente" class="msgerro">Os e-mails informados não conferem</span>');
            if ($('#alert_email').length > 0) {
                $('#alert_email').remove();
                $('#label_vc_acesso_email').removeAttr('style');
                $('#label_vc_acesso_email_confirmar').removeAttr('style');
            }
        }
        return false;
    } else {
        if ($('#alert_email').length > 0) {
            $('#alert_email').remove();
            $('#label_vc_acesso_email').removeAttr('style');
            $('#label_vc_acesso_email_confirmar').removeAttr('style');
        }
        if ($('#alerta_email_diferente').length > 0) {
            $('#label_vc_acesso_email').removeAttr('style');
            $('#label_vc_acesso_email_confirmar').removeAttr('style');
            $('#alerta_email_diferente').remove();
        }
        return true;
    }

}

/**
 * 
 * @returns {Boolean}
 */
function verificaSenha() {
    if ($('#vc_acesso_senha').val() != $('#vc_acesso_senha_confirmar').val()) {
        $('#label_vc_acesso_senha').css('border', '1px solid #ff0000');
        $('#label_vc_acesso_senha_confirmar').css('border', '1px solid #ff0000');

        if ($('#alerta_senha_diferente').length == 0) {
            $('#label_vc_acesso_senha_confirmar').after('<span id="alerta_senha_diferente" class="msgerro">As senhas informadas não conferem</span>');
        }
        return false;
    }
    if ($('#alerta_senha_diferente').length > 0) {
        $('#label_vc_acesso_senha').removeAttr('style');
        $('#label_vc_acesso_senha_confirmar').removeAttr('style');
        $('#alerta_senha_diferente').remove();
    }
    return true;
}

/**
 * 
 * @returns {void}
 */
function buscarEnderecoPorCep() {
    if ($('#alerta_cep').length == 0) {
        $('#vc_endereco').val('Aguarde...');
    }
    if ($('#alerta_cep_nao_encontrado').length > 0) {
        $('#vc_endereco').val('');
    }
    $.ajax({
        url: "/frontend/acesso-empresa/buscar-endereco-por-cep/cep/" + $("#it_cep").val(),
        global: false,
        type: "GET",
        dataType: "json",
        success: function(json) {
            if (json.endereco == null && json.endereco == '') {
                $('#vc_endereco').val('Cep não encontrado');
                $('#label_vc_endereco').hide();
                $('#label_vc_cidade').hide();
                $('#label_vc_estado').hide();
                $('#label_vc_bairro').hide();
            } else {
                $("#vc_endereco").val(json.endereco);
                $("#vc_bairro").val(json.bairro);
                $("#vc_cidade").val(json.cidade);
                $("#vc_estado").val(json.estado);
                $("#vc_numero").focus();
            }
            $('#alerta_cep').remove();
        }
    });

    $('#label_vc_endereco').show();
    $('#label_vc_cidade').show();
    $('#label_vc_estado').show();
    $('#label_vc_bairro').show();
}

/**
 * Retorna os itens selcionados em um checkbox ou radio button
 * apartir da classe
 * @param {string} classe
 * @returns {int}
 */
function getItens(classe) {
    var id_foto = 0;
    $('.' + classe).each(function() {
        if ($(this).is(':checked')) {
            id_foto = $(this).val();
        }
    });

    return id_foto;
}

/**
 * Função adionada ao clicar no radio button "Logotipo"
 * 
 * @param {int} id_fornecedor
 * @returns {boolean}
 */
function salvarLogotipo(id_fornecedor) {
    $.ajax({
        url: "/frontend/acesso-empresa/salvar-logotipo/",
        global: false,
        type: "POST",
        dataType: "json",
        data: ({
            id_foto: getItens('bl_logotipo'),
            id_fornecedor: id_fornecedor
        }),
        success: function(json) {
            alert(json.mensagem);
        }
    });
}

/**
 * Função acionada ao clicar no radio button "Foto Principal"
 * 
 * @param {int} id_fornecedor
 * @returns {void}
 */
function salvarFotoPrincipal(id_fornecedor) {
    $.ajax({
        url: "/frontend/acesso-empresa/salvar-foto-principal/",
        global: false,
        type: "POST",
        dataType: "json",
        data: ({
            id_foto: getItens('bl_foto_principal'),
            id_fornecedor: id_fornecedor
        }),
        success: function(json) {
            alert(json.mensagem);
        }
    });
}

/**
 * Função acionada ao arrastar uma foto dentro ja enviada"
 * 
 * @param {int} id_fornecedor
 * @returns {void}
 */
function salvarOrdem(id_fornecedor) {
    var ordemFotos = Array();

    $('#sortable li').each(function() {
        ordemFotos.push($(this).attr('id'));
    });

    $.ajax({
        url: "/frontend/acesso-empresa/atualizar-ordem/",
        global: false,
        type: "POST",
        dataType: "html",
        data: ({
            fotos: ordemFotos,
            id_fornecedor: id_fornecedor
        }),
        success: function(json) {
            if (json.mensagem) {
                alert(json.mensagem);
            }
        }
    });
}