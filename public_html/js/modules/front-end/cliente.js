$(document).ready(function() {
    $('#label_vc_endereco').hide();
    $('#label_vc_cidade').hide();
    $('#label_vc_estado').hide();
    $('#label_vc_bairro').hide();
});

function validarStep1CadastroCliente(submit) {
    $('.alertaerro').hide('slow');
    $('#formulariodecadastro label').removeAttr('style');

    var error = 0;

    if ($('#vc_nome').val() == '') {
        $('#label_vc_nome').css('border', '1px solid #ff0000');
        error++;
    }

    if ($('#vc_sobrenome').val() == '') {
        $('#label_vc_sobrenome').css('border', '1px solid #ff0000');
        error++;
    }

    if (!$('#bl_sexo-masculino').is(':checked') && !$("#bl_sexo-feminino").is(':checked')) {
        $('#label_bl_sexo').css('border', '1px solid #ff0000');
        error++;
    }

    if ($('#dt_nascimento').val() == '') {
        $('#label_dt_nascimento').css('border', '1px solid #ff0000');
        error++;
    }

    if ($('#dt_nascimento').val() == '') {
        $('#label_dt_nascimento').css('border', '1px solid #ff0000');
        error++;
    }

    if ($('#it_cep').val() == '') {
        $('#label_it_cep').css('border', '1px solid #ff0000');
        error++;
    }

    if ($('#vc_endereco').val() == '') {
        $('#label_vc_endereco').css('border', '1px solid #ff0000');
        error++;
    }

    if ($('#vc_numero').val() == '') {
        $('#label_vc_numero').css('border', '1px solid #ff0000');
        error++;
    }

    if ($('#vc_bairro').val() == '') {
        $('#label_vc_bairro').css('border', '1px solid #ff0000');
        error++;
    }

    if ($('#vc_cidade').val() == '') {
        $('#label_vc_cidade').css('border', '1px solid #ff0000');
        error++;
    }

    if ($('#vc_estado').val() == '') {
        $('#label_vc_estado').css('border', '1px solid #ff0000');
        error++;
    }

    if ($('#vc_email').val() == '') {
        $('#label_vc_email').css('border', '1px solid #ff0000');
        error++;
    }

    if ($('#vc_email_confirmar').val() == '') {
        $('#label_vc_email_confirmar').css('border', '1px solid #ff0000');
        error++;
    }

    if ($('#vc_senha').val() == '') {
        $('#label_vc_senha').css('border', '1px solid #ff0000');
        error++;
    }

    if ($('#vc_senha_confirmar').val() == '') {
        $('#label_vc_senha_confirmar').css('border', '1px solid #ff0000');
        error++;
    }

    if (!verificaEmail()) {
        error++;
    }

    if (!verificaSenha()) {
        error++;
    }

    if (!$('#bl_termos_de_uso').is(':checked')) {
        $('#label_bl_termos_de_uso').css('padding', '3px 0px 5px 0');
        $('#texto-termos').css('display', 'block');
        $('#texto-termos').css('padding-top', '3px');
        $('#label_bl_termos_de_uso').css('border', '1px solid #ff0000');
        error++;
    }


    if (error > 0) {
        $('.alertaerro').show('fast');
        return false;
    } else {
        $('.alertaerro').hide('slow');
        if (submit) {
            $('#formulariodecadastro').submit();
        } else {
            return true;
        }
    }
}

function validarStep2CadastroCliente(submit) {
    $('.alertaerro').hide('slow');
    $('#formulariodecadastro label').removeAttr('style');

    var error = 0;

    if ($('#vc_cpf').val() == '') {
        $('#label_vc_cpf').css('border', '1px solid #ff0000');
        error++;
    }

    if ($('#vc_telefone').val() == '') {
        $('#label_vc_telefone').css('border', '1px solid #ff0000');
        error++;
    }

    if ($('#vc_celular').val() == '') {
        $('#label_vc_celular').css('border', '1px solid #ff0000');
        error++;
    }

    if (error > 0) {
        $('.alertaerro').show('fast');
        return false;
    } else {
        $('.alertaerro').hide('slow');
        if (submit) {
            $('#formulariodecadastro').submit();
        } else {
            return true;
        }
    }
}

function validarStep3CadastroCliente(submit) {
    $('.alertaerro').hide('slow');
    $('#formulariodecadastro label').removeAttr('style');

    var error = 0;

    if ($('#vc_nome_pet').val() == '') {
        $('#label_vc_nome_pet').css('border', '1px solid #ff0000');
        error++;
    }

    if (error > 0) {
        $('.alertaerro').show('fast');
        return false;
    } else {
        $('.alertaerro').hide('slow');
        if (submit) {
            $('#formulariodecadastro').submit();
        } else {
            return true;
        }
    }
}

function blSexoValue() {
    if ($('#bl_sexo-feminino').is(':checked')) {
        $('#bl_sexo_value').val('feminino');
    } else {
        $('#bl_sexo_value').val('masculino');
    }
}

function proximoPasso(step) {
    if (step == 1) {
        if (validarStep1CadastroCliente(false)) {
            $.ajax({
                url: "/frontend/cadastro-cliente/index/",
                type: "POST",
                data: ({
                    vc_nome: $('#vc_nome').val(),
                    vc_sobrenome: $('#vc_sobrenome').val(),
                    bl_sexo: $('#bl_sexo_value').val(),
                    dt_nascimento: $('#dt_nascimento').val(),
                    it_cep: $('#it_cep').val(),
                    vc_endereco: $('#vc_endereco').val(),
                    it_numero: $('#it_numero').val(),
                    vc_complemento: $('#vc_complemento').val(),
                    vc_bairro: $('#vc_bairro').val(),
                    vc_cidade: $('#vc_cidade').val(),
                    vc_estado: $('#vc_estado').val(),
                    vc_email: $('#vc_email').val(),
                    vc_senha: $('#vc_senha').val(),
                    bl_receber_convites: $('#bl_receber_convites').val(),
                    bl_receber_alertas: $('#bl_receber_alertas').val(),
                    ajax: true
                }),
                dataType: 'json',
                success: function(data) {
                    $.cookie(data.nome, data.data, {expires: data.expira, path: data.caminho, domain: data.dominio});
                    if (data) {
                        window.location = '/frontend/cadastro-cliente/complemento/';
                    } else {
                    }

                }
            });
        } else {
            return false;
        }
    } else if (step == 2) {
        if (validarStep2CadastroCliente(false)) {
            $.ajax({
                url: "/frontend/cadastro-cliente/complemento/",
                type: "POST",
                data: ({
                    vc_cpf: $('#vc_cpf').val(),
                    vc_telefone: $('#vc_telefone').val(),
                    vc_celular: $('#vc_celular').val(),
                    ajax: true
                }),
                dataType: "html",
                success: function() {
                    window.location = '/frontend/cadastro-cliente/adicionar-pet/';
                }
            });
            return true;
        } else {
            return false;
        }
    }
}

function validarUploadFoto() {
    if ($('#vc_cliente_foto').val() == '') {
        return false;
    } else {
        $('#alerta-foto').remove();
        //$('#progresso_upload').append('<h1 id="progresso">ALTERAR PARA UM progress bar</h1>');
        return true;
    }
}

function finalizarUploadFoto(status) {
    if (status == true) {

    }
}

function validarEsqueceuSenha() {
    $(':input[type="text"]', '#form-esqueceu-senha').removeClass('erro_text');

    var error = 0;

    if ($('#vc_email').val() == '') {
        $('#vc_email').addClass('erro_text');
        error++;
    }

    if (error > 0) {
        return false;
    } else {
        return true;
    }
}

function validarRecuperarSenha() {
    $(':input[type="password"]', '#form-recuperar-senha').removeClass('erro_text');

    var error = 0;

    if ($('#vc_senha').val() == '') {
        $('#vc_senha').addClass('erro_text');
        error++;
    }

    if ($('#vc_senha_confirmar').val() == '') {
        $('#vc_senha_confirmar').addClass('erro_text');
        error++;
    }

    if (error > 0) {
        return false;
    } else {
        return true;
    }
}

function verificaEmail() {
    $.ajax({
        url: "/frontend/cadastro-cliente/verifica-email",
        type: "POST",
        data: ({
            vc_email: $('#vc_email').val()
        }),
        dataType: "json",
        success: function(data) {
            if (!data) {
                if ($('#alert_email').length > 0) {
                    $('#alert_email').remove();
                }
                return true;
            } else {
                if ($('#alert_email').length == 0 && $('#vc_email').val() != '' && $('vc_email_confirmar').val() != '') {
                    $('#vc_email_confirmar').after('<span id="alert_email" class="msgerro">E-mail desejado ja está em uso no nosso sistema</span>');
                }
                if ($('#alerta_email_diferente').length > 0) {
                    $('#alerta_email_diferente').remove();
                }
                $('#vc_email').val('');
                $("#vc_email_confirmar").val('');
                return false;
            }
        }
    });

    if ($('#vc_email').val() != $('#vc_email_confirmar').val()) {
        $('#label_vc_email').css('border', '1px solid #ff0000');
        $('#label_vc_email_confirmar').css('border', '1px solid #ff0000');

        if ($('#alerta_email_diferente').length == 0) {
            $('#vc_email_confirmar').after('<span id="alerta_email_diferente" class="msgerro">Os e-mails informados não conferem</span>');
            if ($('#alert_email').length > 0) {
                $('#alert_email').remove();
                $('#label_vc_email').removeAttr('style');
                $('#label_vc_email_confirmar').removeAttr('style');
            }
        }
        return false;
    } else {
        if ($('#alert_email').length > 0) {
            $('#alert_email').remove();
            $('#label_vc_email').removeAttr('style');
            $('#label_vc_email_confirmar').removeAttr('style');
        }
        if ($('#alerta_email_diferente').length > 0) {
            $('#label_vc_email').removeAttr('style');
            $('#label_vc_email_confirmar').removeAttr('style');
            $('#alerta_email_diferente').remove();
        }
        return true;
    }

}

function verificaSenha() {
    if ($('#vc_senha').val() != $('#vc_senha_confirmar').val()) {
        $('#label_vc_senha').css('border', '1px solid #ff0000');
        $('#label_vc_senha_confirmar').css('border', '1px solid #ff0000');

        if ($('#alerta_senha_diferente').length == 0) {
            $('#vc_senha_confirmar').after('<span id="alerta_senha_diferente" class="msgerro">As senhas informadas não conferem</span>');
        }
        return false;
    }
    if ($('#alerta_senha_diferente').length > 0) {
        $('#label_vc_senha').removeAttr('style');
        $('#label_vc_senha_confirmar').removeAttr('style');
        $('#alerta_senha_diferente').remove();
    }
    return true;
}

function buscarEnderecoPorCep() {
    if ($('#alerta_cep').length == 0) {
        $('#vc_endereco').val('Aguarde...');
    }
    if ($('#alerta_cep_nao_encontrado').length > 0) {
        $('#vc_endereco').val('');
    }
    $.ajax({
        url: "/frontend/cadastro-cliente/buscar-endereco-por-cep/cep/" + $("#it_cep").val(),
        global: false,
        type: "GET",
        dataType: "json",
        success: function(json) {
            if (json.endereco == null && json.endereco == '') {
                $('#vc_endereco').val('Cep não encontrado');
                $('#label_vc_endereco').hide();
                $('#label_vc_cidade').hide();
                $('#label_vc_estado').hide();
                $('#label_vc_bairro').hide();
            } else {
                $("#vc_endereco").val(json.endereco);
                $("#vc_bairro").val(json.bairro);
                $("#vc_cidade").val(json.cidade);
                $("#vc_estado").val(json.estado);
                $("#vc_numero").focus();
            }
            $('#alerta_cep').remove();
        }
    });

    $('#label_vc_endereco').show();
    $('#label_vc_cidade').show();
    $('#label_vc_estado').show();
    $('#label_vc_bairro').show();
}

function popUpBuscarCepCorrerios() {
    var URL = 'http://www.buscacep.correios.com.br/servicos/dnec/menuAction.do?Metodo=menuEndereco';
    var width = 500;
    var height = 400;

    var left = 99;
    var top = 99;

    window.open(URL, 'janela', 'width=' + width + ', height=' + height + ', top=' + top + ', left=' + left + ', scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no');
}

function deletarFotoCliente(foto) {
    $.ajax({
        url: "/frontend/cadastro-cliente/deletar-foto",
        type: "POST",
        data: ({
            id_cliente_foto: foto
        }),
        dataType: "json",
        success: function(data) {
            if (data) {
                $('#foto' + foto).fadeOut('hide');
            } else {
                alert('Ocorreu um erro ao excluir a foto desejada, tente novamente mais tarde');
            }
        }
    });
}

function confirmarFinalizacaoCadastro() {
    return confirm('Realmente deseja finalizar seu cadastro agora ?');
}