/**
 * - Ativa o colorbox para as fotos das empresas listadas
 * - Carrega o js do sharethis
 */
$(document).ready(function() {
    $('.colorbox').colorbox();
    stLight.options({publisher: "7a84abb0-ebef-4473-90fa-8d52f6c530be", doNotHash: false, doNotCopy: false, hashAddressBar: false});
});

/**
 * 
 * @returns {undefined}
 */
function realizarPedidoOrcamento() {
    $.ajax({
        url: "/frontend/vitrine-empresa/realizar-pedido-orcamento",
        type: "POST",
        data: ({
            vc_nome: $('#vc_nome').val(),
            vc_sobrenome: $('#vc_sobrenome').val(),
            vc_email: $('#vc_email').val(),
            it_telefone: $('#it_telefone').val(),
            vc_estado: $('#vc_estado').val(),
            vc_cidade: $('#vc_cidade').val(),
            vc_mensagem: $('#vc_mensagem').val()
        }),
        dataType: 'json',
        success: function(json) {
            $('.input-redondo').removeAttr('style');
            $('.alertaerro').hide('slow');

            if (json.erro) {
                $('.alertaerro').show('slow');

                for (i in json['id']) {
                    $('#' + json['id'][i]).css(json['css'].atributo, json['css'].valor);
                }
            } else {
                $(':input').val();
                $('.introducao-orcamento, .formulario-pedido').fadeOut('slow', function() {
                    $(this).hide();
                });

                $('.orcamento-enviado').fadeIn('slow', function() {
                    $(this).show();
                });
            }

        }
    });
}

function guaradarEmpresa(id_fornecedor) {
    $.ajax({
        url: "/frontend/vitrine-empresa/realizar-pedido-orcamento",
        type: "POST",
        data: ({
            id_fornecedor: id_fornecedor
        }),
        dataType: 'json',
        success: function(json) {

        }
    });
}