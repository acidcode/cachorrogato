function removeQuestionario(id_questionario) {
    if (confirm('Tem certeza que deseja remover esta pergunta?')) {
        $.post('/backend/questionario/deletar/', {'id_questionario': id_questionario}, function() {
            window.location = '/backend/questionario/';
        }, 'json');
    }
}

function exibirOpcoes(opcao) {
    if (opcao == 'radio' || opcao == 'checkbox' || opcao == 'select') {
        $('#conteudo-opcoes').show('slow');
    } else {
        $('#conteudo-opcoes').hide('slow');
    }
}

function adicionarOpcao() {
    $('#opcoes').append('<div class="mws-form-row" id="' + $('.mws-form-row').length + '"><label>Opção:</label><input type="text" name="vc_opcao_valor[]" value="" class="mws-textinput" /><input type="button" value="-" class="mws-button orange" onclick="removerOpcao(' + $('.mws-form-row').length + ');" /></div>');
}

function removerOpcao(id) {
    $('#' + id).remove();
}