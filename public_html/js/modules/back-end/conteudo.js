function adicionarCategoria() {

    if ($('#vc_nova_categoria').val() != '') {
        $.ajax({
            url: "/backend/conteudo/adicionar-categoria",
            type: "POST",
            data: ({
                vc_categoria: $('#vc_nova_categoria').val(),
                id_conteudo: $('#id_conteudo').val(),
                id_parent: $('#id_parent').val()
            }),
            dataType: "html",
            success: function(data) {
                $('#lista-categoria').html(data);
            }
        });
    }
}

function removeConteudo(id_conteudo) {
    if (confirm('Tem certeza que deseja remover este conteúdo?')) {
        $.post('/backend/conteudo/remove/', {'id_conteudo': id_conteudo}, function() {
            window.location = "/backend/conteudo/index";
        }, 'json');
    }
}

function adicionarTag() {

    if ($('#vc_nova_tag').val() != '') {
        $.ajax({
            url: "/backend/conteudo/adicionar-tag",
            type: "POST",
            data: ({
                vc_nova_tag: $('#vc_nova_tag').val(),
                id_conteudo: $('#id_conteudo').val()
            }),
            dataType: "html",
            success: function(data) {
                $('#lista-tags').html(data);
            }
        });
    }
}

function removerTag(id_conteudo_tag) {

        $.ajax({
            url: "/backend/conteudo/remover-tag",
            type: "POST",
            data: ({
                id_conteudo_tag: id_conteudo_tag,
                id_conteudo: $('#id_conteudo').val()
            }),
            dataType: "html",
            success: function(data) {
                $('#lista-tags').html(data);
            }
        });
}
