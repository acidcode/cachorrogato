function removeAtividade(id_setor_atividade) {
    if (confirm('Tem certeza que deseja remover este Setor de Atividade?')) {
        $.post('/backend/setor-de-atividade/deletar/', {'id_setor_atividade': id_setor_atividade}, function() {
            window.location = '/backend/setor-de-atividade';
        }, 'json');
    }
}