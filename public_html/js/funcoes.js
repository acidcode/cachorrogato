$(function() {

    // PAGINAÃ‡ÃƒO
    $('.box-paginacao ul li:first-child a').css('padding', '3px 7px 3px 0');
    /*************************** CONTATO - SCROLL *********************************/
    $('.link-simulador').click(function() {
        $('html,body').animate({
            scrollTop: $('.simulador').offset().top
        }, 1000);
        //$('input[value="Nome"]').focus();
    });

    /*************************** SIMULADOR - SCROLL *********************************/
    $('.scroll-bottom').click(function() {
        $('html,body').animate({
            scrollTop: $('#ancora').offset().top
        }, 500);
        //$('input[value="Nome"]').focus();
    });

    $('.aba li:last-child').css({
        'border': 'none',
        'width': '132px'
    });


    /*************************** VALIDAÃ‡ÃƒO DO LOGIN - PÃGINA ANUNCIAR *********************************/

    $('.esqueci-senha').click(function() {
        $('.box-login').hide();

        $('.recuperar-senha').show();
    });

    $('.voltar').click(function() {
        $('.recuperar-senha').hide();
        $('.box-login').show();
    });

    /*************************** GERENCIAMENTO DE IMÃ“VEIS *********************************/
    $('.tbl tr:even').css('background-color', '#eee');

});

(function($) {
    $.fn.placeholder = function() {
        if (typeof document.createElement("input").placeholder == 'undefined') {
            $('[placeholder]').focus(function() {
                var input = $(this);
                if (input.val() == input.attr('placeholder')) {
                    input.val('');
                    input.removeClass('placeholder');
                }
            }).blur(function() {
                var input = $(this);
                if (input.val() == '' || input.val() == input.attr('placeholder')) {
                    input.addClass('placeholder');
                    input.val(input.attr('placeholder'));
                }
            }).blur().parents('form').submit(function() {
                $(this).find('[placeholder]').each(function() {
                    var input = $(this);
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                    }
                })
            });
        }
    }
})(jQuery);

$.fn.placeholder();

/*************************** VALIDAÃ‡ÃƒO DO FORMULÃRIO DE CONTATO *********************************/

function validaContato() {

    with (document.contato) {

        if (nome.value.length == 0 || nome.value == "Nome") {
            shadowMensagem("Digite seu nome, por favor.");
            return false;
        }

        if (email.value.length == 0 || email.value == "E-mail") {
            shadowMensagem("Digite seu e-mail, por favor.");
            return false;
        }

        if (email.value.length > 0 && email.value != "E-mail") {
            var exclude = /[^@\-\.\w]|^[_@\.\-]|[\._\-]{2}|[@\.]{2}|(@)[^@]*\1/;
            var check = /@[\w\-]+\./;
            var checkend = /\.[a-zA-Z]{2,3}$/;

            if (((email.value.search(exclude) != -1) || (email.value.search(check)) == -1) || (email.value.search(checkend) == -1)) {
                shadowMensagem("E-mail invÃ¡lido. Verifique por favor.");
                return false;
            }
        }

        if (telefone.value.length == 0 || telefone.value == "(DDD) Telefone") {
            shadowMensagem("Digite um telefone para contato, por favor.");
            return false;
        }

        if (assunto.value.length == 0) {
            shadowMensagem("Selecione um assunto, por favor.");
            return false;
        }

        if (assunto.value == "Outros" && outros.value.length == 0) {
            shadowMensagem("Digite o assunto, por favor.");
            return false;
        }

        if (mensagem.value.length == 0) {
            shadowMensagem("Digite a mensagem, por favor.");
            return false;
        }

    }
}

function ContarCaracteres(campo, contador, limite) {
    if (campo.value.length > limite) {
        campo.value = campo.value.substring(0, limite);
    }
    else {
        contador.value = limite - campo.value.length;

        if (contador.value <= 1) {
            document.getElementById('caracteres').innerHTML = "caracter restante";
        }
        else {
            document.getElementById('caracteres').innerHTML = "caracteres restantes";
        }
    }
}

/*************************** VALIDAÃ‡ÃƒO DO LOGIN - PÃGINA ANUNCIAR *********************************/

function validaLogin() {
    with (document.login_anunciar) {
        if (usuario_anunciar.value.length == 0) {
            alert("Digite o usuÃ¡rio, por favor.");
            usuario_anunciar.focus();
            return false;
        }
        if (senha_anunciar.value.length == 0) {
            alert("Digite a senha, por favor.");
            senha_anunciar.focus();
            return false;
        }
    }
}

function validaLoginTop() {
    with (document.login) {
        if (usuario.value.length == 0) {
            alert("Digite o usuÃ¡rio, por favor.");
            usuario.focus();
            return false;
        }
        if (senha.value.length == 0) {
            alert("Digite a senha, por favor.");
            senha.focus();
            return false;
        }
    }
}

function validaRecuperarSenha() {
    with (document.recuperar_senha) {
        if (email_cadastrado.value.length == 0) {
            alert("Digite o e-mail cadastrado, por favor.");
            email_cadastrado.focus();
            return false;
        }

        if (email_cadastrado.value.length > 0) {
            var exclude = /[^@\-\.\w]|^[_@\.\-]|[\._\-]{2}|[@\.]{2}|(@)[^@]*\1/;
            var check = /@[\w\-]+\./;
            var checkend = /\.[a-zA-Z]{2,3}$/;

            if (((email_cadastrado.value.search(exclude) != -1) || (email_cadastrado.value.search(check)) == -1) || (email_cadastrado.value.search(checkend) == -1)) {
                alert("E-mail invÃ¡lido. Verifique por favor.");
                return false;
            }
        }
    }
}

/*************************** GERENCIAMENTO DE IMÃ“VEIS *********************************/

function allCheckbox(master) {
    if (master.checked == true) {
        for (i = 0; i < document.lst_anuncios.elements.length; i++) {
            if (document.lst_anuncios.elements[i].type == "checkbox") {
                document.lst_anuncios.elements[i].checked = 1;
            }
        }
    }
    else {
        for (i = 0; i < document.lst_anuncios.elements.length; i++) {
            if (document.lst_anuncios.elements[i].type == "checkbox") {
                document.lst_anuncios.elements[i].checked = 0
            }
        }
    }
}

/*************************** CADASTRO DE IMÃ“VEIS *********************************/

function abas(tela, aba) {
    $('.tela').css('display', 'none');
    $('#' + tela).css('display', 'block');

    $('.aba-item').removeClass('ativa');
    $('#' + aba).addClass('ativa');
}

function selecionarCategoriaImovel() {
    $.ajax({
        url: "/frontend/ajax/carregar-subcategoria",
        global: false,
        type: "POST",
        data: ({
            id_categoria_anuncio: $('#id_categoria_anuncio').val(),
            ajax: true
        }),
        dataType: "html",
        success: function(msg) {
            $('#html-subcategoria-anuncio').html(msg);
        }
    });
}

function buscarEnderecoCep() {
    $('#msg_it_cep').html('Buscando...');
    if ($.trim($("#it_cep").val()) != "") {
        $.getScript("http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep=" + $("#it_cep").val(),
                function() {
                    if (resultadoCEP["resultado"]) {
                        $('#msg_it_cep').val('');
                        $("#vc_endereco").val(unescape(resultadoCEP["tipo_logradouro"]) + " " + unescape(resultadoCEP["logradouro"]));
                        $("#vc_bairro").val(unescape(resultadoCEP["bairro"]));
                        $("#vc_cidade").val(unescape(resultadoCEP["cidade"]));
                        $("#vc_estado").val(unescape(resultadoCEP["uf"]));
                        buscarGeoCep(resultadoCEP);
                        $('#msg_it_cep').html('Feito!');
                    } else {
                        $('#msg_it_cep').html('EndereÃ§o nÃ£o encontrado.');
                    }
                });
    } else {
        $('#msg_it_cep').html('Digite seu CEP.');
    }
}

function buscarGeoCep(resultadoCEP) {

    var URL = '/frontend/ajax/geocoordenada/endereco/' + unescape(resultadoCEP["tipo_logradouro"]) + '+' + unescape(resultadoCEP["logradouro"]) + ',+' + unescape(resultadoCEP["cidade"]) + '+-+' + unescape(resultadoCEP["uf"]) + ''

    $.ajax({
        url: URL,
        type: 'GET',
        dataType: 'html',
        success: function(html) {
            $('#vc_geocoordenada').val(html);
        },
        error: function() {
            alert("Ajax Error: ");
        }

    });

}

function validarCPF() {
    var cpf = str_replace('.', '', $('#it_cpf').val());
    cpf = str_replace('-', '', cpf);

    //675.434.816-00
    if (!valida_cpf(cpf)) {
        shadowMensagem('CPF invÃ¡lido');
        $('#it_cpf').val('');
        return false;
    }

    $.ajax({
        url: "/frontend/ajax/checar-cpf",
        global: false,
        type: "POST",
        data: ({
            it_cpf: cpf,
            ajax: true
        }),
        dataType: "html",
        success: function(msg) {
            if (msg == 1) {
                shadowMensagem('CPF jÃ¡ cadastrado em nossa base de dados.');
                $('#it_cpf').val('');
                return false;
            } else {
                return true;
            }
        }
    });
}

function valida_cpf(cpf)
{
    var numeros, digitos, soma, i, resultado, digitos_iguais;
    digitos_iguais = 1;
    if (cpf.length < 11)
        return false;
    for (i = 0; i < cpf.length - 1; i++)
        if (cpf.charAt(i) != cpf.charAt(i + 1))
        {
            digitos_iguais = 0;
            break;
        }
    if (!digitos_iguais)
    {
        numeros = cpf.substring(0, 9);
        digitos = cpf.substring(9);
        soma = 0;
        for (i = 10; i > 1; i--)
            soma += numeros.charAt(10 - i) * i;
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;
        numeros = cpf.substring(0, 10);
        soma = 0;
        for (i = 11; i > 1; i--)
            soma += numeros.charAt(11 - i) * i;
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
            return false;
        return true;
    }
    else
        return false;
}



/*************************** BUSCA *********************************/

function validaBusca() {
    with (document.busca) {
        if (pesquisa.value.length == 0) {
            pesquisa.focus();
            return false;
        }
    }
}

/*************************** RESULTADOS: CIDADES *********************************/

function resultadosCidade(valor) {
    window.location.href = "busca.php?cidade=" + valor;
}

function modalBox(url, largura, altura) {
    window.location.href = url;
    window.location.rel = "shadowbox;" + largura + ";" + altura;
}

/*************************** CADASTRO DE IMÃ“VEIS *********************************/

function showNomeLancamento(elemento) {
    if (elemento.value == "sim") {
        $('.nome_lancamento').fadeIn(500);
    }
    else if (elemento.value == "nao") {
        $('.nome_lancamento').fadeOut(500);
    }
}

function allow(elemento) {
    if (elemento.checked == true) {
        $('.prosseguir').fadeIn(500);
    }
    else {
        $('.prosseguir').hide();
    }
}

function enviarFaleConosco() {

    if (validaContato() != undefined) {
        return false;
    }

    $.ajax({
        url: "/frontend/ajax/fale-conosco",
        global: false,
        type: "POST",
        data: ({
            nome: $('#nome').val(),
            email: $('#email').val(),
            telefone: $('#telefone').val(),
            celular: $('#celular').val(),
            assunto: $('#assunto').val(),
            outros: $('#outros').val(),
            mensagem: $('#mensagem').val(),
            informativo: $('#informativo').val(),
            ajax: true
        }),
        dataType: "html",
        success: function(msg) {
            shadowMensagem('Obrigado pelo contato!');
            $('#nome').val('');
            $('#email').val('');
            $('#telefone').val('');
            $('#celular').val('');
            $('#assunto').val('');
            $('#outros').val('');
            $('#mensagem').val('');
            $('#informativo').val('');
        }
    });

}

function fecharDireto() {


    $.ajax({
        url: "/frontend/ajax/fechar-direto",
        global: false,
        type: "POST",
        data: ({
            nome: $('#nome').val(),
            email: $('#email').val(),
            telefone: $('#telefone').val(),
            observacao: $('#observacao').val(),
            id_anuncio: $('#id_anuncio').val(),
            ajax: true
        }),
        dataType: "html",
        success: function(msg) {
            alert("Aguarde o Retorno do Proprietario.");
            self.parent.Shadowbox.close();
        }
    });

}

function denunciar() {


    $.ajax({
        url: "/frontend/ajax/denuncia",
        global: false,
        type: "POST",
        data: ({
            nome: $('#nome').val(),
            email: $('#email').val(),
            telefone: $('#telefone').val(),
            observacao: $('#observacao').val(),
            tipo_denuncia: $('#tipo_denuncia').val(),
            id_anuncio: $('#id_anuncio').val(),
            ajax: true
        }),
        dataType: "html",
        success: function(msg) {
            alert("Obrigado!");
            self.parent.Shadowbox.close();
        }
    });


}


function indicar() {

    $.ajax({
        url: "/frontend/ajax/indicar",
        global: false,
        type: "POST",
        data: ({
            nome_de: $('#nome_de').val(),
            nome_para: $('#nome_para').val(),
            email_de: $('#email_de').val(),
            email_para: $('#email_para').val(),
            id_anuncio: $('#id_anuncio').val(),
            ajax: true
        }),
        dataType: "html",
        success: function(msg) {
            alert("Obrigado pela indicaÃ§Ã£o!");
            self.parent.Shadowbox.close();
        }
    });

}

function selecionarBairro() {

    $.ajax({
        url: "/frontend/ajax/selecionar-bairro",
        global: false,
        type: "POST",
        data: ({
            vc_cidade: $('#vc_busca_rapida_cidade').val(),
            ajax: true
        }),
        dataType: "html",
        success: function(msg) {
            $('#html-select-bairro').html(msg);
        }
    });

}

function selecionarBairroServico() {

    $.ajax({
        url: "/frontend/ajax/selecionar-bairro-servico",
        global: false,
        type: "POST",
        data: ({
            vc_cidade: $('#busca_servico_cidade').val(),
            ajax: true
        }),
        dataType: "html",
        success: function(msg) {
            $('#html-select-bairro').html(msg);
        }
    });

}

//VALIDAR CADATRO DO USUARIO
function validarCadastroUsuario()
{

    $(':input[type="text"]', '#formularioCadastro').css('background', '#EAEAEA');
    $(':input[type="text"]', '#formularioCadastro').css('border', 'solid 1px #959595');

    $('#vc_nome').attr('background', '#FFF');
    $('#it_cpf').attr('background', '#FFF');
    $('#it_telefone').attr('background', '#FFF');
    $('#vc_email').attr('background', '#FFF');
    $('#vc_senha').attr('background', '#FFF');
    $('#vc_conteudo').attr('background', '#FFF');
    $('#vc_endereco').attr('background', '#FFF');
    $('#vc_bairro').attr('background', '#FFF');
    $('#it_endereco_numero').attr('background', '#FFF');
    $('#vc_estado').attr('background', '#FFF');

    if ($('#vc_nome').val() == '' || $('#vc_nome').val() == 'Nome')
    {
        $('#vc_nome').css('background', '#FEF6DB');
        $('#vc_nome').css('border', 'solid 1px #E3A345');
        shadowMensagem('Nome ObrigatÃ³rio!');
        return false;
    }

    if ($('#it_cep').val() == '')
    {
        $('#it_cep').css('background', '#FEF6DB');
        $('#it_cep').css('border', 'solid 1px #E3A345');
        shadowMensagem('CEP ObrigatÃ³rio!');
        return false;
    }


    if ($('#vc_endereco').val() == '')
    {
        $('#vc_endereco').css('background', '#FEF6DB');
        $('#vc_endereco').css('border', 'solid 1px #E3A345');
        shadowMensagem('EndereÃ§o ObrigatÃ³rio!');
        return false;
    }

    if ($('#it_endereco_numero').val() == '')
    {
        $('#it_endereco_numero').css('background', '#FEF6DB');
        $('#it_endereco_numero').css('border', 'solid 1px #E3A345');
        shadowMensagem('Numero ObrigatÃ³rio!');
        return false;
    }

    if ($('#vc_estado').val() == '')
    {
        $('#vc_estado').css('background', '#FEF6DB');
        $('#vc_estado').css('border', 'solid 1px #E3A345');
        shadowMensagem('Estado ObrigatÃ³rio!');
        return false;
    }

    if ($('#vc_bairro').val() == '')
    {
        $('#vc_bairro').css('background', '#FEF6DB');
        $('#vc_bairro').css('border', 'solid 1px #E3A345');
        shadowMensagem('Bairro ObrigatÃ³rio!');
        return false;
    }

    if ($('#it_cpf').val() == '')
    {
        $('#it_cpf').css('background', '#FEF6DB');
        $('#it_cpf').css('border', 'solid 1px #E3A345');
        shadowMensagem('CPF ObrigatÃ³rio!');
        return false;
    }

    if ($('#it_telefone_1').val() == '')
    {
        $('#it_telefone_1').css('background', '#FEF6DB');
        $('#it_telefone_1').css('border', 'solid 1px #E3A345');
        shadowMensagem('Telefone ObrigatÃ³rio!');
        return false;
    }

    if ($('#vc_email').val() == '' || $('#vc_email').val() == 'E-mail')
    {
        $('#vc_email').css('background', '#FEF6DB');
        $('#vc_email').css('border', 'solid 1px #E3A345');
        shadowMensagem('Email ObrigatÃ³rio!');
        return false;
    }

    if ($('#vc_senha').val() == '' && $('#id_usuario').val() == '')
    {
        $('#vc_senha').css('background', '#FEF6DB');
        $('#vc_senha').css('border', 'solid 1px #E3A345');
        shadowMensagem('Digite uma senha!');
        return false;
    }

    if (($('#vc_senha').val() != $('#vc_senha_confirmacao').val()) && $('#id_usuario').val() == '')
    {
        $('#vc_senha').css('background', '#FEF6DB');
        $('#vc_senha').css('border', 'solid 1px #E3A345');
        $('#vc_senha_confirmacao').css('background', '#FEF6DB');
        $('#vc_senha_confirmacao').css('border', 'solid 1px #E3A345');
        shadowMensagem('As senhas nÃ£o sÃ£o iguais!');
        return false;
    }

    $('#formularioCadastro').submit();
}

function simularFinanciamento() {

    var valor_imovel = converteMoedaFloat($('#valor_imovel').val());
    var valor_entrada = converteMoedaFloat($('#valor_entrada').val());
    var prazo_financiamento = parseInt($('#prazo_financiamento').val()) * 12;
    var taxa_juros = $('#taxa_juros').val();

    var valorFinanciado = parseFloat(valor_imovel) - parseFloat(valor_entrada);
    $('#valor_financiar').val(valorFinanciado);

    var juros = (valorFinanciado / 100) * taxa_juros;
    var parcelas = (valorFinanciado + juros) / prazo_financiamento;

    $('#valor_financiar').val(converteFloatMoeda(valorFinanciado.toFixed(2)));
    $('#parcela_m_est').val(converteFloatMoeda(parcelas.toFixed(2)))

    return false;
}

/*@file utils.js
 @brief Conjunto de funÃ§Ãµes para tratamento dos dados
 @author Marcone Gledson de Almeida
 @date 2008
 */

/*   @brief Converte uma string em formato moeda para float
 @param valor(string) - o valor em moeda
 @return valor(float) - o valor em float
 */
function converteMoedaFloat(valor) {

    if (valor === "") {
        valor = 0;
    } else {
        valor = valor.replace(".", "");
        valor = valor.replace(",", ".");
        valor = parseFloat(valor);
    }
    return valor;

}

/*   @brief Converte um valor em formato float para uma string em formato moeda
 @param valor(float) - o valor float
 @return valor(string) - o valor em moeda
 */
function converteFloatMoeda(valor) {
    var inteiro = null, decimal = null, c = null, j = null;
    var aux = new Array();
    valor = "" + valor;
    c = valor.indexOf(".", 0);
    //encontrou o ponto na string
    if (c > 0) {
        //separa as partes em inteiro e decimal
        inteiro = valor.substring(0, c);
        decimal = valor.substring(c + 1, valor.length);
    } else {
        inteiro = valor;
    }

    //pega a parte inteiro de 3 em 3 partes
    for (j = inteiro.length, c = 0; j > 0; j -= 3, c++) {
        aux[c] = inteiro.substring(j - 3, j);
    }

    //percorre a string acrescentando os pontos
    inteiro = "";
    for (c = aux.length - 1; c >= 0; c--) {
        inteiro += aux[c] + '.';
    }
    //retirando o ultimo ponto e finalizando a parte inteiro

    inteiro = inteiro.substring(0, inteiro.length - 1);

    decimal = parseInt(decimal);
    if (isNaN(decimal)) {
        decimal = "00";
    } else {
        decimal = "" + decimal;
        if (decimal.length === 1) {
            decimal = decimal + "0";
        }
    }


    valor = "" + inteiro + "," + decimal;


    return valor;

}

function validarFormularioServico() {

    $(':input[type="text"]', '#formularioCadastro').css('background', '#EAEAEA');
    $(':input[type="text"]', '#formularioCadastro').css('border', 'solid 1px #959595');

    $('#vc_servico').attr('background', '#FFF');
    $('#vc_nome_empresa').attr('background', '#FFF');
    $('#it_telefone').attr('background', '#FFF');
    $('#vc_email').attr('background', '#FFF');
    $('#it_cep').attr('background', '#FFF');

    if ($('#vc_servico').val() == '')
    {
        $('#vc_servico').css('background', '#FEF6DB');
        $('#vc_servico').css('border', 'solid 1px #E3A345');
        shadowMensagem('ServiÃ§o obrigatÃ³rio!');
        return false;
    }
    if ($('#vc_nome_empresa').val() == '')
    {
        $('#vc_nome_empresa').css('background', '#FEF6DB');
        $('#vc_nome_empresa').css('border', 'solid 1px #E3A345');
        shadowMensagem('Nome da empresa obrigatÃ³rio!');
        return false;
    }

    if ($('#it_telefone').val() == '')
    {
        $('#it_telefone').css('background', '#FEF6DB');
        $('#it_telefone').css('border', 'solid 1px #E3A345');
        shadowMensagem('Telefone ObrigatÃ³rio!');
        return false;
    }

    if ($('#vc_email').val() == '')
    {
        $('#vc_email').css('background', '#FEF6DB');
        $('#vc_email').css('border', 'solid 1px #E3A345');
        shadowMensagem('Email ObrigatÃ³rio!');
        return false;
    }

    if ($('#it_cep').val() == '')
    {
        $('#it_cep').css('background', '#FEF6DB');
        $('#it_cep').css('border', 'solid 1px #E3A345');
        shadowMensagem('Cep ObrigatÃ³rio!');
        return false;
    }
}

function shadowMensagem(mensagem) {
    Shadowbox.open({
        content: '<div style="padding:40px; text-align:center;">' + mensagem + '</div>',
        player: "html",
        title: "AtenÃ§Ã£o!",
        height: 150,
        width: 350
    });
}

function trocaFinalidade() {
    var finalidade = $('#vc_busca_rapida_finalidade').val();

    if (finalidade == 0) {
        $('#vc_busca_rapida_faixa_preco_compra').show();
        $('#vc_busca_rapida_faixa_preco_locacao').hide();
    }
    if (finalidade == 1) {
        $('#vc_busca_rapida_faixa_preco_compra').show();
        $('#vc_busca_rapida_faixa_preco_locacao').hide();
    }
    if (finalidade == 2) {
        $('#vc_busca_rapida_faixa_preco_compra').hide();
        $('#vc_busca_rapida_faixa_preco_locacao').show();
    }
    if (finalidade == 3) {
        $('#vc_busca_rapida_faixa_preco_compra').hide();
        $('#vc_busca_rapida_faixa_preco_locacao').show();
    }
}

function mudarPlano(id_plano) {

    $.ajax({
        url: "/frontend/ajax/selecionar-periodo-plano",
        global: false,
        type: "POST",
        data: ({
            periodo: $('#bl_periodo_plano_' + id_plano).val(),
            id_plano: id_plano,
            ajax: true
        }),
        dataType: "html",
        success: function(msg) {
            $('#label-mn-valor-' + id_plano).html(msg);
        }
    });
}

function abreModal() {
    $('.background-modal').toggle();
    $('#modal-acesso').toggle();
}