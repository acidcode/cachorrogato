$(document).ready(function() {
    $(".box_destaque").children(".oferta-imperdivel").each(function(ind, item) {
        var emp_id = $(item).attr("id");
        if (emp_id != undefined) {
            ContadorOferta(emp_id);
        }
    });
})

function ContadorOferta(emp) {
    var date_array = new Array();
    var i = 0;
    var date = $("#" + emp).find('.dataOferta').val();
    var date_array = (date.split("/"));
    var _dia = date_array[2];
    var min_array = (_dia.split(":"));
    var hora = _dia.substring(5, 2);
    var min = min_array[1];
    var ano = date_array[0];
    var mes = date_array[1];
    var dia = _dia.substring(0, 2);

    var YY = ano;
    var MM = mes;
    var DD = dia;
    var HH = hora;
    var MI = min;

    var saida = $("#" + emp).find(".contagemRegressiva");
    var SS = 00;
    var hoje = new Date();
    var futuro = new Date(YY, MM - 1, DD, HH, MI, SS);
    var ss = parseInt((futuro - hoje) / 1000);
    var mm = parseInt(ss / 60);
    var hh = parseInt(mm / 60);
    var dd = parseInt(hh / 24);

    ss = ss - (mm * 60);
    mm = mm - (hh * 60);
    hh = hh - (dd * 24);

    dd = (dd < 10) ? "0" + dd : dd;
    ss = (ss < 10) ? "0" + ss : ss;
    mm = (mm < 10) ? "0" + mm : mm;
    hh = (hh < 10) ? "0" + hh : hh;

    var faltam = '';
    faltam += (dd && dd > 1) ? '<span class="dias">' + dd + '</span><span class="espaco"></span>' : (dd == 1 ? '<span class="dias">1</span><span class="espaco"></span>' : '<span class="dias">0</span><span class="espaco"></span>');
    faltam += " ";
    faltam += (toString(hh).length) ? '<span class="hora">' + hh + '</span><span class="espaco" style="margin-top:-3px;">:</span>' : '';
    faltam += (toString(mm).length) ? '<span class="minuto">' + mm + '</span><span class="espaco" style="margin-top:-3px;">:</span>' : '';
    faltam += '<span class="segundo">' + ss + '</span>';

    if (dd + hh + mm + ss > 0) {
        saida.html(faltam);
        setTimeout(function() {
            ContadorOferta(emp)
        }, 1000);
    }
    else {
        setTimeout(function() {
            ContadorOferta(emp)
        }, 1000);
    }
}