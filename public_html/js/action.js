$(document).ready(function() {

    jQuery(window).bind('scroll', function(e) {
        var scrolled = jQuery(window).scrollTop();

        if (scrolled >= 660) {
            jQuery('body').css('background', 'url("img/bg_conteudo.jpg") repeat-x');
            jQuery('body').css('background-attachment', 'fixed');
            jQuery('.index .conteudo').css('background', 'none');
        }
        if (scrolled <= 659) {
            jQuery('.index .conteudo').css('background', 'url("img/bg_conteudo.jpg") repeat-x');
            jQuery('body').css('background', 'none');
        }
    });

});

