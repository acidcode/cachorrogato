function abrir(URL) {
    var width = 470;
    var height = 450;
    var left = 99;
    var top = 99;
    window.open(URL, 'janela', 'width=' + width + ', height=' + height + ', top=' + top + ', left=' + left + ', scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no');
}

function fecharLigamosParaVoce() {
    $("#prepage-ligamos-voce").css("display", "none");
    $("#box-atend-ligamos").css("display", "none");
}

function abrirLigamosParaVoce() {
    $("#prepage-ligamos-voce").css("display", "block");
    $("#box-atend-ligamos").css("display", "block");
}

function fecharAtendimentoPorEmail() {
    $("#prepage-atendimento-email").css("display", "none");
    $("#box-atend-email").css("display", "none");
}

function abrirAtendimentoPorEmail() {
    $("#prepage-atendimento-email").css("display", "block");
    $("#box-atend-email").css("display", "block");
}

function abrir(URL) {
    var width = 470;
    var height = 450;
    var left = 99;
    var top = 99;
    window.open(URL, 'janela', 'width=' + width + ', height=' + height + ', top=' + top + ', left=' + left + ', scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no');
}

/**
 * Realiza o login do usuário, através do modal
 * exibido no topo da página
 * @returns {undefined}
 */
function efetuarLogin() {
    $('.alertaerro').hide('slow');
    $.ajax({
        url: "/frontend/cadastro-cliente/login/",
        type: "POST",
        data: ({
            vc_email: $('#vc_email_login').val(),
            vc_senha: $('#vc_senha_login').val(),
            ajax: true
        }),
        dataType: 'json',
        success: function(json) {
            $('.fieldset_modal_login').removeAttr('style');

            if (json.erro) {
                $('.alertaerro').show('slow');
                for (i in json['id']) {
                    $('#' + json['id'][i]).css(json['css'].atributo, json['css'].valor);
                }
            } else {
                if (json.alertaLogin) {
                    $('.alertaerro').show('slow');
                    $('.alertaerro span').html(json.alertaLogin);
                } else {
                    $('.alertaerro').hide('slow', function() {
                        window.location = json.url;
                    });

                }
            }
        }
    });
}