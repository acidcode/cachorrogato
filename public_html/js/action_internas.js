$(document).ready(function() {

    jQuery(window).bind('scroll', function(e) {
        var scrolled = jQuery(window).scrollTop();

        if (scrolled >= 120) {
            jQuery('body').css('background', 'url("img/bg_conteudo.jpg") repeat-x');
            jQuery('body').css('background-attachment', 'fixed');
            jQuery('.index .conteudo').css('background', 'none');
        }
        if (scrolled <= 119) {
            jQuery('.index .conteudo').css('background', 'url("img/bg_conteudo.jpg") repeat-x');
            jQuery('body').css('background', 'none');
        }
    });

});

