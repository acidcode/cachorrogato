$(document).ready(function() {
    $(".validacao").validate({
        rules: {
            spinner: {
                required: true,
                max: 5
            }
        },
        invalidHandler: function(form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                var message = errors == 1
                        ? 'You missed 1 field. It has been highlighted'
                        : 'You missed ' + errors + ' fields. They have been highlighted';
                $("#mws-validate-error").html(message).show();
            } else {
                $("#mws-validate-error").hide();
            }
        }
    });

    var opts = {
            cssClass : 'el-rte',
            height   : 300,
            toolbar  : 'normal',
            cssfiles : ['plugins/elrte/css/elrte-inner.css'], 
            fmAllow: true, 
            fmOpen : function(callback) {
                    $('<div id="myelfinder"></div>').elfinder({
                            url : '/plugins/elfinder/connectors/php/connector.php', 
                            lang : 'en', 
                            height: 500, 
                            dialog : { width : 980, modal : true, title : 'Gerenciador de Imagens' }, 
                            closeOnEditorCallback : true,
                            editorCallback : callback
                    });
            }
    }

    $('#vc_ficha_tecnica').elrte(opts);
    $('#vc_historia').elrte(opts);
	
});