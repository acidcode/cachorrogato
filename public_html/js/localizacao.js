var directionDisplay;
var directionsService = new google.maps.DirectionsService();
var route = false;
var map;
var end = "Rua Vergueiro 360, São Paulo, SP";
var marker;
var geocoder;
var x = false;
function initialize() {

    if (x == true) {
        directionsDisplay = new google.maps.DirectionsRenderer();
        geocoder = new google.maps.Geocoder();
        var myLatlng = new google.maps.LatLng(-23.563717, -46.637247);
        var myOptions = {
            zoom: 16,
            center: myLatlng,
            mapTypeControl: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById("mapa"), myOptions);
        directionsDisplay.setMap(map);
        directionsDisplay.setPanel(document.getElementById("directionsPanel"));

    }
    else {
    }
}
function calcRoute() {

    if (marker)
        marker.setMap(null);
    route = true;
    var start = document.getElementById("inputStart").value;
    var end = document.getElementById("inputEnd").value;
    var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.DirectionsTravelMode.DRIVING
    };
    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
    });
}
function revRoute() {
    var divStart = document.getElementById("inputStart");
    var divEnd = document.getElementById("inputEnd");
    var start = divStart.value;
    var end = divEnd.value;
    divStart.value = end;
    divEnd.value = start
    if (route == true) {
        calcRoute();
    }
}




$(document).ready(function() {
    function removeAcento(strToReplace) {
        str_acento = "áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ";
        str_sem_acento = "aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC";
        var nova = "";
        for (var i = 0; i < strToReplace.length; i++) {
            if (str_acento.indexOf(strToReplace.charAt(i)) != -1) {
                nova += str_sem_acento.substr(str_acento.search(strToReplace.substr(i, 1)), 1);
            } else {
                nova += strToReplace.substr(i, 1);
            }
        }

        return nova;
    }


    $('.btn_imprimir').click(function() {
        var local = $('#inputEnd').val();

        local = removeAcento(local);


        var url = $(this).attr('href');
        window.open('imprimir.html?id=' + local + '', '_blank');

    });

    $('.tracar, .btn_imprimir').click(function() {

        var local = $('#inputEnd').val();

        if (local == "") {
            alert("Escolha um endereço");
        }
        else {
            x = true;
            initialize();
        }
    });
});


// JavaScript Document