jQuery.fn.homeBrasilSlider = function(params) {
    var container = $(this);
    var containerSlider = $(container).find(".containerSlider");
    var listagem = $(container).find(".slider");
    var pos = -1;
    var nItens = $(listagem).find("li").length + 2;
    var largura_item = $(listagem).find("li").width();
    var itens = $(listagem).find("li");
    var imgs = $(listagem).find("img");
    var todosItens = $(listagem).find("li");
    var _intervalo;
    var pause = false;

    var plugin = {
        itensVisiveis: 3,
        intervaloTransicao: ((params == undefined || (params.intervaloTransicao == undefined)) || !(params.intervaloTransicao > 0)) ? 5 : params.intervaloTransicao,
        velocidadeTransicao: ((params == undefined || (params.velocidadeTransicao == undefined)) || !(params.velocidadeTransicao > 0)) ? 0.5 : params.velocidadeTransicao,
        inicializado: false,
        init: function() {
            this.prepararItens();
            if (nItens > 3) {
                if (nItens > 5) {
                    this.criarMiniaturas();
                    this.atualizaClassesPaginacao();
                }
                this.atualizaClassesSlide();
                avancarSlide();
                _intervalo = setInterval(avancarSlide, (this.intervaloTransicao * 1000));
            } else if (nItens == 3) {
                $(listagem).animate({left: 480}, 500);
            }
            $(".rodape_flutuante").hide();
            this.atribuiEventos();
            this.inicializado = true;
        },
        prepararItens: function() {
            for (ind = 0; ind < nItens; ind += 1) {
                $(itens[ind]).attr("data-ind", ind);
            }
            ;
            if (nItens > 3) {
                var ultimoItem = $(itens[(nItens - 3)]).clone();
                var primeiroItem = $(itens[0]).clone();
                $(ultimoItem).prependTo(listagem).removeClass("ativa").removeClass("anterior").removeClass("old_anterior").removeClass("proxima").attr("data-ref", "ultimo_item_falso");
                $(primeiroItem).appendTo(listagem).removeClass("ativa").removeClass("anterior").removeClass("old_anterior").removeClass("proxima").attr("data-ref", "primeiro_item_falso");
                todosItens = $(listagem).find("li");
            }
        },
        criarMiniaturas: function() {
            var divLeft = "";
            var divRight = "";
            if ((nItens - 3) > 9) {
                divLeft = "<div class='left'><a class='btn_left'></a></div>";
                divRight = "<div class='right'><a class='btn_right'></a></div>";
            }
            $(container).append("<div class='containerPaginacao'>" + divLeft + "<ul class='paginacao'></ul>" + divRight + "</div>");
            for (ind = 0; ind < nItens; ind += 1) {
                $(container).find(".paginacao").append("<li><a href='#pag-item-" + ind + "' class='item_pag_" + ind + "' data-ind='" + ind + "'></a>");
                var thumb = $(imgs[ind]).clone();
                thumb.appendTo(".item_pag_" + ind).css({
                    width: "150px"
                });
            }
        },
        atualizaClasses: function() {
            this.atualizaClassesSlide();
            this.atualizaClassesPaginacao();
        },
        atualizaClassesSlide: function() {
            //alert(pos);
            var realPos = pos + 1;
            var ant = (realPos > 0) ? (realPos - 1) : (nItens - 3);
            var next = (realPos < nItens) ? (realPos + 1) : 0;
            $(todosItens).removeClass("ativa").removeClass("proxima").removeClass("old_anterior");
            if (realPos > 1) {
                $(listagem).find(".anterior").removeClass("anterior").removeClass("ativa").removeClass("proxima").addClass("old_anterior");
            }
            $(todosItens[ant]).removeClass("proxima").removeClass("ativa").removeClass("old_anterior").addClass("anterior");
            $(todosItens[next]).removeClass("anterior").removeClass("ativa").removeClass("old_anterior").addClass("proxima");
            $(todosItens[realPos]).removeClass("anterior").removeClass("proxima").removeClass("old_anterior").addClass("ativa");
        },
        atualizaClassesPaginacao: function() {
            var ant = (pos > 0) ? (pos - 1) : (nItens - 3);
            var next = (pos < nItens) ? (pos + 1) : 0;
            $(container).find(".paginacao").find("a").children("img").css({"border-style": "solid", "border-color": "#ffffff", "border-width": "2px"});
            $(container).find(".paginacao").find("a[data-ind=" + ant + "]").children("img").css({"border-style": "solid", "border-color": "#cccccc", "border-width": "2px"});
            $(container).find(".paginacao").find("a[data-ind=" + next + "]").children("img").css({"border-style": "solid", "border-color": "#cccccc", "border-width": "2px"});
            $(container).find(".paginacao").find("a[data-ind=" + pos + "]").children("img").css({"border-style": "solid", "border-color": "#111111", "border-width": "2px"});
        },
        atribuiEventos: function() {
            this.atribuiEventosPaginacao();
        },
        atribuiEventosPaginacao: function() {
            $(container).find(".paginacao").find("a").live("click", function() {
                clearInterval(_intervalo);
                newPos = $(this).attr("data-ind") - 1;
                pos = newPos;
                avancarSlide();
                _intervalo = setInterval(avancarSlide, (plugin.intervaloTransicao * 1000));
            });

            $('.ativa').live("mouseenter", function() {
                $('.setas').show();
                pause = true;
            });

            $('.ativa').live("mouseleave", function() {
                $('.setas').fadeOut();
                pause = false;
            });

            $(params.closeButton).live("click", function() {
                clearInterval(_intervalo);
                this.inicializado = false;
                $("#fullscreen_slider").fadeOut(500, function() {
                    $(".homeBrasilSlider").html("<div class='containerSlider'></div>");
                    $(".rodape_flutuante").show();
                });
            });
        }
    };

    var avancarSlide = function() {
        if (plugin.inicializado) {
            if (pause == false) {
                if (pos < (nItens - plugin.itensVisiveis)) {
                    pos += 1;
                } else {
                    pos = 0;
                }
                $(listagem).attr("data-atual", pos);
                var leftPos = pos * (-1 * largura_item);
                $(listagem).animate({left: leftPos}, (plugin.velocidadeTransicao * 1000));
                var myInd = (((pos - 1) < 0) ? (pos) : (pos - 1)) + 1;
                var titulo = $(imgs[myInd]).attr("title");
                if (titulo != "") {
                    $("#legenda_img").html(titulo);
                }
                plugin.atualizaClasses();
            }
        } else {
            clearInterval(_intervalo);
            plugin.inicializado = false;
        }
    };

    if (!plugin.inicializado) {
        plugin.inicializado = true;
        plugin.init();
    }

    return plugin;
};







$(document).ready(function() {
    var btn_proximo = ".btn_right";
    var btn_anterior = ".btn_left";
    var listar = ".paginacao";
    var bg_anterior = "url(images/seta_esquerda.png) no-repeat 40px 45px";
    var bg_proximo = "url(images/seta_direita.png) no-repeat 71px 45px";
    var tamanhobox = 165;
    var quantidadeitensexibido = 7;

    var quantidade = ($(listar).find("li").length);
    var larguratotal = quantidade * tamanhobox;

    $(listar).css("width", larguratotal);



    quantidade = quantidade - quantidadeitensexibido;
    totalx = quantidade * -tamanhobox;
    var executando = false;

    $(btn_proximo).live('click', function() {
        if (executando) {
            return false;
        }


        executando = true;
        var deslocamento_left = $(listar).css('left');

        var andamento_maximo = totalx; // + "px";;
        andamento_maximo = parseInt(andamento_maximo);

        var deslocamento_ja_feito = $(listar).css('left');
        deslocamento_ja_feito = deslocamento_ja_feito.replace("px", "");
        if (deslocamento_ja_feito == "auto") {
            deslocamento_ja_feito = 0;
        }

        deslocamento_ja_feito = parseInt(deslocamento_ja_feito);

        if (deslocamento_ja_feito > (andamento_maximo * -1)) {
            $(btn_proximo).css('background', bg_proximo);
            $(btn_anterior).css('background', bg_anterior);
            $(listar).stop().animate({"left": "-=" + tamanhobox}, 300, function() {
                executando = false;
            });
        } else {
            $(btn_proximo).css('background', "none");
            executando = false;
            return false;
        }
    });


    $(btn_anterior).live('click', function() {
        if (executando) {
            return false;
        }
        executando = true;

        var deslocamento_left = $(listar).css('left');
        deslocamento_left = deslocamento_left.replace("px", "");

        if (deslocamento_left == "auto") {
            deslocamento_left = 0;
        }

        deslocamento_left = parseInt(deslocamento_left);


        if (deslocamento_left < 0) {
            $(btn_proximo).css('background', bg_proximo);
            $(btn_anterior).css('background', bg_anterior);
            $(listar).stop().animate({"left": "+=" + tamanhobox}, 300, function() {
                executando = false;
            });
            return false;
        } else {
            $(btn_anterior).css('background', "none");

            executando = false;
            return false;

        }

    });
});
