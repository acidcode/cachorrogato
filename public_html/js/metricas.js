$(document).ready(function() {
    BrowserDetect.init();

    _acesso.navegador = BrowserDetect.browser;
    _acesso.versao = BrowserDetect.version;
    _acesso.sistema = BrowserDetect.OS;
    _acesso.user_agent = navigator.userAgent;
    _acesso.referer = $("#acesso_referer").val();

    $.ajax({
        url: "/ajax/registrar-acesso",
        data: _acesso,
        type: "POST",
        dataType: "JSON",
        success: function(retorno) {
            _acesso.registrado = retorno.status;
        }
    });
});
