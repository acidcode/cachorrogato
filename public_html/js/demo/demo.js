$(document).ready(function() {
	/* Demo Start */
	
	$(".mws-tabs").tabs();
	
	$.datepicker.setDefaults($.datepicker.regional["pt-BR"]);
	
	$(".mws-datepicker").datepicker({dateFormat: 'dd/mm/yy',
		dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado'],
		dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
		dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'] ,
		monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],showOtherMonths:true}); 
	
	
	
	$("#mws-jui-dialog").dialog({
		autoOpen: false, 
		title: "jQuery-UI Dialog", 
		modal: true, 
		width: "640", 
		buttons: [{
				text: "Close Dialog", 
				click: function() {
					$( this ).dialog( "close" );
				}}]
	});
	$("#mws-form-dialog").dialog({
		autoOpen: false, 
		title: "Editar descrição da imagem", 
		modal: true, 
		width: "640",
		draggable: false,

		buttons: [{
				text: "Atualizar", 
				click: function() {
				  $.post('/backend/empreendimento/editdesc/', {
					    	id_image:$('#id_image').val(),
					        descricao:$('#descricao').val()},
					               function(data){
					                     alert(data.Message);

					                     $("#mws-form-dialog").dialog("close");
					                     location.reload();
					                }
					                ,'json');


				}}]
	});

	
	$('.mws-datatable').dataTable( {"oLanguage": {
	    "sProcessing":   "Processando...",
	    "sLengthMenu":   "Mostrar _MENU_ registros",
	    "sZeroRecords":  "Não foram encontrados resultados",
	    "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registros",
	    "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registros",
	    "sInfoFiltered": "(filtrado de _MAX_ registros no total)",
	    "sInfoPostFix":  "",
	    "sSearch":       "Buscar:",
	    "sUrl":          "",
	    "oPaginate": {
	        "sFirst":    "Primeiro",
	        "sPrevious": "Anterior",
	        "sNext":     "Próximo",
	        "sLast":     "Último"
	    }
	},sPaginationType: "full_numbers","bSort": false	
	});
	
	
	/* Validation Plugin */


jQuery.validator.addMethod("CurrencyFormat", function(value, element) { 
		var currency = /^\d{1,3}(\.\d{3})*\,\d{2}$/;
		if (currency.test($(element).val())) {
		    return true;
		    } else return false;
		
}, "Formato da moeda inválido.<br> Ex: 1.500.000,00");
   

jQuery.validator.addMethod("dateFormat", function(value, element) { 
		var date = /^((0[1-9]|[12]\d)\/(0[1-9]|1[0-2])|30\/(0[13-9]|1[0-2])|31\/(0[13578]|1[02]))\/\d{4}$/;
		if (date.test($(element).val())) {
		    return true;
		    } else return false;
		
}, "Formato de data inválido.<br> Ex: 22/12/2012 ");

jQuery.validator.addMethod("mlength", function(value, element, p) { 	
	p[0] = $(element).attr('mlength');
	console.log((!$(element).val().length < $(element).attr('mlength')));
	if ($(element).val().length < $(element).attr('mlength')) {
	    return true;
	    } else return false;
	
}, "O numero maxímo de caracteres é de {0}");

jQuery.extend(jQuery.validator.messages, {
    required: "Este campo é obrigatório.",
    remote: "Por favor conserte este campo.",
    email: "Por favor entre com um email válido.",
    url: "Por favor entre com uma URL válido.",
    date: "Por favor entre com uma data válido. Ex: dd/mm/aaaa",
    dateISO: "Por favor entre com uma data (ISO) válido.",
    number: "Por favor entre com um numero válido. Ex: 10.00 o 10",
    digits: "Por favor entre somente com digitos.",
    creditcard: "Por favor entre com o numero de cartão de credito válido.",
    equalTo: "Por favor entre com o mesmo valor.",
    accept: "Por favor entre com um extensão válida.",
    maxlength: jQuery.validator.format("Por favor não entre com menos  {0} characteres."),
    minlength: jQuery.validator.format("Por favor entre com pelo menos {0} characteres."),
    rangelength: jQuery.validator.format("Por favor insira um valor entre {0} e {1} de characteres."),
    range: jQuery.validator.format("Por favor insira um valor entre {0} e {1}."),
    max: jQuery.validator.format("Por favor entre com um valor menor ou igual a {0}."),
    min: jQuery.validator.format("Por favor entre com um valor maior ou igual a {0}.")
});

function validate(form,formAjaxsubmit)	{
	
	formAjaxsubmit = typeof formAjaxsubmit !== 'undefined' ? formAjaxsubmit : false;
	
	if(!formAjaxsubmit){
	  $(form).validate({
		 errorClass: "error-message",
		 errorElement: "li",
		 wrapper:"ul"
	  });
	}else{
		$(form).validate({
			 errorClass: "error-message",
			 errorElement: "li",
			 wrapper:"ul",
			 submitHandler: formAjaxsubmit
		  });
	}
	
	$(form).each(function(i, el){       
        var settings = $.data(this, 'validator').settings;
        settings.ignore += ':not(.chzn-done)';
    });
	
	$(form).find('input').each(function(i, el){       
       
		if('text'==$(el).attr('type') && $(el).hasClass('required')){
			
			$('<div class="obrigatorio"></div>').insertBefore(el);
			
		}
		
    });
	
	$(form).find('textarea').each(function(i, el){       
	       
		if($(el).hasClass('required')){
			
			$('<div class="obrigatorio"></div>').insertBefore(el);
			
		}
		
    });
	
	
	$(form).find('select').each(function(i, el){       
	       
		if($(el).hasClass('required') && 'multiple' == $(el).attr('multiple')){
			
			$('<div class="obrigatorio" style="z-index:100000;height:29px!important;"></div>').insertBefore(el);
			
		}else if($(el).hasClass('required') && 'multiple' != $(el).attr('multiple')){
			
			$('<div class="obrigatorio-select"></div>').insertBefore(el);
			
		}
		
    });
	
	
}

validate('#cadastro-empreedimento');
validate('#cadastro-imagem');
validate('#busca-galeria');
validate('#cadastro-pagina');
validate('#cadastro-destaque');
validate('#cadastro-grupo');
validate('#cadastro-tipo');
validate('#cadastro-destaque');
validate('#cadastro-incorporator');
validate('#cadastro-fase-de-obra');
validate('#galeria-categoria');
validate('#localizacao-bairro');
validate('#localizacao-cidade');
validate('#localizacao-estado');
validate('#localizacao-pais');
validate('#ficha-tecnica-cadastro');
validate('#ficha-lazer-cadastro');
validate('#copiar-ficha');
validate('#copiar-lazer');
validate('.incorporator');
validate('.banners');
validate('#informacoes-extra-edit');



validate('#email-config',function(form){	
	$.post($(form).attr('action'),{
		 'host':$('#host').val(),
		 'username':$('#username').val(),
		 'password':$('#password').val(),
		 'port':$('#port').val(),
		 'ssl':$('#ssl').val(),
		 'auth':$('#auth').val(),
		 'to':$('#to').val(),
		 'cc':$('#cc').val()
	     },function(Messages){
		    
			  alert(Messages.Message);

 },'json');});
validate('#email-config2',function(form){	
	$.post($(form).attr('action'),{
		 'host':$('#host2').val(),
		 'username':$('#username2').val(),
		 'password':$('#password2').val(),
		 'port':$('#port2').val(),
		 'ssl':$('#ssl2').val(),
		 'auth':$('#auth2').val(),
		 'to':$('#to2').val(),
		 'cc':$('#cc2').val()
	     },function(Messages){
		    
			  alert(Messages.Message);

},'json');});
validate('#email-config3',function(form){	
	$.post($(form).attr('action'),{
		 'host':$('#host3').val(),
		 'username':$('#username3').val(),
		 'password':$('#password3').val(),
		 'port':$('#port3').val(),
		 'ssl':$('#ssl3').val(),
		 'auth':$('#auth3').val(),
		 'to':$('#to3').val(),
		 'cc':$('#cc3').val()
	     },function(Messages){
		    
			  alert(Messages.Message);

},'json');});
validate('#email-config4',function(form){	
	$.post($(form).attr('action'),{
		 'host':$('#host4').val(),
		 'username':$('#username4').val(),
		 'password':$('#password4').val(),
		 'port':$('#port4').val(),
		 'ssl':$('#ssl4').val(),
		 'auth':$('#auth4').val()
	     },function(Messages){
		    
			  alert(Messages.Message);

},'json');});

validate('#email-config5',function(form){	
	$.post($(form).attr('action'),{
		 'host':$('#host5').val(),
		 'username':$('#username5').val(),
		 'password':$('#password5').val(),
		 'port':$('#port5').val(),
		 'ssl':$('#ssl5').val(),
		 'auth':$('#auth5').val()
	     },function(Messages){
		    
			  alert(Messages.Message);

},'json');});

validate('#email-padrao-config',function(form){	
	$.post($(form).attr('action'),{
		 'email':$('#email').val()
	     },function(Messages){
		    
			  alert(Messages.Message);

},'json');});

validate('#endereco-config',function(form){	
	$.post($(form).attr('action'),{
		 'endereco':$('#endereco').val()
	     },function(Messages){
		    
			  alert(Messages.Message);

},'json');});

validate('#chat-config',function(form){	
	$.post($(form).attr('action'),{
		 'defaultlink':$('#defaultlink').val()
	     },function(Messages){
		    
			  alert(Messages.Message);

},'json');});
validate('#google-analytics-config',function(form){
	$.post($(form).attr('action'),{
		 'script':$('#script').val()
	     },function(Messages){
		    
			  alert(Messages.Message);

},'json');});
validate('#add-Block',function(form){
	$.post($(form).attr('action'),{
		 'CmsBlock_Title':$('#CmsBlock_Title').val(),
		 'CmsBlock_Identifier':$('#CmsBlock_Identifier').val(),
		 'CmsBlock_Content':$('#elrte').val()},function(data){
			  
			  alert(data.message);

  },'json');
});
validate('#edit-Block',function(form){
	$.post($(form).attr('action'),{
		 'CmsBlock_Id':$('#CmsBlock_Id').val(),
		 'CmsBlock_Title'      :$('#CmsBlock_Title').val(),
		 'CmsBlock_Identifier' :$('#CmsBlock_Identifier').val(),
		  'CmsBlock_Content'   :$('#elrte').val()},function(data){
			  alert(data.message);

  },'json');
});
validate('#add-statistics',
function (form){
	$.post($(form).attr('action'),{
		
		 'evaluation_date':$('#evaluation_date').val(),
		 'variation_per_period':$('#variation_per_period').val(),
		 'variation_per_month':$('#variation_per_month').val(),
		 'square_meter_value':$('#square_meter_value').val(),
		 'neighborhood_id':$('#neighborhood_id').val()},function(messages){
		    
			  alert(messages.message);
             if(messages.response){
			  window.location="/backend/statistics/list";
             } 
  },'json');
}		
);
validate('#edit-statistics',
		function (form){
	      $.post($(this).attr('action'),{
		 'id':$('#id').val(),
		 'evaluation_date': $('#evaluation_date').val(),
		 'variation_per_period':$('#variation_per_period').val(),
		 'variation_per_month':$('#variation_per_month').val(),
		 'square_meter_value':$('#square_meter_value').val(),
		 'neighborhood_id':$('#neighborhood_id').val()},function(messages){
		    
			  alert(messages.message);
             if(messages.response){
			  window.location="/backend/statistics/list";
             } 
  },'json');
		}		
		);
validate('#add-Video',
function(form){
	
	$.post($(this).attr('action'),{
		 'title':$('#title').val(),
		 'link':$('#link').val(),
		 'description':$('#description').val()
		 },function(Messages){
		    
			  alert(Messages.Message);

             if(Messages.response){
			  
           	  $('#title').val('');
           	  $('#link').val('');
           	  $('#description').val('');
         }
  },'json');
	
}		
);
validate('#edit-Video',
function(form){
	$.post($(this).attr('action'),{
		 'id':$('#id').val(),
		 'title':$('#title').val(),
		 'link':$('#link').val(),
		 'description':$('#description').val()
		 },function(Messages){
			  alert(Messages.Message);
             if(Messages.response){
			    window.location="/backend/video/list";
             }
  },'json');
}
);
;

validate('#add-user',function(form){	
	$.post($(form).attr('action'),{
		 'username':$('#username').val(),
		 'firstname':$('#firstname').val(),
		 'secondname':$('#secondname').val(),
		 'email':$('#email').val(),
		 'password':$('#password').val(),
		 'about':$('#about').val(),
		 'is_active':$('#is_active').val(),
		 'role_id':$('#permission').val()},function(Messages){
		    
			  alert(Messages.Message);

           if(Messages.response){
			  
			  $('#username').val('');
			  $('#firstname').val('');
			  $('#secondname').val('');
			  $('#email').val('');
			  $('#password').val('');
           }
  },'json');});



var privileges_ids = [];

var ids = '';

$('#privileges-list select').each(function(index,element){
	 
	$('#'+$(element).attr('id')).change(function(ind,el){
      
		  //console.log($(element).attr('id')+' option:selected');
		privileges_ids.length = 0;
		 ids ='';
		  		  
	    $('select option:selected').each(function(i,e){

		   privileges_ids.push($(this).val());
           
       });

	    ids =  privileges_ids.join(',');
	   console.log(ids);
 });
  
	

});


validate('#user-permission-add',function(form){	
		$.post($(form).attr('action'),{
			 'name':$('#name').val(),
			  'ids': ids
		     },function(data){
			    
				  alert(data.message);

	},'json');});




validate('#user-permission-edit',function(form){	
	$.post($(form).attr('action'),{
		 'name':$('#name').val(),
		  'ids': ids,
		  'role_id':$('#role_id').val()
	     },function(data){
		    
			  alert(data.message);

},'json');});


});