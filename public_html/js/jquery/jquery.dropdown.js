$(document).ready(function(){
	$('#sobre').dialog({ width: 480, height: 320, modal: true });
        $('#sobre').dialog('close');
        $('ul.dropdown li').dropdown();
});

$.fn.dropdown = function() {

	$(this).hover(function(){
		$(this).addClass("hover");
		$('> .dir',this).addClass("open");
		$('ul:first',this).css('visibility', 'visible');
	},function(){
		$(this).removeClass("hover");
		$('.open',this).removeClass("open");
		$('ul:first',this).css('visibility', 'hidden');
	});

}