$(document).ready(function() {
    $(".oferta-ativa").find(".foto_oferta_da_semana").slideDown(400);
    $(".oferta-ativa").find(".text_oferta_da_semana").slideDown(400);

    $('.incluir_oferta').click(function() {
        $(".oferta-ativa").find(".foto_oferta_da_semana").hide();
        $(".oferta-ativa").find(".text_oferta_da_semana").hide();
        $(".incluir_oferta").removeClass("oferta-ativa");
        $(".incluir_oferta").css("height", "auto");
        $(".oferta-ativa").css("height", "auto");



        $(this).addClass("oferta-ativa");
        $(".oferta-ativa").animate({height: '+=211'}, 400,
                function() {
                    $(".oferta-ativa").find(".foto_oferta_da_semana").fadeIn(400);
                    $(".oferta-ativa").find(".text_oferta_da_semana").fadeIn(400);
                });
    });



    var y_fixo = $("#mostrar").offset().top;
    $('#mostrar').hide();
    $(window).scroll(function() {
        $("#mostrar").animate({
            top: y_fixo + $(document).scrollTop() + "px"
        }, {duration: 500, queue: false}
        );

        x = $(window).scrollTop();
        y = 100;

        if (x > y)
        {
            $('#mostrar').fadeIn();
        }
        else
        {
            $('#mostrar').fadeOut();
        }
    });


    /* Menu */
    var pagina = $("body").attr("id");

    var status = true;
    var altura = $(".encontre-seu-imovel").height();

    if (pagina == "produto" || pagina == "listagem") {
        $("#menu_encontre").css("height", "50px");
        $("#menu_encontre").css("overflow", "hidden");
        if (pagina == "listagem") {
            $(".box_busca_avancada").animate({height: altura}, 300);
            $(".box_busca_avancada").delay(500).css("height", " auto");
        }
        status = false;
    }

    $('#abrir_menu').live('click', function() {
        if (status == false) {
            $("#menu_encontre").css("overflow", "none");
            $("#menu_encontre").animate({height: altura}, 300);
            $("#abrir_menu").css("background", "#bfcbc1 url(img/btn_topo.png) no-repeat 170px 15px");
            status = true;
        } else {
            $("#menu_encontre").css("overflow", "hidden");
            $("#menu_encontre").animate({height: "50px"}, 300);
            $("#abrir_menu").css("background", "#bfcbc1 url(img/btn_botton.png) no-repeat 170px 15px");

            status = false;
        }
    });

    /* Fim Menu */



    /* Produtos */

    $('.item_apresentacao').find("a").live('click', function() {
        var destino = "." + $(this).attr("rel");
        console.debug(destino);
        //	$(".box_emp").css("display","none");
        $(".box_emp").slideUp(300, function() {
            $(".box_emp").animate({height: "auto"}, 300, function() {
                $(destino).slideDown();
            });

        });
    });


    /* Fim Produtos */


    $('.btn_busca_avancada').live('click', function() {

        $(".box_busca_avancada").animate({height: altura}, 300);
        $("#menu_encontre").css("overflow", "hidden");
        $("#menu_encontre").animate({height: "50px"}, 300);
        $("#abrir_menu").css("background", "#bfcbc1 url(img/btn_botton.png) no-repeat 170px 15px");

    });


});


