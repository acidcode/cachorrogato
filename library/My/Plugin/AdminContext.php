<?php

class My_Plugin_AdminContext extends Zend_Controller_Plugin_Abstract {

    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        if ($request->getParam("module") == "backend" && $request->getParam("isLogin") != 1) {
            $layout = Zend_Layout::getMvcInstance();
            $auth = Zend_Auth::getInstance();

            if (!$auth->hasIdentity()) {
                $redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
                $redirector->gotoUrl('admin');
            }
            $layout->setLayoutPath(APPLICATION_PATH . "/layouts/scripts/back-end");
            $layout->setLayout('backend');
        }
    }

}