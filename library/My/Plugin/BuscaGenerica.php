<?php

class My_Plugin_BuscaGenerica extends Zend_Controller_Plugin_Abstract {

    public function preDispatch(Zend_Controller_Request_Abstract $request) {

        $sessao = new Zend_Session_Namespace('NULL');
        if (!is_null($_SESSION['localizacao'])) {
            $localizacao = $_SESSION['localizacao'];
        } else {
            $localizacao = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $_SERVER['REMOTE_ADDR']));
            //$localizacao = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=187.56.240.65'));
            $_SESSION['localizacao'] = $localizacao;
        }

        $estado = $_SESSION['uf_usuario'];
        $cidade = $_SESSION['cidade_usuario'];

        if (is_null($cidade) && empty($_POST['busca_cidade'])) {
            $cidade = utf8_encode(html_entity_decode($localizacao["geoplugin_city"]));
        }

        if ((empty($estado) || is_null($estado)) && empty($_SESSION['uf_usuario'])) {
            $estado = $this->UfReverse(utf8_encode(html_entity_decode($localizacao["geoplugin_region"])), true);
        }

        if ($_POST['buscaestado']) {
            $estado = $_POST['buscaestado'];
            $cidade = $this->capital($_POST['buscaestado']);
        }
        if ($_POST['busca_cidade']) {
            $cidade = $_POST['busca_cidade'];
        }

        $_SESSION['uf_usuario'] = $estado;
        $_SESSION['cidade_usuario'] = $cidade;
    }

    public function UfReverse($estado, $reverse = false) {

        $estados = array("SP" => "São Paulo",
            "AC" => "Acre",
            "AL" => "Alagoas",
            "AM" => "Amazonas",
            "AP" => "Amapá",
            "BA" => "Bahia",
            "CE" => "Ceará",
            "DF" => "Distrito Federal",
            "ES" => "Espirito Santo",
            "GO" => "Goiás",
            "MA" => "Maranhão",
            "MT" => "Mato Grosso",
            "MS" => "Mato Grosso do Sul",
            "MG" => "Minas Gerais",
            "PA" => "Pará",
            "PB" => "Paraíba",
            "PR" => "Paraná",
            "PE" => "Pernambuco",
            "PI" => "Piauí",
            "RJ" => "Rio de Janeiro",
            "RN" => "Rio Grande do Norte",
            "RO" => "Rondônia",
            "RS" => "Rio Grande do Sul",
            "RR" => "Roraima",
            "SC" => "Santa Catarina",
            "SE" => "Sergipe",
            "TO" => "Tocantins"
        );
        if ($reverse) {
            $estados = array_flip($estados);
        }

        return $estados[$estado];
    }

    public function capital($estado) {

        $estados = array("SP" => "São Paulo",
            "AC" => "Rio Branco",
            "AL" => "Maceió",
            "AM" => "Manaus",
            "AP" => "Macapá",
            "BA" => "Salvador",
            "CE" => "Fortaleza",
            "DF" => "Brasília",
            "ES" => "Vitória",
            "GO" => "Goiânia",
            "MA" => "São Luiz",
            "MT" => "Cuiabá",
            "MS" => "Campo Grande",
            "MG" => "Belo Horizonte",
            "PA" => "Belém",
            "PB" => "João Pessoa",
            "PR" => "Curitiba",
            "PE" => "Recife",
            "PI" => "Terezina",
            "RJ" => "Rio de Janeiro",
            "RN" => "Natal",
            "RO" => "Porto Velho",
            "RS" => "Porto Alegre",
            "RR" => "Boa Vista",
            "SC" => "Florianópolis",
            "SE" => "Aracajú",
            "TO" => "Palmas"
        );

        return $estados[$estado];
    }

}