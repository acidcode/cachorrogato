<?php

class Homebrasil_helpers_DestaquesRodape extends Zend_View_Helper_Abstract {

    public function destaquesRodape() {
        return $this;
    }

    public function getDestaquesRodape() {
        $empM = new Backend_Model_DbTable_TbEmpreendimento();
        return $empM->findDestaquesRodape();
    }

}

?>