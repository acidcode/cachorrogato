<?php

class Homebrasil_helpers_MenusLaterais extends Zend_View_Helper_Abstract {

    public function menusLaterais() {
        return $this;
    }

    public function getMenuCidades() {
        $cidadesM = new Backend_Model_DbTable_TbCidades();
        return $cidadesM->findMoreViewed();
    }

    public function getMenuBairros() {
        $bairrosM = new Backend_Model_DbTable_TbBairros();
        return $bairrosM->findMoreViewed();
    }

    public function getMenuDormitorios() {
        $empM = new Backend_Model_DbTable_TbEmpreendimento();
        return $empM->findDormsMoreViewed();
    }

    public function getMenuFootages() {
        $empM = new Backend_Model_DbTable_TbEmpreendimento();
        return $empM->findFootagesMoreViewed();
    }

    public function getMenuPrices() {
        $empM = new Backend_Model_DbTable_TbEmpreendimento();
        return $empM->findPricesMoreViewed();
    }

    public function getMenuTipos() {
        $tipoM = new Backend_Model_DbTable_TbTipoImoveis();
        return $tipoM->findMoreViewed();
    }

    public function getMenuEstagios() {
        $estM = new Backend_Model_DbTable_TbFaseObras();
        return $estM->findMoreViewed();
    }

}

?>