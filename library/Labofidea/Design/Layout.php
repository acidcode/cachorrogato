<?php

/**
 * Controle do Handler de Layout/Template
 * @author @labofidea
 * @refatorado @acidcode
 */
class Labofidea_Design_Layout extends Zend_Controller_Plugin_Abstract {

    public function preDispatch(Zend_Controller_Request_Abstract $request) {

        $this->_layoutHandler($request);
    }

    protected function _layoutHandler(Zend_Controller_Request_Abstract $request) {


        $layout = Zend_Layout::getMvcInstance();

        if ($request->getControllerName() == 'acesso-empresa' && $request->getModuleName() == 'frontend') {
            $layout->setLayoutPath(APPLICATION_PATH . "/layouts/scripts/front-end/fornecedor")->setLayout('template');
        } else if ($request->getControllerName() == 'cadastro-cliente' && $request->getModuleName() == 'frontend') {
            $layout->setLayoutPath(APPLICATION_PATH . "/layouts/scripts/front-end/cadastro")->setLayout('template');
        } else if ($request->getModuleName() == 'backend') {
            $layout->setLayoutPath(APPLICATION_PATH . "/layouts/scripts/back-end")->setLayout('template');
        } else {
            $layout->setLayoutPath(APPLICATION_PATH . "/layouts/scripts/front-end/portal")->setLayout('template');
        }
    }

}
