<?php
class Labofidea_View_Helper_Image extends Zend_View_Helper_Abstract
{
    protected $_image;
    protected $_quality;
    protected $_width;
    protected $_height;
    
    public function image ($_image)
    {
        $this->_image = $_image;
        return $this;
    }
    public function resize ($width, $height = null, $quality = 50)
    {
        $this->_width = $width;
        $this->_height = $height;
        $this->_quality = $quality;
        if ($height == null) {
            $this->_height = $width;
        }
        if(''!=$this->_image || 0!=$this->_image || !$this->_image ){
        if (@file_exists(UPLOAD_PATH . '/galerias/' . $this->_image)) {
            if (@! file_exists(
            UPLOAD_PATH . "/galerias/thumbs/" . $this->_width . 'x' .
             $this->_height.'/'.$this->_image)) {
                
                $varien_image = new Varien_Image(UPLOAD_PATH . '/galerias/' . $this->_image);
                
                $varien_image->keepTransparency(true);
                $varien_image->quality($this->_quality);
                $varien_image->resize($this->_width, $this->_height);
                
               $varien_image->save(UPLOAD_PATH . "/galerias/thumbs/" . $this->_width . 'x' .$this->_height, $this->_image);
           }
        }
     }
        return $this;
    }
    protected function _imageExists ($image)
    {
        if (file_exists($image)) {
            return true;
        }
    }
    public function resizedImage ()
    {
      
        return 'thumbs/' . $this->_width . 'x' . $this->_height . '/' .$this->_image;
         
    }
}
?>