<?php

class Labofidea_View_Helper_Config extends Zend_View_Helper_Abstract {
		
	public function config(){
		return $this;
	}
	
	public function getConfig($path){
		$config = new Backend_Model_Config();
		$data   = $config->getConfig($path);
		return Zend_Json::decode($data['config_data']);
	}

}

?>