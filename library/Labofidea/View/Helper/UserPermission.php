<?php

class Labofidea_View_Helper_UserPermission  extends Zend_View_Helper_Abstract{

	
	public function userPermission(){
		
		return $this;
	}
	
	public function getAllPrivilegesByResourceId($resource){
		$db = new Backend_Model_DbTable_AdminUserResources();
		return $db->getDefaultAdapter()
		->select()->from('admin_resources_privileges')
		->where('resource_id = ?',(int)$resource)
		->query()
		->fetchAll();
	}
	
	public function getAllPrivilegesByRoleId($Role){
		$db = new Backend_Model_DbTable_AdminUserRoles();
		return $db->getDefaultAdapter()
		->select()->from('admin_resources_privileges')
		->where('role_id = ?',(int)$Role)
		->query()
		->fetchAll();
	}
	
	
	public function isAllowed($resource = null, $privilege = null){
		
		$user        = new Backend_Model_DbTable_AdminUser();
		$credential  = $user->getAuth()->getStorage()->read();
		$permission  = new Labofidea_Auth_Permission();
	
		return  $permission->isAllowed($credential,$resource , $privilege,new Zend_Acl ());
	}
	
	
}

?>