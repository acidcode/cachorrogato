<?php
class Labofidea_View_Helper_Cms extends Zend_View_Helper_Abstract
{
    public $view;
        
     
    public function cms ()
    {
        return $this;
    }
    public function getBlock ($identifier)
    {
        if (null != $identifier) {
            $cms = new Backend_Model_DbTable_CmsBlock();
            $block = $cms->setCmsBlock_Identifier($identifier)->getBlock();
            return $block[0]['CmsBlock_Content'];
        }
    }
    public function drawnUrl ($id, $target = Backend_Model_DbTable_UrlRewrite::Products)
    {
       
        $url = new Backend_Model_DbTable_UrlRewrite();
        $Path = 
        $url->setUrlRewrite_Target($target)
            ->setUrlRewrite_Product_Id($id)
            ->setUrlRewrite_Category_Id($id)
            ->setUrlContentsById()
            ->getUrlRewrite_Path();
        
        if ($target == Backend_Model_DbTable_UrlRewrite::Products) {
            
            if (trim(str_replace('.html', '', $Path)) == '') {
                $Path = 'detalhes/produto/id/' . $id;
            }
            
        } elseif ($target == Backend_Model_DbTable_UrlRewrite::Categories) {
            
            if (trim(str_replace('.html', '', $Path)) == '') {
                $Path = 'resultado-busca/resultado/buscar/termos/bairro/' . $id.'/';
            }
            
        }
        return '/' . $Path;
    }
    
    public function setView(Zend_View_Interface $view){
    	
        $this->view = $view;
        
    }
}
?>