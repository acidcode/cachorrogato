<?php

class Labofidea_View_Helper_User extends Zend_View_Helper_Abstract
{
    protected $_instance;
    
    
    public function User(){
    	
        $this->_instance = new Backend_Model_DbTable_AdminUser();

        return $this;
        
    }
    
    public function getStorageData(){
    	
        return $this->_instance->getAuth()->getStorage()->read();
        
    }
    
    public function getFullName(){
    	
        $secondname = ucfirst((strlen($this->getStorageData()->secondname) > 7)?substr($this->getStorageData()->secondname, 0,1).'.':$this->getStorageData()->secondname);
        
        return $this->getStorageData()->firstname.' '. $secondname;
        
    }
    
    
    
}
