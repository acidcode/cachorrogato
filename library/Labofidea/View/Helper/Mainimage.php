<?php
class Labofidea_View_Helper_Mainimage extends Zend_View_Helper_Abstract
{
    public function mainimage ($IdProducts, $mainImg)
    {
        if ($IdProducts) {
            $db = Zend_Registry::get('db');
            $img = $db->select()
                ->from(array('imgs' => 'empreendimento_galeria_itens'))
                ->where(
            'imgs.id_empreendimento =' . (int) $IdProducts . ' AND imgs.id= ' .
             (int) $mainImg)
                ->query()
                ->fetch();
        }
        return $img;
    }
}