<?php

class Labofidea_Auth_Access extends Zend_Controller_Plugin_Abstract {

    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        $this->_disableAuthHandler($request);
    }

    protected function _disableAuthHandler(Zend_Controller_Request_Abstract $request) {

        $Auth = new Backend_Model_DbTable_AdminUser();

        if ($request->getModuleName() == 'backend' &&
                $request->getActionName() != 'login' &&
                $request->getActionName() != 'recover-password' &&
                $request->getActionName() != 'gen-token' &&
                $request->getActionName() != 'reset-password' &&
                $request->getActionName() != 'change-password' &&
                $request->getActionName() != 'logout'
        ) {

            $redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');

            if (!$Auth->getAuth()->hasIdentity()) {

                $redirector->gotoUrl('admin');
            } else {

                $credentials = $Auth->getAuth()->getStorage()->read();
                $user = new Labofidea_Auth_Permission();

                if ($request->getControllerName() != 'no-permission' &&
                        $request->getControllerName() != 'index' &&
                        $request->getControllerName() != 'collections-order' &&
                        $request->getActionName() != 'editdesc' &&
                        $request->getActionName() != 'extra-info-save' &&
                        $request->getActionName() != 'extra-info-remove'
                ) {
                    if (!$user->isAllowed($credentials, $request->getControllerName(), $request->getActionName(), new Zend_Acl())) {
                        $redirector->gotoUrl('/backend/no-permission/');
                    }
                }
            }
        }
    }

}
