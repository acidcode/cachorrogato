<?php

class Labofidea_Auth_Access extends Zend_Controller_Plugin_Abstract {

    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        $this->_accessHandler($request);
    }

    protected function _accessHandler(Zend_Controller_Request_Abstract $request) {

        $Auth = new Backend_Model_DbTable_AdminUser();
        if ($request->getModuleName() == 'backend' &&
                $request->getActionName() != 'login' &&
                $request->getActionName() != 'recover-password' &&
                $request->getActionName() != 'gen-token' &&
                $request->getActionName() != 'reset-password') {

            if (!$Auth->getAuth()->hasIdentity()) {
                $redirector = Zend_Controller_Action_HelperBroker::getStaticHelper(
                                'redirector');
                $redirector->gotoUrl('admin');
            }
        }
    }

}
