<?php

class Labofidea_Auth_Permission {

    protected $_permission;
    protected $_resource;
    protected $_role;

    public function __construct() {

        $this->_role = new Backend_Model_DbTable_AdminUserRoles ();
        $this->_resource = new Backend_Model_DbTable_AdminUserResources ();
        $this->_permission = new Backend_Model_DbTable_AdminUserPermissions ();
    }

    public function addRole($roleName, $parent = null) {

        return $this->_role->insert(array('role_id' => new Zend_Db_Expr('NULL'), 'role_name' => $roleName, 'role_parent' => (null === $parent) ? new Zend_Db_Expr('NULL') : $parent, 'created' => new Zend_Db_Expr('CURRENT_TIMESTAMP')));
    }

    public function editRole($id, $roleName, $parent) {

        return $this->_role->update(array('role_name' => $roleName, 'role_parent' => $parent, 'created' => new Zend_Db_Expr('CURRENT_TIMESTAMP')), array('role_id' => $id));
    }

    public function removeRole($id) {

        return $this->_role->delete(array('role_id' => $id));
    }

    public function addResource($resourceName) {

        return $this->_resource->insert(array('id' => new Zend_Db_Expr('NULL'), 'resource_name' => $resourceName, 'created' => new Zend_Db_Expr('CURRENT_TIMESTAMP')));
    }

    public function editResource($id, $resourceName) {

        return $this->_resource->update(array('resource_name' => $resourceName, 'created' => new Zend_Db_Expr('CURRENT_TIMESTAMP')), array('role_id' => $id));
    }

    public function removeResource($id) {

        return $this->_resource->delete(array('id' => $id));
    }

    public function setPermissions($credentials, Zend_Acl $Acl = null) {

        if (null === $Acl) {
            $Acl = new Zend_Acl ();
        }

        $AllResources = $this->_resource->getDefaultAdapter()->select()->from('admin_resources')->query()->fetchAll();
        $AllRoles = $this->_role->getDefaultAdapter()->select()->from('admin_roles')->query()->fetchAll();

        // load the all resources.
        foreach ($AllResources as $resource) {

            $Acl->add(new Zend_Acl_Resource($resource ['name']));
        }

        // load the all roles.
        foreach ($AllRoles as $role) {

            $Acl->addRole(new Zend_Acl_Role($role ['role_id']));
        }

        $this->_setPrivileges($Acl, $this->_resource, $credentials->roleid);

        return $Acl;
    }

    public function isAllowed($credentials, $resource = null, $privilege = null, Zend_Acl $Acl) {

        $Acl = $this->setPermissions($credentials, $Acl);

        if (!$Acl->has($resource)) {
            $resource = null;
            $privilege = null;
        }

        return $Acl->isAllowed($credentials->roleid, $resource, $privilege);
    }

    protected function _setPrivileges(Zend_Acl $Acl, Zend_Db_Table_Abstract $db, $RoleId) {

        $permissions = ($db->getDefaultAdapter()->select()->from(array('roles' => 'admin_roles'), 'roles.role_id as role')->join(array('roles_privilege' => 'admin_roles_privileges'), 'roles.role_id = roles_privilege.role_id', '')->join(array('resource_privilege' => 'admin_resources_privileges'), 'resource_privilege.id = roles_privilege.privilege_id', 'resource_privilege.privilege as privilege')->join(array('resources' => 'admin_resources'), 'resources.id  = resource_privilege.resource_id', 'resources.name as resource')/* ->__toString() */
                        ->where('roles_privilege.role_id = ?', $RoleId)->query()->fetchAll());

        if (count($permissions) > 0) {
            foreach ($permissions as $permission) {

                $Acl->allow($permission ['role'], $permission ['resource'], $permission ['privilege']);
            }
        }
    }

}
