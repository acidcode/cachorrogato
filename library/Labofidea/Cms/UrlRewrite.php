<?php

class Labofidea_Cms_UrlRewrite extends Zend_Controller_Plugin_Abstract {

    protected $_UrlController = 'Cms';
    protected $_UrlAction = 'index';
    protected $_UrlModule;

    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        $this->_UrlHandler($request);
    }

    /**
     *
     * @return the $_UrlController
     */
    public function getUrlController() {
        return $this->_UrlController;
    }

    /**
     *
     * @param $_UrlController string           
     */
    public function setUrlController($_UrlController) {
        $this->_UrlController = (string) $_UrlController;
        return $this;
    }

    /**
     *
     * @return the $_UrlAction
     */
    public function getUrlAction() {
        if (null == $this->_UrlAction) {
            $this->_UrlAction = Zend_Controller_Front::getInstance()->getDispatcher()->getDefaultAction();
        }
        return $this->_UrlAction;
    }

    /**
     *
     * @param $_UrlAction string           
     */
    public function setUrlAction($_UrlAction) {
        $this->_UrlAction = (string) $_UrlAction;
        return $this;
    }

    /**
     *
     * @return the $_UrlModule
     */
    public function getUrlModule() {
        if (null == $this->_UrlModule) {
            $this->_UrlModule = Zend_Controller_Front::getInstance()->getDispatcher()->getDefaultModule();
        }
        return $this->_UrlModule;
    }

    /**
     *
     * @param $_UrlModule field_type           
     */
    public function setUrlModule($_UrlModule) {
        $this->_UrlModule = (string) $_UrlModule;
        return $this;
    }

    protected function _UrlHandler(Zend_Controller_Request_Abstract $request) {
        $frontController = Zend_Controller_Front::getInstance();
        $dispatcher = $frontController->getDispatcher();
        if (!$dispatcher->isDispatchable($request)) {
            $urlModel = new Backend_Model_DbTable_UrlRewrite();
            $url = $request->getControllerName();
            if ($urlModel->setUrlRewrite_Path($url)->pathExists()) {
                $frontController->setParam('noErrorHandler', true);
                $request->setControllerName($this->getUrlController())
                        ->setActionName($this->getUrlAction())
                        ->setModuleName($this->getUrlModule())
                        ->setParam(1, $url)
                        /* ->setParam('pagina', $this->getUrlAction()) */
                        ->setDispatched(false);
            }
        }
    }

}

?>