<?php

/**
 * Action Helper para limpar Strings
 * @since 24-11-2010
 * @author @acidcode
 */
class Zend_Controller_Action_Helper_LimpaString extends
Zend_Controller_Action_Helper_Abstract {

    /**
     * Limpa as Strings vindas com mascara dos formularios
     *
     * @param string $string
     * @return string 
     */
    function direct($string) {
        $caracteres = array(".", ",", "-", "/", "\\", ")", "(", " ");
        foreach ($caracteres as $caractere) {
            if (strpos($string, $caractere) !== false) {
                $string = str_replace($caractere, '', $string);
            }
        }
        return $string;
    }

}

