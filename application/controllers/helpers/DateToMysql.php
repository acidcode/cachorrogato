<?php

/**
 * Action Helper para traduzir datas para o mysql
 * @since 27-04-2011
 * @author @acidcode
 */
class Zend_Controller_Action_Helper_DateToMysql extends
Zend_Controller_Action_Helper_Abstract {

    /**
     * Data para mysql
     * @param String $date
     * @return string Data formato Mysql
     */
    function direct($date) {
        $date = explode("/", $date);
        return $date[2] . "-" . $date[1] . "-" . $date[0];
    }

    function dtMysql($date) {
        $date = explode("/", $date);
        return $date[2] . "-" . $date[1] . "-" . $date[0];
    }

}

