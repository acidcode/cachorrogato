<?php

/**
 * Action Helper para traduzir money para o mysql
 * @since 27-04-2011
 * @author @acidcode
 */
class Zend_Controller_Action_Helper_MoneyToMysql extends
Zend_Controller_Action_Helper_Abstract {

    /**
     * Data para mysql
     * @param String $date
     * @return string Money formato Mysql
     */
    function direct($money) {
        $money = str_replace(".", "", $money);
        $money = str_replace(",", ".", $money);

        return $money;
    }

}

