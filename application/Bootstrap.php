<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {
    /*
     * Carrega os modulos do sistema
     */

    protected function _initAutoload() {
        $modules = array("Backend" => "/modules/backend",
            "Frontend" => "/modules/frontend");
        foreach ($modules as $module => $path) {
            $autoloader[] = new Zend_Application_Module_Autoloader(
                    array('namespace' => $module,
                'basePath' => APPLICATION_PATH . $path));
        }
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->registerNamespace('Varien_');
    }

    protected function _initRewrite() {
        $front = Zend_Controller_Front::getInstance();
        $router = $front->getRouter();
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/routes.ini', 'production');
        $router->addConfig($config, 'routes');
    }

    /**
     * Classe Jquery UI e Jquery Core
     */
    protected function _initJqueryLoad() {
        $view = new Zend_View();
        $view->addHelperPath("ZendX/JQuery/View/Helper", "ZendX_JQuery_View_Helper");

        $view->jQuery()->enable()
                ->uiEnable();

        $function = '$ = jQuery.noConflict();';
        $view->jQuery()->addOnload($function);
    }

    /**
     * allow share helpers
     */
    protected function _initHelpers() {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->addHelperPath(
                APPLICATION_PATH . '/modules/backend/views/helpers/', 'Zend_View_Helper');
        $view->addHelperPath(LIB_PATH . '/Labofidea/View/Helper', 'Labofidea_View_Helper');
        $view->addHelperPath(LIB_PATH . '/Homebrasil/helpers', 'Homebrasil_helpers');
    }

    /**
     * Conexao com a base de dados
     */
    protected function _initConnection() {
        $options = $this->getOption('resources');
        $db_adapter = $options['db']['adapter'];

        $params = $options['db']['params'];
        //$params['driver_options'] = array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY=>true);

        try {
            $db = Zend_Db::factory($db_adapter, $params);
            $db->getConnection();
            $registry = Zend_Registry::getInstance();
            $registry->set('db', $db);
        } catch (Zend_Exception $e) {
            echo $e->getMessage();
        }
    }

}

