<?php

class Frontend_Model_DbTable_TbSmtp extends Zend_Db_Table_Abstract {

    protected $_name = 'config';

    public function getConfig() {
        $find = parent::find('1');
        if ($find) {
            $array = $find->toArray();
            return $array[0];
        } else {
            return false;
        }
    }

    public function getZendMail(array $config) {
        try {
            $decode = json_decode($config['config_data']);
            $novaConfig = array(
                'auth' => $decode->auth,
                'username' => $decode->username,
                'password' => $decode->password,
                'port' => $decode->port,
                'ssl' => $decode->ssl
            );

            return new Zend_Mail_Transport_Smtp($decode->host, $novaConfig);
        } catch (Exception $erro) {
            return '<strong>Erro ao carregar as configurações de e-mail </strong><br/>' . $erro->getMessage();
        }
    }

    public function getCc(array $config) {
        $decode = json_decode($config['config_data']);
        if (is_array($decode->cc)) {
            return explode(';', $decode->cc);
        } else {
            return array(0 => $decode->cc);
        }
    }

}

?>
