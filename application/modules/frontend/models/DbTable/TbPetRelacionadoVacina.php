<?php

/**
 * Classe de abstração da tabela tb_pet_relacionado_vacina
 * @author Matheus Marabesi @MatheusMarabesi
 * @since 2013-01-02
 * @package cachorrogato.cliente.db.tb_pet_relacionado_vacina
 * @license www.cachorrogato.com.br/sge
 */
class Frontend_Model_DbTable_TbPetRelacionadoVacina extends Zend_Db_Table_Abstract {

    protected $_name = 'tb_pet_relacionado_vacina';

    public function inserirRelacionamento($request) {
        foreach ($request['it_vacina'] as $vacina) {
            $data = array(
                'id_pet' => (int) $request['id_pet'],
                'id_vacina' => $vacina
            );
            parent::insert($data);
        }
    }

}

?>