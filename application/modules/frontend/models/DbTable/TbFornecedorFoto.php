<?php

/**
 * Classe de abstração da tabela tb_fornecedor_foto
 * @author Matheus Marabesi @MatheusMarabesi
 * @since 2013-01-09
 * @package cachorrogato.cliente.db.tb_fornecedor_foto
 * @license www.kernelpanic.com.br
 */
class Frontend_Model_DbTable_TbFornecedorFoto extends Zend_Db_Table_Abstract {

    protected $_name = 'tb_fornecedor_foto';

    /**
     * 
     * @param int $idFornecedor
     * @return int
     */
    public function getProximoIdFoto($idFornecedor) {
        $data = array(
            'id_fornecedor' => $idFornecedor,
            'bl_logotipo' => 'nao',
            'bl_foto_principal' => 'nao');
        $idFoto = parent::insert($data);

        return $idFoto;
    }

    /**
     * 
     * @param int $idFoto
     * @param stringn $nomeFoto
     * @return int
     */
    public function atualizarNomeFoto($idFoto, $nomeFoto) {
        $data = array(
            'vc_foto' => $nomeFoto
        );
        $where = array(
            'id_foto = ?' => $idFoto
        );

        return parent::update($data, $where);
    }

    /**
     * 
     * @param int $idFornecedor
     * @return array
     */
    public function findFotosByFornecedor($idFornecedor) {
        $where = array('id_fornecedor = ?' => $idFornecedor);
        $findFotosByFornecedor = parent::fetchAll($where, 'it_ordem');

        return $findFotosByFornecedor->toArray();
    }

    /**
     * 
     * @param type $idFornecedor
     * @return boolean
     */
    public function findFotoPrincipalByFornecedor($idFornecedor) {
        $where = array('id_fornecedor = ' . $idFornecedor . ' AND bl_foto_principal = "sim"');
        $data = parent::fetchAll($where, 'it_ordem');
        if ($data) {
            return $data->toArray();
        }

        return false;
    }

    /**
     * 
     * @param type $idFornecedor
     * @return boolean
     */
    public function findFotoLogotipoByFornecedor($idFornecedor) {
        $where = array('id_fornecedor = ' . $idFornecedor . ' AND bl_logotipo = "sim"');
        $data = parent::fetchAll($where, 'it_ordem');
        if ($data) {
            return $data->toArray();
        }

        return false;
    }

    /**
     * 
     * @param int $idFoto
     * @return int
     */
    public function deletarFotoById($idFoto) {
        $where = array('id_foto = ' . (int) $idFoto);

        return parent::delete($where);
    }

    /**
     * 
     * @param int $idFoto
     * @return array
     */
    public function findFotoById($idFoto) {
        $where = array('id_foto' => $idFoto);

        $findFotoById = parent::fetchRow($where);
        return $findFotoById->toArray();
    }

    /**
     * Esse método pode ser aprimorado para performance,
     * o mesmo deve ser alterado para buscar somente a foto que ja
     * está de logotipo (bl_logotipo = sim) e então atualiza-la para nao.
     * Após isso atualizar a nova foto para logotipo, e não atualizar todas as linhas
     * para nao antes de atualizar o logotipo (Como está agora)
     * 
     * @param array $request
     * @return int
     */
    public function salvarLogotipo($request) {
        $data = array('bl_logotipo' => 'nao');
        $where = array('id_fornecedor = ?' => $request['id_fornecedor']);
        parent::update($data, $where);

        $data = array('bl_logotipo' => 'sim');
        $where = array('id_foto = ?' => $request['id_foto']);
        return parent::update($data, $where);
    }

    /**
     * Esse método pode ser aprimorado para performance,
     * o mesmo deve ser alterado para buscar somente a foto que ja
     * está de logotipo (bl_logotipo = sim) e então atualiza-la para nao.
     * Após isso atualizar a nova foto para logotipo, e não atualizar todas as linhas
     * para nao antes de atualizar o logotipo (Como está agora)
     * 
     * @param array $request
     * @return int
     */
    public function salvarFotoPrincipal($request) {
        $data = array('bl_foto_principal' => 'nao');
        $where = array('id_fornecedor = ?' => $request['id_fornecedor']);
        parent::update($data, $where);

        $data = array('bl_foto_principal' => 'sim');
        $where = array('id_foto = ?' => $request['id_foto']);
        return parent::update($data, $where);
    }

    /**
     * Verificar e fazer o tratamento do retorno no método abaixo
     * @param array $request
     * @return boolean
     */
    public function atualizarOrdem($request) {
        $fotos = str_replace('foto', '', $request['fotos']);

        $ordem = 1;
        foreach ($fotos as $id) {
            $data = array('it_ordem' => $ordem);
            $where = array('id_foto = ?' => $id);

            parent::update($data, $where);
            $ordem++;
        }

        return true;
    }

}

?>
