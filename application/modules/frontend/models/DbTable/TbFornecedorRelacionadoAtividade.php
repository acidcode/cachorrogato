<?php

/**
 * Classe de abstração da tabela tb_fornecedor_relacionado_atividade
 * @author Matheus Marabesi @MatheusMarabesi
 * @since 2013-01-09
 * @package cachorrogato.cliente.db.tb_fornecedor_relacionado_atividade
 * @license www.kernelpanic.com.br
 */
class Frontend_Model_DbTable_TbFornecedorRelacionadoAtividade extends Zend_Db_Table_Abstract {

    protected $_name = 'tb_fornecedor_relacionado_atividade';

    public function inserirRelacionamento($request) {
        foreach ($request['vc_dias_atividade'] as $id_atividade) {
            $data = array(
                'id_fornecedor' => (int) $request['id_fornecedor'],
                'id_atividade' => (int) $id_atividade
            );

            parent::insert($data);
        }
    }

}

?>
