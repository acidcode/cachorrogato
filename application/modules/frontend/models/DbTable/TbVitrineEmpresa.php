<?php

class Frontend_Model_DbTable_TbVitrineEmpresa extends Zend_Db_Table_Abstract {

    protected $_name = 'tb_pedido_orcamento';

    public function armazenarPedido($request, $enviarEmail) {
        $data = array(
            'vc_nome' => $request['vc_nome'],
            'vc_sobrenome' => $request['vc_sobrenome'],
            'vc_email' => $request['vc_email'],
            'it_telefone' => Zend_Controller_Action_HelperBroker::getStaticHelper('LimpaString')->direct($request['it_telefone']),
            'vc_estado' => $request['vc_estado'],
            'vc_cidade' => $request['vc_cidade'],
            'vc_mensagem' => $request['vc_mensagem']
        );

        if ($enviarEmail) {
            $this->enviarEmailOrcamento($request);
        }

        return parent::insert($data);
    }

    private function enviarEmailOrcamento($request) {
        $tbSmtp = new Frontend_Model_DbTable_TbSmtp();
    }

}

?>
