<?php

/**
 * Classe de abstração da tabela tb_pet
 * @author Matheus Marabesi @MatheusMarabesi
 * @since 2013-01-02
 * @package cachorrogato.cliente.db.tb_pet
 * @license www.cachorrogato.com.br/sge
 */
class Frontend_Model_DbTable_TbPet extends Zend_Db_Table_Abstract {

    protected $_name = 'tb_pet';

    public function inserirPet($request, $idCliente) {
        $data = array(
            'vc_nome_pet' => $request['vc_nome_pet'],
            'it_idade_aproximada' => $request['it_idade_aproximada'],
            'dt_nascimento' => Zend_Controller_Action_HelperBroker::getStaticHelper('DateToMysql')->direct($request['dt_nascimento']),
            'bl_especie' => $request['bl_especie'],
            'bl_sexo' => $request['bl_sexo'],
            'bl_castrado' => $request['bl_castrado'],
            'id_raca' => $request['id_raca'],
            'id_porte' => $request['id_porte'],
            'id_peso' => $request['id_peso'],
            'id_cor' => $request['id_cor'],
            'id_pelagem' => $request['id_pelagem'],
            'it_ultima_pesagem' => $request['it_ultima_pesagem'],
            'bl_cruzar' => $request['bl_cruzar'],
        );

        $idPet = parent::insert($data);

        $tbClientePet = new Frontend_Model_DbTable_TbClientePet();
        $tbPetRelacionadoVacina = new Frontend_Model_DbTable_TbPetRelacionadoVacina();
        $tbPetRelacionadoAtividade = new Frontend_Model_DbTable_TbPetRelacionadoAtividade();

        $tbClientePet->inserirRelacionamento(
                array(
                    'id_cliente' => $idCliente,
                    'id_pet' => $idPet
        ));
        /*
          $tbPetRelacionadoVacina->inserirRelacionamento(
          array(
          'it_vacina' => $request['it_vacina'],
          'id_pet' => $idPet
          ));

          $tbPetRelacionadoAtividade->inserirRelacionamento(
          array(
          'it_atividade' => $request['it_atividade'],
          'id_pet' => $idPet
          )); */

        return $idPet;
    }

    public function findAllRaca() {
        /*
         * Ao finalizar o cadastro de raça
         * refatorar esse trecho.
         * @MatheusMarabesi 25-04-25
         */
        /*$db = Zend_Registry::get('db');
        $raca = $db->select()
                ->from('tb_pet_raca')
                ->query()
                ->fetchAll();*/
        return $raca = array();
    }

    public function findAllPorte() {
        $db = Zend_Registry::get('db');
        $porte = $db->select()
                ->from('tb_pet_porte')
                ->query()
                ->fetchAll();
        return $porte;
    }

    public function findAllPeso() {
        $db = Zend_Registry::get('db');
        $peso = $db->select()
                ->from('tb_pet_peso')
                ->query()
                ->fetchAll();
        return $peso;
    }

    public function findAllCor() {
        $db = Zend_Registry::get('db');
        $cor = $db->select()
                ->from('tb_pet_cor')
                ->query()
                ->fetchAll();
        return $cor;
    }

    public function findAllPelagem() {
        $db = Zend_Registry::get('db');
        $pelagem = $db->select()
                ->from('tb_pet_pelagem')
                ->query()
                ->fetchAll();
        return $pelagem;
    }

    public function findAllVacina() {
        $db = Zend_Registry::get('db');
        $vacina = $db->select()
                ->from('tb_pet_vacina')
                ->query()
                ->fetchAll();
        return $vacina;
    }

    public function findAllAtividade() {
        $db = Zend_Registry::get('db');
        $atividade = $db->select()
                ->from('tb_pet_atividade')
                ->query()
                ->fetchAll();
        return $atividade;
    }

}

?>
