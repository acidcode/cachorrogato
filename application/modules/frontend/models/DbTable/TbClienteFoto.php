<?php

/**
 * Classe de abstração da tabela tb_cliente_foto
 * @author Matheus Marabesi @MatheusMarabesi
 * @since 2012-12-28
 * @refatorado Matheus Marabesi 2013-01-24
 * @package cachorrogato.cliente.db.tb_cliente_foto
 * @license www.cachorrogato.com.br/sge
 */
class Frontend_Model_DbTable_TbClienteFoto extends Zend_Db_Table_Abstract {

    protected $_name = 'tb_cliente_foto';

    public function init() {
        require_once(LIB_PATH . '/Canvas/canvas.php');
    }

    /**
     * 
     * @param int $idCliente
     * @return int
     */
    public function getProximoIdFoto($idCliente) {
        $idFoto = parent::insert(array('id_cliente' => $idCliente));
        return $idFoto;
    }

    /**
     * 
     * @param int $idCliente
     * @return array
     */
    public function findFotosByCliente($idCliente) {
        $where = array('id_cliente = ?' => $idCliente);
        $fotos = parent::fetchAll($where);

        return $fotos->toArray();
    }

    /**
     * 
     * @param type $idFoto
     * @return type
     */
    public function findFotoById($idFoto) {
        $where = array('id_cliente_foto' => $idFoto);
        $findFotoById = parent::fetchRow($where);
        return $findFotoById->toArray();
    }

    /**
     * 
     * @param int $idFoto
     * @return int
     */
    public function deletarFotoById($idFoto) {
        return parent::delete('id_cliente_foto = ' . (int) $idFoto);
    }

    /**
     * 
     * @param int $idFoto
     * @param string $nomeFoto
     * @return int
     */
    public function atualizarNomeFoto($idFoto, $nomeFoto) {
        $data = array(
            'vc_cliente_foto' => $nomeFoto
        );
        $where = array(
            'id_cliente_foto = ?' => $idFoto
        );

        return parent::update($data, $where);
    }

}

?>
