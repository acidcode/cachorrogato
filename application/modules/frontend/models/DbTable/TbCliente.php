<?php

/**
 * Classe de abstração da tabela tb_cliente
 * @author Matheus Marabesi @MatheusMarabesi
 * @since 2013-04-01
 * @package cachorrogato.frontend.db.tb_cliente
 * @license www.cachorrogato.com.br/sge
 */
class Frontend_Model_DbTable_TbCliente extends Zend_Db_Table_Abstract {

    protected $_name = 'tb_cliente';
    private $_tbClienteFoto;

    public function init() {
        $this->_tbClienteFoto = new Frontend_Model_DbTable_TbClienteFoto();
    }

    /**
     * 
     * @param array $request 
     * @return int  
     */
    public function adicionarCliente($request) {
        $data = array(
            'vc_nome' => $request['vc_nome'],
            'vc_sobrenome' => $request['vc_sobrenome'],
            'vc_nome_meio' => $request['vc_nome_meio'],
            'bl_sexo' => $request['bl_sexo'],
            'dt_nascimento' => $request['dt_nascimento']['ano'] . '-' . $request['dt_nascimento']['mes'] . '-' . $request['dt_nascimento']['dia'],
            'it_cep' => Zend_Controller_Action_HelperBroker::getStaticHelper('LimpaString')->direct($request['it_cep']),
            'vc_endereco' => $request['vc_endereco'],
            'vc_numero' => $request['vc_numero'],
            'vc_complemento' => $request['vc_complemento'],
            'vc_bairro' => $request['vc_bairro'],
            'vc_cidade' => $request['vc_cidade'],
            'vc_estado' => $request['vc_estado'],
            'vc_email' => $request['vc_email'],
            'vc_senha' => md5($request['vc_senha'])
        );

        if (!$request['bl_receber_convites']) {
            $data['bl_receber_convites'] = 'nao';
        } else {
            $data['bl_receber_convites'] = 'sim';
        }

        if (!$request['bl_receber_alertas']) {
            $data['bl_receber_alertas'] = 'nao';
        } else {
            $data['bl_receber_alertas'] = 'sim';
        }

        $data['bl_ativo'] = 'nao';

        if (empty($request['id_cliente'])) {

            $data['dt_cadastro'] = date('Y-m-d');
            $pk = $this->insert($data);
        } else {
            $pk = $request['id_cliente'];
            $where = "id_cliente = " . $request['id_cliente'];
            $this->update($data, $where);
        }

        return $pk;
    }

    /**
     * 
     * @param array $request 
     * @param int $idCliente 
     * @return boolean
     */
    public function atualizarComplemento($request, $idCliente) {
        $data = array(
            'vc_cpf' => Zend_Controller_Action_HelperBroker::getStaticHelper('LimpaString')->direct($request['vc_cpf']),
            'vc_telefone' => Zend_Controller_Action_HelperBroker::getStaticHelper('LimpaString')->direct($request['vc_telefone']),
            'vc_celular' => Zend_Controller_Action_HelperBroker::getStaticHelper('LimpaString')->direct($request['vc_celular'])
        );

        $where = array('id_cliente = ?' => $idCliente);

        if (parent::update($data, $where)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 
     * @param string $email 
     * @return array
     */
    public function findByEmail($email) {
        $db = Zend_Registry::get('db');
        $findByEmail = $db->select()
                ->from('tb_cliente', array('vc_email', 'vc_nome', 'vc_sobrenome', 'md5(id_cliente) AS id_cliente'))
                ->where('vc_email = ?', $email)
                ->query()
                ->fetch();
        return $findByEmail;
    }

    /**
     * 
     * @param string $idMd5
     * @return boolean
     */
    public function findByIdMd5($idMd5) {
        $db = Zend_Registry::get('db');
        $findByIdMd5 = $db->select()
                ->from('tb_cliente', array('vc_email', 'vc_nome', 'vc_sobrenome', 'md5(id_cliente) AS id_cliente'))
                ->where('md5(id_cliente) = ?', $idMd5)
                ->query()
                ->fetch();
        return $findByIdMd5;
    }

    /**
     * 
     * @param string $mensagem
     * @param string $enderecoDeEmail
     * @return boolean
     */
    public function enviarEmailRecuperarSenha($mensagem, $enderecoDeEmail) {
        $email = new Zend_Mail('utf-8');
        $email->setBodyHtml($mensagem);
        $email->addTo($enderecoDeEmail);
        $email->setSubject('Recuperação de senha - Cachorro Gato');

        if ($email->send()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 
     * @param string $mensagem
     * @param string $enderecoDeEmail
     * @return boolean
     */
    public function enviarEmailConfirmacaoCadastro($mensagem, $enderecoDeEmail) {
        $smtp = new Frontend_Model_DbTable_TbSmtp();
        $config = $smtp->getConfig();

        if (is_array($config)) {
            $email = new Zend_Mail('utf-8');
            $email->setFrom($config['username']);

            foreach ($smtp->getCc($config) as $emailCc) {
                $email->addCc($emailCc);
            }

            $email->addTo($enderecoDeEmail);
            $email->setBodyHtml($mensagem);
            //$email->addTo($enderecoDeEmail);
            $email->setSubject('Confirmação de Cadastro - Cachorro Gato');

            if ($smtp->getZendMail($config) instanceof Zend_Mail_Transport_Smtp) {
                $email->send($smtp->getZendMail($config));
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 
     * @param array $request
     * @return boolean
     */
    public function atualizarSenha($request) {
        $data = array(
            'vc_senha' => md5($request['vc_senha'])
        );

        $where = array(
            'md5(id_cliente)' => $request['id_cliente']
        );

        return parent::update($data, $where);
    }

    /**
     * 
     * @param string $cep
     * @example findAddressFromCep('99999-999') Retorna os dados do endereço apartir do cep
     * @return array
     */
    public function findAddressFromCep($cep) {
        $db = Zend_Registry::get('db');
        $busca = $db->select()
                ->from(array('e' => 'tb_endereco'), array('e.vc_uf', 'e.vc_rua', 'e.vc_logradouro', 'b.vc_bairro', 'c.vc_cidade'))
                ->joinLeft(array('b' => 'tb_bairro'), 'e.id_bairro = b.id_bairro', array())
                ->joinLeft(array('c' => 'tb_cidade'), 'b.id_cidade = c.id_cidade', array())
                ->joinLeft(array('u' => 'tb_uf'), 'c.id_uf = u.id_uf', array())
                ->where("e.it_cep = '$cep'")
                ->query()
                ->fetch();

        $endereco = array('endereco' => $busca['vc_logradouro'] . ' ' . $busca['vc_rua'],
            'bairro' => $busca['vc_bairro'],
            'cidade' => $busca['vc_cidade'],
            'estado' => $busca['vc_uf']);

        return $endereco;
    }

    /**
     * 
     * @param array $request
     * @example acesso(array('vc_email' => 'someemail@ domain.com.br', 'vc_senha' => '123456')) description
     * @return array
     */
    public function acesso($request) {
        $where = array(
            'vc_email = ?' => $request['vc_email'],
            'vc_senha = ?' => md5($request['vc_senha'])
        );

        return $this->fetchRow($where);
    }

    /**
     * 
     * @param string $idMd5
     * @return boolean
     */
    public function confirmarCadastro($idMd5) {
        $data = array(
            'bl_ativo' => 'sim'
        );

        $where = array(
            'md5(id_cliente)' => $idMd5
        );

        return parent::update($data, $where);
    }

    /**
     * 
     * @param int $idCliente
     * @return array 
     */
    public function findBackoffice($idCliente) {
        $db = Zend_Registry::get('db');

        $cliente = $db->select()
                ->from(array('c' => 'tb_cliente'))
                ->where("c.id_cliente = $idCliente")
                ->query()
                ->fetch();

        return $cliente;
    }

    /**
     * 
     * @param string $where
     * @return array 
     */
    public function findAllBackoffice($where) {
        $db = Zend_Registry::get('db');

        $campos = array(
            'id_cliente',
            'vc_nome',
            'vc_sobrenome',
            'vc_email',
            'dt_cadastro',
            'bl_ativo'
        );

        $clientes = $db->select()
                ->from(array('c' => 'tb_cliente'), $campos);


        if (count($where) > 0 && !empty($where)) {
            $clientes = $clientes->where($where);
        }

        $clientes = $clientes->query()
                ->fetchAll();

        return $clientes;
    }

    /**
     * 
     * @param array $request
     * @return string 
     */
    public function filterBackoffice($request) {
        $where = array();

        if ($request['id_cliente'] != 0 ||
                !empty($request['id_cliente'])) {
            $where[] = 'id_cliente = ' .
                    Zend_Filter::filterStatic(
                            $request['id_cliente'], 'Digits'
            );
        }

        if (!empty($request['vc_nome'])) {
            $where[] = 'vc_nome LIKE "%' .
                    trim($request['vc_nome']) . '%"';
        }

        return implode('AND', $where);
    }

    /**
     * Deletar a partir de chave primária
     * @param type $pk 
     */
    public function deletarBackoffice($idCliente) {
        parent::delete('id_cliente = ' . $idCliente);
    }

    /**
     * 
     * @param array $request
     * @return int
     */
    public function adicionarClienteBackoffice($request) {

        $data = array(
            'vc_nome' => $request['vc_nome'],
            'vc_nome_meio' => $request['vc_nome_meio'],
            'vc_sobrenome' => $request['vc_sobrenome'],
            'bl_sexo' => $request['bl_sexo'],
            'dt_nascimento' => Zend_Controller_Action_HelperBroker::getStaticHelper('DateToMysql')->direct($request['dt_nascimento']),
            'it_cep' => Zend_Controller_Action_HelperBroker::getStaticHelper('LimpaString')->direct($request['it_cep']),
            'vc_endereco' => $request['vc_endereco'],
            'vc_numero' => $request['vc_numero'],
            'vc_complemento' => $request['vc_complemento'],
            'vc_bairro' => $request['vc_bairro'],
            'vc_cidade' => $request['vc_cidade'],
            'vc_estado' => $request['vc_estado'],
            'vc_email' => $request['vc_email'],
            'vc_senha' => md5($request['vc_senha']),
            'vc_cpf' => Zend_Controller_Action_HelperBroker::getStaticHelper('LimpaString')->direct($request['vc_cpf']),
            'vc_telefone' => Zend_Controller_Action_HelperBroker::getStaticHelper('LimpaString')->direct($request['vc_telefone']),
            'vc_celular' => Zend_Controller_Action_HelperBroker::getStaticHelper('LimpaString')->direct($request['vc_celular']),
            'bl_receber_convites' => $request['bl_receber_convites'],
            'bl_receber_alertas' => $request['bl_receber_alertas']
        );

        $data['dt_cadastro'] = 'NULL';
        $data['bl_ativo'] = 'nao';

        if (empty($request['id_cliente'])) {

            $data['dt_cadastro'] = date('Y-m-d');
            $pk = $this->insert($data);
        } else {
            $pk = $request['id_cliente'];
            $where = "id_cliente = " . $request['id_cliente'];
            $this->update($data, $where);

            if ($request['deletar_cliente_foto'] == 'sim') {
                foreach ($this->_tbClienteFoto->findFotosByCliente($pk) as $array) {
                    unlink(PUBLIC_PATH . '/uploads/modules/cliente/fotos/' . $array['vc_cliente_foto']);
                    $this->_tbClienteFoto->deletarFotoById($array['id_cliente_foto']);
                }
            }
        }
        $this->_tbClienteFoto->inserirFotoClienteBackoffice($pk);

        return $pk;
    }

}

?>
