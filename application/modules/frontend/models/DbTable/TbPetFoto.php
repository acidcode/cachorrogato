<?php

/**
 * Classe de abstração da tabela tb_pet_foto
 * @author Matheus Marabesi @MatheusMarabesi
 * @since 2012-12-28
 * @package cachorrogato.cliente.db.tb_pet_foto
 * @license www.cachorrogato.com.br/sge
 */
class Frontend_Model_DbTable_TbPetFoto extends Zend_Db_Table_Abstract {

    protected $_name = 'tb_pet_foto';

    public function adicionarFoto($idPet, $request) {

        if ($request['vc_pet_foto']['error'] == 0) {
            $adapter = new Zend_File_Transfer_Adapter_Http();
            $adapter->setDestination('uploads/modules/cliente/pet');

            $extensaoFoto = end(explode('.', $request['vc_pet_foto']['name']));
            $nomeAntigo = $request['vc_pet_foto']['name'];
            $nomeNovo = Zend_Controller_Action_HelperBroker::getStaticHelper('LimpaString')->direct($request['vc_pet_foto']['name']) . '.' . $extensaoFoto;

            if ($adapter->receive()) {
                rename($adapter->getDestination() . '/' . $nomeAntigo, $adapter->getDestination() . '/' . $nomeNovo);
            }

            $data = array(
                'vc_pet_foto' => $nomeNovo,
                'id_pet' => $idPet
            );

            parent::insert($data);
        }
    }

    public function findFotoByPet($id_pet) {
        return parent::fetchAll('id_pet = ' . $id_pet);
    }

    public function deletarFotoById($id_pet_foto) {
        return parent::delete('id_pet_foto = ' . $id_pet_foto);
    }

}

?>
