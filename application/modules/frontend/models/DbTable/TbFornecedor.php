<?php

/**
 * Classe de abstração da tabela tb_fornecedor
 * @author Matheus Marabesi @MatheusMarabesi
 * @since 2013-01-09
 * @package cachorrogato.cliente.db.tb_fornecedor
 * @license www.kernelpanic.com.br
 */
class Frontend_Model_DbTable_TbFornecedor extends Zend_Db_Table_Abstract {

    protected $_name = 'tb_fornecedor';

    /**
     * 
     * @param array $request
     * @return array
     */
    public function cadastrarFornecedor($request) {
        $data = array(
            'vc_nome_empresa' => $request['vc_nome_empresa'],
            'vc_acesso_email' => $request['vc_acesso_email'],
            'vc_acesso_senha' => md5($request['vc_acesso_senha']),
            'vc_descricao_empresa' => $request['vc_descricao_empresa'],
            'it_cep' => Zend_Controller_Action_HelperBroker::getStaticHelper('LimpaString')->direct($request['it_cep']),
            'vc_estado' => $request['vc_estado'],
            'vc_cidade' => $request['vc_cidade'],
            'vc_endereco' => $request['vc_endereco'],
            'vc_numero' => $request['vc_numero'],
            'vc_complemento' => $request['vc_complemento'],
            'vc_pessoa_contato' => $request['vc_pessoa_contato'],
            'vc_nome_proprietario' => $request['vc_nome_proprietario'],
            'bl_dono' => ($request['bl_dono']) ? 'sim' : 'nao',
            'vc_contato_email' => $request['vc_contato_email'],
            'it_telefone' => Zend_Controller_Action_HelperBroker::getStaticHelper('LimpaString')->direct($request['it_telefone']),
            'it_celular' => Zend_Controller_Action_HelperBroker::getStaticHelper('LimpaString')->direct($request['it_celular']),
            'vc_email_contato_empresa' => $request['vc_email_contato_empresa'],
            'vc_pagina_web' => $request['vc_pagina_web'],
            'vc_dias_atividade' => $request['vc_dias_atividade'],
            'tm_periodo_de_atividade_inicio' => $request['tm_periodo_de_atividade_inicio'],
            'tm_periodo_de_atividade_termino' => $request['tm_periodo_de_atividade_termino'],
            'bl_periodo_de_atividade' => $request['bl_periodo_de_atividade'],
            'id_setor_de_atividade' => $request['id_setor_de_atividade'],
            'bl_ativo' => 'nao'
        );

        $idFornecedor = parent::insert($data);

        if ($idFornecedor) {
            $relacionamentoAtividade = array(
                'id_fornecedor' => $idFornecedor,
                'vc_dias_atividade' => $request['vc_dias_atividade']
            );
            $tbFornecedorAtividadeRelacionadoAtividade = new Frontend_Model_DbTable_TbFornecedorRelacionadoAtividade();
            $tbFornecedorAtividadeRelacionadoAtividade->inserirRelacionamento($relacionamentoAtividade);
        }

        $retorno = array(
            'id_fornecedor' => $idFornecedor,
            'id_setor_de_atividade' => $request['id_setor_de_atividade']
        );

        return $retorno;
    }

    /**
     * 
     * @param void
     * @return array
     */
    public function findAtividade() {
        $db = Zend_Registry::get('db');
        $atividade = $db->select()
                ->from('tb_fornecedor_atividade')
                ->query()
                ->fetchAll();
        return $atividade;
    }

    /**
     * 
     * @param void
     * @return array
     */
    public function findSetorAtividade() {
        $bd = Zend_Registry::get('db');
        $setorAtividade = $bd->select()
                ->from('tb_fornecedor_setor_atividade')
                ->query()
                ->fetchAll();
        return $setorAtividade;
    }

    /**
     * 
     * @param string $mensagem
     * @param string $enderecoDeEmail
     * @return boolean
     */
    public function enviarEmailConfirmacaoCadastro($mensagem, $enderecoDeEmail) {
        $smtp = new Frontend_Model_DbTable_TbSmtp();
        $config = $smtp->getConfig();

        if (is_array($config)) {
            $email = new Zend_Mail('utf-8');
            $email->setFrom($config['username']);

            foreach ($smtp->getCc($config) as $emailCc) {
                $email->addCc($emailCc);
            }

            $email->addTo($enderecoDeEmail);
            $email->setBodyHtml($mensagem);
            //$email->addTo($enderecoDeEmail);
            $email->setSubject('Confirmação de Cadastro - Cachorro Gato');

            if ($smtp->getZendMail($config) instanceof Zend_Mail_Transport_Smtp) {
                $email->send($smtp->getZendMail($config));
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 
     * @param string $mensagem
     * @param string $enderecoDeEmail
     * @return boolean
     */
    public function enviarEmailRecuperarSenha($mensagem, $enderecoDeEmail) {
        $email = new Zend_Mail('utf-8');
        $email->setBodyHtml($mensagem);
        $email->addTo($enderecoDeEmail);
        $email->setSubject('Recuperação de senha - Cachorro Gato');

        if ($email->send()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 
     * @param int $idFornecedor
     * @return array
     */
    public function findFornecedorById($idFornecedor) {
        $db = Zend_Registry::get('db');
        $findFornecedorById = $db->select()
                ->from('tb_fornecedor', array('md5(id_fornecedor) AS id_fornecedor', 'vc_nome_empresa', 'vc_acesso_email'))
                ->where('id_fornecedor = ?', $idFornecedor)
                ->query()
                ->fetch();
        return $findFornecedorById;
    }

    /**
     * 
     * @param string $idMd5
     * @return boolean
     */
    public function findByIdMd5($idMd5) {
        $db = Zend_Registry::get('db');
        $findByIdMd5 = $db->select()
                ->from('tb_fornecedor', array('vc_acesso_email', 'vc_nome_empresa'))
                ->where('md5(id_fornecedor) = ?', $idMd5)
                ->query()
                ->fetch();
        return $findByIdMd5;
    }

    /**
     * 
     * @param string $idMd5
     * @return boolean
     */
    public function confirmarCadastro($idMd5) {
        $data = array(
            'bl_ativo' => 'sim'
        );

        $where = array(
            'md5(id_fornecedor)' => $idMd5
        );

        return parent::update($data, $where);
    }

    /**
     * 
     * @param string $email 
     * @return array
     */
    public function findByEmail($email) {
        $db = Zend_Registry::get('db');
        $findByEmail = $db->select()
                ->from('tb_fornecedor', array('vc_acesso_email', 'vc_nome_empresa', 'md5(id_fornecedor) AS id_fornecedor'))
                ->where('vc_acesso_email = ?', $email)
                ->query()
                ->fetch();
        return $findByEmail;
    }

    /**
     * 
     * @param array $request
     * @return array
     */
    public function acesso($request) {
        $where = array(
            'vc_acesso_email = ?' => $request['vc_acesso_email'],
            'vc_acesso_senha = ?' => md5($request['vc_acesso_senha'])
        );

        return $this->fetchRow($where);
    }

    /**
     * 
     * Encontra pela chave primaria
     * @param int $pk
     * @return array 
     */
    public function find($idFornecedor) {
        $db = Zend_Registry::get('db');

        $fornecedor = $db->select()
                ->from(array('f' => 'tb_fornecedor'))
                ->where("f.id_fornecedor = $idFornecedor")
                ->query()
                ->fetch();

        return $fornecedor;
    }

    /**
     * 
     * @param array $request
     * @return boolean
     */
    public function atualizarSenha($request) {
        $data = array(
            'vc_acesso_senha' => md5($request['vc_acesso_senha'])
        );

        $where = array(
            'md5(id_fornecedor)' => $request['id_fornecedor']
        );

        return parent::update($data, $where);
    }

    /**
     * 
     * @param string $cep
     * @example findAddressFromCep('99999-999') Retorna os dados do endereço apartir do cep
     * @return array
     */
    public function findAddressFromCep($cep) {
        $db = Zend_Registry::get('db');
        $busca = $db->select()
                ->from(array('e' => 'tb_endereco'), array('e.vc_uf', 'e.vc_rua', 'e.vc_logradouro', 'c.vc_cidade'))
                ->joinLeft(array('b' => 'tb_bairro'), 'e.id_bairro = b.id_bairro', array())
                ->joinLeft(array('c' => 'tb_cidade'), 'b.id_cidade = c.id_cidade', array())
                ->joinLeft(array('u' => 'tb_uf'), 'c.id_uf = u.id_uf', array())
                ->where("e.it_cep = '$cep'")
                ->query()
                ->fetch();

        $endereco = array('endereco' => $busca['vc_logradouro'] . ' ' . $busca['vc_rua'],
            'cidade' => $busca['vc_cidade'],
            'estado' => $busca['vc_uf']);

        return $endereco;
    }

    /**
     * Deletar a partir de chave primária
     * @param type $pk 
     */
    public function deletar($pk) {
        parent::delete('id_fornecedor = ' . $pk);
    }

    public function fetchAll($id_fornecedor = null) {
        $tbFornecedorFoto = new Frontend_Model_DbTable_TbFornecedorFoto();
        $data = array();

        if ($id_fornecedor) {
            $fetchAll = parent::fetchAll(array('id_fornecedor = ?' => $id_fornecedor));
        } else {
            $fetchAll = parent::fetchAll();
        }

        foreach ($fetchAll as $array) {
            $data[] = array(
                'id_fornecedor' => $array['id_fornecedor'],
                'vc_nome_empresa' => $array['vc_nome_empresa'],
                'vc_acesso_email' => $array['vc_acesso_email'],
                'vc_acesso_senha' => $array['vc_acesso_senha'],
                'vc_descricao_empresa' => $array['vc_descricao_empresa'],
                'it_cep' => $array['it_cep'],
                'vc_estado' => $array['vc_estado'],
                'vc_cidade' => $array['vc_cidade'],
                'vc_endereco' => $array['vc_endereco'],
                'vc_numero' => $array['vc_numero'],
                'vc_complemento' => $array['vc_complemento'],
                'vc_pessoa_contato' => $array['vc_pessoa_contato'],
                'vc_nome_proprietario' => $array['vc_nome_proprietario'],
                'bl_dono' => $array['bl_dono'],
                'vc_contato_email' => $array['vc_contato_email'],
                'it_telefone' => $array['it_telefone'],
                'it_celular' => $array['it_celular'],
                'vc_email_contato_empresa' => $array['vc_email_contato_empresa'],
                'vc_pagina_web' => $array['vc_pagina_web'],
                'vc_dias_atividade' => $array['vc_dias_atividade'],
                'tm_periodo_de_atividade_inicio' => $array['tm_periodo_de_atividade_inicio'],
                'tm_periodo_de_atividade_termino' => $array['tm_periodo_de_atividade_termino'],
                'bl_periodo_de_atividade' => $array['bl_periodo_de_atividade'],
                'id_setor_de_atividade' => $array['id_setor_de_atividade'],
                'bl_ativo' => $array['bl_ativo'],
                'fotoPrincipal' => $tbFornecedorFoto->findFotoPrincipalByFornecedor($array['id_fornecedor']),
                'fotoLogotipo' => $tbFornecedorFoto->findFotoLogotipoByFornecedor($array['id_fornecedor'])
            );
        }

        return $data;
    }

}

?>
