<?php

/**
 * Classe de abstração da tabela tb_fornecedor_resposta
 * @author Matheus Marabesi @MatheusMarabesi
 * @since 2013-01-14
 * @package cachorrogato.cliente.db.tb_fornecedor_resposta
 * @license www.kernelpanic.com.br
 */
class Frontend_Model_DbTable_TbFornecedorResposta extends Zend_Db_Table_Abstract {

    protected $_name = 'tb_fornecedor_resposta';
    private $_tbFornecedorRelacionadoQuestionario;

    public function init() {
        $this->_tbFornecedorRelacionadoQuestionario = new Frontend_Model_DbTable_TbFornecedorRelacionadoQuestionario();
    }

    /**
     * 
     * @param array $request
     */
    public function inserirResposta($request, $idFornecedor) {
        foreach ($request['campo'] as $id_questionario => $array) {
            foreach ($array as $id_setor_de_atividade => $resposta) {
                if (is_array($resposta)) {
                    foreach ($resposta as $escolha) {
                        $data = array(
                            'id_setor_atividade' => $id_setor_de_atividade,
                            'id_questionario' => $id_questionario,
                            'vc_resposta' => $escolha,
                            'id_fornecedor' => $idFornecedor
                        );
                        parent::insert($data);
                    }
                } else {
                    $data = array(
                        'id_setor_atividade' => $id_setor_de_atividade,
                        'id_questionario' => $id_questionario,
                        'vc_resposta' => $resposta,
                        'id_fornecedor' => $idFornecedor
                    );
                    parent::insert($data);
                }
            }
        }
    }

}

?>
