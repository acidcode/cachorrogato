<?php

/**
 * Classe de abstração da tabela tb_fornecedor_questionario
 * @author Matheus Marabesi @MatheusMarabesi
 * @since 2013-01-11
 * @package cachorrogato.cliente.db.tb_fornecedor_questionario
 * @license www.kernelpanic.com.br
 */
class Frontend_Model_DbTable_TbFornecedorQuestionario extends Zend_Db_Table_Abstract {

    protected $_name = 'tb_fornecedor_questionario';

    public function findQuestionarioBySetorAtividade($idSetorAtividade) {
        $db = Zend_Registry::get('db');
        $questionario = $db->select()
                ->from(array('tfq' => 'tb_fornecedor_questionario'))
                ->join(array('tfqc' => 'tb_fornecedor_questionario_campo'), 'tfq.id_tipo_campo = tfqc.id_tipo_campo', array('tfq.id_tipo_campo', 'tfqc.vc_campo'))
                ->where('tfq.id_setor_de_atividade = ' . (int) $idSetorAtividade)
                ->query()
                ->fetchAll();
        return $questionario;
    }

    public function findCampoOpcoes($idCampo, $idQuestionario) {
        $db = Zend_Registry::get('db');
        $opcoes = $db->select()
                ->from('tb_fornecedor_questionario_opcao')
                ->where('id_tipo_campo = ' . (int) $idCampo . ' AND id_questionario = ' . (int) $idQuestionario)
                ->query()
                ->fetchAll();
        return $opcoes;
    }

}

?>
