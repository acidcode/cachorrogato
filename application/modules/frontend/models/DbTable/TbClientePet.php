<?php

/**
 * Classe de abstração da tabela tb_cliente_pet
 * @author Matheus Marabesi @MatheusMarabesi
 * @since 2013-01-02
 * @package cachorrogato.cliente.db.tb_cliente_pet
 * @license www.cachorrogato.com.br/sge
 */
class Frontend_Model_DbTable_TbClientePet extends Zend_Db_Table_Abstract {

    protected $_name = 'tb_cliente_pet';

    public function inserirRelacionamento($request) {
        $data = array(
            'id_cliente' => (int) $request['id_cliente'],
            'id_pet' => (int) $request['id_pet']
        );

        parent::insert($data);
    }

    public function findPetByCliente($id_cliente) {
        
    }

}

?>