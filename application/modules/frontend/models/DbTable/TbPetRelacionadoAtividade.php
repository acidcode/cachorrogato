<?php

/**
 * Classe de abstração da tabela tb_pet_relacionado_atividade
 * @author Matheus Marabesi @MatheusMarabesi
 * @since 2013-01-02
 * @package cachorrogato.cliente.db.tb_pet_relacionado_atividade
 * @license www.cachorrogato.com.br/sge
 */
class Frontend_Model_DbTable_TbPetRelacionadoAtividade extends Zend_Db_Table_Abstract {

    protected $_name = 'tb_pet_relacionado_atividade';

    public function inserirRelacionamento($request) {
        foreach ($request['it_atividade'] as $vacina) {
            $data = array(
                'id_pet' => (int) $request['id_pet'],
                'id_atividade' => $vacina
            );
            parent::insert($data);
        }
    }

}

?>