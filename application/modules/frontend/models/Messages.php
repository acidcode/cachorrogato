<?php
class Frontend_Model_Messages
{
    protected  $_Transport;
    
    protected $_Auth;
    
    public function __construct(){
    	
        $config=Zend_Controller_Front::getInstance()
        ->getParam('bootstrap')
        ->getOptions();
        $Auth = $config ['Account'];
        $this->_Auth = $Auth;
      
        $Settings = array('auth' => 'login',
                          'ssl'=>'ssl',
                          'port'=>'465',
        		          'username' => $Auth['user'],
        		          'password' => $Auth['pass']);
        
        $this->_Transport = new Zend_Mail_Transport_Smtp( $Auth['host'],$Settings);

    }
    
    public function _sendMail($request, $to){
        
        $mail = new Zend_Mail('utf-8');
        $mail->setBodyHtml($request['Msg']);
        $mail->setFrom($request["email"], $request['nome']);
        $mail->addTo( $this->_Auth['user'],'');
        $mail->setSubject($request['subject']);
        return $mail->send( $this->_Transport);
        
    }
    
    
    public function sendContact($request){
    
    	if($request['nome']!='' && $request['email']!='' && $request['mensagem']!='') {
    
    
    		$request['Msg'] =  'Nome:' . $request['nome'] . '<br>' ;
    		$request['Msg'] .= 'Email:' . $request['email'] . "<br>" ;
    		$request['Msg'] .= 'Cel:' . $request['celular']  . "<br>" ;
    		$request['Msg'] .= 'Telefone: '. $request['telefone'] . '<br>' ;
    		$request['Msg'] .= 'Mensagem: '.$request['mensagem'].'<br>';
    		$request['subject'] = 'Contato :'. $request['email'];
    		$to['name'] = 'felipe';
    		$to['email'] = 'felipe@labofidea.com';
    		 
    		 
    		if($this->_sendMail($request , $to)){
    
    			$data['Message'] ='Enviado com sucesso';
    		}else{
    
    			$data['Message'] = 'Erro ao enviar a mensagem!';
    
    		}
    
    	}else{
    
    		$data['Message'] = 'Nome , email  e a mensagem são campos obrigatórios!';
    		 
    	}
    
    	echo Zend_Json_Encoder::encode($data);
    
    
    
    
    }
    
    
    
    
    
}
?>