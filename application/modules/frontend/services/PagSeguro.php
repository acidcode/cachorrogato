<?php

require_once LIB_PATH ."/PagSeguroLibrary/PagSeguroLibrary.php";

/**
 * Pagamento via pag seguro
 * @author @acidcode
 * @since 17-05-2012
 * @copyright GPO4  - www.cachorrogato.com.br - andre.kernelpanic@gmail.com
 */
class Frontend_Service_PagSeguro {
	
	public static function main ($id_plano = 1, $id_usuario = false, $id_anuncio, $periodo) {
		
		// Instantiate a new payment request
		$paymentRequest = new PagSeguroPaymentRequest();
		
		// Sets the currency
		$paymentRequest->setCurrency("BRL");
		
                $tbPlano = new Backend_Model_DbTable_TbPlano();
                $plano = $tbPlano->find($id_plano);
          
                // Add another item for this payment request
		$paymentRequest->addItem($plano['id_plano'], $plano['vc_plano'], 1,  $plano['mn_valor_' . $periodo]);
		
		// Sets a reference code for this payment request, it is useful to identify this payment in future notifications.
		$paymentRequest->setReference("ANUNCIO:".$id_anuncio);
		
                $tbUsuario = new Backend_Model_DbTable_TbUsuario();
                $usuario = $tbUsuario->find($id_usuario);
                
		// Sets your customer information.
		$paymentRequest->setSender($usuario['vc_nome'], $usuario['vc_email'], substr($usuario['it_telefone_1'], 0, 2), substr($usuario['it_telefone_1'], 2, 8));
		
		$paymentRequest->setRedirectUrl("http://www.fecheidireto.com.br/area-cliente");
		
		try {
			
			/*
			* #### Crendencials ##### 
			* Substitute the parameters below with your credentials (e-mail and token)
			* You can also get your credentails from a config file. See an example:
			* $credentials = PagSeguroConfig::getAccountCredentials();
			*/			
			$credentials = new PagSeguroAccountCredentials("alsrep@alsrep.com.br", "27F242529F414E2195655C70FAEFEAE3");
			
			
			// Register this payment request in PagSeguro, to obtain the payment URL for redirect your customer.
			$url = $paymentRequest->register($credentials);
			
			return $url;
			
		} catch (PagSeguroServiceException $e) {
			die($e->getMessage());
		}
		
	}
	
	public static function printPaymentUrl($url) {
		if ($url) {
			echo "<p><a title=\"URL do pagamento\" href=\"$url\">Clique Aqui para efetuar o pagamento</a></p>";
		}
	}
	
}

?>