<?php

class Zend_View_Helper_ChamarConteudo extends Zend_View_Helper_Abstract {

    public function ChamarConteudo($id_categoria, $destaque = false, $limit = 2) {

        if (!$id_categoria) {
            return array();
        }

        $db = Zend_Registry::get('db');

        $conteudos = $db->select()
                ->from(array('c' => 'tb_conteudo'))
                ->joinInner(array('cr' => 'tb_conteudo_relacionado_categoria'), 'c.id_conteudo = cr.id_conteudo')
                ->where('cr.id_conteudo_categoria = ' . $id_categoria)
                ->where("c.bl_ativo = '1'");

        if ($destaque) {
            $conteudos = $conteudos->where("c.bl_destaque = '$destaque'");
        }

        $conteudos = $conteudos->limit($limit)
                ->order('RAND()')
                ->query()
                ->fetchAll();

        return $conteudos;
    }

}