<?php

/**
 * Classe com todas as ações (funções/métodos) que são necessários
 * para cadastrar um usuários.
 * 
 * @author @acidcode
 * @since 
 * @author @MatheusMarabesi
 * @since 2013-04-01
 * @copyright GPO4  e Desenvolvimento ( www.cachorrogato.com.br )
 */
class Frontend_CadastroClienteController extends Zend_Controller_Action {

    private $_facebook;
    private $FACEBOOK_APP_ID = 536384943068161;
    private $FACEBOOK_SECRET = '12d0b387948db72d8880be99839264d9';
    private $dataNascimento = array();
    private $bootstrap;

    public function init() {
        require_once(LIB_PATH . '/Canvas/canvas.php');
        require_once(LIB_PATH . '/facebook/facebook.php');

        $this->_facebook = new Facebook(array(
            'appId' => $this->FACEBOOK_APP_ID,
            'secret' => $this->FACEBOOK_SECRET
                ));

        for ($d = 1; $d <= 31; $d++) {
            $this->dataNascimento['dia'][$d] = $d;
        }

        for ($m = 1; $m <= 12; $m++) {
            $this->dataNascimento['mes'][$m] = $m;
        }

        for ($y = 1970; $y <= date('Y'); $y++) {
            $this->dataNascimento['ano'][$y] = $y;
        }

        $this->bootstrap = $this->getInvokeArg('bootstrap');
    }

    /**
     * Função para cadastrar um novo usuário e inicia a sessão 
     * para os passos seguintes do cadastro
     * 
     * @name cadastroAction
     * @access public
     * @param void
     * @return void 
     */
    public function indexAction() {
        $this->view->headTitle('Cadastro do Cliente');
        $this->view->headMeta()->appendName('description', '');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->action = '/frontend/cadastro-cliente/';
        $this->view->dataNascimento = $this->dataNascimento;


        if ($this->_request->isPost()) {
            $request = $this->_request->getPost();

            $tbCliente = new Frontend_Model_DbTable_TbCliente();
            $idCliente = $tbCliente->adicionarCliente($request);

            if (empty($idCliente)) {
                $this->view->erro_cadastro = 'Ocorreu um erro ao efetuar o cadastro desculpe pelo transtorno';
            } else {
                $data = array(
                    'id_cliente' => $idCliente
                );

                $cookie = array(
                    'nome' => $this->bootstrap->getOption('clienteCookieName'),
                    'expira' => $this->bootstrap->getOption('clienteCookieExpire'),
                    'caminho' => $this->bootstrap->getOption('clienteCookiePath'),
                    'dominio' => $this->bootstrap->getOption('clienteCookieDomain'),
                    'data' => serialize($data)
                );

                if (!$request['ajax']) {
                    setcookie($cookie['nome'], serialize($data), $cookie['expira'], $cookie['caminho'], $cookie['dominio']);
                    $this->_redirect('/frontend/cadastro-cliente/finalizado/');
                } else {
                    $this->_helper->layout()->disableLayout();
                    $this->_helper->viewRenderer->setNoRender(true);

                    echo json_encode($cookie);
                }
            }
        }
        /**
         * Verifica se foi feito login com o facebook
         * para a busca de informações para preencher o formulário
         */
        $me = $this->_facebook->api('/me');
        if ($me) {
            $data = array(
                'vc_nome' => $me['first_name'],
                'vc_sobrenome' => $me['last_name'],
                'bl_sexo' => $me[''],
                'dt_nascimento' => $me['birthday'],
                'it_cep' => $me[''],
                'vc_endereco' => $me[''],
                'vc_numero' => $me[''],
                'vc_complemento' => $me[''],
                'vc_bairro' => $me[''],
                'vc_cidade' => $me[''],
                'vc_estado' => $me[''],
                'vc_email' => $me['email'],
                'vc_email_confirmar' => $me['email']
            );

            $this->view->me = $data;
        }
    }

    /**
     * Função para enviar um e-mail para o usuário contendo os 
     * passos necessários para a redefinição de sua senha de acesso
     * 
     * @name esqueceuSenhaAction
     * @access public
     * @param void
     * @return void
     */
    public function esqueceuSenhaAction() {
        $this->view->headTitle('Esqueci Minha Senha');
        $this->view->headMeta()->appendName('description', '');
        $this->view->headMeta()->appendName('keywords', '');

        if ($this->_request->isPost()) {
            $request = $this->_request->getPost();
            if (!empty($request['vc_email'])) {
                $tbCliente = new Frontend_Model_DbTable_TbCliente();

                $data = $tbCliente->findByEmail($request['vc_email']);
                if ($data) {
                    $mensagem = $this->view->partial('/cadastro/partial/email-recuperar-senha.phtml', array('data' => $data));
                    $tbCliente->enviarEmailRecuperarSenha($mensagem, $data['vc_email']);

                    $this->view->mensagem = 'Um e-mail foi enviado com os passos para redefinir a senha de acesso';
                } else {
                    $this->view->mensagem = 'O e-mail informado não consta em nosso sistema';
                }
            }
        }
    }

    /**
     * Função que efetiva a recuperação da senha
     * fazendo a atualização na base de dados
     * 
     * @name recuperarSenhaAction
     * @access public
     * @param void 
     * @return void 
     */
    public function recuperarSenhaAction() {
        $this->view->headTitle('Recuperar Senha de Acesso');
        $this->view->headMeta()->appendName('description', '');
        $this->view->headMeta()->appendName('keywords', '');

        $token = $this->_request->getParam('token');

        $tbCliente = new Frontend_Model_DbTable_TbCliente();
        $data = $tbCliente->findByIdMd5($token);

        if (!strlen($token) == 32 || !$data) {
            $this->_redirect('/');
        } else {
            $data['token'] = $token;
            $this->view->data = $data;

            if ($this->_request->isPost()) {
                $request = $this->_request->getPost();
                if ($tbCliente->atualizarSenha($request)) {
                    $this->view->mensagem = 'Sua senha foi atualizada com sucesso!';
                } else {
                    $this->view->mensagem = 'Ocorreu um erro durante a atualização da senha de acesso, por favor tente mais tarde';
                }
            }
        }
    }

    /**
     * Função para o upload de fotos de clientes,
     * transfere o arquivo enviado pelo usuário para o servidor e realiza 
     * a inserção no banco de dados
     * 
     * @name uploadFotoAction
     * @access public
     * @param void 
     * @return void 
     */
    public function uploadFotoAction() {
        $this->_helper->layout()->disableLayout();

        $cookieCadastroUsuario = unserialize($this->_request->getCookie($this->bootstrap->getOption('clienteCookieName')));

        if (!empty($cookieCadastroUsuario['id_cliente'])) {
            $tbClienteFoto = new Frontend_Model_DbTable_TbClienteFoto();

            if ($this->_request->isPost()) {
                $upload = new Zend_File_Transfer();
                $arquivoInfo = $upload->getFileInfo();

                if ($arquivoInfo['vc_cliente_foto']['error'] == 0) {

                    foreach ($tbClienteFoto->findFotosByCliente($cookieCadastroUsuario['id_cliente']) as $arrayFotos) {
                        unlink(PUBLIC_PATH . '/uploads/modules/front-end/clientes/fotos/' . $arrayFotos['vc_cliente_foto']);
                        $tbClienteFoto->deletarFotoById($arrayFotos['id_cliente_foto']);
                    }

                    $adapter = new Zend_File_Transfer_Adapter_Http();
                    $adapter->setDestination(PUBLIC_PATH . '/uploads/modules/front-end/clientes/fotos/');

                    $idFoto = $tbClienteFoto->getProximoIdFoto($cookieCadastroUsuario['id_cliente']);
                    if ($idFoto) {
                        $extensaoFoto = end(explode('.', $arquivoInfo['vc_cliente_foto']['name']));
                        $nomeNovo = $cookieCadastroUsuario['id_cliente'] . $idFoto . date('Ymdhms') . '.' . $extensaoFoto;

                        $adapter->addFilter('Rename', array('target' => $adapter->getDestination() . '/' . $nomeNovo, 'overwrite' => true));
                        if ($adapter->receive()) {
                            $canvas = new canvas();
                            $canvas->carrega($adapter->getDestination() . '/' . $nomeNovo);
                            $canvas->redimensiona(150, 130, 'crop');
                            $canvas->grava($adapter->getDestination() . '/' . $nomeNovo);
                        }

                        $tbClienteFoto->atualizarNomeFoto($idFoto, $nomeNovo);
                    }
                }
            }
            $this->view->foto = $tbClienteFoto->findFotosByCliente($cookieCadastroUsuario['id_cliente']);
        }
    }

    /**
     * Função para o upload de fotos de pet,
     * transfere o arquivo enviado pelo usuário para o servidor e realiza 
     * a inserção no banco de dados
     * 
     * @name uploadFotoPetAction
     * @access public
     * @param void 
     * @return void 
     */
    public function uploadFotoPetAction() {
        $this->_helper->layout()->disableLayout();

        $cookieCadastroUsuario = unserialize($this->_request->getCookie($this->bootstrap->getOption('clienteCookieName')));

        if (!empty($cookieCadastroUsuario['id_cliente'])) {
            $tbPetFoto = new Frontend_Model_DbTable_TbPetFoto();

            if ($this->_request->isPost()) {
                $upload = new Zend_File_Transfer();
                $arquivoInfo = $upload->getFileInfo();

                if ($arquivoInfo['vc_pet_foto']['error'] == 0) {

                    if (isset($cookieCadastroUsuario['id_pet'])) {
                        $id_pet = $cookieCadastroUsuario['id_pet'];
                    } else {
                        $pet = new Frontend_Model_DbTable_TbPet();
                        $id_pet = $pet->inserirPet(array(), $cookieCadastroUsuario['id_cliente']);

                        $cookieCadastroUsuario['id_pet'] = $id_pet;
                    }

                    foreach ($tbPetFoto->findFotoByPet($id_pet) as $arrayFotos) {
                        unlink(PUBLIC_PATH . '/uploads/modules/front-end/pet/fotos/' . $arrayFotos['vc_pet_foto']);
                        $tbPetFoto->deletarFotoById($arrayFotos['id_cliente_foto']);
                    }
                    echo '<pre>';
                    print_r($cookieCadastroUsuario);
                    echo '</pre>';

                    $cookie = array(
                        'nome' => $this->bootstrap->getOption('clienteCookieName'),
                        'expira' => $this->bootstrap->getOption('clienteCookieExpire'),
                        'caminho' => $this->bootstrap->getOption('clienteCookiePath'),
                        'dominio' => $this->bootstrap->getOption('clienteCookieDomain'),
                        'data' => serialize($cookieCadastroUsuario)
                    );

                    echo '<pre>';
                    print_r($cookie);
                    echo '</pre>';
                    //setcookie('cookiematheus', $cookie['data'], $cookie['expira'], $cookie['caminho'], $cookie['dominio']);
                    /* $adapter = new Zend_File_Transfer_Adapter_Http();
                      $adapter->setDestination(PUBLIC_PATH . '/uploads/modules/front-end/clientes/fotos/');

                      $idFoto = $tbClienteFoto->getProximoIdFoto($cookieCadastroUsuario['id_cliente']);
                      if ($idFoto) {
                      $extensaoFoto = end(explode('.', $arquivoInfo['vc_cliente_foto']['name']));
                      $nomeNovo = $cookieCadastroUsuario['id_cliente'] . $idFoto . date('Ymdhms') . '.' . $extensaoFoto;

                      $adapter->addFilter('Rename', array('target' => $adapter->getDestination() . '/' . $nomeNovo, 'overwrite' => true));
                      if ($adapter->receive()) {
                      $canvas = new canvas();
                      $canvas->carrega($adapter->getDestination() . '/' . $nomeNovo);
                      $canvas->redimensiona(150, 130, 'crop');
                      $canvas->grava($adapter->getDestination() . '/' . $nomeNovo);
                      }

                      $tbClienteFoto->atualizarNomeFoto($idFoto, $nomeNovo);
                      } */
                }
            }
            //$this->view->foto = $tbClienteFoto->findFotosByCliente($cookieCadastroUsuario['id_cliente']);
        }
    }

    /**
     * Função que deletar uma foto do fornecedor
     * apartir do seu id via ajax
     * 
     * @name deletarFotoAction
     * @access public
     * @param void
     * @return json_encode 
     */
    public function deletarFotoAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ($this->_request->isPost()) {
            $data = $this->_request->getPost();
            $tbClienteFoto = new Frontend_Model_DbTable_TbClienteFoto();

            $foto = $tbClienteFoto->findFotoById($data['id_cliente_foto']);
            if ($foto) {
                if ($tbClienteFoto->deletarFotoById($data['id_cliente_foto'])) {
                    unlink(PUBLIC_PATH . '/uploads/modules/front-end/clientes/fotos/' . $foto['vc_cliente_foto']);
                    echo json_encode(true);
                } else {
                    echo json_encode(false);
                }
            } else {
                echo json_encode(false);
            }
        }
    }

    /**
     * Função que atualiza o registro de cadastro do usuário com 
     * os novos dados fornecidos
     * 
     * @name cadastroComplementoAction
     * @access public
     * @param void
     * @return void 
     */
    public function complementoAction() {
        $this->view->headTitle('Cadastrar Complemento');
        $this->view->headMeta()->appendName('description', '');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->action = '/frontend/cadastro-cliente/complemento/';
        $this->view->action_foto = '/frontend/cadastro-cliente/upload-foto/';

        $cookie = $this->_request->getCookie();
        $cookieCadastroUsuario = unserialize($cookie[$this->bootstrap->getOption('clienteCookieName')]);

        if (!empty($cookieCadastroUsuario['id_cliente'])) {
            if ($this->_request->isPost()) {
                $request = $this->_request->getPost();
                $tbCliente = new Frontend_Model_DbTable_TbCliente();
                $tbCliente->atualizarComplemento($request, $cookieCadastroUsuario['id_cliente']);

                if (!$request['ajax']) {
                    $this->_redirect('/frontend/cadastro-cliente/finalizado/');
                }
            }
        } else {
            //$this->_redirect('/frontend/cadastro-cliente/');
        }
    }

    /**
     * Função que cadastra um pet para o usuário e o redireciona
     * para a próxima fase do cadastro
     * 
     * @name cadastroPetAction
     * @access public
     * @param void
     * @return void 
     */
    public function adicionarPetAction() {
        $this->view->headTitle('Cadastrar Pet');
        $this->view->headMeta()->appendName('description', '');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->action = '/frontend/cadastro-cliente/adicionar-pet/';
        $this->view->action_foto_pet = '/frontend/cadastro-cliente/upload-foto-pet/';

        $tbPet = new Frontend_Model_DbTable_TbPet();
        $cookie = $this->_request->getCookie();
        $cookieCadastroUsuario = unserialize($cookie[$this->bootstrap->getOption('clienteCookieName')]);
        /* echo '<pre>';
          print_r($cookieCadastroUsuario);
          echo '</pre>'; */
        if (!empty($cookieCadastroUsuario['id_cliente'])) {
            if ($this->_request->isPost()) {
                $this->_redirect('/frontend/cadastro-cliente/finalizado/');
                $tbPet = new Frontend_Model_DbTable_TbPet();
                $tbPet->inserirPet($this->_request->getPost(), $cookieCadastroUsuario['id_cliente']);
            }

            $data = array();

            foreach ($tbPet->findAllRaca() as $array) {
                $data['raca'][$array['id_raca']] = $array['vc_descricao'];
            }

            foreach ($tbPet->findAllPorte() as $array) {
                $data['porte'][$array['id_porte']] = $array['vc_descricao'];
            }

            foreach ($tbPet->findAllPeso() as $array) {
                $data['peso'][$array['id_peso']] = $array['vc_descricao'];
            }
            foreach ($tbPet->findAllCor() as $array) {
                $data['cor'][$array['id_cor']] = $array['vc_descricao'];
            }
            foreach ($tbPet->findAllPelagem() as $array) {
                $data['pelagem'][$array['id_pelagem']] = $array['vc_descricao'];
            }

            foreach ($tbPet->findAllAtividade() as $array) {
                $data['atividade'][$array['id_atividade']] = $array['vc_descricao'];
            }

            $this->view->partial('/cadastro-cliente/partial/email-confirmacao-cadastro.phtml', array('data' => $data));

            $data['vacina'] = $tbPet->findAllVacina();

            $this->view->data = $data;
            $this->view->dataNascimento = $this->dataNascimento;
        } else {
            $this->_redirect('/frontend/cadastro-cliente/');
        }
    }

    /**
     * Função que encerra a sessão de cadastro do usuário
     * e envia um e-mail para a confirmação 
     * 
     * @name cadastroRealizadoAction
     * @access public
     * @param void
     * @return void 
     */
    public function finalizadoAction() {
        $cookie = $this->_request->getCookie();
        $cookieCadastroUsuario = unserialize($cookie[$this->bootstrap->getOption('clienteCookieName')]);

        $this->view->action = '/frontend/cadastro-cliente/acesso/';
        if (!empty($cookieCadastroUsuario['id_cliente'])) {
            $idMd5 = md5($cookieCadastroUsuario['id_cliente']);
            $tbCliente = new Frontend_Model_DbTable_TbCliente();
            $data = $tbCliente->findByIdMd5($idMd5);

            if ($data) {
                $mensagem = $this->view->partial('/cadastro-cliente/partial/email-confirmacao-cadastro.phtml', array('data' => $data));
                $tbCliente->enviarEmailConfirmacaoCadastro($mensagem, $data['vc_email']);
            }

            setcookie($this->bootstrap->getOption('clienteCookieName'), '', time() + 1);
        } else {
            $this->_redirect('/frontend/cadastro-cliente/');
        }
    }

    /**
     * Função que confirma o cadastro do usuário 
     * através do e-mail enviado ao final do cadastro
     * 
     * @name confirmacaoCadastroAction
     * @param void
     * @return void  
     */
    public function confirmacaoCadastroAction() {
        $this->view->headTitle('Confirmar Cadastro');
        $this->view->headMeta()->appendName('description', '');
        $this->view->headMeta()->appendName('keywords', '');

        $token = $this->_request->getParam('token');

        $tbCliente = new Frontend_Model_DbTable_TbCliente();
        $data = $tbCliente->findByIdMd5($token);

        if (!strlen($token) == 32 || !$data) {
            $this->_redirect('/acesso');
        } else {
            if ($tbCliente->confirmarCadastro($token)) {
                $data['mensagem'] = 'Cadastro confirmado com sucesso !';
            } else {
                $data['mensagem'] = 'Ocorreu um erro ao confirmar o seu cadastro, caso ja tenha confirmado seu cadastro ignore essa mensagem';
            }
            $this->view->data = $data;
        }
    }

    /**
     * Função que é usada por ajax para verificar
     * se o e-mail informado exite na base de dados
     * 
     * @name verificaEmailAction
     * @access public
     * @param void
     * @return json_encode 
     */
    public function verificaEmailAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ($this->_request->isPost()) {
            $request = $this->_request->getPost();
            $tbCliente = new Frontend_Model_DbTable_TbCliente();
            $findByEmail = $tbCliente->findByEmail($request['vc_email']);

            echo json_encode($findByEmail);
        }
    }

    /**
     * Função para buscar o endereço, número etc através do cep informado
     * essa função é acessada apenas por ajax
     * 
     * @name buscarEnderecoPorCepAction
     * @access public
     * @param void
     * @return json_encode 
     */
    public function buscarEnderecoPorCepAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $cep = $this->_request->getParam('cep');
        $tbCliente = new Frontend_Model_DbTable_TbCliente();
        $endereço = $tbCliente->findAddressFromCep($cep);

        echo json_encode($endereço);
    }

    /**
     * Função usada para realizar o login do usuário a sua área restrita
     * 
     * @name acessoAction
     * @access public
     * @param void
     * @return void 
     */
    public function loginAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ($this->_request->isPost()) {
            $tbCliente = new Frontend_Model_DbTable_TbCliente();
            $request = $this->_request->getPost();
            $json = array(
                'css' => array(
                    'atributo' => 'border',
                    'valor' => '1px solid #ff0000'),
                'erro' => false,
                'alertaLogin' => '',
                'url' => '/area-cliente/'
            );

            if (empty($request['vc_email'])) {
                $json['erro'] = true;
                $json['id'][] = 'label_vc_email_login';
            }

            if (empty($request['vc_senha'])) {
                $json['erro'] = true;
                $json['id'][] = 'label_vc_senha_login';
            }

            if ($json['erro'] == false) {
                $data = $tbCliente->acesso($request);

                if ($data) {
                    if ($data['bl_ativo'] == 'nao') {
                        $json['alertaLogin'] = $data['vc_nome'] . ' ' . $data['vc_sobrenome'] . ' seu cadastro ainda não foi ativado, cheque sua caixa de e-mail para confirmar seu registro';
                    } else {
                        $sessaoCliente = new Zend_Session_Namespace('Sessao_Cliente');
                        $sessaoCliente->arr = $data;
                    }
                } else {
                    $json['alertaLogin'] = 'Cadastro não encontrado';
                }
            }
            echo json_encode($json);
        }
    }

    public function logoffAction() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $usuario = new Zend_Session_Namespace('Sessao_Cliente');
        $usuario->arr = null;
        $this->_redirect('/');
    }

    /**
     * Função usada para efetuar login no facebook e usar as informações
     * para buscar os dados necessários para o cadastro no sistema.
     * 
     * @name facebookAction
     * @access public
     * @param void
     * @return void 
     */
    public function facebookAction() {
        $userFacebook = $this->_facebook->getUser();

        if ($userFacebook == 0) {
            $appUrl = $this->_facebook->getLoginUrl(array(
                'redirect_uri' => 'http://www.cachorrogato.com.br/cadastro',
                'scope' => array(
                    'email',
                    'user_birthday'
                )
                    ));
            $this->_redirect($appUrl);
        } else {
            $this->_redirect('/cadastro');
        }
    }

}
