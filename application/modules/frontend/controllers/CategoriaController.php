<?php

/**
 * Categorias e sub categorias
 * 
 * @author @acidcode
 * @since 
 * @author @MatheusMarabesi
 * @since 2013-04-09
 * @copyright GPO4  e Desenvolvimento ( www.cachorrogato.com.br )
 */
class Frontend_CategoriaController extends Zend_Controller_Action {
    
    /**
     * 
     */
    public function indexAction() {
        
        $tbConteudoCategoria = new Backend_Model_DbTable_TbConteudoCategoria();
        
        $categoria = $tbConteudoCategoria->find(
                $this->_getParam('id_categoria')
                );
        
        $this->view->headTitle($categoria['vc_seo_title']);
        $this->view->headMeta()->appendName('description', $categoria['vc_seo_description']);
        $this->view->headMeta()->appendName('keywords', $categoria['vc_seo_keywords']);
        $this->view->idPage = "categoria-" . $categoria['vc_seo_url'];
        $this->view->vc_seo_url = "categoria-" . $categoria['vc_seo_url'];
        
        
        $this->view->breadcrumb = array(
            array("legenda" => "Home", "link" => "/", 'class' => 'primeiro'),
            array("legenda" => $categoria['vc_categoria'], "link" => "/".$categoria['vc_seo_url'], 'class' => 'ultimo')
        );
        
        $arvoreCategoria = $tbConteudoCategoria
        ->getArvoreCategoria(
                $categoria['id_conteudo_categoria']
            );
        
        $this->view->categoria = $categoria;
        $this->view->arvoreCategoria = $arvoreCategoria[$categoria['id_conteudo_categoria']]['subcategorias'];
    }
    
    
    /**
     * 
     */
    public function subcategoriaAction() {
        
        $tbConteudoCategoria = new Backend_Model_DbTable_TbConteudoCategoria();
        $categoriaMain = $tbConteudoCategoria->find(
                $this->_getParam('id_categoria')
                );
                
        $categoria = $tbConteudoCategoria->findByUrl(
                $this->_getParam('sub-categoria')
                );

        $arvoreCategoria = $tbConteudoCategoria
        ->getArvoreCategoria(
                $categoria['id_conteudo_categoria']
            );
        
        $this->view->headTitle("Sub Categoria " . $this->_getParam('subcategoria'));
        $this->view->headMeta()->appendName('description', 'Sub Categoria');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->idPage = "subcategoria-" . $this->_getParam('subcategoria');
        
        $this->view->breadcrumb = array(
            array("legenda" => "Home", "link" => "/", 'class' => 'primeiro'),
            array("legenda" => $categoriaMain['vc_categoria'], "link" => "/".$categoriaMain['vc_seo_url'], 'class' => ''),
            array("legenda" => $categoria['vc_categoria'], "link" => "/".$categoriaMain['vc_seo_url']."/" . $categoria['vc_seo_url'], 'class' => 'ultimo')
        );
        
        $this->view->categoria = $categoria;
        $this->view->categoriaMain = $categoriaMain;
        $this->view->arvoreCategoria = $arvoreCategoria[$categoria['id_conteudo_categoria']]['subcategorias'];
  
    }
}