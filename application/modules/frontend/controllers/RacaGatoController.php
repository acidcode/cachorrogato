<?php

/**
 * Controller de Racas
 * 
 * @author @acidcode
 * @copyright GPO4  e Desenvolvimento ( www.cachorrogato.com.br )
 */
class Frontend_RacaGatoController extends Zend_Controller_Action {
    
    /**
     * 
     */
    public function indexAction() {
        
        $this->view->headTitle("Raças de Gatos");
        $this->view->headMeta()->appendName('description', 'Raças');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->idPage = "categoria";
        
        $tbRacaGato = new Backend_Model_DbTable_TbRacaGato();
        $tbConteudo = new Backend_Model_DbTable_TbConteudo();
        
        $this->view->racas = $tbRacaGato->findAll();
        $this->view->conteudos = $tbConteudo->fetchAll(null,"id_conteudo DESC","3");
        $this->view->conteudos2 = $tbConteudo->fetchAll(null,"id_conteudo DESC","3","3");
        
        $this->view->breadcrumb = array(
            array("legenda" => "Home", "link" => "/index", 'class' => 'primeiro'),
            array("legenda" => "Raças de Gato", "link" => "/racas/gato", 'class' => 'ultimo')
        );
    }
    
    /**
     * 
     */
    public function detalheAction() {
        
        $tbRacaGato = new Backend_Model_DbTable_TbRacaGato();
        $racaGato = $tbRacaGato->findByUrl(
                $this->_getParam('raca')
                );
                
        $this->view->headTitle($racaGato['vc_seo_title']);
        $this->view->headMeta()->appendName('description', $racaGato['vc_seo_description']);
        $this->view->headMeta()->appendName('keywords', $racaGato['vc_seo_keywords']);
        $this->view->idPage = "raca-gato";
        
        $this->view->breadcrumb = array(
            array("legenda" => "Home", "link" => "/index", 'class' => 'primeiro'),
            array("legenda" => "Raças", "link" => "/racas", 'class' => 'ultimo')
        );
        
        $this->view->racaGato = $racaGato;
        
    }
    
    
}