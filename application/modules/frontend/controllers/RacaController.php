<?php

/**
 * Controller de Racas
 * 
 * @author @acidcode
 * @since 
 * @author @MatheusMarabesi
 * @since 2013-04-09
 * @copyright GPO4  e Desenvolvimento ( www.cachorrogato.com.br )
 */
class Frontend_RacasController extends Zend_Controller_Action {
    
    /**
     * 
     */
    public function indexAction() {
        
        $this->view->headTitle("Raças");
        $this->view->headMeta()->appendName('description', 'Raças');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->idPage = "categoria";
        
        $this->view->breadcrumb = array(
            array("legenda" => "Home", "link" => "/index", 'class' => 'primeiro'),
            array("legenda" => "Raças", "link" => "/racas", 'class' => 'ultimo')
        );
    }
    
    /**
     * 
     */
    public function detalhesAction() {
        
        $this->view->headTitle("Raça " . $this->_getParam('raca'));
        $this->view->headMeta()->appendName('description', 'Raca');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->idPage = "categoria-" . $this->_getParam('categoria');
        
        $this->view->breadcrumb = array(
            array("legenda" => "Home", "link" => "/index", 'class' => 'primeiro'),
            array("legenda" => "Categoria", "link" => "", 'class' => ''),
            array("legenda" => $this->_getParam('categoria'), 
                "link" => "/categoria/categoria/categoria/" . 
                $this->_getParam('categoria'), 'class' => 'ultimo')
        );
        
        $categoria = array(
            'nome' => $this->_getParam('categoria')
        );
        
        $this->view->categoria = $categoria;
    }
    
    
}