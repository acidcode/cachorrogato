<?

class Frontend_AjaxController extends Zend_Controller_Action {

    
    public function getBairrosAction() {
            $this->_helper->layout->disableLayout();
            $tbBairros = new Backend_Model_DbTable_TbBairros();
            $bairros = $tbBairros->findByCity($this->_getParam('cidade'));
            echo json_encode($bairros);
            die();
    }

    public function getTiposAction() {
            $this->_helper->layout->disableLayout();
            $tbTipos = new Backend_Model_DbTable_TbTipoImoveis();
            $tipos = $tbTipos->findByBairro($this->_getParam('bairro'));
            echo json_encode($tipos);
            die();
    }
        
    public function getCidadesMoreViewedAction() {
            $this->_helper->layout->disableLayout();
            $cidM = new Backend_Model_DbTable_TbCidades();            
            $cidades= $cidM->findMoreViewed();
            echo json_encode($cidades);
            die();
    }
            
    public function getBairrosMoreViewedAction() {
            $this->_helper->layout->disableLayout();
            $bairroM = new Backend_Model_DbTable_TbBairros();
            $bairros= $bairroM->findMoreViewed();
            echo json_encode($bairros);
            die();
    }
            
    public function getDormsMoreViewedAction() {
            $this->_helper->layout->disableLayout();
            $dormsM = new Backend_Model_DbTable_TbEmpreendimento();
            $dorms = $dormsM->findDormsMoreViewed();
            echo json_encode($dorms);
            die();
    }
            
    public function getFootageMoreViewedAction() {
            $this->_helper->layout->disableLayout();
            $metM = new Backend_Model_DbTable_TbEmpreendimento();
            $metragens = $metM->findFootagesMoreViewed();
            echo json_encode($metragens);
            die();
    }
            
    public function getTiposMoreViewedAction() {
            $this->_helper->layout->disableLayout();
            $tiposM = new Backend_Model_DbTable_TbTipoImoveis();
            $tipos = $tiposM->findMoreViewed();
            echo json_encode($tipos);
            die();
    }
            
    public function getEstagiosMoreViewedAction() {
            $this->_helper->layout->disableLayout();
            $fasesM = new Backend_Model_DbTable_TbFaseObras();
            $fases = $fasesM->findMoreViewed();
            echo json_encode($fases);
            die();
    }
    
    public function getMenusByParamsAction() {
            $this->_helper->layout->disableLayout();
            $empM = new Backend_Model_DbTable_TbEmpreendimento();
            $params = $this->_getAllParams();
            $filtros = $empM->findMenusByFilters($params);
            echo json_encode($filtros);
            die();
    }
        
    public function getEmpreendimentosAction() {
            $this->_helper->layout->disableLayout();
            $empM = new Backend_Model_DbTable_TbEmpreendimento();
            $params = $this->_getAllParams();
            $empreendimentos = $empM->findByFilters($params);
            foreach($empreendimentos as $emp) {                
                $this->view->image($emp['imagem_listagem'])->resize(168, 164)->resizedImage();
            }
            echo json_encode($empreendimentos);
            die();
    }

    public function getFasesAction() {
            $this->_helper->layout->disableLayout();
            $tbFases = new Backend_Model_DbTable_TbFaseObras();
            $fases = $tbFases->findByBairroAndTipo($this->_getParam('bairro'), $this->_getParam('tipo'));
            echo json_encode($fases);
            die();
    }

    public function getValoresAction() {
            $this->_helper->layout->disableLayout();
            $tbEmp = new Backend_Model_DbTable_TbEmpreendimento();
            $valores = $tbEmp->listaPrecoPorPesquisa($this->_getParam('bairro'), $this->_getParam('tipo'));
            echo json_encode($valores);
            die();
    }

    public function getNDormsAction() {
            $this->_helper->layout->disableLayout();
            $tbBairros = new Backend_Model_DbTable_TbBairros();
            $bairros = $tbBairros->findByCity($this->_getParam('cidade'));
            echo json_encode($bairros);
            die();
    }
	
    public function carregarSubcategoriaAction() {
        $this->_helper->layout()->disableLayout();
        $tbSubcategoriaAnuncio = new Backend_Model_DbTable_TbSubcategoriaAnuncio();
        $this->view->subcategoriaAnuncios = $tbSubcategoriaAnuncio->findAll(
                "c.id_categoria_anuncio = " . $this->_getParam('id_categoria_anuncio')
        );
    }
    
    public function faleConoscoAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $tbFaleConosco = new Backend_Model_DbTable_TbFaleConosco();
        
        $tbFaleConosco->enviar(
                $this->_getAllParams()
        );
    }
    
    public function enviarFotoAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $uploaddir = UPLOAD_PATH;
        $uploadfile = $uploaddir . basename($_FILES['userfile']['name']);

        if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
            echo "success";
        } else {
        // WARNING! DO NOT USE "FALSE" STRING AS A RESPONSE!
        // Otherwise onSubmit event will not be fired
            echo "error";
        }
    }
    
    public function fecharDiretoAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $tbFecheiDireto = new Backend_Model_DbTable_TbFecheiDireto();
        $tbAnuncio = new Backend_Model_DbTable_TbAnuncio();
        $anuncio = $tbAnuncio->find($this->_getParam('id_anuncio'));
        
        $mensagem = $this->view->partial('index/email/fechei-direto.phtml', 
                        array('nome' => $this->_getParam('nome'),
                            'email' => $this->_getParam('email'),
                            'telefone' => $this->_getParam('telefone'),
                            'observacao' => $this->_getParam('observacao'),
                            'nomeProprietario' => $anuncio['vc_nome']));
        
        $tbFecheiDireto->enviar(
                $this->_getAllParams(), $mensagem
        );
    }
    
    public function indicarAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $tbAnuncio = new Backend_Model_DbTable_TbAnuncio();
        $tbAnuncio->indicar(
                $this->_getAllParams()
        );
    }
    
    public function selecionarBairroAction() {
        $this->_helper->layout()->disableLayout();
        $this->view->vc_cidade = $this->_getParam('vc_cidade');
    }
    
    public function selecionarBairroServicoAction() {
        $this->_helper->layout()->disableLayout();
        $this->view->vc_cidade = $this->_getParam('vc_cidade');
    }
    
    public function denunciaAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $tbAnuncio = new Backend_Model_DbTable_TbAnuncio();
        
        $tbAnuncio->denunciar(
            $this->_getAllParams()
        );
    }
    
    public function geocoordenadaAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $geo = new Backend_Service_Geo(
                $this->_getParam('endereco'),'000');
        echo $geo->latitude.",".$geo->longitude;
    }
    
    public function checarCpfAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $tbUsuario = new Backend_Model_DbTable_TbUsuario();
        $usuario = $tbUsuario->fetchAll(
                "it_cpf = '" . $this->_getParam('it_cpf'). "'
        ")->toArray();
        if(count($usuario) > 0) {
            echo '1';
        } else {
            echo '0';
        }
    }
    
    public function selecionarPeriodoPlanoAction() {
        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $tbPlano = new Backend_Model_DbTable_TbPlano();

        $plano = $tbPlano->find(
                $this->_getParam('id_plano')
        );
        
        switch($this->_getParam('periodo')) {
            case 'mensal':
                echo "<strong>R$ " . number_format($plano['mn_valor_mensal'],"2",",",".") . "</strong>";
            break;
            case 'trimestral':
                echo "<strong>R$ " . number_format($plano['mn_valor_trimestral'],"2",",",".") . "</strong>";
            break;
            case 'semestral':
                echo "<strong>R$ " . number_format($plano['mn_valor_semestral'],"2",",",".") . "</strong>";
            break;
            case 'anual':
                echo "<strong>R$ " . number_format($plano['mn_valor_anual'],"2",",",".") . "</strong>";
            break;
        }
    }
    
    public function registrarAcessoAction() {
        $this->_helper->layout()->disableLayout();
        session_start();
        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        
        $params = $this->_getAllParams();
        
        $acesso = array();
        $pagina_acessada = array();

        $acesso["browser"]  = $params["navegador"];
        $acesso["version"]  = $params["versao"];
        $acesso["os"]       = $params["sistema"];
        $acesso["data"]     = date("Y-m-d H:i:s");
        $acesso["referer"]  = $params["referer"];
        $acesso["user_agent"] = $params["user_agent"];
        $page = $_SERVER["HTTP_REFERER"];
        if(empty($params["page"])) {
            $acesso["page"] = str_replace("https://" . $_SERVER["HTTP_HOST"] . "/", "", (str_replace("http://" . $_SERVER["HTTP_HOST"] . "/", "", $page)));
        } else {
            $acesso["page"] = $params["page"];
        }
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $acesso["ip"] = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $acesso["ip"] = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $acesso["ip"] = $_SERVER['REMOTE_ADDR'];
        }        

        if(!empty($_SESSION["id_acesso"])) {
            $pagina_acessada["id_acesso"] = $_SESSION["id_acesso"];
            $pagina_acessada["pagina"] = $acesso["page"];
            $pagina_acessada["data"] = date("Y-m-d H:i:s");
            
            $paginaM = new Backend_Model_DbTable_PaginasAcessadas();
            $paginaM->inserir($pagina_acessada);
        } else {
            $acessoM = new Backend_Model_DbTable_Acesso();
            $acessoM->inserir($acesso);            
            $id_acesso = $acessoM->getAdapter()->lastInsertId();
            $_SESSION["id_acesso"] = $id_acesso;
            $acesso["id"] = $id_acesso;
            $acesso["status"] = true;
            
            $pagina_acessada["id_acesso"] = $id_acesso;
            $pagina_acessada["pagina"] = $acesso["page"];
            $pagina_acessada["data"] = date("Y-m-d H:i:s");
            
            $paginaM = new Backend_Model_DbTable_PaginasAcessadas();
            $paginaM->inserir($pagina_acessada);
        }

        echo json_encode($acesso);
        die();
    }
}
