<?php
/**
 * Controller de Erros do FrontEnd
 * @author @acidcode
 * @since 03-03-2011
 * @copyright GPO4  - www.cachorrogato.com.br - andre.kernelpanic@gmail.com
 */
class Frontend_ErrorController extends Zend_Controller_Action
{
    public function init(){
        
    }

    public function errorAction() {
        $this->view->headTitle("Ops..");

        $errors = $this->_getParam('error_handler');
         //Joga para exception
        $this->view->exceptionMessage = $errors->exception->getMessage();
        $this->view->code = $errors->exception->getCode();
        $this->view->exception = $errors->exception;
    }

    public function getLog()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        if (!$bootstrap->hasResource('Log')) {
            return false;
        }
        $log = $bootstrap->getResource('Log');
        return $log;
    }

    public function indexAction()
    {
        // action body
    }


}

