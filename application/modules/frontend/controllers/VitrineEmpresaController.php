<?php

class Frontend_VitrineEmpresaController extends Zend_Controller_Action {

    public function indexAction() {
        $_getAllParams = $this->_getAllParams();

        /* if ($_getAllParams['']) {

          } */
        $tbFornecedor = new Frontend_Model_DbTable_TbFornecedor();

        $pagina = $this->_getParam('pagina', 1);
        $paginator = Zend_Paginator::factory($tbFornecedor->fetchAll());
        $paginator->setItemCountPerPage(20);
        $paginator->setCurrentPageNumber($pagina);

        $this->view->empresas = $paginator;
        $this->view->primeiro = 1;
        $this->view->ultimo = count($tbFornecedor->fetchAll());
    }

    public function detalheAction() {
        /* $this->_helper->layout()->disableLayout();
          $this->_helper->viewRenderer->setNoRender(true); */

        $id_fornecedor = $this->_getParam('id_fornecedor');

        if (!empty($id_fornecedor) && (int) $id_fornecedor) {
            $tbFornecedor = new Frontend_Model_DbTable_TbFornecedor();
            $empresa = $tbFornecedor->fetchAll($id_fornecedor);

            $this->view->empresa = $empresa[0];
        } else {
            $this->_redirect('/vitrine-empresa/');
        }
    }

    public function realizarPedidoOrcamentoAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ($this->_request->isPost()) {
            $request = $this->_request->getPost();

            $json = array(
                'css' => array(
                    'atributo' => 'border',
                    'valor' => '1px solid #ff0000'),
                'erro' => false
            );

            if (empty($request['vc_nome'])) {
                $json['erro'] = true;
                $json['id'][] = 'label_vc_nome';
            }
            if (empty($request['vc_sobrenome'])) {
                $json['erro'] = true;
                $json['id'][] = 'label_vc_sobrenome';
            }
            if (empty($request['vc_email'])) {
                $json['erro'] = true;
                $json['id'][] = 'label_vc_email';
            }
            if (empty($request['it_telefone'])) {
                $json['erro'] = true;
                $json['id'][] = 'label_it_telefone';
            }
            if (empty($request['vc_estado'])) {
                $json['erro'] = true;
                $json['id'][] = 'label_vc_estado';
            }
            if (empty($request['vc_cidade'])) {
                $json['erro'] = true;
                $json['id'][] = 'label_vc_cidade';
            } if (empty($request['vc_mensagem'])) {
                $json['erro'] = true;
                $json['id'][] = 'label_vc_mensagem';
            }

            if ($json['erro'] == false) {
                $json['erroMensagem'] = '';

                $tbVitrineEmpresa = new Frontend_Model_DbTable_TbVitrineEmpresa();
                $tbVitrineEmpresa->armazenarPedido($request, true);
            }

            echo json_encode($json);
        }
    }

    public function guardarEmpresaAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ($this->_request->isPost()) {
            $request = $this->_request->getPost();
        }
    }

    public function empresaabas2Action() {
        
    }

    public function empresaabas3Action() {
        
    }

}

?>
