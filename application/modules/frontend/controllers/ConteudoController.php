<?php
/**
 * @author @acidcode
 * @copyright (c) 2013, andré gomes ( www.kernelpanic.com.br )
 */
class Frontend_ConteudoController extends Zend_Controller_Action {

    public function indexAction() {

        $tbConteudo = new Backend_Model_DbTable_TbConteudo();
        $tbConteudoCategoria = new Backend_Model_DbTable_TbConteudoCategoria();
        $tbConteudo = new Backend_Model_DbTable_TbConteudo();
       
        $conteudo = $tbConteudo->findByUrl(
                $this->_getParam('conteudo')
                );
        
        $conteudoRelacionados = $tbConteudo->findRelacionados(
                $conteudo['conteudo']['id_conteudo']
                );
        
        $categoria = $tbConteudoCategoria->find($this->_getParam('id_categoria'));
        $subcategoria = $tbConteudoCategoria->findByUrl($this->_getParam('sub-categoria'));
                
        $this->view->headTitle($conteudo['conteudo']['vc_seo_title']);
        $this->view->headMeta()->appendName('description', $conteudo['conteudo']['vc_seo_description']);
        $this->view->headMeta()->appendName('keywords', $conteudo['conteudo']['vc_seo_keywords']);
        $this->view->idPage = "conteudo-" . $conteudo['conteudo']['vc_seo_url'];
        
        $this->view->breadcrumb = array(
            array("legenda" => "Home", "link" => "/", 'class' => 'primeiro'),
            array("legenda" => $categoria['vc_categoria'], "link" => "/".$categoria['vc_seo_url'], 'class' => ''),
            array("legenda" => $subcategoria['vc_categoria'], "link" => "/".$categoria['vc_seo_url']."/".$subcategoria['vc_seo_url'], 'class' => ''),
            array("legenda" => $conteudo['conteudo']['vc_seo_title'], "link" => "/".$categoria['vc_seo_url']."/".$subcategoria['vc_seo_url']."/" . $conteudo['conteudo']['vc_seo_url'], 'class' => 'ultimo')
        );
        
        
        $this->view->conteudo = $conteudo;
        $this->view->conteudo_relacionado = $conteudoRelacionados;
        
       
    }

}

?>
