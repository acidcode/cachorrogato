<?php

/**
 * Area do Cliente
 * 
 * @author @acidcode
 * @since 
 * @since 2013-04-04
 * @copyright GPO4  e Desenvolvimento ( www.cachorrogato.com.br )
 */
class Frontend_AreaClienteController extends Zend_Controller_Action {
    
    public function init() {
        //Aqui vem a função que identifica se o usuario esta logado
    }
    
    /**
     * Dashboard do cliente
     */
    public function indexAction() {
        
        $this->view->headTitle("Area do Cliente");
        $this->view->headMeta()->appendName('description', 'Acessso a Empresa');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->idPage = "area-cliente-index";
        
        $this->view->breadcrumb = array(
            array("legenda" => "Home", "link" => "/index", 'class' => 'primeiro'),
            array("legenda" => "Área do Cliente", "link" => "/area-cliente", 'class' => ''),
            array("legenda" => "Dashboard", "link" => "/area-cliente", 'class' => 'ultimo')
        );
        
    }
    
    /**
     * Action para editar o cadastro do cliente
     */
    public function editarPerfilAction() {
        
        $this->view->headTitle("Editar Perfil");
        $this->view->headMeta()->appendName('description', '');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->idPage = "area-cliente-editar-perfil";
        
        $this->view->breadcrumb = array(
            array("legenda" => "Home", "link" => "/index", 'class' => 'primeiro'),
            array("legenda" => "Área do Cliente", "link" => "/area-cliente", 'class' => ''),
            array("legenda" => "Editar Perfil", "link" => "/area-cliente/editar-perfil", 'class' => 'ultimo')
        );
        
    }
    
    /**
     * Action com lista dos meus pets
     */
    public function meuPetAction() {
        
        $this->view->headTitle("Meu Pet");
        $this->view->headMeta()->appendName('description', 'Meu Pet');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->idPage = "area-cliente-meu-pet";
        
        $this->view->breadcrumb = array(
            array("legenda" => "Home", "link" => "/index", 'class' => 'primeiro'),
            array("legenda" => "Área do Cliente", "link" => "/area-cliente", 'class' => ''),
            array("legenda" => "Meu Pet", "link" => "/area-cliente/meu-pet", 'class' => 'ultimo')
        );
        
    }
    
    /**
     * Dashboard de um pet especifico
     */
    public function dashboardPetAction() {
        
        $this->view->headTitle("PET: " . $nomePet);
        $this->view->headMeta()->appendName('description', '');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->idPage = "area-cliente-";
        
        $this->view->breadcrumb = array(
            array("legenda" => "Home", "link" => "/index", 'class' => 'primeiro'),
            array("legenda" => "Área do Cliente", "link" => "/area-cliente", 'class' => ''),
            array("legenda" => "Dashboard Pet", "link" => "/area-cliente/dashboard-pet", 'class' => ''),
            array("legenda" =>  $nomePet, "link" => "/area-cliente/dashboard-pet/pet/" . $id_pet, 'class' => 'ultimo')
        );
        
    }
    
    /**
     * Cadastrar um novo pet no usuario logado
     */
    public function cadastrarPetAction() {
        
        $this->view->headTitle("Cadastrar Novo Pet");
        $this->view->headMeta()->appendName('description', '');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->idPage = "area-cliente-cadastro-pet";
        
        $this->view->breadcrumb = array(
            array("legenda" => "Home", "link" => "/index", 'class' => 'primeiro'),
            array("legenda" => "Dashboard Pet", "link" => "/area-cliente/dashboard-pet", 'class' => ''),
            array("legenda" =>  "Cadastro de Pet", "link" => "/area-cliente/cadastrar-pet", 'class' => 'ultimo')
        );
        
    }
    
    /**
     * Tela para cadastrar um pet ja selecionado como perdido
     */
    public function perdiMeuPetAction() {
        
        $this->view->headTitle("Perdi meu Pet :(");
        $this->view->headMeta()->appendName('description', '');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->idPage = "area-cliente-perdi-meu-pet";
        
        $this->view->breadcrumb = array(
            array("legenda" => "Home", "link" => "/index", 'class' => 'primeiro'),
            array("legenda" => "Dashboard Pet", "link" => "/area-cliente/dashboard-pet", 'class' => ''),
            array("legenda" =>  "Perdi meu Pet :(", "link" => "/area-cliente/perdi-meu-pet", 'class' => 'ultimo')
        );
        
    }
    
    /**
     * lista dos agendamentos do usuario logado
     */
    public function meusAgendamentosAction() {
        
        $this->view->headTitle("Meus Agendamentos");
        $this->view->headMeta()->appendName('description', '');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->idPage = "area-cliente-meus-agendamentos";
        
        $this->view->breadcrumb = array(
            array("legenda" => "Home", "link" => "/index", 'class' => 'primeiro'),
            array("legenda" => "Área do Cliente", "link" => "/area-cliente", 'class' => ''),
            array("legenda" => "Meus Agendamentos", "link" => "/area-cliente/meus-agendamentos", 'class' => 'ultimo')
        );
        
    }
    
    /**
     * Action de lista de fornecedores
     */
    public function meusFornecedoresAction() {
        
        $this->view->headTitle("Meus Fornecedores");
        $this->view->headMeta()->appendName('description', '');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->idPage = "area-cliente-meus-fornecedores";
        
        $this->view->breadcrumb = array(
            array("legenda" => "Home", "link" => "/index", 'class' => 'primeiro'),
            array("legenda" => "Área do Cliente", "link" => "/area-cliente", 'class' => ''),
            array("legenda" => "Meus Fornecedores", "link" => "/area-cliente/meus-fornecedores", 'class' => 'ultimo')
        );
        
    }
    
    /**
     * Action para listar os orçamentos
     */
    public function meusOrcamentosAction() {
        
        $this->view->headTitle("Meus Orçamentos");
        $this->view->headMeta()->appendName('description', '');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->idPage = "area-cliente-meus-orcamentos";
        
        $this->view->breadcrumb = array(
            array("legenda" => "Home", "link" => "/index", 'class' => 'primeiro'),
            array("legenda" => "Área do Cliente", "link" => "/area-cliente", 'class' => ''),
            array("legenda" => "Meus Orçamentos", "link" => "/area-cliente/meus-orcamentos", 'class' => 'ultimo')
        );
        
    }
    
    /**
     * Ação para gerenciar os ajustes da conta do usuario
     */
    public function ajustesContaAction() {
        
        $this->view->headTitle("Ajustes de Conta");
        $this->view->headMeta()->appendName('description', '');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->idPage = "area-cliente-ajustes-conta";
        
        $this->view->breadcrumb = array(
            array("legenda" => "Home", "link" => "/index", 'class' => 'primeiro'),
            array("legenda" => "Área do Cliente", "link" => "/area-cliente", 'class' => ''),
            array("legenda" => "Ajustes de Conta", "link" => "/area-cliente/ajustes-conta", 'class' => 'ultimo')
        );
        
    }
    
    /**
     * Action para listar os orçamentos
     */
    public function notificacoesAction() {
        
        $this->view->headTitle("Ajuste de Notificações");
        $this->view->headMeta()->appendName('description', '');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->idPage = "area-cliente-notificacoes";
        
        $this->view->breadcrumb = array(
            array("legenda" => "Home", "link" => "/index", 'class' => 'primeiro'),
            array("legenda" => "Área do Cliente", "link" => "/area-cliente", 'class' => ''),
            array("legenda" => "Notificações", "link" => "/area-cliente/notificacoes", 'class' => 'ultimo')
        );
        
    }
    
    /**
     * Action para listar as mensagens do sistema
     */
    public function mensagensAction() {
        
        $this->view->headTitle("Mensagens");
        $this->view->headMeta()->appendName('description', '');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->idPage = "area-cliente-mensagens";
        
        $this->view->breadcrumb = array(
            array("legenda" => "Home", "link" => "/index", 'class' => 'primeiro'),
            array("legenda" => "Área do Cliente", "link" => "/area-cliente", 'class' => ''),
            array("legenda" => "Mensagens", "link" => "/area-cliente/mensagens", 'class' => 'ultimo')
        );
        
    }
    
    /**
     * Action para listar as mensagens do sistema
     */
    public function meusFavoritosAction() {
        
        $this->view->headTitle("Meus Favoritos");
        $this->view->headMeta()->appendName('description', '');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->idPage = "area-cliente-meus-favoritos";
        
        $this->view->breadcrumb = array(
            array("legenda" => "Home", "link" => "/index", 'class' => 'primeiro'),
            array("legenda" => "Área do Cliente", "link" => "/area-cliente", 'class' => ''),
            array("legenda" => "Meus Favoritos", "link" => "/area-cliente/meus-favoritos", 'class' => 'ultimo')
        );
        
    }
    
}