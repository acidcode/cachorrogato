<?php

/**
 * Controle de login da empresa
 * 
 * @author @acidcode
 * @since 
 * @since 2013-04-04
 * @copyright GPO4  e Desenvolvimento ( www.cachorrogato.com.br )
 */
class Frontend_AcessoEmpresaController extends Zend_Controller_Action {

    private $bootstrap;

    public function init() {
        $this->bootstrap = $this->getInvokeArg('bootstrap');
    }

    /**
     * Login da Empresa
     */
    public function indexAction() {

        $this->view->headTitle('Acesso Empresa');
        $this->view->headMeta()->appendName('description', 'Acessso a Empresa');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->idPage = 'home';

        $this->view->breadcrumb = array(
            array('legenda' => 'Home', 'link' => '/index', 'class' => 'primeiro'),
            array('legenda' => 'Acesso Empresa', 'link' => '', 'class' => 'ultimo')
        );

        if ($this->_request->isPost()) {
            $tbFornecedor = new Frontend_Model_DbTable_TbFornecedor();
            $data = $tbFornecedor->acesso($this->_request->getPost());

            if ($data) {
                $array = $data->toArray();

                if ($array['bl_ativo'] == 'nao') {
                    $this->view->mensagem = $array['vc_nome_empresa'] . ' seu cadastro ainda não foi ativado, cheque sua caixa de e-mail para confirmar seu registro';
                } else {
                    $sessaoFornecedor = new Zend_Session_Namespace('Sessao_Fornecedor');
                    $sessaoFornecedor->arr = $array;
                    $this->_redirect('/');
                }
            } else {
                $this->view->mensagem = 'Cadastro não encontrado';
            }
        }
    }

    /**
     * Função para cadastrar um novo fornecedor(empresa) e inicia o cookie
     * para os passos seguintes do cadastro
     * 
     * @name cadastroAction
     * @access public
     * @param void
     * @return void 
     */
    public function cadastroAction() {
        $this->view->headTitle("Cadastro da Empresa");
        $this->view->headMeta()->appendName('description', 'Cadastro da Empresa');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->idPage = "home";
        $this->view->action = '/acesso-empresa/cadastro/';
        $this->view->breadcrumb = array(
            array("legenda" => "Home", "link" => "/index", 'class' => 'primeiro'),
            array("legenda" => "Acesso Empresa", "link" => "/acesso-empresa", 'class' => ''),
            array("legenda" => "Cadastro Empresa", "link" => "/acesso-empresa/cadastro", 'class' => ''),
            array("legenda" => "Informações Básicas", "link" => "javascript:;", 'class' => 'ultimo')
        );

        $data = array();
        $_tbFornecedor = new Frontend_Model_DbTable_TbFornecedor();

        foreach ($_tbFornecedor->findAtividade() as $array) {
            $data['dia_atividade'][$array['id_atividade']] = $array['vc_descricao'];
        }

        foreach ($_tbFornecedor->findSetorAtividade() as $array) {
            $data['setor_atividade'][$array['id_setor_atividade']] = $array['vc_setor_atividade'];
        }

        for ($i = 0; $i < 24; $i++) {
            if ($i == 0) {
                $data['atividade_inicio'][''] = 'Selecione';
                $data['atividade_termino'][''] = 'Selecione';
            }
            if ($i < 10) {
                $data['atividade_inicio']['0' . $i . ':' . '00'] = '0' . $i . ':' . '00';
                $data['atividade_termino']['0' . $i . ':' . '00'] = '0' . $i . ':' . '00';
            } else {
                $data['atividade_inicio'][$i . ':' . '00'] = $i . ':' . '00';
                $data['atividade_termino'][$i . ':' . '00'] = $i . ':' . '00';
            }
        }

        if ($this->_request->isPost()) {
            $request = $this->_request->getPost();

            if ($request['ajax'] == true) {
                $this->_helper->layout()->disableLayout();
                $this->_helper->viewRenderer->setNoRender(true);

                $dataCookie = $_tbFornecedor->cadastrarFornecedor($request);
                $cookie = array(
                    'nome' => $this->bootstrap->getOption('empresaCookieName'),
                    'expira' => $this->bootstrap->getOption('empresaCookieExpire'),
                    'caminho' => $this->bootstrap->getOption('empresaCookiePath'),
                    'dominio' => $this->bootstrap->getOption('empresaCookieDomain'),
                    'data' => serialize($dataCookie)
                );

                echo json_encode($cookie);
            }
        }

        $this->view->data = $data;
    }

    public function galeriaAction() {
        $cookieCadastroFornecedor = unserialize($this->_request->getCookie($this->bootstrap->getOption('empresaCookieName')));
        if (!empty($cookieCadastroFornecedor['id_fornecedor'])) {
            $this->view->breadcrumb = array(
                array("legenda" => "Home", "link" => "/index", 'class' => 'primeiro'),
                array("legenda" => "Acesso Empresa", "link" => "/acesso-empresa", 'class' => ''),
                array("legenda" => "Cadastro Empresa", "link" => "/acesso-empresa/cadastro", 'class' => ''),
                array("legenda" => "Galeria de imagens", "link" => "javascript:;", 'class' => 'ultimo')
            );

            $this->view->action_foto = '/acesso-empresa/upload-foto/';
        } else {
            $this->_redirect('/frontend/acesso-empresa/cadastro/');
        }
    }

    /**
     * Função que deletar uma foto do fornecedor
     * apartir do seu id via ajax
     * 
     * @name deletarFotoAction
     * @access public
     * @param void
     * @return json_encode 
     */
    public function deletarFotoAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ($this->_request->isPost()) {
            $data = $this->_request->getPost();
            $fornecedor = new Frontend_Model_DbTable_TbFornecedorFoto();
            $foto = $fornecedor->findFotoById($data['id_foto']);
            if ($foto) {
                if ($fornecedor->deletarFotoById($data['id_foto'])) {
                    unlink(PUBLIC_PATH . '/uploads/modules/front-end/empresa/fotos/' . $foto['vc_foto']);
                    echo json_encode(true);
                } else {
                    echo json_encode(false);
                }
            } else {
                echo json_encode(false);
            }
        }
    }

    /**
     * Função para o upload de fotos de fornecedores,
     * transfere o arquivo enviado pelo usuário para o servidor e realiza 
     * a inserção no banco de dados
     * 
     * @name uploadFotoAction
     * @access public
     * @param void 
     * @return void 
     */
    public function uploadFotoAction() {
        require_once LIB_PATH . '/Canvas/canvas.php';
        $this->_helper->layout()->disableLayout();
        $upload = new Zend_File_Transfer();
        $arquivoInfo = $upload->getFileInfo();

        $cookieCadastroFornecedor = unserialize($this->_request->getCookie($this->bootstrap->getOption('empresaCookieName')));
        $fornecedor = new Frontend_Model_DbTable_TbFornecedorFoto();
        if (!empty($cookieCadastroFornecedor['id_fornecedor'])) {
            if ($this->_request->isPost()) {
                $upload = new Zend_File_Transfer();
                $arquivoInfo = $upload->getFileInfo();

                if ($arquivoInfo['vc_foto']['error'] == 0) {
                    $adapter = new Zend_File_Transfer_Adapter_Http();
                    $adapter->setDestination(PUBLIC_PATH . '/uploads/modules/front-end/empresa/fotos/');

                    $idFoto = $fornecedor->getProximoIdFoto($cookieCadastroFornecedor['id_fornecedor']);
                    if ($idFoto) {
                        $extensaoFoto = end(explode('.', $arquivoInfo['vc_foto']['name']));
                        $nomeNovo = $cookieCadastroFornecedor['id_fornecedor'] . $idFoto . date('Ymdhms') . '.' . $extensaoFoto;

                        $adapter->addFilter('Rename', array('target' => $adapter->getDestination() . '/' . $nomeNovo, 'overwrite' => true));
                        if ($adapter->receive()) {
                            $canvas = new canvas();
                            $canvas->carrega($adapter->getDestination() . '/' . $nomeNovo);
                            $canvas->redimensiona(150, 150, 'crop');
                            $canvas->grava($adapter->getDestination() . '/miniaturas/' . $nomeNovo);
                        }

                        $fornecedor->atualizarNomeFoto($idFoto, $nomeNovo);
                    }
                }
            }

            $this->view->id_fornecedor = $cookieCadastroFornecedor['id_fornecedor'];
            $this->view->foto = $fornecedor->findFotosByFornecedor($cookieCadastroFornecedor['id_fornecedor']);
        }
    }

    /**
     * 
     * @return boolean
     */
    public function verificaTotalFotosAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $cookieCadastroFornecedor = unserialize($this->_request->getCookie($this->bootstrap->getOption('empresaCookieName')));
        if (!empty($cookieCadastroFornecedor['id_fornecedor'])) {
            $fornecedor = new Frontend_Model_DbTable_TbFornecedorFoto();
            $array = $fornecedor->findFotosByFornecedor($cookieCadastroFornecedor['id_fornecedor']);
            $total = count($array);

            if ($total < 8) {
                echo false;
            } else {
                echo true;
            }
        } else {
            echo false;
        }
    }

    /**
     * Função que exibe o questionário cadastrado pelo administrador
     * para o fornecedor de acordo com a escolha feita na opção Setor de Atividade
     * 
     * @name fornecedorQuestionarioAction
     * @access public
     * @param void
     * @return void 
     */
    public function questionarioAction() {
        $this->view->breadcrumb = array(
            array("legenda" => "Home", "link" => "/index", 'class' => 'primeiro'),
            array("legenda" => "Acesso Empresa", "link" => "/acesso-empresa", 'class' => ''),
            array("legenda" => "Cadastro Empresa", "link" => "/acesso-empresa/cadastro", 'class' => ''),
            array("legenda" => "Informações Básicas", "link" => "javascript:;", 'class' => 'ultimo')
        );

        $cookieCadastroFornecedor = unserialize($this->_request->getCookie($this->bootstrap->getOption('empresaCookieName')));

        if (!empty($cookieCadastroFornecedor['id_fornecedor']) && !empty($cookieCadastroFornecedor['id_setor_de_atividade'])) {
            if ($this->_request->isPost()) {
                $tbFornecedor = new Frontend_Model_DbTable_TbFornecedor();
                $tbFornecedorResposta = new Frontend_Model_DbTable_TbFornecedorResposta();

                $tbFornecedorResposta->inserirResposta($this->_request->getPost(), $cookieCadastroFornecedor['id_fornecedor']);
                $data = $tbFornecedor->findFornecedorById($cookieCadastroFornecedor['id_fornecedor']);
                if ($data) {
                    $mensagem = $this->view->partial('/acesso-empresa/partial/email-confirmacao-cadastro.phtml', array('data' => $data));
                    $tbFornecedor->enviarEmailConfirmacaoCadastro($mensagem, $data['vc_acesso_email']);
                }

                $this->_redirect('/frontend/acesso-empresa/cadastro-realizado/');
            }

            $tbFornecedorQuestionario = new Frontend_Model_DbTable_TbFornecedorQuestionario();
            $questionarioFornecedor = $tbFornecedorQuestionario->findQuestionarioBySetorAtividade($cookieCadastroFornecedor['id_setor_de_atividade']);
            $questoesObrigatorias = array();

            foreach ($questionarioFornecedor as $key => $array) {
                if ($array['vc_campo'] == 'radio' || $array['vc_campo'] == 'checkbox' || $array['vc_campo'] == 'select') {
                    $opcoes = $tbFornecedorQuestionario->findCampoOpcoes($array['id_tipo_campo'], $array['id_questionario']);
                    if ($opcoes) {
                        $arrayOpcoes = array();

                        foreach ($opcoes as $opcao) {
                            $arrayOpcoes[$opcao['id_opcao']] = $opcao['vc_valor_opcao'];
                        }
                        $questionarioFornecedor[$key]['opcoes'] = $arrayOpcoes;
                    }
                }

                if ($array['bl_campo_obrigatorio'] == 'sim') {
                    $questoesObrigatorias[$array['id_questionario']] = $array['id_setor_de_atividade'];
                }
            }

            $this->view->action = '/frontend/acesso-empresa/questionario/';
            $this->view->questionarioFornecedor = $questionarioFornecedor;
            $this->view->questoesObrigatoriasJson = json_encode($questoesObrigatorias);
        } else {
            $this->_redirect('/frontend/acesso-empresa/cadastro/');
        }
    }

    /**
     * Função que exibe para o fornecedor a conclusão de seu cadastro
     * 
     * @name cadastroRealizadoAction
     * @access public
     * @param void
     * @return void 
     */
    public function cadastroRealizadoAction() {
        $cookieCadastroFornecedor = unserialize($this->_request->getCookie($this->bootstrap->getOption('empresaCookieName')));

        if (!empty($cookieCadastroFornecedor['id_fornecedor'])) {
            
        } else {
            $this->_redirect('/frontend/acesso-empresa/cadastro/');
        }
    }

    /**
     * Função que confirma o cadastro do fornecedor 
     * através do e-mail enviado ao final do cadastro
     * 
     * @name confirmacaoCadastroAction
     * @param void
     * @return void  
     */
    public function confirmacaoCadastroAction() {
        $this->view->headTitle('Confirmar Cadastro');
        $this->view->headMeta()->appendName('description', '');
        $this->view->headMeta()->appendName('keywords', '');

        $token = $this->_request->getParam('token');
        $data = $this->_tbFornecedor->findByIdMd5($token);

        if (!strlen($token) == 32 || !$data) {
            $this->_redirect('/acesso/empresa');
        } else {
            if ($this->_tbFornecedor->confirmarCadastro($token)) {
                $data['mensagem'] = 'Cadastro confirmado com sucesso !';
            } else {
                $data['mensagem'] = 'Ocorreu um erro ao confirmar o seu cadastro, caso ja tenha confirmado seu cadastro ignore essa mensagem';
            }
            $this->view->data = $data;
        }
    }

    /**
     * Função para enviar um e-mail para o fornecedor contendo os 
     * passos necessários para a redefinição de sua senha de acesso
     * 
     * @name esqueceuSenhaAction
     * @access public
     * @param void
     * @return void
     */
    public function esqueceuSenhaAction() {
        $this->view->headTitle('Esqueci Minha Senha');
        $this->view->headMeta()->appendName('description', '');
        $this->view->headMeta()->appendName('keywords', '');

        if ($this->_request->isPost()) {
            $request = $this->_request->getPost();
            if (!empty($request['vc_acesso_email'])) {
                $data = $this->_tbFornecedor->findByEmail($request['vc_acesso_email']);
                if ($data) {
                    $mensagem = $this->view->partial('/cadastro/partial/email-recuperar-senha.phtml', array('data' => $data));
                    $this->_tbFornecedor->enviarEmailRecuperarSenha($mensagem, $data['vc_acesso_email']);

                    $this->view->mensagem = 'Um e-mail foi enviado com os passos para redefinir a senha de acesso';
                } else {
                    $this->view->mensagem = 'O e-mail informado não consta em nosso sistema';
                }
            }
        }
    }

    /**
     * Função que efetiva a recuperação da senha
     * fazendo a atualização na base de dados
     * 
     * @name recuperarSenhaAction
     * @access public
     * @param void 
     * @return void 
     */
    public function recuperarSenhaAction() {
        $this->view->headTitle('Recuperar Senha de Acesso');
        $this->view->headMeta()->appendName('description', '');
        $this->view->headMeta()->appendName('keywords', '');

        $token = $this->_request->getParam('token');
        $data = $this->_tbFornecedor->findByIdMd5($token);

        if (!strlen($token) == 32 || !$data) {
            $this->_redirect('/');
        } else {
            $data['token'] = $token;
            $this->view->data = $data;

            if ($this->_request->isPost()) {
                $request = $this->_request->getPost();
                if ($this->_tbFornecedor->atualizarSenha($request)) {
                    $this->view->mensagem = 'Sua senha foi atualizada com sucesso!';
                } else {
                    $this->view->mensagem = 'Ocorreu um erro durante a atualização da senha de acesso, por favor tente mais tarde';
                }
            }
        }
    }

    /**
     * Função que é usada por ajax para verificar
     * se o e-mail informado exite na base de dados
     * 
     * @name verificaEmailAction
     * @access public
     * @param void
     * @return json_encode 
     */
    public function verificaEmailAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ($this->_request->isPost()) {
            $request = $this->_request->getPost();
            $_tbFornecedor = new Frontend_Model_DbTable_TbFornecedor();
            $findByEmail = $_tbFornecedor->findByEmail($request['vc_acesso_email']);

            echo json_encode($findByEmail);
        }
    }

    /**
     * Função para buscar o endereço, número etc através do cep informado
     * essa função é acessada apenas por ajax
     * 
     * @name buscarEnderecoPorCepAction
     * @access public
     * @param void
     * @return json_encode 
     */
    public function buscarEnderecoPorCepAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $cep = $this->_request->getParam('cep');
        $tbCliente = new Frontend_Model_DbTable_TbCliente();
        $endereço = $tbCliente->findAddressFromCep($cep);

        echo json_encode($endereço);
    }

    /**
     * Função usada para realizar o login do fornecedor a sua área restrita
     * 
     * @name acessoAction
     * @access public
     * @param void
     * @return void 
     */
    public function acessoAction() {
        $this->view->headTitle('Faça seu Login');
        $this->view->headMeta()->appendName('description', '');
        $this->view->headMeta()->appendName('keywords', '');

        if ($this->_request->isPost()) {
            $tbFornecedor = new Frontend_Model_DbTable_TbFornecedor();
            $data = $tbFornecedor->acesso($this->_request->getPost());
            if ($data) {
                if ($data['bl_ativo'] == 'nao') {
                    $this->view->mensagem = $data['vc_nome_empresa'] . ' seu cadastro ainda não foi ativado, cheque sua caixa de e-mail para confirmar seu registro';
                } else {
                    $sessaoFornecedor = new Zend_Session_Namespace('Sessao_Fornecedor');
                    $sessaoFornecedor->arr = $data;
                    $this->_redirect('/');
                }
            } else {
                $this->view->mensagem = 'Cadastro não encontrado';
            }
        }
    }

    public function formaPagamentoAction() {
        
    }

    public function salvarLogotipoAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ($this->_request->isPost()) {
            $json = array();

            $request = $this->_request->getPost();
            if (empty($request['id_foto']) || !(int) $request['id_foto']) {
                $json['erro'] = true;
                $json['mensagem'] = 'Não foi possível selecionar a foto desejada para ser o Logotipo';
            } else if (empty($request['id_fornecedor']) || !(int) $request['id_fornecedor']) {
                $json['erro'] = true;
                $json['mensagem'] = 'Não foi possível vincular a foto da empresa ao Logotipo';
            } else {

                $fornecedor = new Frontend_Model_DbTable_TbFornecedorFoto();
                if ($fornecedor->salvarLogotipo($request)) {
                    $json['erro'] = false;
                    $json['mensagem'] = 'Logotipo definido com sucesso!';
                } else {
                    $json['erro'] = true;
                    $json['mensagem'] = 'Ocorreu um erro ao salvar o Logotipo';
                }
            }

            echo json_encode($json);
        }
    }

    public function salvarFotoPrincipalAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ($this->_request->isPost()) {
            $json = array();

            $request = $this->_request->getPost();
            if (empty($request['id_foto']) || !(int) $request['id_foto']) {
                $json['erro'] = true;
                $json['mensagem'] = 'Não foi possível selecionar a foto desejada para ser a Foto Principal';
            } else if (empty($request['id_fornecedor']) || !(int) $request['id_fornecedor']) {
                $json['erro'] = true;
                $json['mensagem'] = 'Não foi possível vincular a foto da empresa a Foto Principal';
            } else {

                $fornecedor = new Frontend_Model_DbTable_TbFornecedorFoto();
                if ($fornecedor->salvarFotoPrincipal($request)) {
                    $json['erro'] = false;
                    $json['mensagem'] = 'Foto Principal definida com sucesso!';
                } else {
                    $json['erro'] = true;
                    $json['mensagem'] = 'Ocorreu um erro ao salvar a Foto Principal';
                }
            }

            echo json_encode($json);
        }
    }

    public function atualizarOrdemAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ($this->_request->isPost()) {
            $json = array();

            $request = $this->_request->getPost();
            if (empty($request['id_fornecedor']) || !(int) $request['id_fornecedor']) {
                $json['erro'] = true;
                $json['mensagem'] = 'Não foi possível atualizar a ordem das fotos, não foi localizada  a empresa';
            } else {

                $fornecedor = new Frontend_Model_DbTable_TbFornecedorFoto();
                if ($fornecedor->atualizarOrdem($request)) {
                    $json['erro'] = false;
                    $json['mensagem'] = '';
                } else {
                    $json['erro'] = true;
                    $json['mensagem'] = 'Ocorreu um erro ao salvar a nova ordem das fotos';
                }
            }

            echo json_encode($json);
        }
    }

}