<?php

/**
 * Controller da Index do FrontEnd
 * @author André Gomes @acidcode
 * @since 16-04-2012
 * @copyright GPO4  - www.cachorrogato.com.br - andre.kernelpanic@gmail.com
 *
 */
class Frontend_IndexController extends Zend_Controller_Action {

    public function init() {
        //para iniciar as sessões em todas requisiçoes. Necessario para validar 
        //a autenticação do usuario
    }

    public function indexAction() {

        $this->view->headTitle("");
        $this->view->headMeta()->appendName('description', '');
        $this->view->headMeta()->appendName('keywords', '');
        $this->view->idPage = "index";

        $tbRacaGato = new Backend_Model_DbTable_TbRacaGato();
        $this->view->racasGatos = $tbRacaGato->findAll();
    }

}