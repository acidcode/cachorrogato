<?php
/**
 * Service de autenticação e validação de usuários
 * @author @acidcode
 * @since 02-03-2011
 * @copyright GPO4  - www.cachorrogato.com.br - andre.kernelpanic@gmail.com
 */
class Backend_Service_Autenticacao {
    
    public function autenticar($request) {

        if (!$request->isPost()) {
            return false;
        }

        $data = $request->getPost();
        
        if(empty($data['vc_usuario']) || empty($data['vc_senha'])) {
            return false;
        }
        //Instancia a classe de autenticacao
        $loginAuth = Zend_Auth::getInstance();
        //Busca no banco os dados do Usuario
        $loginAuthAdpter = new Zend_Auth_Adapter_DbTable(
                Zend_Registry::get('db'),
                'usuarios',
                'login',
                'senha',
                'md5(?)');

        //Seta os dados inseridos no post
        $loginAuthAdpter->setIdentity($data['vc_usuario'])
                ->setCredential($data['vc_senha']);

        //Autentica
        $result = $loginAuth->authenticate($loginAuthAdpter);

        if ($result->isValid()) {
            $authData = $loginAuthAdpter->getResultRowObject(null, 'vc_password');
            $loginAuth->getStorage()->write($authData);

            return true;
            
        } else {
            
            return false;
            
        }
    }
}
