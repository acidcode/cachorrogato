<?php

/**
 * Classe GD 3
 *
 * Esta classe manipula uma imagem de forma simples e descomplicada
 * @author     Marcos Borges  <contato@marcosborges.com>
 * @package     Agressive.Image
 * @version    1.0
 *
 *    #$gd = new Agressive_Image_Gd("./DSC_0096.jpg");
 *    #$gd->setFormatOutPut("jpg");
 *    
 *    #$gd->effectUnSharpMask();
 *    #$gd->effectNegate();
 *    #$gd->effectGrayscale();
 *    #$gd->effectEdgeDetect();
 *    #$gd->effectSelectiveBlur();
 *    #$gd->effectContrast(-50);
 *    #$gd->effectBrightness(-10);
 *    #$gd->effectGusianBlur(20);
 *    #$gd->effectSmooth(100);
 *    #$gd->effectEmboss();
 *    #$gd->effectMeanRemoval();
 *    #$gd->effectSepia();
 *    #$gd->effectColorize(10,100,255);
 *    #$gd->imageRotate(45);
 *    #$gd->setTransparent("#000000");
 *    
 *    
 *    $gd->resizeImage(     1000, //$newWidth,
 *                        1000, //$newHeight,
 *                        1000, //$canvasWidth = null,
 *                        1000, //$canvasHeight = null ,
 *                        "center", //$positionVertical = "center",
 *                        "center", //$positionHorizontal = "center",
 *                        false, //$overSizeEnable = true,
 *                        false //$deformationEnable = true
 *                      );
 *    
 *    #$gd->save('teste/file_name'); //sem a exteno, ela  fornecida pelo metodo setFormatOutPut
 *    #$gd->show();
 *        
 */
class Frontend_Service_Gd {

    private $_imageHandler;           # Resource da Imagem
    private $_bgColorRed = 255;       # Quantidade de vermelho de 0 a 255 (int)
    private $_bgColorGreen = 255;     # Quantidade de verde de 0 a 255 (int)
    private $_bgColorBlue = 255;      # Quantidade de azul de 0 a 255 (int)
    private $_quality = 100;             # Qualidade da saida da imagem somente para jpeg de 0 a 100 (int)
    private $_format;                 # Formato da imagem gif, jpg, png (string)
    private $_formatAllow = array('gif', 'jpg', 'png');     # lista de extenes permitidas somente gif, jpg e png;
    private $_formatOutPut = null;                         # Formato da saida somente gif, jpg e png (string)

    /**
     * setImageHandler    
     *
     * @param      (resource link)    $arg
     * @return     (void)
     * @access     public
     */

    public function setImageHandler($arg) {
        $this->_imageHandler = $arg;
    }

    /**
     * getImageHandler    
     *
     * @return     (resource link)
     * @access     public
     */
    public function getImageHandler() {
        return $this->_imageHandler;
    }

    /**
     * setBgColorRed    
     *
     * @param      (int)    $arg de 0 a 255
     * @return     (void)
     * @access     public
     */
    public function setBgColorRed($arg) {
        $this->_bgColorRed = $arg;
    }

    /**
     * getBgColorRed    
     *
     * @return     (int)
     * @access     public
     */
    public function getBgColorRed() {
        return $this->_bgColorRed;
    }

    /**
     * setBgColorGreen    
     *
     * @param      (int)    $arg de 0 a 255
     * @return     (void)
     * @access     public
     */
    public function setBgColorGreen($arg) {
        $this->_bgColorGreen = $arg;
    }

    /**
     * getBgColorGreen    
     *
     * @return     (int)
     * @access     public
     */
    public function getBgColorGreen() {
        return $this->_bgColorGreen;
    }

    /**
     * setBgColorGreen    
     *
     * @param      (int)    $arg de 0 a 255
     * @return     (void)
     * @access     public
     */
    public function setBgColorBlue($arg) {
        $this->_bgColorBlue = $arg;
    }

    /**
     * getBgColorBlue    
     *
     * @return     (int)
     * @access     public
     */
    public function getBgColorBlue() {
        return $this->_bgColorBlue;
    }

    /**
     * setQuality    
     *
     * @param      (int)    $arg de 0 a 100
     * @return     (void)
     * @access     public
     */
    public function setQuality($arg) {
        $this->_quality = $arg;
    }

    /**
     * getQuality    
     *
     * @return     (int)
     * @access     public
     */
    public function getQuality() {
        return $this->_quality;
    }

    /**
     * setFormat    
     *
     * @param      (string)    $arg "jpg"
     * @return     (void)
     * @access     public
     */
    public function setFormat($arg) {
        $this->_format = $arg;
    }

    /**
     * getFormat    
     *
     * @return     (string)
     * @access     public
     */
    public function getFormat() {
        return $this->_format;
    }

    /**
     * setFormatAllow
     *
     * @param      (array)    $arg
     * @return     (void)
     * @access     public
     */
    public function setFormatAllow($arg) {
        $this->_formatAllow = $arg;
    }

    /**
     * getFormatAllow    
     *
     * @return     (array)
     * @access     public
     */
    public function getFormatAllow() {
        return $this->_formatAllow;
    }

    /**
     * setFormatOutPut
     *
     * @param      (string)    $arg "jpg"
     * @return     (void)
     * @access     public
     */
    public function setFormatOutPut($arg) {
        $this->_formatOutPut = $arg;
    }

    /**
     * getFormatOutPut    
     *
     * @return     (string)
     * @access     public
     */
    public function getFormatOutPut() {
        return $this->_formatOutPut;
    }

    /**
     * setFormatOutPut
     *
     * @param      (array)    $arg array("R"=>10,"G"=>10, "B"=>10);
     * @return     (void)
     * @access     public
     */
    public function setbgColor($arg) {
        $this->setBgColorRed($arg['R']);
        $this->setBgColorGreen($arg['G']);
        $this->setBgColorBlue($arg['B']);
    }

    /**
     * __construct
     *
     * @param      (string) $arg local do arquivo
     * @return     (void)
     * @access     public
     */
    public function __construct($arg = null) {
        $this->init($arg);
    }

    /**
     * init
     *
     * @param      (string) $arg local do arquivo
     * @return     (void)
     * @access     public
     */
    public function init($arg = null) {
        # Checa a verso do php e as funes necessrias para rodar essa classe
        $this->checkPHPVersion();
        # Inicia o bgcolor com branco
        $this->setbgColor(array(
            "R" => 255,
            "G" => 255,
            "B" => 255,
        ));
        # Define a qualidade de saida em 100%    
        $this->setQuality(100);
        # Define quais as extenes permitidas
        $this->setFormatAllow(array("gif", "jpg", "png"));
        if (is_file($arg) && $this->IfSupportedImageTypes($arg)) {
            $this->loadFile($arg);
        }
    }

    /**
     * __construct
     *
     * VERIFICA SE AS FUNES ESSENCIAIS UTILIZADAS NESSA CLASSE EXISTEM
     *
     * @return     (bool)
     * @access     private
     */
    private function checkPHPVersion() {
        #FUNES DE IMAGEM
        if (!(
                function_exists('imagesx') &&
                function_exists('imagesy') &&
                function_exists('imagefilter') &&
                function_exists('imagecreatefromgif') &&
                function_exists('imagecreatefromjpeg') &&
                function_exists('imagecreatefrompng') &&
                function_exists('imagerotate') &&
                function_exists('imagecolorallocate') &&
                function_exists('imagecreatetruecolor') &&
                function_exists('imagecolortransparent') &&
                function_exists('imagecopymerge') &&
                function_exists('imagecopy') &&
                function_exists('imagestring') &&
                #FUNES GERAIS
                function_exists('version_compare') &&
                function_exists('phpversion') &&
                function_exists('is_null') &&
                function_exists('header') &&
                #FUNES FILE SYSTEM
                function_exists('is_file') &&
                #FUNES DE ARRAY
                function_exists('explode') &&
                function_exists('count') &&
                #FUNES DE STRING
                function_exists('strpos') &&
                function_exists('substr') &&
                function_exists('strlen') &&
                function_exists('wordwrap') &&
                function_exists('trim') &&
                #FUNes MATEMATICAS
                function_exists('ceil') &&
                function_exists('hexdec') &&
                function_exists('abs') &&
                function_exists('round') &&
                function_exists('max') &&
                function_exists('min')
                )
        ) {
            //die('Alguma funo necessria para a execuo desta classe no foi encontrada. Verifique Method -> checkPHPVersion');
        }
        if (version_compare("5.0.0", phpversion(), "<") == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * ifSupportedImageTypes
     *
     * @param      (string) $arg local do arquivo
     * @return     (bool)
     * @access     public
     */
    public function ifSupportedImageTypes($file) {
        if (is_file($file)) {
            if (in_array($this->getFormatFile($file), $this->getFormatAllow())) {
                return true;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * loadFile
     *
     * carrega um arquivo de imagem para o atributo _imageHandler
     *
     * @param      (string) $arg local do arquivo
     * @return     (void)
     * @access     public
     */
    public function loadFile($file) {
        if ($this->ifSupportedImageTypes($file)) {
            $ext = $this->getFormatFile($file);
            switch ($ext) {
                case "gif":
                    $this->setImageHandler(imagecreatefromgif($file));
                    break;
                case "jpg":
                    $this->setImageHandler(imagecreatefromjpeg($file));
                    break;
                case "png":
                    $this->setImageHandler(imagecreatefrompng($file));
                    break;
                default:
                    $this->createErrorImage("formato invlido.");
                    return false;
                    break;
            }
            $this->setFormat($ext);
            return true;
        }
    }

    /**
     * createImage
     *
     * cria uma imagem para o atributo _imageHandler
     *
     * @param      (int) $x largura
     * @param      (int) $y altura
     * @return     (void)
     * @access     public
     */
    public function createImage($x = 0, $y = 0) {
        $newImage = imagecreatetruecolor($x, $y);
        imagefill($newImage, 0, 0, imagecolorallocate(
                        $newImage, $this->getBgColorRed(), $this->getBgColorGreen(), $this->getBgColorBlue()
                )
        );
        $this->setImageHandler($newImage);
    }

    /**
     * getFormatFile
     *
     * Obtem o formato do arquivo
     *
     * @param      (string) $file caminho do arquivo
     * @return     (string) jpg ou gif ou png
     * @access     public
     */
    public function getFormatFile($file) {
        $format = "";
        $ext = explode(".", $file);
        $ext = $ext[count($ext) - 1];
        switch ($ext) {
            case "jpg":
                $format = 'jpg';
                break;
            case "gif":
                $format = 'gif';
                break;
            case "png":
                $format = 'png';
                break;
        }
        return $format;
    }

    /**
     * show
     *
     * imprime os headers do tipo da imagem e imprime a imagem no atributo _imageHandler
     *
     * @return     (void)
     * @access     public
     */
    public function show() {
        if (!is_null($this->getFormatOutPut()) || trim($this->getFormatOutPut()) != "") {
            $type = $this->getFormatOutPut();
        } elseif (!is_null($this->getFormat()) || trim($this->getFormat()) != "") {
            $type = $this->getFormat();
        } else {
            $type = "jpg";
        }
        switch ($type) {
            case "gif":
                header('Content-type: image/gif');
                imagegif($this->getImageHandler());
                break;
            case "jpg":
            case "jpeg":
                header('Content-type: image/jpeg');
                imagejpeg($this->getImageHandler());
                break;
            case "png":
                header('Content-type: image/png');
                imagepng($this->getImageHandler());
                break;
        }
        exit;
    }

    /**
     * save
     *
     * imprime os headers do tipo da imagem e imprime a imagem no atributo _imageHandler
     *
     * @param      (string) $nome do arquivo a ser salvo sem exteno
     * @return     (void)
     * @access     public
     */
    public function save($nome) {
        if (!is_null($this->getFormatOutPut()) || trim($this->getFormatOutPut()) != "") {
            $type = $this->getFormatOutPut();
        } elseif (!is_null($this->getFormat()) || trim($this->getFormat()) != "") {
            $type = $this->getFormat();
        } else {
            $type = "jpg";
        }

        switch ($type) {
            case "gif":
                imagegif($this->getImageHandler(), $nome . "." . $this->getFormatOutPut());
                break;
            case "jpg":
            case "jpeg":
                imagejpeg($this->getImageHandler(), $nome . "." . $this->getFormatOutPut(), $this->getQuality());
                break;
            case "png":

                imagepng($this->getImageHandler(), $nome . "." . $this->getFormatOutPut());
                break;
        }
        echo "[" . $nome . "." . $this->getFormatOutPut() . "]";
        @chmod($nome . "." . $this->getFormatOutPut(), 0777);
        return true;
    }

    /**
     * resizeImage
     *
     * redimensiona a imagem contida em _imageHandler, tambm fornece opes de alinhamento e crop.
     *
     * @param      (int) $newWidth nova largura,
     * @param      (int) $newHeight nova altura,
     * @param      (int) $canvasWidth largura do palco,
     * @param      (int) $canvasHeight  largura do palco ,
     * @param      (string/int) $positionVertical posio vertical (center, top, bottom, 10)
     * @param      (string/int) $positionHorizontal  posio horizontal (left, center, right, 100)
     * @param      (string) $overSizeEnable habilita ampliao da imagem acima de 100% de seu tamanho,
     * @param      (string) $deformationEnable habilita a deformao da imagem (no usa proporo)
     * @return     (void)
     * @access     public
     */
    public function resizeImage($newWidth, $newHeight, $canvasWidth = null, $canvasHeight = null, $positionVertical = "center", $positionHorizontal = "center", $overSizeEnable = true, $deformationEnable = true) {

        $width = imagesx($this->getImageHandler());
        $height = imagesy($this->getImageHandler());

        if (!$overSizeEnable) {
            if ($width < $newWidth || $height < $newHeight) {
                $newWidth = $width;
                $newHeight = $height;
            }
        }

        if (!$deformationEnable) {

            #redimensiona calcula a proporo da imagem         
            if ($newWidth >= $newHeight) {
                $sizePercent = (int) ($width / ($newWidth / 100));
                $widthAux = $width;
                $heightAux = ($sizePercent * ($newHeight / 100));
            } else {
                $sizePercent = (int) ($height / ($newHeight / 100));
                $heightAux = $height;
                $widthAux = ($sizePercent * ($newWidth / 100));
            }
            #verifica se a proporo no est correta.
            if ($widthAux == $width && $heightAux == $height) {
                $newWidth = $widthAux;
                $newHeight = $heightAux;
            } else {
                if ($width >= $height) {
                    $sizePercent = (int) ($newWidth / ($width / 100));
                    $heightAux = ($sizePercent * ($height / 100));
                    $newHeight = $heightAux;
                } else {
                    //$newHeight = $height;
                    $sizePercent = (int) ($newHeight / ($height / 100));
                    $widthAux = ($sizePercent * ($width / 100));
                    $newWidth = $widthAux;
                }
            }
        }

        if (is_null($canvasWidth)) {
            $canvasWidth = $newWidth;
        }
        if (is_null($canvasHeight)) {
            $canvasHeight = $newHeight;
        }

        switch ($positionVertical) {
            case "center":
                $y = ($canvasHeight / 2) - ($newHeight / 2);
                break;
            case "top":
                $y = 0;
                break;
            case "bottom":
                $y = $canvasHeight - $newHeight;
                break;
            default:
                if (is_numeric($positionVertical)) {
                    $y = $positionVertical;
                } else {
                    $y = 0;
                }
                break;
        }
        switch ($positionHorizontal) {
            case "center":
                $x = ($canvasWidth / 2) - ($newWidth / 2);
                break;
            case "left":
                $x = 1;
                break;
            case "right":
                $x = $canvasWidth - $newWidth;
                break;
            default:
                if (is_numeric($positionHorizontal)) {
                    $x = $positionHorizontal;
                } else {
                    $x = 0;
                }
                break;
        }
        #PARTE QUE REDIMENCIONA
        $newImage = imagecreatetruecolor($newWidth, $newHeight);
        imagefill(
                $newImage, 0, 0, imagecolorallocate(
                        $newImage, $this->getBgColorRed(), $this->getBgColorGreen(), $this->getBgColorBlue()
                )
        );

        imagecopyresampled($newImage, $this->getImageHandler(), 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
        #PARTE DO PALCO
        $newPalco = imagecreatetruecolor($canvasWidth, $canvasHeight);
        imagefill(
                $newPalco, 0, 0, imagecolorallocate(
                        $newPalco, $this->getBgColorRed(), $this->getBgColorGreen(), $this->getBgColorBlue()
                )
        );
        imagecopyresampled($newPalco, $newImage, $x, $y, 0, 0, $newWidth, $newHeight, $newWidth, $newHeight);
        $this->setImageHandler($newPalco);
        return true;
    }

    /**
     * effectNegate
     *
     * Aplica o efeito de negativo em _imageHandler
     *
     * @return     (void)
     * @access     public
     */
    public function effectNegate() {
        imagefilter($this->_imageHandler, IMG_FILTER_NEGATE);
    }

    /**
     * effectGrayscale
     *
     * Aplica o efeito de negativo em _imageHandler
     *
     * @return     (void)
     * @access     public
     */
    public function effectGrayscale() {
        imagefilter($this->_imageHandler, IMG_FILTER_GRAYSCALE);
    }

    /**
     * effectEdgeDetect
     *
     * Aplica o efeito de Edge Detect em _imageHandler
     *
     * @return     (void)
     * @access     public
     */
    public function effectEdgeDetect() {
        imagefilter($this->_imageHandler, IMG_FILTER_EDGEDETECT);
    }

    /**
     * effectSelectiveBlur
     *
     * Aplica o efeito de Selective Blur em _imageHandler
     *
     * @return     (void)
     * @access     public
     */
    public function effectSelectiveBlur() {
        imagefilter($this->_imageHandler, IMG_FILTER_SELECTIVE_BLUR);
    }

    /**
     * effectContrast
     *
     * Aplica o efeito de Contrast em _imageHandler
     *
     * @param      (int) $val,
     * @return     (void)
     * @access     public
     */
    public function effectContrast($val) {
        imagefilter($this->_imageHandler, IMG_FILTER_CONTRAST, $val);
    }

    /**
     * effectBrightness
     *
     * Aplica o efeito de Brightness em _imageHandler
     *
     * @param      (int) $val,
     * @return     (void)
     * @access     public
     */
    public function effectBrightness($val) {
        imagefilter($this->_imageHandler, IMG_FILTER_BRIGHTNESS, $val);
    }

    /**
     * effectGusianBlur
     *
     * Aplica o efeito de Gusian Blur em _imageHandler
     *
     * @param      (int) $val,
     * @return     (void)
     * @access     public
     */
    public function effectGusianBlur($val) {
        imagefilter($this->_imageHandler, IMG_FILTER_GAUSSIAN_BLUR, $val);
    }

    /**
     * effectSmooth
     *
     * Aplica o efeito de Smooth em _imageHandler
     *
     * @param      (int) $val,
     * @return     (void)
     * @access     public
     */
    public function effectSmooth($val) {
        imagefilter($this->_imageHandler, IMG_FILTER_SMOOTH, $val);
    }

    /**
     * effectEmboss
     *
     * Aplica o efeito de Emboss em _imageHandler
     *
     * @return     (void)
     * @access     public
     */
    public function effectEmboss() {
        imagefilter($this->_imageHandler, IMG_FILTER_EMBOSS);
    }

    /**
     * effectMeanRemoval
     *
     * Aplica o efeito de Mean Removal em _imageHandler
     *
     * @return     (void)
     * @access     public
     */
    public function effectMeanRemoval() {
        imagefilter($this->_imageHandler, IMG_FILTER_MEAN_REMOVAL);
    }

    /**
     * effectColorize
     *
     * Aplica o efeito de Colorize em _imageHandler
     *
     * @return     (void)
     * @param      (int) $R,
     * @param      (int) $G,
     * @param      (int) $B,    
     * @access     public
     */
    public function effectColorize($R, $G, $B) {
        imagefilter($this->_imageHandler, IMG_FILTER_COLORIZE, $R, $G, $B);
    }

    /**
     * effectSepia
     *
     * Aplica o efeito de Sepia em _imageHandler
     *
     * @return     (void)
     * @access     public
     */
    public function effectSepia() {
        $this->effectGrayscale();
        $this->effectColorize(90, 60, 40);
        return true;
    }

    /**
     * imageRotate
     *
     * Rotaciona _imageHandler conforme o angulo
     *
     * @param      (int) $degrees angulo,
     * @return     (void)
     * @access     public
     */
    public function imageRotate($degrees) {
        $this->_imageHandler = imagerotate($this->_imageHandler, $degrees, imagecolorallocate($this->_imageHandler, $this->getBgColorRed(), $this->getBgColorGreen(), $this->getBgColorBlue()));
    }

    /**
     * setTransparent
     *
     * Aplica o efeito de Selective Transparent em _imageHandler
     *
     * @param      (string) $val, #000000 hexadecimal da cor
     * @return     (void)
     * @access     public
     */
    public function setTransparent($hexcolor) {
        $rgb = $this->Hex2Rgb($hexcolor);
        $trans = imagecolorallocate($this->_imageHandler, $rgb['R'], $rgb['G'], $rgb['B']);
        imagecolortransparent($this->_imageHandler, $trans);
    }

    /**
     * hex2Rgb
     *
     * converte hex para rgb
     *
     * @param      (string) $hex, #000000 hexadecimal da cor
     * @return     (array)
     * @access     private
     */
    private function hex2Rgb($hex) {
        if (0 === strpos($hex, '#')) {
            $hex = substr($hex, 1);
        } else if (0 === strpos($hex, '&H')) {
            $hex = substr($hex, 2);
        } else if (0 === strpos($hex, 'x')) {
            $hex = substr($hex, 2);
        }
        $cutpoint = ceil(strlen($hex) / 2) - 1;
        $rgb = explode(':', wordwrap($hex, $cutpoint, ':', $cutpoint), 3);
        $rgb_aux['R'] = (isset($rgb[0]) ? hexdec($rgb[0]) : 0);
        $rgb_aux['G'] = (isset($rgb[1]) ? hexdec($rgb[1]) : 0);
        $rgb_aux['B'] = (isset($rgb[2]) ? hexdec($rgb[2]) : 0);
        return $rgb_aux;
    }

    /**
     * unSharpMask
     *
     * Aplica o efeito de unSharp Mask em _imageHandler -ESSE  O EFEITO MAIS DEMORADO A SER APLICADO - EXIGE MAIOR PROCESSAMENTO
     *
     * @param      (int) $amount, #000000 hexadecimal da cor
     * @param      (float) $radius, #000000 hexadecimal da cor
     * @param      (float) $threshold, #000000 hexadecimal da cor
     * @return     (void)
     * @access     public
     */
    public function effectUnSharpMask($amount = "80", $radius = "0.5", $threshold = "2") {

        if ($amount > 500)
            $amount = 500;
        $amount = $amount * 0.016;

        if ($radius > 50)
            $radius = 50;
        $radius = $radius * 2;

        if ($threshold > 255)
            $threshold = 255;

        $radius = abs(round($radius));     // Only integers make sense. 

        if ($radius == 0) {
            return false;
        }

        $w = imagesx($this->getImageHandler());
        $h = imagesy($this->getImageHandler());
        $imgCanvas = imagecreatetruecolor($w, $h);
        $imgBlur = imagecreatetruecolor($w, $h);

        // Gaussian blur matrix:
        //                         
        //    1    2    1         
        //    2    4    2         
        //    1    2    1         
        ////////////////////////////////////////////////// 

        if (function_exists('imageconvolution')) { // PHP >= 5.1  
            $matrix = array(
                array(1, 2, 1),
                array(2, 4, 2),
                array(1, 2, 1)
            );
            imagecopy($imgBlur, $this->getImageHandler(), 0, 0, 0, 0, $w, $h);
            imageconvolution($imgBlur, $matrix, 16, 0);
        } else {
            // Move copies of the image around one pixel at the time and merge them with weight
            // according to the matrix. The same matrix is simply repeated for higher radii.
            for ($i = 0; $i < $radius; $i++) {
                imagecopy($imgBlur, $this->getImageHandler(), 0, 0, 1, 0, $w - 1, $h); // left
                imagecopymerge($imgBlur, $this->getImageHandler(), 1, 0, 0, 0, $w, $h, 50); // right
                imagecopymerge($imgBlur, $this->getImageHandler(), 0, 0, 0, 0, $w, $h, 50); // center
                imagecopy($imgCanvas, $imgBlur, 0, 0, 0, 0, $w, $h);

                imagecopymerge($imgBlur, $imgCanvas, 0, 0, 0, 1, $w, $h - 1, 33.33333); // up
                imagecopymerge($imgBlur, $imgCanvas, 0, 1, 0, 0, $w, $h, 25); // down
            }
        }
        if ($threshold > 0) {
            // Calculate the difference between the blurred pixels and the original
            // and set the pixels
            for ($x = 0; $x < $w - 1; $x++) { // each row
                for ($y = 0; $y < $h; $y++) { // each pixel 
                    $rgbOrig = ImageColorAt($this->_imageHandler, $x, $y);
                    $rOrig = (($rgbOrig >> 16) & 0xFF);
                    $gOrig = (($rgbOrig >> 8) & 0xFF);
                    $bOrig = ($rgbOrig & 0xFF);

                    $rgbBlur = ImageColorAt($imgBlur, $x, $y);

                    $rBlur = (($rgbBlur >> 16) & 0xFF);
                    $gBlur = (($rgbBlur >> 8) & 0xFF);
                    $bBlur = ($rgbBlur & 0xFF);

                    // When the masked pixels differ less from the original
                    // than the threshold specifies, they are set to their original value.
                    $rNew = (abs($rOrig - $rBlur) >= $threshold) ? max(0, min(255, ($amount * ($rOrig - $rBlur)) + $rOrig)) : $rOrig;
                    $gNew = (abs($gOrig - $gBlur) >= $threshold) ? max(0, min(255, ($amount * ($gOrig - $gBlur)) + $gOrig)) : $gOrig;
                    $bNew = (abs($bOrig - $bBlur) >= $threshold) ? max(0, min(255, ($amount * ($bOrig - $bBlur)) + $bOrig)) : $bOrig;
                    if (($rOrig != $rNew) || ($gOrig != $gNew) || ($bOrig != $bNew)) {
                        $pixCol = ImageColorAllocate($this->_imageHandler, $rNew, $gNew, $bNew);
                        ImageSetPixel($this->_imageHandler, $x, $y, $pixCol);
                    }
                }
            }
        } else {
            for ($x = 0; $x < $w; $x++) { // each row
                for ($y = 0; $y < $h; $y++) { // each pixel
                    $rgbOrig = ImageColorAt($this->_imageHandler, $x, $y);
                    $rOrig = (($rgbOrig >> 16) & 0xFF);
                    $gOrig = (($rgbOrig >> 8) & 0xFF);
                    $bOrig = ($rgbOrig & 0xFF);

                    $rgbBlur = ImageColorAt($imgBlur, $x, $y);

                    $rBlur = (($rgbBlur >> 16) & 0xFF);
                    $gBlur = (($rgbBlur >> 8) & 0xFF);
                    $bBlur = ($rgbBlur & 0xFF);

                    $rNew = ($amount * ($rOrig - $rBlur)) + $rOrig;
                    if ($rNew > 255) {
                        $rNew = 255;
                    } elseif ($rNew < 0) {
                        $rNew = 0;
                    }
                    $gNew = ($amount * ($gOrig - $gBlur)) + $gOrig;
                    if ($gNew > 255) {
                        $gNew = 255;
                    } elseif ($gNew < 0) {
                        $gNew = 0;
                    }
                    $bNew = ($amount * ($bOrig - $bBlur)) + $bOrig;
                    if ($bNew > 255) {
                        $bNew = 255;
                    } elseif ($bNew < 0) {
                        $bNew = 0;
                    }
                    $rgbNew = ($rNew << 16) + ($gNew << 8) + $bNew;
                    ImageSetPixel($this->_imageHandler, $x, $y, $rgbNew);
                }
            }
        }
        return true;
    }

    /**
     * createErrorImage
     *
     * Aplica o efeito de unSharp Mask em _imageHandler
     *
     * @param      (string) $text
     * @return     (void)
     * @access     private
     */
    private function createErrorImage($text) {
        $this->createImage(200, 30);
        $bg = imagecolorallocate($this->_imageHandler, 255, 255, 255);
        $textcolor = imagecolorallocate($this->_imageHandler, 255, 0, 0);
        imagestring($this->_imageHandler, 2, 0, 0, $text, $textcolor);
        $this->show();
        exit;
    }

    /**
     * __destructor
     *
     * Aplica as rotinas de shutdown da classe
     *
     * @return     (void)
     * @access     public
     */
    public function __destructor() {
        $this->clear();
    }

    /**
     * clear();
     *
     * Limpa todos os atributos da classe
     *
     * @return     (void)
     * @access     public
     */
    public function clear() {
        @imagedestroy($this->_imageHandler);
        $this->_imageHandler = null;
        $this->_bgColorRed = null;
        $this->_bgColorGreen = null;
        $this->_bgColorBlue = null;
        $this->_quality = null;
        $this->_format = null;
        $this->_formatAllow = null;
        $this->_formatOutPut = null;
    }

}

?>
