<?php

class Backend_Model_Config { 
      
	
	public function addConfig(array $data){
		$config = new Backend_Model_DbTable_Config();
		return $config->insert($data);
	}
	
	public function updateConfig($data,$path){
		$config = new Backend_Model_DbTable_Config();
		return $config->update(array('config_data'=>$data),'config_path = "'.$path.'"');
	}
	
	public function removeConfig($id){
		$config = new Backend_Model_DbTable_Config();
		return $config->delete(array('config_id'=>$id));
	}
	
	public function getConfig($path){
		$config = new Backend_Model_DbTable_Config();
		return $config->getDefaultAdapter()
		              ->select()
		              ->from('config','config_data')
		              ->where('config_path = ?',$path)
		              ->query()
		              ->fetch();
	}
	
  
}

?>