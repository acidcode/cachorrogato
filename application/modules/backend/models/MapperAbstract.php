<?php
abstract class Backend_Model_MapperAbstract
{
    protected $_dbTable;
    
    public function setDbTable ( $_dbTable)
    {
        if (is_string($_dbTable)) {
            $_dbTable = new $_dbTable();
        }
        if (! $_dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid db table !');
        }
        $this->_dbTable = $_dbTable;
        return $this;
    }
    
    public function getDbTable ()
    {
        return $this->_dbTable;
    }
    
    abstract function save (Backend_Model_Abstract $model);
    abstract function find (Backend_Model_Abstract $model);
    abstract function remove(Backend_Model_Abstract $model);
    abstract function fetchAll ();
}
?>