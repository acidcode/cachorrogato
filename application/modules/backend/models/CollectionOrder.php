<?php
class Backend_Model_CollectionOrder {

	public function updateBannerSlideOrder($data){
		$item     = $data['id_item'];
		$position = $data['position'];
		$banner   = new Backend_Model_DbTable_Banner();
		$banner->getDefaultAdapter()
		       ->update('banners', array('banner_position'=>$position),'banner_id = '.(int)$item); 
		
	}
	
	public function updateGalleryOrders($data){
	  $item     = $data['id_item'];
	  $position = $data['position'];
	  $Gallery  = new Backend_Model_DbTable_TbEmpreendimentoGaleriaItens();
	  $Gallery->getDefaultAdapter()
	          ->update('empreendimento_galeria_itens', array('posicao'=>$position),'id = '.(int)$item);
	  
	}
	
	public function updateVideosOrders($data){
		$item     = $data['id_item'];
		$position = $data['position'];
		$Gallery  = new Backend_Model_DbTable_TbEmpreendimentoGaleriaItens();
		$Gallery->getDefaultAdapter()
		->update('videos', array('position'=>$position),'id = '.(int)$item);
		 
	}
	
	
}

?>