<?php
class Backend_Model_MapperStatistics extends Backend_Model_MapperAbstract
{
    protected $_isValid = false;
    public function getDbTable ()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Backend_Model_DbTable_Statistics');
        }
        return $this->_dbTable;
    }
    public function isValid ()
    {
        return (bool) ($this->_isValid > 0);
    }
    public function save (Backend_Model_Abstract $model)
    {
        $data = array('id' => $model->getId(), 
        'created' => new Zend_Db_Expr('CURRENT_TIMESTAMP'), 
        'evaluation_date' => date('Y-m-d',strtotime($model->getEvaluation_date())), 
        'variation_per_period' => $model->getVariation_per_period(), 
        'variation_per_month' => $model->getVariation_per_month(), 
        'square_meter_value' => $model->getSquare_meter_value(), 
        'neighborhood_id' => $model->getNeighborhood_id());
        if (null === ($id = $model->getId())) {
            $this->_isValid = $this->getDbTable()->insert($data);
        } else {
            unset($data['id']);
            $this->_isValid = $this->getDbTable()->update($data, 
            array('id = ?' => $id));
        }
    }
    public function find (Backend_Model_Abstract $model)
    {
        $result = $this->getDbTable()->find($model->getId());
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $model->setId($row->id)
            ->setEvaluation_date($row->evaluation_date)
            ->setNeighborhood_id($row->neighborhood_id)
            ->setSquare_meter_value($row->square_meter_value)
            ->setVariation_per_month($row->variation_per_month)
            ->setVariation_per_period($row->variation_per_period)
            ->setCreated($row->created);
    }
    public function remove (Backend_Model_Abstract $model)
    {
        if (null === $model->getId()) {
            return;
        }
        return $this->getDbTable()->delete(array('id = ?' => $model->getId()));
    }
    public function fetchAll ()
    {
      
        $resultSet = $this->getDbTable()
            ->select()
            ->setIntegrityCheck(false)
            ->from(array('spsm'=>'statistics_per_square_meter'), 
        array(
        new Zend_Db_Expr(
        " DATE_FORMAT(created,'%d/%c/%Y - %H:%i:%s') as createdFormatted "), '*'))
            ->join(array('b'=>'bairros'),'spsm.neighborhood_id = b.id', array('b.nome as neighborhood'))
            ->query()
            ->fetchAll(Zend_Db::FETCH_OBJ);
        $entries = array();
        foreach ($resultSet as $row) {
            $entry = new Backend_Model_Statistics();
            $entry->setId($row->id)
                ->setEvaluation_date($row->evaluation_date)
                ->setNeighborhood_id($row->neighborhood_id)
                ->setSquare_meter_value($row->square_meter_value)
                ->setVariation_per_month($row->variation_per_month)
                ->setVariation_per_period($row->variation_per_period)
                ->setCreated($row->created)
                ->setNeighborhood($row->neighborhood);
            $entries[] = $entry;
        }
        return $entries;
    }
}
?>