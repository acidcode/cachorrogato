<?php

class Backend_Model_DbTable_TbQuestionarioOpcoes extends Zend_Db_Table_Abstract {

    protected $_name = 'tb_fornecedor_questionario_opcao';

    public function adicionarCampo($request) {
        $data = array(
            'id_questionario' => $request['id_questionario'],
            'id_tipo_campo' => $request['id_tipo_campo'],
            'vc_valor_opcao' => $request['vc_valor_opcao']
        );

        $id_opcao = parent::insert($data);
        $novaOpcao = $this->findOpcaoById($id_opcao);

        return $novaOpcao;
    }

    public function findOpcoesComCampo($where) {
        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from(array('tfqo' => 'tb_fornecedor_questionario_opcao'))
                ->joinInner(array('tfqc' => 'tb_fornecedor_questionario_campo'), 'tfqo.id_tipo_campo = tfqc.id_tipo_campo');
        if ($where) {
            $select->where($where);
        }
        $fetchAll = $select->query()
                ->fetchAll();
        return $fetchAll;
    }

    public function findOpcoesByQuestionario($id_questionario) {
        return parent::fetchAll('id_questionario = ' . $id_questionario);
    }

    public function findOpcaoById($id_opcao) {
        $findOpcaoById = parent::find($id_opcao);

        if ($findOpcaoById) {
            $retorno = array();
            foreach ($findOpcaoById as $array) {
                $retorno = array(
                    'id_opcao' => $array['id_opcao'],
                    'id_questionario' => $array['id_questionario'],
                    'id_tipo_campo' => $array['id_tipo_campo'],
                    'vc_valor_opcao' => $array['vc_valor_opcao']
                );
            }
            return $retorno;
        } else {
            return false;
        }
    }

    public function deletar($id_opcao) {
        $where = array('id_opcao = ' . $id_opcao);

        $teste = parent::delete($where);
        echo '<pre>';
        print_r($teste);
        echo '</pre>';
        echo '<pre>';
        print_r($id_opcao);
        echo '</pre>';
        exit();
    }

    public function deleteByQuestionario($id_questionario) {
        $where = 'id_questionario = ' . $id_questionario;
        return parent::delete($where);
    }

}

?>
