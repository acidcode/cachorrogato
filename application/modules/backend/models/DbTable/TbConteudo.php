<?php

class Backend_Model_DbTable_TbConteudo extends Zend_Db_Table_Abstract {

    protected $_name = 'tb_conteudo';
    protected $_Instance;

    public function __construct() {
        $this->_Instance = Zend_Registry::get('db');
        parent::__construct();
    }

    public function init() {
        parent::init();
    }
    
    public function cadastro($request) {

        $data = array(
            'bl_ativo' => $request['bl_ativo'],
            'bl_destaque' => $request['bl_destaque'],
            'bl_exibir_home' => $request['bl_exibir_home'],
            'vc_titulo' => $request['vc_titulo'],
            'vc_subtitulo' => $request['vc_titulo'],
            'vc_conteudo' => $request['vc_conteudo'],
            'vc_seo_title' => $request['vc_seo_title'],
            'vc_seo_url' => $request['vc_seo_url'],
            'vc_seo_keywords' => $request['vc_seo_keywords'],
            'vc_seo_description' => $request['vc_seo_description']);

        if(empty($request['id_conteudo']))
        {
           
            $data['dt_cadastro'] = date('Y-m-d');
            $data['id_usuario'] = Zend_Auth::getInstance()->getStorage()->read()->id;
            $pk = $this->insert($data);
        }
        else
        {
            $pk = $request['id_conteudo'];
            $where = "id_conteudo = " . $request['id_conteudo'];
            $this->update($data, $where);
        }
        
        $this->cadastroConteudoCategoria(
                $request['conteudo_categoria'],
                $pk);
        
        return $pk;
        
    }
    
    public function cadastroConteudoCategoria($categorias, $id_conteudo) {
        $tbConteudoRelacionadoCategoria = new Backend_Model_DbTable_TbConteudoRelacionadoCategoria();
        
        $tbConteudoRelacionadoCategoria->deletarByConteudo($id_conteudo);
        
        foreach($categorias as $categoria) {
            $data = array('id_conteudo_categoria' => $categoria,
                'id_conteudo' => $id_conteudo);
            
            $tbConteudoRelacionadoCategoria->insert($data);
        }
        
        return true;
    }
    
    
    /**
     * Encontra pela chave primaria
     * @param int $pk
     * @return array 
     */
    public function find($pk)
    {
        $db = Zend_Registry::get('db');
        
        $conteudo_categoria = $db->select()
                ->from(array('e' => 'tb_conteudo'))
                ->where("id_conteudo = $pk")
                ->query()
                ->fetch();
        
        return $conteudo_categoria;
        
    }
    
    /**
     * Encontra pela chave primaria
     * @param int $pk
     * @return array 
     */
    public function findByUrl($url)
    {
        $db = Zend_Registry::get('db');
        
        $tbConteudoRelacionadoCategoria = new Backend_Model_DbTable_TbConteudoRelacionadoCategoria();
        $tbConteudoRelacionadoTag = new Backend_Model_DbTable_TbConteudoRelacionadoTag();
        $adminUser = new Backend_Model_DbTable_AdminUser();
        
        $conteudo['conteudo'] = $db->select()
                ->from(array('e' => 'tb_conteudo'))
                ->where("vc_seo_url = '$url'")
                ->query()
                ->fetch();
        
        //Categorias
        $conteudo['categoria'] = $tbConteudoRelacionadoCategoria->findByConteudo(
                $conteudo['conteudo']['id_conteudo']
                );
        //Tags
        $conteudo['tag'] = $tbConteudoRelacionadoTag->findByConteudo(
                $conteudo['conteudo']['id_conteudo']
                );
        
        //Author
        $conteudo['autor'] = $adminUser->fetchRow(
                "id = " . $conteudo['conteudo']['id_usuario']
                )->toArray();
        
        return $conteudo;
        
    }
    
    public function findRelacionados($pk) {

        $tbConteudoRelacionadoTag = new Backend_Model_DbTable_TbConteudoRelacionadoTag();
        
        $tags = $tbConteudoRelacionadoTag->findByConteudo($pk);
        
        $db = Zend_Registry::get('db');
        
        $conteudos = $db->select()
                ->from(array('c' => 'tb_conteudo'))
                ->joinInner(array('ct' => 'tb_conteudo_relacionado_tag'),
                    'c.id_conteudo = ct.id_conteudo');
        
        foreach($tags as $tag) {
            $conteudos = $conteudos->orWhere("ct.id_conteudo_tag = " . $tag['id_conteudo_tag']);
        }
        
        $conteudos = $conteudos->order('c.id_conteudo DESC')
                ->limit(2)
                ->group('c.id_conteudo')
                ->query()
                
                ->fetchAll();
                
        
        return $conteudos;
    }

    /**
     * 
     * @param string $by
     * @return array
     */
    public function listConteudo($by = null) {
        $conteudo = $this->_Instance->select()
                ->from(array('tb_conteudo'))
                ->order('dt_cadastro');
        if ($by != null) {
            $conteudo->where($by);
        }
        return $conteudo->query()->fetchAll();
    }

    /**
     * 
     * @param array $request
     * @return int
     */
    public function insertConteudo($request) {
        $usuario = Zend_Auth::getInstance();
        $data = array('id_usuario' => $usuario->getStorage()->read()->id,
            'dt_cadastro' => new Zend_Db_Expr('CURRENT_TIMESTAMP'),
            'bl_exibir_no_site' => ($request['bl_exibir_no_site']) ? $request['bl_exibir_no_site'] : 'nao',
            'vc_titulo' => $request['vc_titulo'],
            'vc_subtitulo' => $request['vc_titulo'],
            'vc_conteudo' => $request['vc_conteudo'],
            'vc_seo_title' => $request['vc_seo_title'],
            'vc_seo_url' => $request['vc_seo_url'],
            'vc_seo_keywords' => $request['vc_seo_keywords'],
            'vc_seo_description' => $request['vc_seo_description']);
        if (!$request['id_conteudo']) {
            $id_conteudo = parent::insert($data);
        } else {
            parent::update($data, 'id_conteudo = ' . $request['id_conteudo']);
            $id_conteudo = $request['id_conteudo'];
        }

        $this->inserirRelacionamentoComCategoria($id_conteudo, $request['conteudo_categoria']);
        $this->inserirRelacionamentoComConteudo($id_conteudo, $request['conteudo_relacionado']);

        return $id_conteudo;
    }

    /**
     * 
     * @param int $id_conteudo
     * @return int
     */
    public function removeConteudo($id_conteudo) {
        return parent::delete('id_conteudo = ' . $id_conteudo);
    }

    /**
     * 
     * @param array $request
     * @return int
     */
    public function adicionarCategoria($request) {
        $data = array(
            'vc_categoria' => $request['vc_categoria']
        );

        if (!empty($request['id_parent'])) {
            $data['id_parent'] = $request['id_parent'];
        } else {
            $data['id_parent'] = null;
        }

        $tbCategoria = new Zend_Db_Table();
        $tbCategoria->_name = 'tb_conteudo_categoria';
        $tbCategoria->insert($data);

        $id_categoria = $tbCategoria->getAdapter()->lastInsertId();

        return $id_categoria;
    }

    /**
     * 
     * @param array $request
     * @return int
     */
    public function removerCategoria($request) {
        $tbCategoria = new Zend_Db_Table();
        $tbCategoria->_name = 'tb_conteudo_categoria';
        return $tbCategoria->delete('id_categoria = ' . $request['id_categoria']);
    }

    /**
     * 
     * @param boolean $todos
     * @return array
     */
    public function listarCategoria($todos) {
        $categorias = $this->_Instance->select()
                ->from('tb_conteudo_categoria')
                ->query()
                ->fetchAll();
        if ($todos) {
            $data = array(0 => 'Categoria Pai');
        } else {
            $data = array();
        }
        /*
         * 
         * 
         
        $categoriaPai = $this->_Instance->select()
                ->from('tb_conteudo_categoria')
                ->where('id_parent IS NULL OR id_parent = ""')
                ->query()
                ->fetchAll();
*/
        //echo '<pre>';
        //print_r($categoriaPai);
        /*foreach ($categoriaPai as $array) {
            $sub = array(
                'vc_categoria' => $array['vc_categoria'],
                $array['id_categoria'] => $this->getSubcategoria($array['id_categoria'])
            );
            //$this->getSubcategoria($sub[0]['id_categoria']);
        }
        //print_r($sub);
        //echo '</pre>';

        /*
         * 
         */
        foreach ($categorias as $array) {
            $data[$array['id_categoria']] = $array['vc_categoria'];
        }
        return $data;
    }

    private function getSubcategoria($id_categoria) {
        $retorno = $this->_Instance->select()
                ->from('tb_conteudo_categoria')
                ->where('id_parent = ' . $id_categoria)
                ->query()
                ->fetchAll();
        $data = array();
        foreach ($retorno as $array) {
            $data[$array['id_categoria']] = $this->getSubcategoria($array['id_categoria']);
        }

        return $data;
    }

    /**
     * 
     * @param int $id_conteudo
     * @param array $request
     */
    private function inserirRelacionamentoComCategoria($id_conteudo, $request) {
        $categoria = new Zend_Db_Table();
        $categoria->_name = 'tb_conteudo_relacionado_categoria';
        $categoria->delete('id_conteudo = ' . $id_conteudo);

        if (is_array($request)) {
            foreach ($request as $id_categoria) {
                $data = array(
                    'id_conteudo' => $id_conteudo,
                    'id_categoria' => $id_categoria
                );
                $categoria->insert($data);
            }
        }
    }

    /**
     * 
     * @param int $id_conteudo
     * @param array $request
     */
    private function inserirRelacionamentoComConteudo($id_conteudo, $request) {
        $conteudoRelacionado = new Zend_Db_Table();
        $conteudoRelacionado->_name = 'tb_conteudo_relacionado_conteudo';
        $conteudoRelacionado->delete('id_conteudo = ' . $id_conteudo);
        if (is_array($request)) {
            foreach ($request as $id_conteudo_relacionado) {
                $data = array(
                    'id_conteudo' => $id_conteudo,
                    'id_conteudo_relacionado' => $id_conteudo_relacionado
                );
                $conteudoRelacionado->insert($data);
            }
        }
    }

    /**
     * 
     * @param type $id_conteudo
     * @return boolean | array
     */
    public function getRelacionamentoCategoria($id_conteudo) {
        $conteudoRelacionadoCategoria = new Zend_Db_Table();
        $conteudoRelacionadoCategoria->_name = 'tb_conteudo_relacionado_categoria';
        $fetchAll = $conteudoRelacionadoCategoria->fetchAll('id_conteudo = ' . $id_conteudo);

        $retorno = false;
        if ($fetchAll->valid()) {
            $retorno = array();
            foreach ($fetchAll->toArray() as $array) {
                $retorno[$array['id_conteudo']][] = $array['id_categoria'];
            }
        }

        return $retorno;
    }

    /**
     * 
     * @param type $id_conteudo
     * @return boolean | array
     */
    public function getRelacionamentoConteudo($id_conteudo) {
        $conteudoRelacionadoConteudo = new Zend_Db_Table();
        $conteudoRelacionadoConteudo->_name = 'tb_conteudo_relacionado_conteudo';
        $fetchAll = $conteudoRelacionadoConteudo->fetchAll('id_conteudo = ' . $id_conteudo);

        $retorno = false;
        if ($fetchAll->valid()) {
            $retorno = array();
            foreach ($fetchAll->toArray() as $array) {
                $retorno[$array['id_conteudo']][$array['id_conteudo_relacionado']] = $array['id_conteudo_relacionado'];
            }
        }

        return $retorno;
    }

    /**
     * 
     * @param int $id_categoria
     * @return array
     */
    private function getDetalhesCategoria($id_categoria) {
        $categoria = new Zend_Db_Table();
        $categoria->_name = 'tb_conteudo_categoria';
        $fetchRow = $categoria->fetchRow('id_categoria = ' . $id_categoria);
        return $fetchRow->toArray();
    }

    private function getDetalhesConteudo($id_conteudo) {
        $categoria = new Zend_Db_Table();
        $categoria->_name = 'tb_conteudo';
        $fetchRow = $categoria->fetchRow('id_conteudo = ' . $id_conteudo);
        return $fetchRow->toArray();
    }

    /**
     * Método usado pelo fronend para exibir 
     * as informações do conteúdo
     * 
     * @param int $id_conteudo
     * @return array
     */
    public function findAll($id_conteudo) {
        //Conteudo 
        $conteudo = $this->listConteudo('id_conteudo = ' . $id_conteudo);
        $relacionado = $this->getRelacionamentoConteudo($id_conteudo);
        $conteudoRelacionado = array();
        foreach ($relacionado[$id_conteudo] as $id_conteudo_relacionado) {
            $conteudoRelacionado[$id_conteudo_relacionado] = $this->getDetalhesConteudo($id_conteudo_relacionado);
        }

        //Categoria
        $categoria = $this->getRelacionamentoCategoria($id_conteudo);
        $conteudoCategoria = array();
        foreach ($categoria[$id_conteudo] as $id_categoria) {
            $conteudoCategoria[$id_categoria] = $this->getDetalhesCategoria($id_categoria);
        }

        //Dados do usuário
        $usuario = new Backend_Model_DbTable_AdminUser();
        $usuario->setId($conteudo[0]['id_usuario']);
        $dadosUsuario = $usuario->fetchUserByid();

        $retorno = array(
            'conteudo' => $conteudo[0],
            'categoria' => $conteudoCategoria,
            'conteudo_relacionado' => $conteudoRelacionado,
            'usuario' => $dadosUsuario[0]
        );

        return $retorno;
    }

}

?>