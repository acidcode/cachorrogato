<?php
class Backend_Model_DbTable_TbConteudoRelacionadoCategoria 
    extends Zend_Db_Table_Abstract 
{

    protected $_name = "tb_conteudo_relacionado_categoria";
    
    public function findByConteudo($id_conteudo) {
        
         $db = Zend_Registry::get('db');
         $categorias = $db->select()
                 ->from(array('c' => 'tb_conteudo_relacionado_categoria'))
                 ->joinInner(array('cc' => 'tb_conteudo_categoria'),
                         'c.id_conteudo_categoria = cc.id_conteudo_categoria')
                 ->where('c.id_conteudo = ' . $id_conteudo)
                 ->order('c.id_conteudo_categoria DESC')
                 ->query()
                 ->fetchAll();
         
         return $categorias;
                 
    }
    
    public function deletarByConteudo($id_conteudo) {
        $this->delete('id_conteudo = ' . $id_conteudo);
    }
    
    
    
}