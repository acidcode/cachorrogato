<?php
class Backend_Model_DbTable_ExtraInfo extends Zend_Db_Table_Abstract {

	protected $_name = 'empreedimento_info_extras';
       
     public function save($request){
     	
     	$data = array(
     			'empreendimento_id'=>$request['empreendimento_id'],
     			'unidade'=> $request['unidade'],
     			'metragem_1' => $request['metragem_1'],
     			'metragem_2' => $this->_isEmpty($request['metragem_2']),
     			'dormitorio_a' => $request['dormitorio_a'],
     			'dormitorio_b' => $this->_isEmpty($request['dormitorio_b']),
     			'vaga_a' => $request['vaga_a'], 'vaga_b' => $this->_isEmpty($request['vaga_b']),
     			'suite_a' => $request['suite_a'], 'suite_b' => $this->_isEmpty($request['suite_b']),
     			'valor' => $this->_formattingMoney($request['valor']),
     			'mensais' =>$this->_formattingMoney($request['mensais']),
     			'nome_do_proprietario'=>$this->_isEmpty($request['nome_do_proprietario']),
     			'email_do_proprietario'=>$this->_isEmpty($request['email_do_proprietario']),
     			'telfixo_do_proprietario'=>$this->_isEmpty($request['telfixo_do_proprietario']),
     			'cel_do_proprietario'=>$this->_isEmpty($request['cel_do_proprietario']),
     			'observacao'=>$this->_isEmpty($request['observacao']),
     			'saldo_devedor'=>$this->_formattingMoney($request['saldo_devedor']),
     			'data'=> new Zend_Db_Expr('CURRENT_TIMESTAMP'),
     			'id' => new Zend_Db_Expr('NULL')
     			);
     	
     	if(!$request['id_ExtraInfo']){
     		
     		$rowsAffected = parent::insert($data);
     		 
     	}else{
     		
     		unset($data['id']);
     	 
     		$rowsAffected = parent::update($data, array('id = ?'=> $request['id_ExtraInfo']));
     		
     	}

     	return $rowsAffected;
     }  
     
     protected  function _formattingMoney($value){
     	 
     	if(null===$value){
     		return $this->_isEmpty($value);
     	}else{
     		return str_replace(array('.',','),array('','.'),$value);
     	}
     	 
     }
     
     protected  function _isEmpty($value){
     	if(!empty($value)){
     		 
     		return $value;
     
     	}else{
     		 
     		return new Zend_Db_Expr('NULL');
     	}
     }
}

?>