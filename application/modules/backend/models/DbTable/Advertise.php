<?php
class Backend_Model_DbTable_Advertise extends Zend_Db_Table_Abstract
{
    protected $_name = "advertise";
    
    protected $_instance;
    
    protected  $_fields = array();
    
    public function init ()
    {
        parent::init();
    }
    
    
     public function __construct($fields){
      
      if(!is_array($fields)){
         	throw new Exception('The fields must be array !');
       }
       
       $this->_fields = $fields;
       
       $this->_instance = Zend_Registry::get('db');

       parent::__construct();
    }    
    
    public function add(){
        
        $Advertise = $this->_fields;
        
        $data = array(
        'id' => new Zend_Db_Expr('NULL') ,
        'cep_property' => $Advertise['cep_property'],
        'address_property' =>$Advertise['address_property'],
        'number_property' =>$Advertise['number_property'],
        'complement_property' =>$Advertise['complement_property'],
        'feature_offer' =>$Advertise['feature_offer'],
        'number_dorm_or_rooms' =>$Advertise['number_dorm_or_rooms'],
        'number_suites' =>$Advertise['number_suites'],
        'useful_area_number' =>$Advertise['useful_area_number'],
        'total_area' =>$Advertise['total_area'],
        'income' =>$Advertise['income' ],
        'downpayment' =>$Advertise['downpayment'],
        'monthly_valor' =>$Advertise['monthly_valor'],
        'commercial_conditions' =>$Advertise['commercial_conditions'],
        'commercial_conditions_details' =>$Advertise['commercial_conditions_details'],
        'vacancies_garage' =>$Advertise['vacancies_garage'],
        'email_advertiser' =>$Advertise['email_advertiser'],
        'name_advertiser' =>$Advertise['name_advertiser'],
        'phone_advertiser' =>$Advertise['phone_advertiser'],
        'cel_phone_advertiser' =>$Advertise['cel_phone_advertiser'],
        'cep_advertiser'=>$Advertise['cep_advertiser'],
        'created'=> new Zend_Db_Expr('CURRENT_TIMESTAMP'),
        'advertise_receive_sms_promotional_texts'=>$Advertise['advertise_receive_sms_promotional_texts'],
        'show_offer_on_map'=>$Advertise['show_offer_on_map']
        );
        
    	
        return parent::insert($data);
        
    }
    
    public function edit(){
    	
        $Advertise = $this->_fields;
        
       $data = array(
        'id' => new Zend_Db_Expr('NULL') ,
        'cep_property' => $Advertise['cep_property'],
        'address_property' =>$Advertise['address_property'],
        'number_property' =>$Advertise['number_property'],
        'complement_property' =>$Advertise['complement_property'],
        'feature_offer' =>$Advertise['feature_offer'],
        'number_dorm_or_rooms' =>$Advertise['number_dorm_or_rooms'],
        'number_suites' =>$Advertise['number_suites'],
        'useful_area_number' =>$Advertise['useful_area_number'],
        'total_area' =>$Advertise['total_area'],
        'income' =>$Advertise['income' ],
        'downpayment' =>$Advertise['downpayment'],
        'monthly_valor' =>$Advertise['monthly_valor'],
        'commercial_conditions' => implode(',', $Advertise['commercial_conditions']),
        'commercial_conditions_details' =>$Advertise['commercial_conditions_details'],
        'vacancies_garage' =>$Advertise['vacancies_garage'],
        'email_advertiser' =>$Advertise['email_advertiser'],
        'name_advertiser' =>$Advertise['name_advertiser'],
        'phone_advertiser' =>$Advertise['phone_advertiser'],
        'cel_phone_advertiser' =>$Advertise['cel_phone_advertiser'],
        'cep_advertiser'=>$Advertise['cep_advertiser'],
        'created'=> new Zend_Db_Expr('CURRENT_TIMESTAMP'),
        'advertise_receive_sms_promotional_texts'=>$Advertise['advertise_receive_sms_promotional_texts'],
        'show_offer_on_map'=>$Advertise['show_offer_on_map']
        );
        
        return parent::update($data,$this->_instance->where('id = ?',$Advertise['id']));
        
    }
    
    public function remove(){
    	
        $Advertise = $this->_fields;
        
        return parent::delete($this->_instance->where('id = ?',$Advertise['id']));
    }
    
    public function listAll(){
    	
        return $this->_instance->select()
                               ->from(array('advertise'))
                               ->query()
                               ->fetchAll();
        
    }
    
    
    
    
    
}
?>