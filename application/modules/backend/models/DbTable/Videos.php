<?php
class Backend_Model_DbTable_Videos extends Zend_Db_Table_Abstract
{
    protected $_name = "videos";
    
    protected $_primary = 'id';
    
    protected $_Instance;
    
    protected $_id = NULL;
    
    protected $_title;
    
    protected $_link;
    
    protected $_description;
    
    protected $_created = NULL;
    
    protected $_status  = 0;
    
    protected $_position = 0;
    
    
    
    public function __construct ()
    {
    	$this->_Instance = Zend_Registry::get('db');
    	parent::__construct();
    }
    public function init ()
    {
    	parent::init();
    }
    public function addVideo ()
    {
    	return parent::insert(
    			array('id' =>new Zend_Db_Expr('NULL'),
    			      'title' => $this->getTitle(),
    				  'link' => $this->getLink(),
    				  'description' => $this->getDescription(),
    				  'created' => new Zend_Db_Expr('CURRENT_TIMESTAMP'),
    				  'status' => $this->getStatus(),
    				  'position'=> $this->getPosition()
    				  ));
    }
    public function editVideo ()
    {
        if($this->update( array(
    			      'title' => $this->getTitle(),
    				  'link' => $this->getLink(),
    				  'description' => $this->getDescription(),
        		       'status' => $this->getStatus()
    				  ), 'id = ' .$this->getId())){
        	
            return true;
    				  }
     
   	  return false;
        
    }
    
    public function editPositionVideo ()
    {
    	if($this->update( array(
    			'position'=> $this->getPosition()
    	), 'id = ' .$this->getId())){
    		 
    		return true;
    	}
    	 
    	return false;
    
    }
    
    
    public function fetchVideoByid ()
    {
    	return $this->_Instance->select()
    	->from('videos')
    	->where('videos.id = ' . $this->getId())
    	->query()
    	->fetchAll();
    }
    
    public function listAllVideos ()
    {
    	return $this->_Instance->select()
    	->from('videos',array(new Zend_Db_Expr("DATE_FORMAT(created,'%d/%c/%Y - %H:%i:%s') as createdFormatted"),'*'))
    	->order('position')
    	->query()
    	->fetchAll();
    }
    
    public function removeVideo ()
    {
    	return parent::delete('id = ' . $this->getId());
    }
	/**
	 * @return the $_id
	 */
	public function getId() {
	    if(NULL==$this->_id)
	        $this->_id = new Zend_Db_Expr('NULL');
		return $this->_id;
	}

	/**
	 * @param field_type $_id
	 */
	public function setId($_id) {
		$this->_id = $_id;
		return $this;
	}

	/**
	 * @return the $_title
	 */
	public function getTitle() {
		return $this->_title;
	}

	/**
	 * @param field_type $_title
	 */
	public function setTitle($_title) {
		$this->_title = $_title;
		return $this;
	}

	/**
	 * @return the $_link
	 */
	public function getLink() {
		return $this->_link;
	}

	/**
	 * @param field_type $_link
	 */
	public function setLink($_link) {
		$this->_link = $_link;
		return $this;
	}

	/**
	 * @return the $_description
	 */
	public function getDescription() {
		return $this->_description;
	}

	/**
	 * @param field_type $_description
	 */
	public function setDescription($_description) {
		$this->_description = $_description;
		return $this;
	}
	/**
	 * @return the $_datetime_record
	 */
	public function getCreated() {
	    
	    if(NULL==$this->_created)
	        $this->_created = new Zend_Db_Expr('CURRENT_TIMESTAMP');
	    
		return $this->_created;
	}

	/**
	 * @param field_type $_datetime_record
	 */
	public function setCreated($_datetime_record) {
		$this->_created = $_datetime_record;
		return $this;
	}
	/**
	 * @return the $_status
	 */
	public function getStatus() {
		return $this->_status;
		
	}

	/**
	 * @param number $_status
	 */
	public function setStatus($_status) {
		$this->_status = $_status;
		return $this;
		
	}
	/**
	 * @return the $_position
	 */
	public function getPosition() {
		return $this->_position;
	}

	/**
	 * @param number $_position
	 */
	public function setPosition($_position) {
		$this->_position = $_position;
		return $this;
		
	}




    
    
    
}
?>