<?php

/**
 * Modelo ConteudoTag
 * @author André Gomes @acidcode
 * @license www.kernelpanic.com.br
 */
class Backend_Model_DbTable_TbConteudoTag 
    extends Zend_Db_Table_Abstract 
{

    protected $_name = "tb_conteudo_tag";
    
    /**
     * Cadastro
     * @param array $request
     * @return int Primary key 
     */
    public function cadastro($request)
    {
        $data = array('vc_tag' => $request['vc_tag']);
        $pk = $this->insert($data);
        return $pk;
    }
    
    /**
     * Cadastro
     * @param array $request
     * @return int Primary key 
     */
    public function cadastroAjax($request)
    {
        $tags = explode(",", $request['vc_nova_tag']);
        $tbConteudoRelacionadoTag = new Backend_Model_DbTable_TbConteudoRelacionadoTag();
        
        foreach($tags as $tag) {
            
            $tag = strtolower($tag);
            
            $tagExistente = $this->findTag($tag);

            if(!$tagExistente) {
                 $pk = $this->insert(array('vc_tag' => $tag));
                 $tbConteudoRelacionadoTag->cadastro(
                        $pk, 
                        $request['id_conteudo']);
            } else {
                $tbConteudoRelacionadoTag->cadastro(
                        $tagExistente['id_conteudo_tag'], 
                        $request['id_conteudo']);
            }
        }
        
        
    }
    
    public function findTag($tag) {
       $db = Zend_Registry::get('db');
        
       $conteudo_tag = $db->select()
                ->from(array('e' => 'tb_conteudo_tag'))
                ->where("vc_tag = '$tag'")
                ->query()
                ->fetch();
        
       return $conteudo_tag;
    }
    
    /**
     * Encontra pela chave primaria
     * @param int $pk
     * @return array 
     */
    public function find($pk)
    {
        $db = Zend_Registry::get('db');
        
        $conteudo_tag = $db->select()
                ->from(array('e' => 'tb_conteudo_tag'))
                ->where("id_conteudo_tag = $pk")
                ->query()
                ->fetch();
        
        return $conteudo_tag;
        
    }
    
    /**
     * Encontra pela chave primaria
     * @param int $pk
     * @return array 
     */
    public function findByUrl($url)
    {
        $db = Zend_Registry::get('db');
        
        $conteudo_tag = $db->select()
                ->from(array('e' => 'tb_conteudo_tag'))
                ->where("vc_seo_url = '$url'")
                ->query()
                ->fetch();
        
        return $conteudo_tag;
        
    }
    
    /**
     * Encontra todos pela condição do filtro
     * @param string $where
     * @return array 
     */
    public function findAll($where = '')
    {
        $db = Zend_Registry::get('db');
        
        $conteudo_tags = $db->select()
                ->from(array('e' => 'tb_conteudo_tag'))
                ->joinInner(array('r' => 'tb_conteudo_relacionado_tag'),
                        'e.id_conteudo_tag = r.id_conteudo_tag');
        
        if(count($where) > 0 && !empty($where))
        {
            $conteudo_tags = $conteudo_tags->where($where);
        }
        $conteudo_tags = $conteudo_tags->query()
                ->fetchAll();

        return $conteudo_tags;
    }
    
    /**
     * Retorna um array para ser usados em formularios Select
     * @access static
     * @param type $todos
     * @return type 
     */
    public function getFormSelect($todos = false)
    {
        $select = array();
        
        $select[0] = "Main";
        
        $rowSet = $this->fetchAll();

        foreach($rowSet as $data) {
            $select[$data["id_conteudo_tag"]] = $data["vc_tag"];
        }
        return $select;
    }
    
    
}