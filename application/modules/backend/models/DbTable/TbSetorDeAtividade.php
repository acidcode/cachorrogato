<?php

class Backend_Model_DbTable_TbSetorDeAtividade extends Zend_Db_Table_Abstract {

    protected $_name = 'tb_fornecedor_setor_atividade';

    public function cadastrarSetor($request) {
        $data = array(
            'vc_setor_atividade' => $request['vc_setor_atividade'],
            'vc_seo_title' => $request['vc_seo_title'],
            'vc_seo_url' => $request['vc_seo_url'],
            'vc_seo_keywords' => $request['vc_seo_keywords'],
            'vc_seo_description' => $request['vc_seo_description']
        );

        if (isset($request['id_setor_atividade']) && (int) $request['id_setor_atividade']) {
            $id_atividade = $request['id_setor_atividade'];
            $where = 'id_setor_atividade = ' . $id_atividade;
            parent::update($data, $where);
        } else {
            $id_atividade = parent::insert($data);
        }
    }

}

?>
