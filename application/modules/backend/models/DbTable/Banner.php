<?php
/**
 * Banners Model
 * @author Felipe Almeida
 * @since 2012-10-11
 * @version 1.0.0
 */
class Backend_Model_DbTable_Banner extends Zend_Db_Table_Abstract
{
    protected $_name = "banners";
    protected $_Instance;
    public function __construct ()
    {
        $this->_Instance = Zend_Registry::get('db');
        
        parent::__construct();
    }
    public function init ()
    {
        parent::init();
    }
    public function fetch ()
    {   
       
        return $this->_Instance->select()
             ->from(array('b' => 'banners'))
             ->order('b.banner_position')
             ->query()
             ->fetchAll();
    }
    
    public function fetchFrontend(){

        return $this->_Instance->select()
        ->from(array('b' => 'banners'))
        ->where('b.banner_isVisible!=0 ')
        ->where('DATEDIFF(banner_from_date,CURRENT_DATE) <= 0 AND DATEDIFF(banner_to_date,CURRENT_DATE) > 0')
        ->order('b.banner_position')
        ->query()
        ->fetchAll();
    }
    
    public function genBanner($_file ,$_link){
    	if($_link==''){
    		$link= "<img src='/upload/banners/".$_file."' width='969' height='391' />";
    	}else{
    		$link = "<a href='".$_link."'><img src='/upload/banners/".$_file."' width='969' height='391' border='0'/></a>";
    	}
    	
    	return $link;
    }
    
    public function fetchById ($id)
    {
        return $this->_Instance->select()
            ->from(array('b' => 'banners'))
            ->where('b.banner_id = ?', $id, Zend_Db::PARAM_INT)
            ->order('b.banner_position')
            ->query()
            ->fetch();
    }
    public function add ($fields)
    {
        $data = array('banner_id' => new Zend_Db_Expr('NULL'), 
        'banner_position' => 0, 
        'banner_hash_file' => $fields['banner_hash_file'], 
        'banner_link' => $fields['banner_link'], 
        'banner_from_date' => $this->formatDate($fields['banner_from_date']), 
        'banner_to_date' => $this->formatDate($fields['banner_to_date']), 
        'banner_title' => $fields['banner_title'],
        'banner_isVisible'=>$fields['banner_isVisible']);
        return $this->insert($data);
    }
    public function edit ($fields, $id)
    {
        $data = array(
        'banner_position' => 0, 
        'banner_hash_file' => $fields['banner_hash_file'], 
        'banner_link' => $fields['banner_link'], 
        'banner_from_date' => $this->formatDate($fields['banner_from_date']), 
        'banner_to_date' => $this->formatDate($fields['banner_to_date']), 
        'banner_title' => $fields['banner_title'],
        'banner_isVisible'=>$fields['banner_isVisible']);
       /*  $where = $this->_Instance->quote('banner_id = ? ', $id, 
        Zend_Db::PARAM_INT); */
        return $this->update($data, 'banner_id = '. $id);
    }
    public function remove ($id)
    {
        return $this->delete('banner_id = '.$id);
    }
    public function formatDate ($date)
    { //30/10/2012
        $date = explode('/', $date);
        $formatted = array( $date[2], $date[1],  $date[0]);
        return implode('-', $formatted);
    }
    
    public function getdate($Date){
    
    	//2012-10-14
    	//10/15/2012
    	$date = explode('-', $Date);
    	$formatted = array( $date[2], $date[1],  $date[0]);
    	return implode('/', $formatted);
    }
    
    public function upload ($file)
    {
       /*  $response = array();
        if ($file['size'] > 2000) {
            $response[] = 'Extensão do arquivo inválida , extensões validas são : jpeg ou png';
        }
        if ($file['type'] != 'image/jpeg' || $file['type'] !== 'image/png') {
            $response[] = 'Extensão do arquivo inválida , extensões validas são : jpeg ou png';
        }
        if (count($response) == 0) {
            $ext = explode('/', $file['image/jpeg']);
            $rename = md5($file['name'] . time() . '.' . $ext[1]);
            if (@move_uploaded_file($file['tmp_name'], $location . $rename)) {
                $response['return'] = true;
                $response['file'] = $rename;
            } else {
                $response[] = 'Erro ao carregar banner !';
            }
        }
        return $response; */
        
       
        
        
        
    }
}