<?php
class Backend_Model_DbTable_TbConteudoRelacionadoTag 
    extends Zend_Db_Table_Abstract 
{

    protected $_name = "tb_conteudo_relacionado_tag";
    
    public function cadastro($id_conteudo_tag, $id_conteudo) {
        $data = array('id_conteudo' => $id_conteudo,
            'id_conteudo_tag' => $id_conteudo_tag
            );
        
        $db = Zend_Registry::get('db');
        $check = $db->select()
                ->from(array('e' => 'tb_conteudo_relacionado_tag'))
                ->where("id_conteudo = $id_conteudo AND id_conteudo_tag = $id_conteudo_tag")
                ->query()
                ->fetch();

        if(!$check) {
            $this->insert($data);
        }
        return true;
    }
    
    public function findByConteudo($id_conteudo) {
        
         $db = Zend_Registry::get('db');
         $tags = $db->select()
                 ->from(array('c' => 'tb_conteudo_relacionado_tag'))
                 ->joinInner(array('cc' => 'tb_conteudo_tag'),
                         'c.id_conteudo_tag = cc.id_conteudo_tag')
                 ->where('c.id_conteudo = ' . $id_conteudo)
                 ->order('c.id_conteudo_tag DESC')
                 ->query()
                 ->fetchAll();
         
         return $tags;
                 
    }
    
    public function deletarByConteudo($id_conteudo) {
        $this->delete('id_conteudo = ' . $id_conteudo);
    }
    
     /**
     * Deletar a partir de chave primária
     * @param type $pk 
     */
    public function delete($pk, $id_conteudo)
    {
        parent::delete("id_conteudo_tag = $pk AND id_conteudo = $id_conteudo");
    }
    
    
    
}