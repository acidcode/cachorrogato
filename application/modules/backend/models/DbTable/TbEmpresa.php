<?php

/**
 * Modelo fornecedores(empresa)
 * @author Matheus Marabesi <matheus.marabesi@gmail.com>
 * @since 2013-04-28
 * @license www.cachorrogato.com.br/sge
 */
class Backend_Model_DbTable_TbEmpresa extends Zend_Db_Table_Abstract {

    protected $_name = 'tb_fornecedor';

}

?>
