<?php

class Backend_Model_DbTable_TbQuestionario extends Zend_Db_Table_Abstract {

    protected $_name = 'tb_fornecedor_questionario';

    public function inserirPergunta($request) {
        echo '<pre>';
        print_r($request);
        echo '</pre>';
        $data = array(
            'id_setor_de_atividade' => $request['id_setor_de_atividade'],
            'id_tipo_campo' => $request['id_tipo_campo'],
            'vc_rotulo_campo' => $request['vc_rotulo_campo'],
            'bl_campo_obrigatorio' => ($request['bl_campo_obrigatorio']) ? $request['bl_campo_obrigatorio'] : 'nao'
        );

        if (empty($request['id_questionario'])) {
            $id_questionario = parent::insert($data);
        } else {
            $id_questionario = $request['id_questionario'];
            $where = array('id_questionario = ?' => $id_questionario);
            parent::update($data, $where);
        }

        $tbQuestionarioOpcoes = new Backend_Model_DbTable_TbQuestionarioOpcoes();
        $tbQuestionarioOpcoes->deleteByQuestionario($id_questionario);
        foreach ($request['vc_opcao_valor'] as $opcao) {
            $dataOpcao = array(
                'id_questionario' => $id_questionario,
                'id_tipo_campo' => $request['id_tipo_campo'],
                'vc_valor_opcao' => $opcao
            );
            $tbQuestionarioOpcoes->adicionarCampo($dataOpcao);
        }
    }

    public function findTiposDeCampos() {
        $db = Zend_Registry::get('db');

        $campos = $db->select()
                ->from('tb_fornecedor_questionario_campo')
                ->query()
                ->fetchAll();
        return $campos;
    }

    public function findQuestionario($where = null, $toArray = false) {
        $fetchAll = parent::fetchAll($where);

        if ($toArray) {
            return $fetchAll->toArray();
        } else {
            return $fetchAll;
        }
    }

    public function findAllQuestionario($where = null) {
        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from(array('tfq' => 'tb_fornecedor_questionario'))
                ->joinInner(array('tfqc' => 'tb_fornecedor_questionario_campo'), 'tfq.id_tipo_campo = tfqc.id_tipo_campo')
                ->joinInner(array('tfsa' => 'tb_fornecedor_setor_atividade'), 'tfq.id_setor_de_atividade = tfsa.id_setor_atividade');
        if ($where) {
            $select->where($where);
        }
        $fetchAll = $select->query()
                ->fetchAll();

        return $fetchAll;
    }

}

?>
