<?php

/**
 * Modelo RacaGato
 * @author André Gomes @acidcode
 * @since 2012-04-27
 * @license www.kernelpanic.com.br
 * @version 1.2.1.3
 */
class Backend_Model_DbTable_TbRacaGato 
    extends Zend_Db_Table_Abstract 
{

    protected $_name = "tb_raca_gato";
    
    /**
     * Cadastro
     * @param array $request
     * @return int Primary key 
     */
    public function cadastro($request)
    {
        $data = array('vc_raca_gato' => $request['vc_raca_gato'],
            'vc_apelidos' => $request['vc_apelidos'],
            'vc_descricao' => $request['vc_descricao'],
            'it_altura_a' => $request['it_altura_a'],
            'it_altura_b' => $request['it_altura_b'],
            'it_peso_a' => $request['it_peso_a'],
            'it_peso_b' => $request['it_peso_b'],
            'it_expectativa_vida_a' => $request['it_expectativa_vida_a'],
            'it_expectativa_vida_b' => $request['it_expectativa_vida_b'],
            'vc_cor_a' => $request['vc_cor_a'],
            'vc_cor_b' => $request['vc_cor_b'],
            'vc_cor_c' => $request['vc_cor_c'],
            'vc_cor_d' => $request['vc_cor_d'],
            'bl_temperamento' => $request['bl_temperamento'],
            'bl_relacionamento_crianca' => $request['bl_relacionamento_crianca'],
            'bl_relacionamento_gatos' => $request['bl_relacionamento_gatos'],
            'it_expectativa_vida_a' => $request['it_expectativa_vida_a'],
            'vc_espaco_necessario' => $request['vc_espaco_necessario'],
            'vc_cuidados' => $request['vc_cuidados'],
            'vc_alimentacao' => $request['vc_alimentacao'],
            'vc_habilidades' => $request['vc_habilidades'],
            'vc_manutencao' => $request['vc_manutencao'],
            'vc_ficha_tecnica' => $request['vc_ficha_tecnica'],
            'vc_historia' => $request['vc_historia'],
            'vc_seo_title' => $request['vc_seo_title'],
            'vc_seo_url' => $request['vc_seo_url'],
            'vc_seo_description' => $request['vc_seo_description'],
            'vc_seo_keywords' => $request['vc_seo_keywords'],
            'vc_seo_title' => $request['vc_seo_title']);

        if(empty($request['id_raca_gato']))
        {
           
            $data['dt_cadastro'] = date('Y-m-d');
            $data['id_usuario'] = Zend_Auth::getInstance()->getStorage()->read()->id;
            $pk = $this->insert($data);
        }
        else
        {
            $pk = $request['id_raca_gato'];
            $where = "id_raca_gato = " . $request['id_raca_gato'];
            $this->update($data, $where);
        }
        
        return $pk;
    }
    
    /**
     * Deletar a partir de chave primária
     * @param type $pk 
     */
    public function delete($pk)
    {
        parent::delete("id_raca_gato = $pk");
    }
    
    /**
     * Encontra pela chave primaria
     * @param int $pk
     * @return array 
     */
    public function find($pk)
    {
        $db = Zend_Registry::get('db');
        
        $raca_gato = $db->select()
                ->from(array('e' => 'tb_raca_gato'))
                ->where("id_raca_gato = $pk")
                ->query()
                ->fetch();
        
        return $raca_gato;
        
    }
    
    /**
     * Encontra pela chave primaria
     * @param int $pk
     * @return array 
     */
    public function findByUrl($url)
    {
        $db = Zend_Registry::get('db');
        
        $raca_gato = $db->select()
                ->from(array('e' => 'tb_raca_gato'))
                ->where("vc_seo_url = '$url'")
                ->query()
                ->fetch();
        
        return $raca_gato;
        
    }
    
    /**
     * Encontra todos pela condição do filtro
     * @param string $where
     * @return array 
     */
    public function findAll($where = '')
    {
        $db = Zend_Registry::get('db');
        
        $raca_gatos = $db->select()
                ->from(array('e' => 'tb_raca_gato'));
        
        if(count($where) > 0 && !empty($where))
        {
            $raca_gatos = $raca_gatos->where($where);
        }

        $raca_gatos = $raca_gatos->query()
                ->fetchAll();
        
        return $raca_gatos;
    }
    
    /**
     * Função para criar condição a partir do filtro
     * @param array $request
     * @return string 
     */
    public function condicao($request)
    {
        $where = array();
        
        if($request['id_raca_gato'] != 0 ||
                !empty($request['id_raca_gato']))
        {
            $where[] = 'id_raca_gato = ' . 
                    Zend_Filter::filterStatic(
                            $request['id_raca_gato'], 'Digits'
                            );
        }
        
        if(!empty($request['vc_titulo']))
        {
            $where[] = 'vc_titulo LIKE "%' . 
                    trim($request['vc_titulo']) . '%"';
        }
        
        return implode('AND', $where);
               
    }
    
    /**
     * Retorna um array para ser usados em formularios Select
     * @access static
     * @param type $todos
     * @return type 
     */
    public function getFormSelect($todos = false)
    {
        $select = array();
        
        $select[0] = "Main";
        
        $rowSet = $this->fetchAll();

        foreach($rowSet as $data) {
            $select[$data["id_raca_gato"]] = $data["vc_categoria"];
        }
        return $select;
    }
    
    
}