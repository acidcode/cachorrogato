<?php

/**
 * Modelo ConteudoCategoria
 * @author André Gomes @acidcode
 * @since 2012-04-27
 * @license www.kernelpanic.com.br
 * @version 1.2.1.3
 */
class Backend_Model_DbTable_TbConteudoCategoria 
    extends Zend_Db_Table_Abstract 
{

    protected $_name = "tb_conteudo_categoria";
    
    /**
     * Cadastro
     * @param array $request
     * @return int Primary key 
     */
    public function cadastro($request)
    {
        
        $data = array('vc_categoria' => $request['vc_categoria'],
            'id_parent' => $request['id_parent'],
            'vc_subtitulo' => $request['vc_subtitulo'],
            'vc_seo_title' => $request['vc_seo_title'],
            'vc_seo_url' => $request['vc_seo_url'],
            'vc_seo_keywords' => $request['vc_seo_keywords'],
            'vc_seo_description' => $request['vc_seo_description'],
            'vc_conteudo' => $request['vc_conteudo']);

        if(empty($request['id_conteudo_categoria']))
        {
           
            $data['dt_cadastro'] = date('Y-m-d');
            $data['id_usuario'] = Zend_Auth::getInstance()->getStorage()->read()->id;
            $pk = $this->insert($data);
        }
        else
        {
            $pk = $request['id_conteudo_categoria'];
            $where = "id_conteudo_categoria = " . $request['id_conteudo_categoria'];
            $this->update($data, $where);
        }
        
        return $pk;
    }
    
    /**
     * Cadastro
     * @param array $request
     * @return int Primary key 
     */
    public function cadastroAjax($request)
    {
        
        $data = array('vc_categoria' => $request['vc_categoria'],
            'id_parent' => $request['id_parent'],
            'dt_cadastro' => date('Y-m-d'),
            'id_usuario' => Zend_Auth::getInstance()->getStorage()->read()->id);

            $pk = $this->insert($data);
        
        return $pk;
    }
    
    /**
     * Deletar a partir de chave primária
     * @param type $pk 
     */
    public function delete($pk)
    {
        parent::delete("id_conteudo_categoria = $pk");
    }
    
    /**
     * Encontra pela chave primaria
     * @param int $pk
     * @return array 
     */
    public function find($pk)
    {
        $db = Zend_Registry::get('db');
        
        $conteudo_categoria = $db->select()
                ->from(array('e' => 'tb_conteudo_categoria'))
                ->where("id_conteudo_categoria = $pk")
                ->query()
                ->fetch();
        
        return $conteudo_categoria;
        
    }
    
    /**
     * Encontra pela chave primaria
     * @param int $pk
     * @return array 
     */
    public function findByUrl($url)
    {
        $db = Zend_Registry::get('db');
        
        $conteudo_categoria = $db->select()
                ->from(array('e' => 'tb_conteudo_categoria'))
                ->where("vc_seo_url = '$url'")
                ->query()
                ->fetch();
        
        return $conteudo_categoria;
        
    }
    
    /**
     * Encontra pela chave primaria
     * @param int $pk
     * @return array 
     */
    public function findByParent($pk)
    {
        $db = Zend_Registry::get('db');
        
        $conteudo_categoria = $db->select()
                ->from(array('e' => 'tb_conteudo_categoria'))
                ->where("id_parent = $pk")
                ->query()
                ->fetchAll();
        
        return $conteudo_categoria;
        
    }
    
    /**
     * Encontra todos pela condição do filtro
     * @param string $where
     * @return array 
     */
    public function findAll($where)
    {
        $db = Zend_Registry::get('db');
        
        $conteudo_categorias = $db->select()
                ->from(array('e' => 'tb_conteudo_categoria'));
        
        if(count($where) > 0 && !empty($where))
        {
            $conteudo_categorias = $conteudo_categorias->where($where);
        }

        $conteudo_categorias = $conteudo_categorias->query()
                ->fetchAll();
        
        return $conteudo_categorias;
    }
    
    /**
     * Função para criar condição a partir do filtro
     * @param array $request
     * @return string 
     */
    public function condicao($request)
    {
        $where = array();
        
        if($request['id_conteudo_categoria'] != 0 ||
                !empty($request['id_conteudo_categoria']))
        {
            $where[] = 'id_conteudo_categoria = ' . 
                    Zend_Filter::filterStatic(
                            $request['id_conteudo_categoria'], 'Digits'
                            );
        }
        
        if(!empty($request['vc_titulo']))
        {
            $where[] = 'vc_titulo LIKE "%' . 
                    trim($request['vc_titulo']) . '%"';
        }
        
        return implode('AND', $where);
               
    }
    
    /**
     * Retorna um array para ser usados em formularios Select
     * @access static
     * @param type $todos
     * @return type 
     */
    public function getFormSelect($todos = false)
    {
        $select = array();
        
        $select[0] = "Main";
        
        $rowSet = $this->fetchAll();

        foreach($rowSet as $data) {
            $select[$data["id_conteudo_categoria"]] = $data["vc_categoria"];
        }
        return $select;
    }
    
    public function getArvoreCategoria($id_categoria) {
        
        $db = Zend_Registry::get('db');
        $arrCategoria = array();
        
        //Busca a Categoria
        $categoria = $db->select()
                ->from(array('c' => 'tb_conteudo_categoria'))
                ->where('id_conteudo_categoria = ' . $id_categoria)
                ->query()
                ->fetch();
        
        $arrCategoria[$categoria['id_conteudo_categoria']] = $categoria;
        //Busca a Subcategoria
        $subCategorias = $db->select()
                ->from(array('c' => 'tb_conteudo_categoria'))
                ->where('id_parent = ' . $categoria['id_conteudo_categoria'])
                ->query()
                ->fetchAll();
        
        foreach($subCategorias as $subCategoria) {
            $arrCategoria[$categoria['id_conteudo_categoria']]['subcategorias'][$subCategoria['id_conteudo_categoria']] = $subCategoria;
        }
        
        return $arrCategoria;
        
    }
    
}