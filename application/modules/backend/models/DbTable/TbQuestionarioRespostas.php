<?php

class Backend_Model_DbTable_TbQuestionarioRespostas extends Zend_Db_Table_Abstract {

    protected $_name = 'tb_fornecedor_resposta';

    public function findAllByFornecedor($id_fornecedor) {
        $db = Zend_registry::get('db');
        $select = $db->select()
                ->from(array('tfr' => 'tb_fornecedor_resposta'))
                ->joinInner(array('tq' => 'tb_fornecedor_questionario'), 'tfr.id_questionario = tq.id_questionario')
                ->joinInner(array('tfqc' => 'tb_fornecedor_questionario_campo'), 'tq.id_tipo_campo = tfqc.id_tipo_campo')
                ->where('id_fornecedor = ' . (int) $id_fornecedor)
                ->query()
                ->fetchAll();

        $respostas = array();
        foreach ($select as $array) {
            if ($array['vc_campo'] == 'radio' || $array['vc_campo'] == 'checkbox' || $array['vc_campo'] == 'select') {
                $respostas['vc_resposta'][$array['vc_resposta']] = $this->getOpcoesResposta($array['vc_resposta']);
            }
        }

        $retorno = array(
            'questionario' => $select,
            'respostas' => $respostas
        );

        return $retorno;
    }

    private function getOpcoesResposta($id_opcao) {
        $db = Zend_registry::get('db');
        $select = $db->select()
                ->from('tb_fornecedor_questionario_opcao')
                ->where('id_opcao = ' . (int) $id_opcao)
                ->query()
                ->fetchAll();
        return $select;
    }

}

?>