<?php
/**
 * Modelo da tabela de Noticia
 * @author @acidcode
 * @since 20-02-2013
 */
class Backend_Model_DbTable_TbNoticia extends Zend_Db_Table_Abstract 
{

    protected $_name = "tb_noticia";
    
    /**
     * Cadastro
     * @param array $request
     * @return int Primary key 
     */
    public function cadastro($request)
    {
        
        
        $data = array('vc_titulo' => $request['vc_titulo'],
            'vc_subtitulo' => $request['vc_subtitulo'],
            'vc_conteudo' => $request['vc_conteudo'],
            'vc_keywords' => $request['vc_keywords'],
            'vc_description' => $request['vc_description']);
        
        if($nomeFoto = $this->uploadImagem()) {
           $data['vc_imagem'] = $nomeFoto; 
        } 
        
        if(empty($request['id_noticia']))
        {
            $data['dt_cadastro'] = date('Y-m-d h:i:s');
            $pk = $this->insert($data);
        }
        else
        {
            $pk = $request['id_noticia'];
            $where = "id_noticia = " . $request['id_noticia'];
            $this->update($data, $where);
        }
        
        return $pk;
    }
    
    public function uploadImagem() {
        $upload = new Zend_File_Transfer_Adapter_Http();
        if(count($upload->getFileName("vc_imagem",false)) != 0) {
            
            $caminho = PUBLIC_PATH . "/upload/noticias/";
            
            $upload->setDestination($caminho);
            $upload->receive();
            $upload->getFileName("vc_imagem");
            $nomeFoto = $upload->getFileName("vc_imagem",false);
            chmod($caminho.$upload->getFileName("vc_imagem",false), 0777);
            $this->_vc_imagem = $upload->getFileName("vc_imagem",false);
            return $nomeFoto;
        } else {
            return false;
        }
    }
    
    /**
     * Deletar a partir de chave primária
     * @param type $pk 
     */
    public function delete($pk)
    {
        parent::delete("id_noticia = $pk");
    }
    
    /**
     * Encontra pela chave primaria
     * @param int $pk
     * @return array 
     */
    public function find($pk)
    {
        $db = Zend_Registry::get('db');
        
        $noticia = $db->select()
                ->from(array('n' => 'tb_noticia'))
                ->where("id_noticia = $pk")
                ->query()
                ->fetch();
        
        return $noticia;
        
    }
    
    /**
     * Encontra todos pela condição do filtro
     * @param string $where
     * @return array 
     */
    public function findAll($where)
    {
        $db = Zend_Registry::get('db');
        
        $noticias = $db->select()
                ->from(array('e' => 'tb_noticia'));
        
        if(count($where) > 0 && !empty($where))
        {
            $noticias = $noticias->where($where);
        }

        $noticias = $noticias->query()
                ->fetchAll();
        
        return $noticias;
    }
    
    /**
     * Função para criar condição a partir do filtro
     * @param array $request
     * @return string 
     */
    public function condicao($request)
    {
        $where = array();
        
        if($request['id_noticia'] != 0 ||
                !empty($request['id_noticia']))
        {
            $where[] = 'id_noticia = ' . 
                    Zend_Filter::filterStatic(
                            $request['id_noticia'], 'Digits'
                            );
        }
        
        if(!empty($request['vc_titulo']))
        {
            $where[] = 'vc_titulo LIKE "%' . 
                    trim($request['vc_titulo']) . '%"';
        }
        
        return implode('AND', $where);
               
    }
    
    /**
     * Retorna um array para ser usados em formularios Select
     * @access static
     * @param type $todos
     * @return type 
     */
    public function getFormSelect($todos = false)
    {
        $select = array();
        
        if($todos) {
            $select[0] = "TODOS";
        }
        
        $rowSet = $this->fetchAll();

        foreach($rowSet as $data) {
            $select[$data["id_noticia"]] = $data["vc_titulo"];
        }
        return $select;
    }
    
    
    public function findNoticiaByTittle($noticia){
        $db = Zend_Registry::get('db');
        
        $fields = array(
            'id_noticia',
            'id_usuario',
            'vc_pagina',
            'vc_titulo',
            'vc_conteudo',
            'vc_description',
            'vc_keywords',
            'dt_cadastro'
        );
        
        $query = $db->select()
                ->from(array('c' => 'tb_noticia'),$fields)
                ->where("c.vc_titulo = '$noticia'")
                ->query()
                ->fetch();
               
                return $query;
                
                        
    }
 
    
}