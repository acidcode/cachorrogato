<?php

class Backend_Model_DbTable_TbRaca extends Zend_Db_Table_Abstract {

    protected $_name = 'tb_pet_raca';
    protected $_Instance;

    public function __construct() {
        $this->_Instance = Zend_Registry::get('db');
        parent::__construct();
    }

    public function init() {
        parent::init();
    }

    public function inserir($request) {
        $id_raca = false;

        $data = array(
            'vc_titulo' => $request['vc_titulo'],
            'vc_descricao' => $request['vc_descricao'],
            'bl_exibir_no_site' => ($request['bl_exibir_no_site'] == 'sim') ? 'sim' : 'nao'
        );
        if (!empty($request['vc_foto'])) {
            
        }
        if (!empty($request['id_raca'])) {
            $id_raca = $request['id_raca'];
            $where = 'id_raca = ' . $id_raca;
            parent::update($data, $where);
        } else {
            $id_raca = parent::insert($data);
        }

        if ($request['excluir_imagem'] == 'sim') {
            $this->deletarFoto($id_raca);
        }

        $this->inserirFoto($id_raca);

        return $id_raca;
    }

    private function inserirFoto($id_raca) {
        $upload = new Zend_File_Transfer();
        $arquivoInfo = $upload->getFileInfo();

        if ($arquivoInfo['vc_foto']['error'] != 4) {
            $adapter = new Zend_File_Transfer_Adapter_Http();
            $adapter->setDestination(PUBLIC_PATH . '/uploads/modules/back-end/raca/fotos/');

            $extensaoFoto = end(explode('.', $arquivoInfo['vc_foto']['name']));
            $nomeNovo = $id_raca . date('Ymdhms') . '.' . $extensaoFoto;

            $adapter->addFilter('Rename', array('target' => $adapter->getDestination() . '/' . $nomeNovo, 'overwrite' => true));
            if ($adapter->receive()) {
                require_once(LIB_PATH . '/Canvas/canvas.php');
                $canvas = new canvas();
                $canvas->carrega($adapter->getDestination() . '/' . $nomeNovo);
                $canvas->redimensiona(81, 81, 'crop');
                $canvas->grava($adapter->getDestination() . '/' . $nomeNovo);
                $this->atualizarFoto($id_raca, $nomeNovo);
            }
        }
    }

    private function atualizarFoto($id_raca, $vc_foto) {
        if (!empty($id_raca)) {
            $where = 'id_raca = ' . $id_raca;
            $data = array('vc_foto' => $vc_foto);

            return parent::update($data, $where);
        }
    }

    private function deletarFoto($id_raca) {
        $foto = $this->find($id_raca);
        unlink(PUBLIC_PATH . '/uploads/modules/back-end/raca/fotos/' . $foto['vc_foto']);
        $this->atualizarFoto($id_raca, NULL);
    }

    public function find($id_raca) {
        $raca = parent::find($id_raca);
        if ($raca) {
            $array = $raca->toArray();
            return $array[0];
        } else {
            return false;
        }
    }

}

?>
