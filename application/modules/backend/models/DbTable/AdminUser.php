<?php
class Backend_Model_DbTable_AdminUser extends Zend_Db_Table_Abstract
{
    protected $_name = "admin_user";
    protected $_Instance;
    protected $_helper;
    protected $_IsValid = false;
    protected $_id = NULL;
    protected $_username;
    protected $_firstname;
    protected $_secondname;
    protected $_password;
    protected $_about;
    protected $_email;
    protected $_created = NULL;
    protected $_modified = NULL;
    protected $_is_active = 1;
    protected $_rp_token = NULL;
    protected $_rp_token_created_at = NULL;
    public function __construct ()
    {
        $this->_Instance = Zend_Registry::get('db');
        $_helper = new Zend_View();
        $this->_helper = $_helper->getHelpers();
        parent::__construct();
    }
    public function init ()
    {
        parent::init();
    }
    public function AddUser ()
    {
        return parent::insert(
        array('id' => $this->getId(), 'username' => $this->getUsername(), 
        'firstname' => $this->getFirstname(), 
        'secondname' => $this->getSecondname(), 
        'password' => ('' != $this->getPassword()) ? md5($this->_password) : $this->getPassword(), 
        'email' => $this->getEmail(), 'created' => $this->getCreated(), 
        'modified' => new Zend_Db_Expr('NULL'), 
        'is_active' => $this->getIs_active(), 
        'rp_token' => new Zend_Db_Expr('NULL'), 
        'rp_token_created_at' => new Zend_Db_Expr('NULL')));
    }
    public function editUser ()
    {
        $data = array('username' => $this->getUsername(), 
        'firstname' => $this->getFirstname(), 
        'secondname' => $this->getSecondname(), 
        'email' => $this->getEmail(), 
        'modified' => $this->getModified(), 
        'about' => $this->getAbout(), 
        'is_active' => $this->getIs_active());

        if ('' != $this->getPassword()) {
            $data['password'] = ('' != $this->getPassword()) ? md5(
            $this->getPassword()) : $this->getPassword();
        }
        return $this->update($data, 'id = ' . $this->getId());
    }
    public function resetPassword ()
    {
        return parent::update(
        array('rp_token' => $this->getRp_token(), 
        'rp_token_created_at' => $this->getRp_token_created_at()), 
        'id = ' . $this->getId());
    }
    public function fetchUserByid ()
    {
        return $this->_Instance->select()
            ->from(array('users' => 'admin_user'))
            ->where('users.id = ' . $this->getId())
            ->query()
            ->fetchAll();
    }
    public function removeUser ()
    {
        return parent::delete('id = ' . $this->getId());
    }
    public function sendEmailUser ()
    {
        $config = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
        $Auth = $config['Account'];
        $Settings = array('auth' => 'login', 'ssl' => 'ssl', 'port' => '465', 
        'username' => $Auth['user'], 'password' => $Auth['pass']);
        $_Transport = new Zend_Mail_Transport_Smtp($Auth['host'], $Settings);
        $mail = new Zend_Mail('utf-8');
        $mail->setBodyHtml(
        'Cadastro realizado com sucesso Utilize o nome de usuário , senha e o link abaixo para acessa o Administrador.
         <a href="' .
         $this->_helper->serverUrl('/admin') . '">' .
         $this->_helper->serverUrl('/admin') . '</a><br/>
         Login:' . $this->getUsername() . '<br/>
         Password:' . $this->getPassword());
        $mail->setFrom($this->_Auth['user'], 'Administrador');
        $mail->addTo($this->getEmail(), 
        $this->getFirstname() . $this->getSecondname());
        $mail->setSubject('Cadastrado realizado com sucesso');
        return $mail->send($_Transport);
    }
    public function userExists ($except = null)
    {
        $user = $this->_Instance->select()
            ->from('admin_user', array('COUNT(*) as count'))
            ->where('username = "' . $this->getUsername() . '" ');
        if ($except != null)
            $user->where('id != ' . $except);
        $user = $user->query()->fetch();
        return $user['count'];
    }
    
   public function fetchUserByEmail($email,$except = null) {
   	 if(null===$except){
    	return $this->getDefaultAdapter()->query('SELECT *,COUNT(id) as count FROM `admin_user` WHERE email = ? ; ',array($email))->fetch();
     }else{
    	return $this->getDefaultAdapter()->query('SELECT *,COUNT(id) as count FROM `admin_user` WHERE email = ? AND id = ? ; ',array($email,$except))->fetch();
     }
   }

   
    public function countUsers(){
        $user = $this->_Instance->select()
                                ->from('admin_user', array('COUNT(*) as count'))
                                ->query()
                                ->fetch();
        return $user['count'];
   }
    
    public function sendResetPassword ()
    {
        $config = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
        $Auth = $config['Account'];
        $Settings = array('auth' => 'login', 'ssl' => 'ssl', 'port' => '465', 
        'username' => $Auth['user'], 'password' => $Auth['pass']);
        $_Transport = new Zend_Mail_Transport_Smtp($Auth['host'], $Settings);
        $mail = new Zend_Mail('utf-8');
        $mail->setBodyHtml(
        'Para redefinir a senha utilize o link abaixo <br>  
        		<a href="' .
         $this->_helper->serverUrl(
        '/backend/user/reset/password/token/' . $this->getRp_token()) . '">' .
         $this->_helper->serverUrl(
        '/backend/user/reset/password/token/' . $this->getRp_token()) . '</a>');
        $mail->setFrom($Auth['user'], 'Administrador');
        $mail->addTo($this->getEmail(), 
        $this->getFirstname() . $this->getSecondname());
        $mail->setSubject('Link de redefinição de senha.');
        return $mail->send($_Transport);
    }
    
    public function changePassword(){

        $data = array('password' => ('' != $this->getPassword()) ? md5($this->getPassword()) : $this->getPassword());
        		
        return parent::update($data, 'id = ' . $this->getId());
        
    }
    
    public function listAllUsers ()
    {
        return $this->_Instance->select()
            ->from('admin_user',array(new Zend_Db_Expr(" DATE_FORMAT(created,'%d/%c/%Y - %H:%i:%s') as createdFormatted "),'*'))
            ->query()
            ->fetchAll();
    }
    public function authUser ()
    {
        $authUser = Zend_Auth::getInstance();
        $adapter = new Zend_Auth_Adapter_DbTable($this->_Instance, 'admin_user', 
        'username', 'password', ' md5(?) AND is_active !=0');
        
        $adapter->setIdentity($this->getUsername())
            ->setCredential($this->getPassword());
        $user = $authUser->authenticate($adapter);
        $this->_IsValid = $user->isValid();
               
        if ($this->isValid()) {
        	$credential = $adapter->getResultRowObject(/* null,'password' */);
        	$useRole    = $this->getDefaultAdapter()
        	                   ->select()
        	                   ->from(array('ur'=>'admin_user_role'))     	
        	                   ->join(array('r'=>'admin_roles'),'r.role_id = ur.role_id')
        	                   ->where('user_id = ?',$credential->id)
        	                   ->query()
        	                   ->fetchObject();
        	
            $user             = new stdClass();
            $user->id         = $credential->id;
            $user->username   = $credential->username;
            $user->firstname  = $credential->firstname;
            $user->secondname = $credential->secondname;
            $user->email      = $credential->email;
            $user->about      = $credential->about;
            $user->roleid     = $useRole->role_id;
            $user->rolename   = $useRole->name;
            
            $authUser->setStorage(
            new Zend_Auth_Storage_Session(md5('Admin_Auth')));
            $authUser->getStorage()->write($user);
        }
        
        return $this;
    }
    public function isValid ()
    {
        return $this->_IsValid;
    }
    public function getAuth ()
    {
        $authUser = Zend_Auth::getInstance();
        return $authUser->setStorage(
        new Zend_Auth_Storage_Session(md5('Admin_Auth')));
        ;
    }
    /**
     *
     * @return the $_id
     */
    public function getId ()
    {
        if (NULL == $this->_id) {
            $this->_id = new Zend_Db_Expr('NULL');
        }
        return $this->_id;
    }
    /**
     *
     * @param $_id field_type           
     */
    public function setId ($_id)
    {
        $this->_id = $_id;
        return $this;
    }
    /**
     *
     * @return the $_username
     */
    public function getUsername ()
    {
        return $this->_username;
    }
    /**
     *
     * @param $_username field_type           
     */
    public function setUsername ($_username)
    {
        $this->_username = $_username;
        return $this;
    }
    /**
     *
     * @return the $_firstname
     */
    public function getFirstname ()
    {
        return $this->_firstname;
    }
    /**
     *
     * @param $_firstname field_type           
     */
    public function setFirstname ($_firstname)
    {
        $this->_firstname = $_firstname;
        return $this;
    }
    /**
     *
     * @return the $_secondname
     */
    public function getSecondname ()
    {
        return $this->_secondname;
    }
    /**
     *
     * @param $_secondname field_type           
     */
    public function setSecondname ($_secondname)
    {
        $this->_secondname = $_secondname;
        return $this;
    }
    /**
     *
     * @return the $_password
     */
    public function getPassword ()
    {
        return $this->_password;
    }
    /**
     *
     * @param $_password field_type           
     */
    public function setPassword ($_password)
    {
        $this->_password = $_password;
        return $this;
    }
    /**
     *
     * @return the $_email
     */
    public function getEmail ()
    {
        return $this->_email;
    }
    /**
     *
     * @param $_email field_type           
     */
    public function setEmail ($_email)
    {
        $this->_email = $_email;
        return $this;
    }
    /**
     *
     * @return the $_created
     */
    public function getCreated ()
    {
        if (NULL == $this->_created) {
            $this->_created = new Zend_Db_Expr('CURRENT_TIMESTAMP');
        }
        return $this->_created;
    }
    /**
     *
     * @param $_created field_type           
     */
    public function setCreated ($_created)
    {
        $this->_created = $_created;
        return $this;
    }
    /**
     *
     * @return the $_modified
     */
    public function getModified ()
    {
        if (NULL == $this->_modified) {
            $this->_modified = new Zend_Db_Expr('CURRENT_TIMESTAMP');
        }
        return $this->_modified;
    }
    
    /**
     *
     * @return the $_modified
     */
    public function getAbout ()
    {
        return $this->_about;
    }
    
    public function setAbout ($_about)
    {
        $this->_about = $_about;
        return $this;
    }
    /**
     *
     * @param $_modified field_type           
     */
    public function setModified ($_modified)
    {
        $this->_modified = $_modified;
        return $this;
    }
    /**
     *
     * @return the $_is_active
     */
    public function getIs_active ()
    {
        return $this->_is_active;
    }
    /**
     *
     * @param $_is_active field_type           
     */
    public function setIs_active ($_is_active)
    {
        $this->_is_active = $_is_active;
        return $this;
    }
    /**
     *
     * @return the $_rp_token
     */
    public function getRp_token ()
    {
        return $this->_rp_token;
    }
    /**
     *
     * @param $_rp_token field_type           
     */
    public function setRp_token ($_rp_token)
    {
        $this->_rp_token = $_rp_token;
        return $this;
    }
    /**
     *
     * @return the $_rp_token_created_at
     */
    public function getRp_token_created_at ()
    {
        if (NULL == $this->_rp_token_created_at) {
            $this->_rp_token_created_at = new Zend_Db_Expr('CURRENT_TIMESTAMP');
        }
        return $this->_rp_token_created_at;
    }
    /**
     *
     * @param $_rp_token_created_at field_type           
     */
    public function setRp_token_created_at ($_rp_token_created_at)
    {
        $this->_rp_token_created_at = $_rp_token_created_at;
        return $this;
    }
}
?>