<?php
class Backend_Model_DbTable_ShareGallery extends Zend_Db_Table_Abstract{

	protected $_name = "empreendimento_galeria_itens";
	
	public function getGalleryItem($id){
		
		return $this->getAdapter()->select()->from($this->_name)->where('id = ?',$id)->query()->fetch();
		
	}
	
	public function removeGalleryItem($id){
		return $this->getAdapter()->delete($this->_name,'id ='.$id);
	}
	
}

?>