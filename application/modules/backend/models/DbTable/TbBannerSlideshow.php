<?php
/**
 * Modelo de acesso a base de dados
 * @author André Gomes @acidcode
 * @since 2012-06-01
 * @copyright GPO4  - www.cachorrogato.com.br - andre.kernelpanic@gmail.com
 */
class Backend_Model_DbTable_TbBannerSlideshow extends Zend_Db_Table_Abstract
{

    protected $_name = 'tb_banner_slideshow';
    private $_vc_imagem;

    public function cadastro($request) {
        $data = array('vc_banner_slideshow' => $request['vc_banner_slideshow'],
            'vc_url' => $request['vc_url'],
            'vc_descricao' => $request['vc_descricao'],
            'bl_ativo' => $request['bl_ativo']);
        
        if($this->uploadBannerSlideshow()) {
            $data['vc_imagem'] = $this->_vc_imagem;
        }
        
        if((int) $request['id_banner_slideshow'] == 0) {
            return $this->insert($data);
        } else {
            $where = "id_banner_slideshow = " . $request['id_banner_slideshow'];
            $this->update($data, $where);
            return $request['id_banner_slideshow'];
            
        }
    }
    
    public function visualizar($tipo) {
        $where = "bl_tipo = '$tipo' AND bl_ativo = 'sim'";
        $banner_slideshow = $this->fetchAll($where, "RAND()", 1)->toArray();
        return $banner_slideshow[0];
    }
    
    public function visualizacao($id_banner_slideshow) {
        
        if(empty($id_banner_slideshow)) {
            return false;
        }
        
        $where = "id_banner_slideshow = " . Zend_Filter::filterStatic($id_banner_slideshow, 'Digits');
        $banner_slideshow = $this->fetchRow($where);
        $clique = $banner_slideshow->it_visualizacao + 1;
        
        $this->update(array('it_visualizacao' => $clique), $where);
    }
    
    public function clique($id_banner_slideshow) {
        $where = "id_banner_slideshow = " . Zend_Filter::filterStatic($id_banner_slideshow, 'Digits');
        $banner_slideshow = $this->fetchRow($where);
        $clique = $banner_slideshow->it_clique + 1;
        
        $this->update(array('it_clique' => $clique), $where);
        
        return $banner_slideshow->vc_url;
    }
    
    private function uploadBannerSlideshow() {

        $upload = new Zend_File_Transfer_Adapter_Http();
        
        if(count($upload->getFileName("vc_imagem",false)) != 0) {
            
            $caminho = PUBLIC_PATH . "/upload/bannerslide/";
            $upload->setDestination($caminho);
            $upload->receive();
            $upload->getFileName("vc_imagem");
            $nomeFoto = $upload->getFileName("vc_imagem",false);
            chmod($caminho.$upload->getFileName("vc_imagem",false), 0777);
            $this->_vc_imagem = $upload->getFileName("vc_imagem",false);
            return true;
        } else {
            return false;
        }
    }
}

