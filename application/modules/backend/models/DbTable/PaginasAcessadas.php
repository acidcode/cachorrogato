<?php
ini_set("display_errors", 1);
class Backend_Model_DbTable_PaginasAcessadas extends Zend_Db_Table_Abstract
{
    protected $_name = "paginas_acessadas";
    protected $_Instance;

    public function __construct () {
        $this->_Instance = Zend_Registry::get('db');
        parent::__construct();
    }

    public function init () {
        parent::init();
    }

    public function inserir($pagina_acessada) {
        $pagina_acessada["id"] = null;
        $retorno =  parent::insert($pagina_acessada);  
        return $retorno;
    }
}
?>