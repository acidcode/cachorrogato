<?php
class Backend_Model_DbTable_Acesso extends Zend_Db_Table_Abstract
{
    protected $_name = "acessos";
    protected $_Instance;

    public function __construct () {
        $this->_Instance = Zend_Registry::get('db');
        parent::__construct();
    }

    public function init () {
        parent::init();
    }

    public function inserir($acesso) {
        $acesso["id"] = null;
        return parent::insert($acesso);
    }
}
?>