<?php

class Backend_Model_DbTable_TbCategoria extends Zend_Db_Table_Abstract {

    protected $_name = 'tb_conteudo_categoria';
    protected $_Instance;

    public function __construct() {
        $this->_Instance = Zend_Registry::get('db');
        parent::__construct();
    }

    public function init() {
        parent::init();
    }

    public function atualizarCategoria($request) {
        $data = array(
            'vc_conteudo' => $request['vc_conteudo'],
            'vc_seo_title' => $request['vc_seo_title'],
            'vc_seo_url' => $request['vc_seo_url'],
            'vc_seo_keywords' => $request['vc_seo_keywords'],
            'vc_seo_description' => $request['vc_seo_description'],
            'bl_rel' => ($request['bl_rel']) ? $request['bl_rel'] : NULL,
            'id_slider' => $request['id_slider']
        );

        $where = 'id_categoria = ' . $request['id_categoria'];

        return parent::update($data, $where);
    }

}

?>