<?php
class Backend_Model_DbTable_UrlRewrite extends Zend_Db_Table_Abstract
{
    const Products = 'Products';
    
    const Categories = 'Category';
    
    protected $_name = "urls_rewrite";
    protected $_Instance;
    protected $_UrlRewrite_Id;
    protected $_UrlRewrite_Target;
    protected $_UrlRewrite_Path;
    protected $_UrlRewrite_Category_Id = NULL;
    protected $_UrlRewrite_Product_Id = NULL;
    
    
    public function __construct ()
    {
        $this->_Instance = Zend_Registry::get('db');
        parent::__construct();
    }
    public function init ()
    {
        parent::init();
    }
    public function insertUrlRewrite ()
    {
        $data = array(
        'UrlRewrite_Id' => new Zend_Db_Expr('NULL'), 
        'UrlRewrite_Target' => $this->getUrlRewrite_Target(),
        'UrlRewrite_Path' => $this->getUrlRewrite_Path(),
        'UrlRewrite_dateRecorder' => new Zend_Db_Expr('CURRENT_TIMESTAMP'), 
        'UrlRewrite_Product_Id' => $this->getUrlRewrite_Product_Id(), 
        'UrlRewrite_Category_Id' => $this->getUrlRewrite_Category_Id()
        );
        return $this->insert($data);
    }
    public function editUrlRewrite ()
    {
    
        if(self::Products==$this->getUrlRewrite_Target()){
            $where = 'UrlRewrite_Product_Id = '.$this->getUrlRewrite_Product_Id();	
        }elseif(self::Categories==$this->getUrlRewrite_Target()){
            $where = 'UrlRewrite_Category_Id = '.$this->getUrlRewrite_Category_Id();
        }
        
        $data = array(
        'UrlRewrite_Path' => $this->getUrlRewrite_Path(), 
        'UrlRewrite_Product_Id' => $this->getUrlRewrite_Product_Id(), 
        'UrlRewrite_Category_Id' => $this->getUrlRewrite_Category_Id());
        return $this->update($data,  $where);
    }
    
    
    public function removeUrlRewrite ($id)
    {
        return $this->delete('UrlRewrite_Id = ' . $id);
    }
    public function pathExists ($except = null)
    {
        $path = $this->_Instance->select()
            ->from('urls_rewrite', array('COUNT(*) as count'))
            ->where(
        ' UrlRewrite_Path = "' .
         trim(
        str_replace(array('.html', '.htm'), array('', ''), 
        $this->getUrlRewrite_Path())) . '" ');
        if ($except != null) {
            $path->where(' UrlRewrite_Id != ' . $except);
        }
        $path = $path->query()->fetch();
        return $path['count'];
    }
    public  function setUrlContentsById(){
        
        if(self::Products==$this->getUrlRewrite_Target()){
            $where = 'UrlRewrite_Product_Id = '.$this->getUrlRewrite_Product_Id();	
        }elseif(self::Categories==$this->getUrlRewrite_Target()){
            $where = 'UrlRewrite_Category_Id = '.$this->getUrlRewrite_Category_Id();
        }
        
    $Url = $this->_Instance->select()
                 ->from('urls_rewrite')
                 ->where($where)
                 ->where('UrlRewrite_Target = "'.$this->getUrlRewrite_Target().'" ')
                 ->query()
                 ->fetchAll();
    
       $Url =$Url[0];
       
     $this->setUrlRewrite_Path($Url['UrlRewrite_Path'])
          ->setUrlRewrite_Target($Url['UrlRewrite_Target'])
          ->setUrlRewrite_Product_Id($Url['UrlRewrite_Product_Id'])
          ->setUrlRewrite_Category_Id($Url['UrlRewrite_Category_Id'])
          ->setUrlRewrite_Id($Url['UrlRewrite_Id']);
     
         return $this;
        
    }
    public function setUrlContents ()
    {
        if ($this->getUrlRewrite_Path() != '') {
            $Urls = $this->_Instance->select()
                ->from('urls_rewrite')
                ->where(
            'UrlRewrite_Path = "' .
             trim(str_replace('.html', '', $this->getUrlRewrite_Path())) . '" ')
               /*  ->where(
            'UrlRewrite_Target = "' . $this->getUrlRewrite_Target() . '" ') */
                ->query()
                ->fetchAll();
            $Url = $Urls[0];
            $this->setUrlRewrite_Target($Url['UrlRewrite_Target'])
                ->setUrlRewrite_Product_Id($Url['UrlRewrite_Product_Id'])
                ->setUrlRewrite_Category_Id($Url['UrlRewrite_Category_Id']);
        }
        return $this;
    }
    public function getContentById ()
    {
       
        $id = '';
        if ('Products' == $this->getUrlRewrite_Target()) {
            $id = $this->getUrlRewrite_Product_Id();
            
        } elseif ('Category' == $this->getUrlRewrite_Category_Id()) {
            $id = $this->getUrlRewrite_Category_Id();
        }
        return  $id;
    }
    public function isValid ($Suffix = '.html')
    {
        if (strstr($this->getUrlRewrite_Path(), $Suffix) != false)
            return true;
    }
    public function format ()
    {
        $searchFor = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'à', 'á', 'â', 'ã', 
        'ä', 'å', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 
        'È', 'É', 'Ê', 'Ë', 'è', 'é', 'ê', 'ë', 'Ç', 'ç', 'Ì', 'Í', 'Î', 'Ï', 
        'ì', 'í', 'î', 'ï', 'Ù', 'Ú', 'Û', 'Ü', 'ù', 'ú', 'û', 'ü', 'ÿ', 'Ñ', 
        'ñ', '(', ')', 'º', '°', '!', '@', '#', '$', '%', '?', ' ', '.html', 
        "'", '"', " ", "--", 'â€“', "---");
        $replaceFor = array('A', 'A', 'A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 
        'a', 'a', 'O', 'O', 'O', 'O', 'O', 'O', 'o', 'o', 'o', 'o', 'o', 'o', 
        'E', 'E', 'E', 'E', 'e', 'e', 'e', 'e', 'C', 'c', 'I', 'I', 'I', 'I', 
        'i', 'i', 'i', 'i', 'U', 'U', 'U', 'U', 'u', 'u', 'u', 'u', 'y', 'N', 
        'n', ' ', '', '', '', '', '', '', '', '', '', '-', '', '', '', "-", "-", 
        '', '-');
        $url = ((strtolower(
        str_replace($searchFor, $replaceFor, 
        trim(urldecode($this->getUrlRewrite_Path()))))));
        $this->setUrlRewrite_Path($url);
        return $this;
    }
    /**
	 * @return the $_UrlRewrite_Id
	 */
	public function getUrlRewrite_Id() {
		return $this->_UrlRewrite_Id;
	}

	/**
	 * @param field_type $_UrlRewrite_Id
	 */
	public function setUrlRewrite_Id($_UrlRewrite_Id) {
		$this->_UrlRewrite_Id = $_UrlRewrite_Id;
		return $this;
	}

	/**
     *
     * @return the $_UrlRewrite_Target
     */
    public function getUrlRewrite_Target ()
    {
        return $this->_UrlRewrite_Target;
    }
    /**
     *
     * @param $_UrlRewrite_Target field_type           
     */
    public function setUrlRewrite_Target ($_UrlRewrite_Target)
    {
        $this->_UrlRewrite_Target = $_UrlRewrite_Target;
        return $this;
    }
    /**
     *
     * @return the $_UrlRewrite_Path
     */
    public function getUrlRewrite_Path ()
    {
        return $this->_UrlRewrite_Path;
    }
    /**
     *
     * @param $_UrlRewrite_Path field_type           
     */
    public function setUrlRewrite_Path ($_UrlRewrite_Path)
    {
        $this->_UrlRewrite_Path = $_UrlRewrite_Path;
        return $this;
    }
    /**
     *
     * @return the $_UrlRewrite_Category_Id
     */
    public function getUrlRewrite_Category_Id ()
    {
        if (NULL == $this->_UrlRewrite_Category_Id) {
            $this->_UrlRewrite_Category_Id = new Zend_Db_Expr('NULL');
        }
        return $this->_UrlRewrite_Category_Id;
    }
    /**
     *
     * @param $_UrlRewrite_Category_Id NULL           
     */
    public function setUrlRewrite_Category_Id ($_UrlRewrite_Category_Id = NULL)
    {
        $this->_UrlRewrite_Category_Id = $_UrlRewrite_Category_Id;
        return $this;
    }
    /**
     *
     * @return the $_UrlRewrite_Product_Id
     */
    public function getUrlRewrite_Product_Id ()
    {
        if (NULL == $this->_UrlRewrite_Product_Id) {
            $this->_UrlRewrite_Product_Id = new Zend_Db_Expr('NULL');
        }
        return $this->_UrlRewrite_Product_Id;
    }
    /**
     *
     * @param $_UrlRewrite_Product_Id NULL           
     */
    public function setUrlRewrite_Product_Id ($_UrlRewrite_Product_Id = NULL)
    {
        $this->_UrlRewrite_Product_Id = $_UrlRewrite_Product_Id;
        return $this;
    }
}
?>