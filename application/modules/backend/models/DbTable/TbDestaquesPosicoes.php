<?php

/**
 * Modelo Anuncio
 * @author André Gomes @acidcode
 * @since 2012-05-08
 * @license www.cachorrogato.com.br/sge
 * @version 1.2.1.3
 */
class Backend_Model_DbTable_TbDestaquesPosicoes
    extends Zend_Db_Table_Abstract 
{

    protected $_name = "destaque_posicoes";

    
    public function init() {
        parent::init();
        
    
    }
    
    /**
     * Cadastro
     * @param array $request
     * @return int Primary key 
     */
      public function cadastro($request) {

        $sessao = new Zend_Session_Namespace('login_sessao');
        $sessao = $sessao->arr;

        $data = array('nome' => $request['nome']);

        
     
        

        if (empty($request['id'])) {
            $pk = $this->insert($data);
        } else {
            $pk = $request['id'];
            $where = "id = " . $request['id'];
            $this->update($data, $where);
        }


        return $pk;
    }
    
    /**
     * Deletar a partir de chave primária
     * @param type $pk 
     */
    public function delete($pk)
    {
        parent::delete("id = $pk");
    }
    
    /**
     * Encontra pela chave primaria
     * @param int $pk
     * @return array 
     */
    public function find($pk)
    {
        $db = Zend_Registry::get('db');
        
        $destaque = $db->select()
                ->from(array('e' => 'destaque_posicoes'))
                ->where("id_empreendimento = $pk")
                ->query()
                ->fetch();
       
        return $destaque;
        
    }
    public function carregar($pk)
    {
        $db = Zend_Registry::get('db');
        
        $destaque = $db->select()
                ->from(array('e' => 'destaque_posicoes'))
                ->where("id = $pk")
                ->query()
                ->fetch();
       
        return $destaque;
        
    }
    
    
    
    /**
     * Encontra todos pela condição do filtro
     * @param string $where
     * @return array 
     */
   public function findAll($where, $order = false)
    {
         $db = Zend_Registry::get('db');
        
        $destaque_posicoes = $db->select()
                ->from(array('e' => 'destaque_posicoes'));
        
        if(count($where) > 0 && !empty($where))
        {
            $destaque_posicoes = $destaque_posicoes->where($where);
        }

        $destaque_posicoes = $destaque_posicoes->query()
                ->fetchAll();
     
        return $destaque_posicoes;
    }
    
    /**
     * Função para criar condição a partir do filtro
     * @param array $request
     * @return string 
     */
    public function condicao($request, $adicional = false)
    {
        $where = array();
        if($request['id_anuncio'] != 0 ||
                !empty($request['id_anuncio']))
        {
            $where[] = 'id_anuncio = ' . 
                    Zend_Filter::filterStatic(
                            $request['id_anuncio'], 'Digits'
                            );
        }
        
        if(!empty($request['vc_email']))
        {
            $where[] = '(u.vc_email LIKE "%' . 
                    trim($request['vc_email']) . '%" OR
                        u.vc_nome LIKE "%' . 
                    trim($request['vc_email']) . '%")';
        }
        
        if($request['id_plano'] != 0 ||
                !empty($request['id_plano']))
        {
            $where[] = 'e.id_plano = ' . 
                    Zend_Filter::filterStatic(
                            $request['id_plano'], 'Digits'
                            );
        }
        
        if($request['bl_status_pagamento'] != 0 ||
                !empty($request['bl_status_pagamento']))
        {
            $where[] = 'e.bl_status_pagamento = "' . 
                            $request['bl_status_pagamento'] . '"';
        }
        
        if($adicional) {
            $where[] = $adicional;
        }

        return implode(' AND ', $where);
               
    }
    
    
    /**
     * Função para criar condição a partir do filtro
     * @param array $request
     * @return string 
     */
    public function condicaoBuscaRapida($request)
    {
        $where = array();
        if($request['codigo'] != "Código" && !empty($request['codigo']))
        {
            $where = 'id_anuncio = ' . 
                    Zend_Filter::filterStatic(
                            $request['codigo'], 'Digits'
                            );
            
            
            return $where;
        }
        
        if(!empty($request['id_anuncio']))
        {
            $where[] = '(e.id_anuncio = "' . 
                    trim($request['id_anuncio']) . '")';
        }
        
        if(!empty($request['vc_busca_rapida_cidade']))
        {
            $where[] = '(e.vc_cidade = "' . 
                    trim($request['vc_busca_rapida_cidade']) . '")';
        }
        if(!empty($request['vc_busca_rapida_bairro']))
        {
            $where[] = '(e.vc_bairro = "' . 
                    trim($request['vc_busca_rapida_bairro']) . '")';
        }
        
        if(!empty($request['vc_busca_rapida_faixa_preco']))
        {
            if( $request['vc_busca_rapida_faixa_preco'] == 0) {
                $where[] = '(e.mn_valor_imovel <= "' . 
                    $request['vc_busca_rapida_faixa_preco'] . '")';
            } 
        }
        
        if(!empty($request['vc_busca_rapida_faixa_preco_compra']))
        {
            if( $request['vc_busca_rapida_faixa_preco_compra'] == 0) {
                $where[] = '(e.mn_valor_imovel <= "' . 
                    $request['vc_busca_rapida_faixa_preco_compra'] . '")';
            } 
        }
        
        if(!empty($request['vc_busca_rapida_faixa_preco_locacao']))
        {
            if( $request['vc_busca_rapida_faixa_preco_locacao'] == 0) {
                $where[] = '(e.mn_valor_imovel <= "' . 
                    $request['vc_busca_rapida_faixa_preco_locacao'] . '")';
            } 
        }
        
        if($request['vc_busca_rapida_tipo_imovel'] != 0 ||
                !empty($request['vc_busca_rapida_tipo_imovel']))
        {
            $where[] = 'e.bl_tipo_imovel = ' . 
                    Zend_Filter::filterStatic(
                            $request['vc_busca_rapida_tipo_imovel'], 'Digits'
                            );
        }
        
        if($request['vc_busca_categoria_imovel'] != 0 ||
                !empty($request['vc_busca_categoria_imovel']))
        {
            $where[] = 'e.id_categoria_anuncio = ' . 
                    Zend_Filter::filterStatic(
                            $request['vc_busca_categoria_imovel'], 'Digits'
                            );
        }
        
        if($request['vc_busca_rapida_finalidade'] != 0 ||
                !empty($request['vc_busca_rapida_finalidade']))
        {
            $where[] = 'e.bl_finalidade = ' . 
                    Zend_Filter::filterStatic(
                            $request['vc_busca_rapida_finalidade'], 'Digits'
                            );
        }

        return implode(' AND ', $where);
               
    }
    
    
    /**
     * 
     * @param type $pk
     * @return string 
     */
    public function uploadFotos($pk) {
        
        $fotos = array();

        for ($x = 1; $x <= 5; $x++) {
            
            if(empty($_FILES["vc_foto_$x"]['name'])) {
                continue;
            } else {
                $nomeFile = $pk . $x . ".jpg";
                $caminho = UPLOAD_PATH . "/anuncio/";
                $novoNome = $caminho . $nomeFile;
                $fotos[$x] = $nomeFile;
                
                if(move_uploaded_file($_FILES["vc_foto_$x"]['tmp_name'], $novoNome)) {
                    chmod($novoNome, 0777);
                    $fotos[$x] = $nomeFile;
                } else {
                    continue;
                }
                
                $gd = new Frontend_Service_Gd($novoNome);
                
                //calculo de proporcao da imagema
                $larg=350; // largura do arquivo ja redimensionado (em PX) 
                $original=imagecreatefromjpeg($novoNome);
                $larg_foto=imagesx($original);
                $alt_foto=imagesy($original);
                $fator=$alt_foto/$larg_foto;

                // faz o calcula da altura nova
                $altura_nova=$larg*$fator;

                $gd->resizeImage(
                    $larg, #$newWidth,
                    $altura_nova #$newHeight,
                );

                $gd->setFormatOutPut("jpg");
                #Salva o arquivo, não fornecer extensão do arquivo, já definido no setFormatOutPut
                $gd->save($caminho.$pk . $x);
                
            }
            
        }
        
        foreach($fotos as $foto) {
            $fotoDb = $this->_tbAnuncioFoto->fetchRow(
                    "vc_foto = '$foto'"
                    );
                    
            if(is_null($fotoDb)) {
                $data = array('id_anuncio' => $pk,
                    'vc_foto' => $foto,
                    'dt_cadastro' => date('Y-m-d'));
                $this->_tbAnuncioFoto->insert($data);
            } else {
                $data = array('vc_foto' => $foto,
                    'dt_cadastro' => date('Y-m-d'));
                if(!empty($fotoDb['id_anuncio_foto'])) {
                    $this->_tbAnuncioFoto->update($data,
                            'id_anuncio_foto = ' . $fotoDb['id_anuncio_foto']);
                }
            }
                   
        }
        return $fotos;
        
    }
    
    public function gerenciarAnuncio($request, $idUsuario) {
        $anuncios = $this->fetchAll(
                "id_usuario = $idUsuario");
        
        foreach($anuncios as $anuncio) {
            $data = array('bl_status' => $request['bl_status_anuncio_' . $anuncio['id_anuncio']]);
            $this->update($data, "id_anuncio = " . $anuncio['id_anuncio']);
        }
    }
    
    /**
     * Retorna um array para ser usados em formularios Select
     * @access static
     * @param type $todos
     * @return type 
     */
    public function getFormSelect($todos = false)
    {
        $select = array();
        
        if($todos) {
            $select[0] = "TODOS";
        }
        
        $rowSet = $this->fetchAll();

        foreach($rowSet as $data) {
            $select[$data["id_anuncio"]] = $data["vc_titulo"];
        }
        return $select;
    }
    
    public function denunciar($request) {

        $mensagem = 
        "Nome: " . $request['nome'] ."<br />" .
        "Email: " . $request['email'] ."<br />" .
        "Telefone: " . $request['telefone'] ."<br />" .
        "Tipo da denuncia: " . $request['tipo_denuncia'] ."<br />" .
        "Codigo do Anuncio: <strong>" . $request['id_anuncio'] ."</strong><br />" .
        "Mensagem:<br />" .
        $request['observacao']. "<br /><br /><br />Enviado pelo site fecheidireto.com.br";
    
        $mail = new Zend_Mail('utf-8');
        $mail->setBodyHtml($mensagem);
        $mail->setFrom($request["email"], $request['nome']);
        $mail->addTo("fabio@famatech.com.br", "Fechei Direto");
        $mail->setSubject('[FECHEIDIRETO] Denuncia de anuncio: ' . $request['id_anuncio']);
        $mail->send();
    }
    
    public function indicar($request) {
        
        $anuncio = $this->find($request['id_anuncio']);
        $link = "/anuncio/" . $this->toascii($anuncio['vc_subcategoria_anuncio'] ."-".$anuncio['vc_bairro'] ."-".$anuncio['vc_cidade']) . "-" . $anuncio['id_anuncio'] . ".html";
        
        $mensagem = 
        "<img src='http://fecheidireto.sourcenet.com.br/images/marca.png'>
            <br ><br />
            Ola  " . $request['nome_para'] .",<br /><br />" .
        "Seu amigo " . $request['nome_de'] ." indicou este link de imovel para você: <br><br />
            <a href='http://fecheidireto.sourcenet.com.br$link'>fecheidireto.sourcenet.com.br$link</a><br />" .
        $request['observacao']. "<br /><br /><br />Equipe FecheiDireto.";
    
        $mail = new Zend_Mail('utf-8');
        $mail->setBodyHtml($mensagem);
        $mail->setFrom($request["email_de"], $request['nome_de']);
        $mail->addTo($request["email_para"], $request['nome_para']);
        $mail->setSubject("[FECHEIDIRETO] " . $request['nome_de'] . " nos visitou e indicou este link para você");
        $mail->send();
    }
    
    function toascii($str, $replace = array(), $delimiter = '-') {
        if (!empty($replace)) {
            $str = str_replace((array) $replace, ' ', $str);
        }
        
        $str = $this->alpha($str);

        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

        return $clean;
    }
    
    function alpha($str) {
        $tofind = array(
                    'À','Á','Â','Ã','Ä','Å','à','á','â','ã','ä','å',
                    'Ò','Ó','Ô','Õ','Ö','Ø','ò','ó','ô','õ','ö','ø',
                    'È','É','Ê','Ë','è','é','ê','ë','Ç','ç',
                    'Ì','Í','Î','Ï','ì','í','î','ï',
                    'Ù','Ú','Û','Ü','ù','ú','û','ü',
                    'ÿ','Ñ','ñ','º','°','!','@','#','$','%','?',''
            );

        $replac = array(
                    'A','A','A','A','A','A','a','a','a','a','a','a',
                    'O','O','O','O','O','O','o','o','o','o','o','o',
                    'E','E','E','E','e','e','e','e','C','c',
                    'I','I','I','I','i','i','i','i',
                    'U','U','U','U','u','u','u','u',
                    'y','N','n','','','','','','','','',''
            );

        $alfa = strtoupper(str_replace($tofind,$replac,$str));
        
        return $alfa;
    }
    
}