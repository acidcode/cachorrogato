<?php

/**
 * Modelo Anuncio
 * @author André Gomes @acidcode
 * @since 2012-05-08
 * @version 1.2.1.3
 */
class Backend_Model_DbTable_TbCadastro
    extends Zend_Db_Table_Abstract 
{

    protected $_name = "tb_cadastro";

    
    public function init() {
        parent::init();
    }
    
    /**
     * Cadastro
     * @param array $request
     * @return int Primary key 
     */
      public function cadastro($request) {


        $data = array('vc_nome' => $request['nome'],
            'vc_email' => $request['email'],
            'it_cpf' => Zend_Controller_Action_HelperBroker::getStaticHelper('LimpaString')->direct($request['cpf']),
            'it_telefone' => Zend_Controller_Action_HelperBroker::getStaticHelper('LimpaString')->direct($request['tel']),
            'it_cep' => Zend_Controller_Action_HelperBroker::getStaticHelper('LimpaString')->direct($request['cep']),
            'vc_endereco' => $request['end'],
            'vc_estado' => $request['est'],
            'vc_cidade' => $request['cid'],
            'vc_bairro' => $request['bairro']
                );
                
        $pk = $this->insert($data);

        return $pk;
    }
    
    /**
     * Deletar a partir de chave primária
     * @param type $pk 
     */
    public function delete($pk) {
        parent::delete("id = $pk");
    }
    
    /**
     * Encontra pela chave primaria
     * @param int $pk
     * @return array 
     */
   public function find($pk)
    {
        $db = Zend_Registry::get('db');
        
        $pais = $db->select()
                ->from(array('e' => 'tb_cadastro'))
                ->where("id_cadastro = $pk")
                ->query()
                ->fetch();
                    
        return $pais;
        
    }	
    
    /**
     * Encontra todos pela condição do filtro
     * @param string $where
     * @return array 
     */
   public function findAll($where, $order = false)
    {
         $db = Zend_Registry::get('db');
        
        $noticias = $db->select()
                ->from(array('c' => 'tb_cadastro'))
                ;
        
        return $noticias->query()
                        ->fetchAll();
    }
    
    public function listAll(){
    	
        $db = Zend_Registry::get('db');
        return $db->select()
                  ->from(array('b'=>'bairros'))
                  ->order('b.nome')
                  ->query()
                  ->fetchAll();

    }
    
    
    /**
     * Função para criar condição a partir do filtro
     * @param array $request
     * @return string 
     */
    public function condicao($request, $adicional = false)
    {
        $where = array();
        if($request['id_anuncio'] != 0 ||
                !empty($request['id_anuncio']))
        {
            $where[] = 'id_anuncio = ' . 
                    Zend_Filter::filterStatic(
                            $request['id_anuncio'], 'Digits'
					);
        }
        
        if(!empty($request['vc_email']))
        {
            $where[] = '(u.vc_email LIKE "%' . 
                    trim($request['vc_email']) . '%" OR
                        u.vc_nome LIKE "%' . 
                    trim($request['vc_email']) . '%")';
        }
        
        if($request['id_plano'] != 0 ||
                !empty($request['id_plano']))
        {
            $where[] = 'e.id_plano = ' . 
                    Zend_Filter::filterStatic(
                            $request['id_plano'], 'Digits'
                            );
        }
        
        if($request['bl_status_pagamento'] != 0 ||
                !empty($request['bl_status_pagamento']))
        {
            $where[] = 'e.bl_status_pagamento = "' . 
                            $request['bl_status_pagamento'] . '"';
        }
        
        if($adicional) {
            $where[] = $adicional;
        }

        return implode(' AND ', $where);
               
    }
    
    
    /**
     * Retorna um array para ser usados em formularios Select
     * @access static
     * @param type $todos
     * @return type 
     */
    public function getFormSelect($todos = false)
    {
        $select = array();
        
        if($todos) {
            $select[0] = "TODOS";
        }
        
        $rowSet = $this->fetchAll();

        foreach($rowSet as $data) {
            $select[$data["id_anuncio"]] = $data["vc_titulo"];
        }
        return $select;
    }
    
    public function indicar($request) {
        
        $anuncio = $this->find($request['id_anuncio']);
        $link = "/anuncio/" . $this->toascii($anuncio['vc_subcategoria_anuncio'] ."-".$anuncio['vc_bairro'] ."-".$anuncio['vc_cidade']) . "-" . $anuncio['id_anuncio'] . ".html";
        
        $mensagem = 
        "<img src='http://fecheidireto.sourcenet.com.br/images/marca.png'>
            <br ><br />
            Ola  " . $request['nome_para'] .",<br /><br />" .
        "Seu amigo " . $request['nome_de'] ." indicou este link de imovel para você: <br><br />
            <a href='http://fecheidireto.sourcenet.com.br$link'>fecheidireto.sourcenet.com.br$link</a><br />" .
        $request['observacao']. "<br /><br /><br />Equipe FecheiDireto.";
    
        $mail = new Zend_Mail('utf-8');
        $mail->setBodyHtml($mensagem);
        $mail->setFrom($request["email_de"], $request['nome_de']);
        $mail->addTo($request["email_para"], $request['nome_para']);
        $mail->setSubject("[FECHEIDIRETO] " . $request['nome_de'] . " nos visitou e indicou este link para você");
        $mail->send();
    }
    
    
}
