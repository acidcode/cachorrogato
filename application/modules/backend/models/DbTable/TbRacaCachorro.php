<?php

/**
 * Modelo RacaCachorro
 * @author André Gomes @acidcode
 * @license www.kernelpanic.com.br
 * @version 1.2.1.3
 */
class Backend_Model_DbTable_TbRacaCachorro 
    extends Zend_Db_Table_Abstract 
{

    protected $_name = "tb_raca_cachorro";
    
    /**
     * Cadastro
     * @param array $requests
     * @return int Primary key 
     */
    public function cadastro($request)
    {
        
        $data = array('vc_raca' => $request['vc_raca'],
            'id_parent' => $request['id_parent'],
            'vc_subtitulo' => $request['vc_subtitulo'],
            'vc_seo_title' => $request['vc_seo_title'],
            'vc_seo_url' => $request['vc_seo_url'],
            'vc_seo_keywords' => $request['vc_seo_keywords'],
            'vc_seo_description' => $request['vc_seo_description'],
            'vc_conteudo' => $request['vc_conteudo']);

        if(empty($request['id_raca_cachorro']))
        {
           
            $data['dt_cadastro'] = date('Y-m-d');
            $data['id_usuario'] = Zend_Auth::getInstance()->getStorage()->read()->id;
            $pk = $this->insert($data);
        }
        else
        {
            $pk = $request['id_raca_cachorro'];
            $where = "id_raca_cachorro = " . $request['id_raca_cachorro'];
            $this->update($data, $where);
        }
        
        return $pk;
    }
    
    /**
     * Deletar a partir de chave primária
     * @param type $pk 
     */
    public function delete($pk)
    {
        parent::delete("id_raca_cachorro = $pk");
    }
    
    /**
     * Encontra pela chave primaria
     * @param int $pk
     * @return array 
     */
    public function find($pk)
    {
        $db = Zend_Registry::get('db');
        
        $raca_cachorro = $db->select()
                ->from(array('e' => 'tb_raca_cachorro'))
                ->where("id_raca_cachorro = $pk")
                ->query()
                ->fetch();
        
        return $raca_cachorro;
        
    }
    
    /**
     * Encontra pela chave primaria
     * @param int $pk
     * @return array 
     */
    public function findByUrl($url)
    {
        $db = Zend_Registry::get('db');
        
        $raca_cachorro = $db->select()
                ->from(array('e' => 'tb_raca_cachorro'))
                ->where("vc_seo_url = '$url'")
                ->query()
                ->fetch();
        
        return $raca_cachorro;
        
    }
    
    /**
     * Encontra todos pela condição do filtro
     * @param string $where
     * @return array 
     */
    public function findAll($where)
    {
        $db = Zend_Registry::get('db');
        
        $raca_cachorros = $db->select()
                ->from(array('e' => 'tb_raca_cachorro'));
        
        if(count($where) > 0 && !empty($where))
        {
            $raca_cachorros = $raca_cachorros->where($where);
        }

        $raca_cachorros = $raca_cachorros->query()
                ->fetchAll();
        
        return $raca_cachorros;
    }
    
    /**
     * Função para criar condição a partir do filtro
     * @param array $request
     * @return string 
     */
    public function condicao($request)
    {
        $where = array();
        
        if($request['id_raca_cachorro'] != 0 ||
                !empty($request['id_raca_cachorro']))
        {
            $where[] = 'id_raca_cachorro = ' . 
                    Zend_Filter::filterStatic(
                            $request['id_raca_cachorro'], 'Digits'
                            );
        }
        
        if(!empty($request['vc_titulo']))
        {
            $where[] = 'vc_titulo LIKE "%' . 
                    trim($request['vc_titulo']) . '%"';
        }
        
        return implode('AND', $where);
               
    }
    
    /**
     * Retorna um array para ser usados em formularios Select
     * @access static
     * @param type $todos
     * @return type 
     */
    public function getFormSelect($todos = false)
    {
        $select = array();
        
        $select[0] = "Main";
        
        $rowSet = $this->fetchAll();

        foreach($rowSet as $data) {
            $select[$data["id_raca_cachorro"]] = $data["vc_raca_cachorro"];
        }
        return $select;
    }
    
}