<?php

/**
 * Modelo Usuario
 * @author André Gomes @acidcode
 * @since 2012-04-00
 * @license www.cachorrogato.com.br/sge
 * @version 1.2.1.3
 */
class Backend_Model_DbTable_TbUsuario 
    extends Zend_Db_Table_Abstract 
{

    protected $_name = "tb_usuario";
    
    /**
     * Cadastro
     * @param array $request
     * @return int Primary key 
     */
    public function cadastro($request)
    {
        $data = array('vc_nome' => $request['vc_nome'],
            'it_cpf' => Zend_Controller_Action_HelperBroker::getStaticHelper(
                    'LimpaString')->direct($request['it_cpf']),
            'it_telefone_1' => Zend_Controller_Action_HelperBroker::getStaticHelper(
                    'LimpaString')->direct($request['it_telefone_1']),
            'it_telefone_2' => Zend_Controller_Action_HelperBroker::getStaticHelper(
                    'LimpaString')->direct($request['it_telefone_2']),
            'it_celular_1' => Zend_Controller_Action_HelperBroker::getStaticHelper(
                    'LimpaString')->direct($request['it_celular_1']),
            'it_cep' => Zend_Controller_Action_HelperBroker::getStaticHelper(
                    'LimpaString')->direct($request['it_cep']),
            'vc_endereco' => $request['vc_endereco'],
            'it_endereco_numero' => $request['it_endereco_numero'],
            'vc_bairro' => $request['vc_bairro'],
            'vc_cidade' => $request['vc_cidade'],
            'vc_estado' => $request['vc_estado'],
            'vc_complemento' => $request['vc_complemento'],
            'vc_email' => $request['vc_email']);
        
        if(!empty($request['vc_senha']) && !empty($request['vc_senha_confirmacao'])) {
            if($request['vc_senha'] == $request['vc_senha_confirmacao']) {
                $data['vc_senha'] = md5($request['vc_senha']);
            } 
        }
        

        if(empty($request['id_usuario']))
        {
           
            $data['dt_cadastro'] = date('Y-m-d h:i:s');
            $pk = $this->insert($data);
        }
        else
        {
            $data['dt_alteracao'] = date('Y-m-d h:i:s');
            $data['bl_master'] = $request['bl_master'];
            $pk = $request['id_usuario'];
            $where = "id_usuario = " . $request['id_usuario'];
            $this->update($data, $where);
        }
        
        return $pk;
    }
    
    public function emailCadastro($nome, $email, $mensagem) {

        $mail = new Zend_Mail('utf-8');
        $mail->setBodyHtml($mensagem);
        $mail->setFrom("contato@fecheidireto.com.br", 'Fechei Direto');
        $mail->addTo($email, $nome);;
        $mail->setSubject('[FECHEIDIRETO] Confirme o seu cadastro');
        $mail->send();
        
    }
    
     /**
     * Deletar a partir de chave primária
     * @param type $pk 
     */
    public function delete($pk)
    {
        parent::delete("id_usuario = $pk");
    }
    
    /**
     * Encontra pela chave primaria
     * @param int $pk
     * @return array 
     */
    public function find($pk)
    {
        $db = Zend_Registry::get('db');
        
        $usuario = $db->select()
                ->from(array('e' => 'tb_usuario'))
                ->where("id_usuario = $pk")
                ->query()
                ->fetch();
        
        return $usuario;
        
    }
    
    /**
     * Encontra todos pela condição do filtro
     * @param string $where
     * @return array 
     */
    public function findAll($where)
    {
        $db = Zend_Registry::get('db');
        
        $usuarios = $db->select()
                ->from(array('e' => 'tb_usuario'));
        
        if(count($where) > 0 && !empty($where))
        {
            $usuarios = $usuarios->where($where);
        }
        
        $usuarios = $usuarios->query()
                ->fetchAll();
        
        return $usuarios;
    }
    
    /**
     * Função para criar condição a partir do filtro
     * @param array $request
     * @return string 
     */
    public function condicao($request)
    {
        $where = array();
        
        if($request['id_usuario'] != 0 ||
                !empty($request['id_usuario']))
        {
            $where[] = 'id_usuario = ' . 
                    Zend_Filter::filterStatic(
                            $request['id_usuario'], 'Digits'
                            );
        }
        
        if(!empty($request['vc_email']))
        {
            $where[] = 'vc_email LIKE "%' . 
                    trim($request['vc_email']) . '%"';
        }
        
        return implode('AND', $where);
               
    }
    
    /**
     * Retorna um array para ser usados em formularios Select
     * @access static
     * @param type $todos
     * @return type 
     */
    public function getFormSelect($todos = false)
    {
        $select = array();
        
        if($todos) {
            $select[0] = "TODOS";
        }
        
        $rowSet = $this->fetchAll();

        foreach($rowSet as $data) {
            $select[$data["id_usuario"]] = $data["vc_usuario"];
        }
        return $select;
    }
    
    /**
     * Logar o usuario
     * @param array $usuario 
     */
    public function logar($usuario) {

        $sessao = new Zend_Session_Namespace('login_sessao');
        
        $login['usuario'] = $usuario;
        $login['logado'] = true;
        
        $sessao->arr = $login;
            
    }
    
    /**
     * checa se o usuario está logados
     * @return type 
     */
    public function checarLoginUsuario() {
        $loginSessao = new Zend_Session_Namespace('login_sessao');
        $loginSessao = $loginSessao->arr;
        return $loginSessao['logado'];
    }
    
    public function recuperarSenha($email) {
        
        $usuario = $this->fetchRow(
                "vc_email = '" . $email . "'"
                );
                
        $novaSenha = $this->gerar_senha(8, true, true, true, false);
        
        
        $this->update(
                array('vc_senha' => md5($novaSenha)),
                "vc_email = '" . $email . "'"
                );
    
        $mensagem = 'Sua nova senha : ' . $novaSenha;
        $mail = new Zend_Mail();
        $mail->setBodyText($mensagem);
        $mail->setFrom("contato@fecheidireto.com.br", "Fechei Direto");
        $mail->addTo($usuario["vc_email"], $usuario['vc_nome']);
        $mail->setSubject("Nova senha do FecheiDireto.com.br");
        $mail->send();
    }
    
    function gerar_senha($tamanho, $maiuscula, $minuscula, $numeros, $codigos)
    {
        $maius = "ABCDEFGHIJKLMNOPQRSTUWXYZ";
        $minus = "abcdefghijklmnopqrstuwxyz";
        $numer = "0123456789";
        $codig = '!@#$%&*()-+.,;?{[}]^><:|';

        $base = '';
        $base .= ($maiuscula) ? $maius : '';
        $base .= ($minuscula) ? $minus : '';
        $base .= ($numeros) ? $numer : '';
        $base .= ($codigos) ? $codig : '';

        srand((float) microtime() * 10000000);
        $senha = '';
        for ($i = 0; $i < $tamanho; $i++) {
            $senha .= substr($base, rand(0, strlen($base)-1), 1);
        }
        return $senha;
    }
}