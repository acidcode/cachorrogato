<?php
class Backend_Model_Mail
{ 
    protected $_options;
    
    protected $_form;
    
    protected $_view;
    
    protected $_viewPath;
    
    protected $_viewName;
    
    protected $_mail;
    
    protected $_tranport;

 
    protected function _setUpDefaultTransport(){
    	
        $option = $this->getOptions();
        
        return new Zend_Mail_Transport_Smtp( $option['host'],$option);
        
    }
    
    public function renderView(){
    	
        $view = $this->getView();
        $view->addScriptPath($this->getViewPath());
        return $view->render($this->getViewName());
        
    }
    
    public function send(){
       $option = $this->getOptions();
       $mail   = $this->getMail();
       $mail->setBodyHtml($this->renderView());
       return $mail->send($this->getTranport());
    }
    
    /**
     * @return the $_tranport
     */
    public function getTranport() {
        if(null===$this->_tranport){
            $this->setTranport($this->_setUpDefaultTransport());
        }
    	return $this->_tranport;
    }
    
    /**
     * @param field_type $_tranport
     */
    public function setTranport($_tranport) {

        if(!$_tranport instanceof Zend_Mail_Transport_Abstract){
           throw new Exception('invalid transport class.');        	
        }
        
    	$this->_tranport = $_tranport;
    	return $this;
    }
    public function getOptions(){
    	 
    	if(null === $this->_options){
    	    $options = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions('form');
    		$this->_options = $options['form'][$this->getForm()];
    	}
    
    	return  $this->_options;
    }
    
    public function setOptions($_options){
    
        $this->_options = $_options;
    	return  $this;
    }
	/**
	 * @return the $_form
	 */
	public function getForm() {
		return $this->_form;
	}

	/**
	 * @param field_type $_form
	 */
	public function setForm($_form) {
		$this->_form = $_form;
		return $this;
	}
	/**
	 * @return the $_view
	 */
	public function getView() {
	    
	    if(null===$this->_view){
	    	$this->setView(new Zend_View());	        
	    }
	    
		return $this->_view;
	}

	/**
	 * @param field_type $_view
	 */
	public function setView($_view) {
		if(!$_view instanceof Zend_View){
			throw new Exception('Invalid view resource.');
		}
	    $this->_view = $_view;
		return $this;
	}
	/**
	 * @return the $_viewPath
	 */
	public function getViewPath() {
	    
	    if(null===$this->_viewPath){
	       $this->setViewPath(APPLICATION_PATH.'/layouts/scripts/email/');
	    }
		return $this->_viewPath;
	}

	/**
	 * @return the $_viewName
	 */
	public function getViewName() {
		return $this->_viewName;
	}

	/**
	 * @param field_type $_viewPath
	 */
	public function setViewPath($_viewPath) {
	    
		$this->_viewPath = $_viewPath;
		return $this;
	}

	/**
	 * @param field_type $_viewName
	 */
	public function setViewName($_viewName) {
		$this->_viewName = $_viewName;
		return $this;
	}
	/**
	 * @return the $_mail
	 */
	public function getMail() {
	    if(null===$this->_mail){
	    	$this->setMail(new Zend_Mail('utf-8'));
	    }
		return $this->_mail;
	}

	/**
	 * @param field_type $_mail
	 */
	public function setMail($_mail) {
	if(!$_mail instanceof Zend_Mail){
			throw new Exception('Invalid mail resource.');
		}
		$this->_mail = $_mail;
		return $this;
	}




    
}
?>