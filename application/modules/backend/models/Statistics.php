<?php
class Backend_Model_Statistics extends Backend_Model_Abstract
{
    protected $_id;
    protected $_created;
    protected $_evaluation_date;
    protected $_variation_per_period;
    protected $_variation_per_month;
    protected $_square_meter_value;
    protected $_neighborhood_id;
    protected $_neighborhood;
    
    public function __construct(array $options = null)
    {
    	if (is_array($options)) {
    		$this->setOptions($options);
    	}
    }
    
	/**
	 * @return the $_id
	 */
	public function getId() {
		return $this->_id;
	}

	/**
	 * @param field_type $_id
	 */
	public function setId($_id) {
		$this->_id = $_id;
		return $this;
	}

	/**
	 * @return the $_created
	 */
	public function getCreated() {
		return $this->_created;
	}

	/**
	 * @param field_type $_created
	 */
	public function setCreated($_created) {
		$this->_created = $_created;
		return $this;
	}

	/**
	 * @return the $_evaluation_date
	 */
	public function getEvaluation_date() {
		return $this->_evaluation_date;
	}

	/**
	 * @param field_type $_evaluation_date
	 */
	public function setEvaluation_date($_evaluation_date) {
		$this->_evaluation_date = $_evaluation_date;
		return $this;
	}

	/**
	 * @return the $_variation_per_period
	 */
	public function getVariation_per_period() {
		return $this->_variation_per_period;
	}

	/**
	 * @param field_type $_variation_per_period
	 */
	public function setVariation_per_period($_variation_per_period) {
		$this->_variation_per_period = $_variation_per_period;
		return $this;
	}

	/**
	 * @return the $_variation_per_month
	 */
	public function getVariation_per_month() {
		return $this->_variation_per_month;
	}

	/**
	 * @param field_type $_variation_per_month
	 */
	public function setVariation_per_month($_variation_per_month) {
		$this->_variation_per_month = $_variation_per_month;
		return $this;
	}

	/**
	 * @return the $_square_meter_value
	 */
	public function getSquare_meter_value() {
		return $this->_square_meter_value;
	}

	/**
	 * @param field_type $_square_meter_value
	 */
	public function setSquare_meter_value($_square_meter_value) {
		$this->_square_meter_value = $_square_meter_value;
		return $this;
	}

	/**
	 * @return the $_neighborhood_id
	 */
	public function getNeighborhood_id() {
		return $this->_neighborhood_id;
	}

	/**
	 * @param field_type $_neighborhood_id
	 */
	public function setNeighborhood_id($_neighborhood_id) {
		$this->_neighborhood_id = $_neighborhood_id;
		return $this;
	}
	/**
	 * @return the $_neighborhood
	 */
	public function getNeighborhood() {
		return $this->_neighborhood;
	}

	/**
	 * @param field_type $_neighborhood
	 */
	public function setNeighborhood($_neighborhood) {
		$this->_neighborhood = $_neighborhood;
		return $this;
	}


    
}
?>