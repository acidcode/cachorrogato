<?php
class Zend_View_Helper_ArvoreCategoria extends Zend_View_Helper_Abstract
{

    public function ArvoreCategoria($id_conteudo_categoria) {
        
        if(!$id_conteudo_categoria) {
            return '';
        }
        
        $tbConteudoCategoria = new Backend_Model_DbTable_TbConteudoCategoria();
        $arvoreCategoria = array();
        
        $categoria = $tbConteudoCategoria->find($id_conteudo_categoria);
        $arvoreCategoria[] = $categoria['vc_categoria'];

        if($categoria['id_parent'] > 0) {
            $categoria2 = $tbConteudoCategoria->find($categoria['id_parent']); 
            $arvoreCategoria[] = $categoria2['vc_categoria'];
        }
        
        if($categoria2['id_parent'] > 0) {
            $categoria3 = $tbConteudoCategoria->find($categoria2['id_parent']);
            $arvoreCategoria[] = $categoria3['vc_categoria'];
        }
        
        return implode(" ➝ ", array_reverse($arvoreCategoria));
             
    }
}