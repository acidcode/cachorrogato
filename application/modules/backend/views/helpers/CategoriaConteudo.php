<?php
class Zend_View_Helper_CategoriaConteudo extends Zend_View_Helper_Abstract
{

    public function CategoriaConteudo($id_conteudo_categoria, $id_conteudo = false) {
        
        if(!$id_conteudo) {
            return '';
        }
        
        $tbConteudoRelacionadoCategoria = new Backend_Model_DbTable_TbConteudoRelacionadoCategoria();
        $existe = $tbConteudoRelacionadoCategoria->fetchAll(
                "id_conteudo_categoria = $id_conteudo_categoria AND id_conteudo = $id_conteudo"
                )->toArray();

        if($existe) 
            return 'checked';
             
    }
}