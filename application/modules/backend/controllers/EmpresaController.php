<?php

class Backend_EmpresaController extends Zend_Controller_Action {

    public function indexAction() {
        $tbEmpresa = new Backend_Model_DbTable_TbEmpresa();
        $this->view->empresas = $tbEmpresa->fetchAll();
    }

    public function cadastroAction() {
        $request = $this->_getAllParams();

        $tbFornecedor = new Frontend_Model_DbTable_TbFornecedor();
        $tbFornecedorResposta = new Backend_Model_DbTable_TbQuestionarioRespostas();

        if (!empty($request['id_fornecedor'])) {
            $respostasFornecedor = $tbFornecedorResposta->findAllByFornecedor($request['id_fornecedor']);

            $this->view->formulario = $tbFornecedor->find($request['id_fornecedor']);
            $this->view->questionario = $respostasFornecedor['questionario'];
            $this->view->respostas = $respostasFornecedor['respostas'];
        }

        foreach ($tbFornecedor->findAtividade() as $array) {
            $data['atividade'][$array['id_atividade']] = $array['vc_descricao'];
        }

        foreach ($tbFornecedor->findSetorAtividade() as $array) {
            $data['setor_atividade'][$array['id_setor_atividade']] = $array['vc_setor_atividade'];
        }

        for ($i = 0; $i < 24; $i++) {
            if ($i == 0) {
                $data['atividade_inicio'][''] = 'Selecione';
                $data['atividade_termino'][''] = 'Selecione';
            }
            if ($i < 10) {
                $data['atividade_inicio']['0' . $i . ':' . '00'] = '0' . $i . ':' . '00';
                $data['atividade_termino']['0' . $i . ':' . '00'] = '0' . $i . ':' . '00';
            } else {
                $data['atividade_inicio'][$i . ':' . '00'] = $i . ':' . '00';
                $data['atividade_termino'][$i . ':' . '00'] = $i . ':' . '00';
            }
        }

        $this->view->data = $data;
    }

}

?>