<?php
/**
 * Controller de Noticia
 * @author @acidcode
 * @since 20-02-2013
 */
class Backend_NoticiaController extends Zend_Controller_Action 
{
    private $_tbNoticia;

    public function init() 
    {
        $this->_tbNoticia = new Backend_Model_DbTable_TbNoticia();
    }
    
    public function indexAction()
    {
        
        
        $this->view->headTitle("Notícias");
        
        $sessionCondicao = new Zend_Session_Namespace("Condicao_Noticia");
        $sessionFiltro = new Zend_Session_Namespace("Filtro_Noticia");
        
        if($this->_request->isPost()) 
        {
            $sessionCondicao->arr = $this->_tbNoticia->condicao(
                    $this->_getAllParams()
                    );
            $sessionFiltro->arr = $this->_getAllParams();
        }
        
        if((int) $this->_getParam('pagina') > 0 || 
                $this->_request->isPost() || 
                is_array($sessionFiltro->arr)) 
        {
            $this->view->filtrado = true;
        }

        $this->view->filtro = $sessionFiltro->arr;
        $noticias = $this->_tbNoticia->findAll(
                $sessionCondicao->arr
                );

        //Controle da Paginação
        $paginator = Zend_Paginator::factory($noticias);
        $paginator->setItemCountPerPage(20);
        $paginator->setCurrentPageNumber($this->_getParam('pagina',1));

        $this->view->itens = $paginator->getTotalItemCount();
        $this->view->paginator = $paginator;
    }
    
    public function cadastroAction()
    {
        $this->view->headTitle("Cadastrar Noticias");
        
        if($this->_getParam('id_noticia', false))
        {
            $this->view->formulario = $this->_tbNoticia->find(
                    $this->_getParam('id_noticia')
                    );
        }
        
        if($this->_request->isPost())
        {
            $id_noticia = $this->_tbNoticia->cadastro(
                    $this->_getAllParams()
                    );
            
            $this->view->efetivado = true;
            
            $this->view->formulario = $this->_tbNoticia->find(
                    $id_noticia
                    );
        }
        
    }
    
    public function deletarAction()
    {
        if($this->_getParam('id_noticia', false))
        {
            $this->_tbNoticia->delete(
                    $this->_getParam('id_noticia')
                    );
        }
        
        $this->_redirect('/backend/noticia');
    }
    
}