<?php

class Backend_RacaCachorroController extends Zend_Controller_Action {

    public function init() {

    }

    public function indexAction() {
        $tbRacaCachorro = new Backend_Model_DbTable_TbRacaCachorro();
        $this->view->racas = $tbRacaCachorro->findAll();
    }
    
    public function cadastroAction() {
        
        $tbRacaCachorro = new Backend_Model_DbTable_TbRacaCachorro();
        $this->view->headTitle("Backend > Raça de Cachorro > Cadastro");
        $this->view->racasSelect = $tbRacaCachorro->getFormSelect();
        
        if($this->_getParam('id_raca_cachorro', false))
        {
            $this->view->formulario = $tbRacaCachorro->find(
                    $this->_getParam('id_raca_cachorro')
                    );
        }
            
        
        if($this->_request->isPost())
        {
            $id_raca = $tbRacaCachorro->cadastro(
                    $this->_getAllParams()
                    );
            
            $this->view->efetivado = true;
            
            $this->view->formulario = $tbRacaCachorro->find(
                    $id_raca
                    );
        }
        
    }
    
    public function deletarAction() {
         
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout()->disableLayout();
        $tbRacaCachorro = new Backend_Model_DbTable_TbRacaCachorro();
        $tbRacaCachorro->delete($this->_getParam('id_raca_cachorro', false));
        $this->_redirect("/backend/raca-cachorro"); 
    }

}