<?php

class Backend_ConteudoCategoriaController extends Zend_Controller_Action {

    public function init() {

    }

    public function indexAction() {
        $tbConteudoCategoria = new Backend_Model_DbTable_TbConteudoCategoria();
        $this->view->categorias = $tbConteudoCategoria->findAll();
    }
    
    public function cadastroAction() {
        
        $tbConteudoCategoria = new Backend_Model_DbTable_TbConteudoCategoria();
        $this->view->headTitle("Backend > Categoria de Conteúdo > Cadastro");
        $this->view->categoriasSelect = $tbConteudoCategoria->getFormSelect();
        
        if($this->_getParam('id_conteudo_categoria', false))
        {
            $this->view->formulario = $tbConteudoCategoria->find(
                    $this->_getParam('id_conteudo_categoria')
                    );
        }
            
        
        if($this->_request->isPost())
        {
            $id_categoria = $tbConteudoCategoria->cadastro(
                    $this->_getAllParams()
                    );
            
            $this->view->efetivado = true;
            
            $this->view->formulario = $tbConteudoCategoria->find(
                    $id_categoria
                    );
        }
        
    }
    
    public function deletarAction() {
         
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout()->disableLayout();
        $tbConteudoCategoria = new Backend_Model_DbTable_TbConteudoCategoria();
        $tbConteudoCategoria->delete($this->_getParam('id_conteudo_categoria', false));
        $this->_redirect("/backend/conteudo-categoria"); 
    }

}