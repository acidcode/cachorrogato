<?php

/**
 * Controller da Index do FrontEnd
 * @author @acidcode
 * @since 11-04-2013
 * @copyright GPO4  - www.cachorrogato.com.br - andre.kernelpanic@gmail.com
 */
class Backend_IndexController extends Zend_Controller_Action {

    public function init() {
        
    }

    public function indexAction() {

        $this->view->totalConteudos = 0;
        $this->view->totalUsuariosCadastrados = 0;
        $this->view->totalFornecedoresCadastrados = 0;
        $this->view->totalVisitas = 0;
        $this->view->totalAgendamentos = 0;
        
        $this->view->paginasMaisAcessadas = array();
        $this->view->ultimosUsuarios = array();
        $this->view->ultimasEmpresas = array();
        
    }


}