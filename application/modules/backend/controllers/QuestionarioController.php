<?php

class Backend_QuestionarioController extends Zend_Controller_Action {

    public function indexAction() {
        $_getAllParams = $this->_getAllParams();

        if (!empty($_getAllParams['id_setor_de_atividade'])) {
            $tbQuestionario = new Backend_Model_DbTable_TbQuestionario();
            $this->view->questionario = $tbQuestionario->findAllQuestionario('id_setor_atividade = ' . $_getAllParams['id_setor_de_atividade']);
            $this->view->action = '/backend/questionario/cadastro/id_setor_de_atividade/' . $_getAllParams['id_setor_de_atividade'] . '/';
        } else {
            $this->_redirect('/backend/setor-de-atividade/cadastro/');
        }
    }

    public function cadastroAction() {
        $_getAllParams = $this->_getAllParams();

        if (!empty($_getAllParams['id_setor_de_atividade']) && (int) $_getAllParams['id_setor_de_atividade']) {
            $tbQuestionario = new Backend_Model_DbTable_TbQuestionario();
            $tbSetor = new Backend_Model_DbTable_TbSetorDeAtividade();
            $opcoes = new Backend_Model_DbTable_TbQuestionarioOpcoes();

            if ($this->_request->isPost()) {
                $tbQuestionario->inserirPergunta($this->_request->getPost());
                $this->_redirect('/backend/questionario/index/id_setor_de_atividade/' . $_getAllParams['id_setor_de_atividade'] . '/');
            }

            $id_setor_de_atividade = $_getAllParams['id_setor_de_atividade'];


            $this->view->tipo = $tbQuestionario->findTiposDeCampos();
            $this->view->setor = $tbSetor->fetchRow('id_setor_atividade = ' . $id_setor_de_atividade)->toArray();
            $this->view->action = '/backend/questionario/cadastro/';
            $this->view->voltar = '/backend/questionario/index/id_setor_de_atividade/' . $_getAllParams['id_setor_de_atividade'] . '/';

            $id_questionario = $_getAllParams['id_questionario'];
            if ($id_questionario) {
                $where = 'id_questionario = ' . $id_questionario;
                $data = $tbQuestionario->findAllQuestionario($where);

                if ($data) {
                    $this->view->data = $data[0];
                    $this->view->opcoes = $opcoes->findOpcoesByQuestionario($id_questionario)->toArray();
                } else {
                    $this->view->data = array();
                }
            } else {
                $this->view->data = array('id_setor_de_atividade' => $id_setor_de_atividade);
            }
        } else {
            $this->_redirect('/backend/questionario/visualizar-setor-de-atividade/');
        }
    }

    public function deletarAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout()->disableLayout();

        $tbQuestionario = new Backend_Model_DbTable_TbQuestionario();
        if ($this->_request->isPost()) {
            $request = $this->_request->getPost();
            $where = 'id_questionario = ' . $request['id_questionario'];
            echo Zend_Json_Encoder::encode($tbQuestionario->delete($where));
        }
    }

    public function visualizarSetorDeAtividadeAction() {
        $tbSetor = new Backend_Model_DbTable_TbSetorDeAtividade();
        $this->view->setores = $tbSetor->fetchAll();
    }

}

?>
