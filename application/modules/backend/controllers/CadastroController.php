<?php
/**
 * Controller de Cadastro
 * @author @acidcode
 * @since 20-02-2013
 */
class Backend_CadastroController extends Zend_Controller_Action 
{
    private $_tbCadastro;

    public function init() 
    {
        $this->_tbCadastro = new Backend_Model_DbTable_TbCadastro();
    }
    
    public function indexAction()
    {
        $this->view->headTitle("Cadastros");
        $this->view->cadastros = $this->_tbCadastro->findAll(
                $sessionCondicao->arr
                );

    }
    
    public function visualizarAction()
    {
        $this->view->headTitle("Visualizar Cadastros");
        
        if($this->_getParam('id_cadastro', false))
        {
            $this->view->formulario = $this->_tbCadastro->find(
                    $this->_getParam('id_cadastro')
                    );
        }
        
        if($this->_request->isPost())
        {
            $id_cadastro = $this->_tbCadastro->cadastro(
                    $this->_getAllParams()
                    );
            
            $this->view->efetivado = true;
            
            $this->view->formulario = $this->_tbCadastro->find(
                    $id_cadastro
                    );
        }
    }
    
    public function deletarAction()
    {
        if($this->_getParam('id_cadastro', false))
        {
            $this->_tbCadastro->delete(
                    $this->_getParam('id_cadastro')
                    );
        }
        
        $this->_redirect('/backend/cadastro');
    }
    
}