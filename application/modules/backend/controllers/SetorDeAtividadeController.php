<?php

class Backend_SetorDeAtividadeController extends Zend_Controller_Action {

    public function indexAction() {
        $tbSetor = new Backend_Model_DbTable_TbSetorDeAtividade();
        $this->view->setores = $tbSetor->fetchAll();
    }

    public function cadastroAction() {
        $_getAllParams = $this->_getAllParams();
        $tbSetor = new Backend_Model_DbTable_TbSetorDeAtividade();

        if ($this->_request->isPost()) {
            $tbSetor->cadastrarSetor($this->_request->getPost());
            $this->view->efetivado = true;
        }

        if (!empty($_getAllParams['id_setor_atividade']) && (int) $_getAllParams['id_setor_atividade']) {
            $this->view->formulario = $tbSetor->fetchRow('id_setor_atividade = ' . $_getAllParams['id_setor_atividade']);
        }
    }

    public function deletarAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ($this->_request->isPost()) {
            $id_atividade = $this->_getParam('id_setor_atividade');

            if ($id_atividade) {
                $tbSetor = new Backend_Model_DbTable_TbSetorDeAtividade();
                $tbSetor->delete('id_setor_atividade = ' . $id_atividade);
                echo json_encode($id_atividade);
            } else {
                echo json_encode(false);
            }
        }
    }

}

?>
