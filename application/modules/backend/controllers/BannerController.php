<?php
class Backend_BannerController extends Zend_Controller_Action
{
    public function init ()
    {}
    public function indexAction ()
    {
        $Banner = new Backend_Model_DbTable_Banner();
        // print_r($Banner->fetchAll());
        $this->view->Banners = $Banner->fetch();
        $this->view->Banner = $Banner;
    }
    public function addAction ()
    {
        if ($this->_request->isPost()) {
            $Messages = array();
            $request = $this->_getAllParams();
            if (! Zend_Validate::is(trim($request['banner_title']), 'NotEmpty')) {
                $Messages[] = 'O título não pode estar vazio !';
                $Messages['response'] = false;
            }
            /* if (! Zend_Validate::is(trim($request['banner_position']), 
            'NotEmpty') ||
             ! Zend_Validate::is(trim($request['banner_position']), 'Digits')) {
                $Messages[] = 'Valor da posição inválida !';
                $Messages['response'] = false;
            } */
            /*
             * if (!
             * Zend_Validate::is(trim($request['banner_hash_file']['name']),
             * 'NotEmpty')) { $Messages[] = 'O banner não pode estar vazio !';
             * $Messages['error'] = true; }
             */
            if (! Zend_Validate::is(trim($request['banner_from_date']), 
            'NotEmpty') /* ||
                Zend_Validate::is(($request['banner_from_date']), 'Date',array('format' => 'dd/mm/YYYY')) */) {
                $Messages[] = 'A data de início inválida !';
                $Messages['response'] = false;
            }
            if (! Zend_Validate::is(trim($request['banner_to_date']), 
            'NotEmpty') /* ||
                Zend_Validate::is(($request['banner_from_date']), 'Date',array('format' => 'dd/mm/YYYY')) */) {
                $Messages[] = 'A data de Término inválida !';
                $Messages['response'] = false;
            }
            if (@array_key_exists('banner_isVisible', $request)) {
                $request['banner_isVisible'] = 1;
            } else {
                $request['banner_isVisible'] = 0;
            }
            $upload_adapter = new Zend_File_Transfer_Adapter_Http();
            $files = $upload_adapter->getFileInfo();
            $file = $files['banner_hash_file'];
            
          /*   print_r($files);
            die(); */
            
            /* list ($width, $height, $type, $attr) = getimagesize(
            $file['tmp_name']);
            if ( $type != 'image/jpeg' && $file['type'] != 'image/png' &&
             $file['type'] != 'image/jpg') {
                $Messages[] = 'Extensão do arquivo inválida , extensões válidas são : jpg ou png';
                $Messages['response'] = false;
            } */
           
            list ($width, $height, $type, $attr) = getimagesize(
            		$file['tmp_name']);
            if ( $type != 2 &&  $type != 3) {
            	$Messages[] = 'Extensão do arquivo inválida , extensões válidas são : jpg ou png';
            	$Messages['response'] = false;
            }
            if ($file['size'] > 2000000) {
                $Messages[] = 'Tamanho do ultrapassou 2mb';
                $Messages['response'] = false;
            }
            if ($width != 582 && $height != 295) {
                $Messages[] = 'Dimensão do banner inválido , a dimensão deve ser Largura:582px e Altura : 295px ';
                $Messages['response'] = false;
            }
            if (count($Messages) == 0) {
                $ext= array(2=>'jpg',3=>'png');
                $FileName = md5($file['name'] . time()) . '.' . $ext[$type];
                $upload_adapter->addFilter('rename', 
                UPLOAD_PATH . '/banners/' . $FileName);
                if ($upload_adapter->receive()) {
                    $request['banner_hash_file'] = $FileName;
                    $Banner = new Backend_Model_DbTable_Banner();
                    if ($Banner->add($request)) {
                        $Messages['response'] = true;
                        $Messages[] = 'Banner adicionado com sucesso !';
                        unset($request);
                        $request = array();
                    } else {
                        $Messages['response'] = false;
                        $Messages[] = 'Erro ao  adicionar banner , tente novamente  !';
                    }
                } else {
                    $Messages['response'] = false;
                    $Messages[] = 'Erro ao carregar banner , tente novamente  !';
                }
            }
        }
        $this->view->messages = $Messages;
        $this->view->fields = $request;
    }
    public function editAction ()
    {   $request = array();
        $Banner = new Backend_Model_DbTable_Banner();
        $id = $this->_getParam('id');
        $request = $Banner->fetchById($id);
        $FileName = $request['banner_hash_file'];
        $request['banner_title'] = $request['banner_title'];
        $request['banner_from_date'] = $Banner->getdate(
        $request['banner_from_date']);
        $request['banner_to_date'] = $Banner->getdate(
        $request['banner_to_date']);
        
        if ($this->_request->isPost()) {
            $Messages = array();
            $request = $this->_getAllParams();
            if (! Zend_Validate::is(trim($request['banner_title']), 'NotEmpty')) {
                $Messages[] = 'O título não pode estar vazio !';
                $Messages['response'] = false;
            }
            /* if (! Zend_Validate::is(trim($request['banner_position']), 
            'NotEmpty') ||
             ! Zend_Validate::is(trim($request['banner_position']), 'Digits')) {
                $Messages[] = 'Posição inválida !';
                $Messages['response'] = false;
            } */
            /*
             * if (!
             * Zend_Validate::is(trim($request['banner_hash_file']['name']),
             * 'NotEmpty')) { $Messages[] = 'O banner não pode estar vazio !';
             * $Messages['error'] = true; }
             */
            if (! Zend_Validate::is(trim($request['banner_from_date']), 
            'NotEmpty') /* ||
    		Zend_Validate::is(($request['banner_from_date']), 'Date',array('format' => 'dd/mm/YYYY')) */) {
                $Messages[] = 'A data de início inválida !';
                $Messages['response'] = false;
            }
            if (! Zend_Validate::is(trim($request['banner_to_date']), 
            'NotEmpty') /* ||
    			Zend_Validate::is(($request['banner_from_date']), 'Date',array('format' => 'dd/mm/YYYY')) */) {
                $Messages[] = 'A data de Término inválida !';
                $Messages['response'] = false;
            }
            if (@array_key_exists('banner_isVisible', $request)) {
                $request['banner_isVisible'] = 1;
            } else {
                $request['banner_isVisible'] = 0;
            }
            $recive = true;
            $upload_adapter = new Zend_File_Transfer_Adapter_Http();
            $files = $upload_adapter->getFileInfo();
            $file = $files['banner_hash_file'];
            
             /*  print_r($file);

              die(); */
            
            if ($file['name'] != '') {
                list ($width, $height, $type, $attr) = getimagesize(
                $file['tmp_name']);
                /* if ($file['type'] != 'image/jpeg' && $file['type'] != 'image/png' &&
                 $file['type'] != 'image/jpg') {
                    $Messages[] = 'Extensão do arquivo inválida , extensões válidas são : jpg ou png';
                    $Messages['response'] = false;
                } */
                if ( $type != 2 &&  $type != 3) {
                	$Messages[] = 'Extensão do arquivo inválida , extensões válidas são : jpg ou png';
                	$Messages['response'] = false;
                }
                if ($file['size'] > 2000000) {
                    $Messages[] = 'Tamanho do ultrapassou 2mb';
                    $Messages['response'] = false;
                }
                if ($width != 582 && $height != 295) {
                    $Messages[] = 'Dimensão do banner inválido , a dimensão deve ser Largura:582px e Altura : 295px ';
                    $Messages['response'] = false;
                }
                $ext= array(2=>'jpg',3=>'png');
                $FileName = md5($file['name'] . time()) . '.' . $ext[$type];
                $upload_adapter->addFilter('rename', 
                UPLOAD_PATH . '/banners/' . $FileName);
                $recive = $upload_adapter->receive();
            }
            /*
             * print_r($request); die();
             */
            if (count($Messages) == 0) {
                if ($recive) {
                    $request['banner_hash_file'] = $FileName;
                    if ($Banner->edit($request, $id)) {
                        $Messages['response'] = true;
                        $Messages[] = 'Banner atualizado com sucesso !';
                    } else {
                        $Messages['response'] = false;
                        $Messages[] = 'Erro ao  Atualizar banner , tente novamente  !';
                    }
                } else {
                    $Messages['response'] = false;
                    $Messages[] = 'Erro ao carregar banner , tente novamente  !';
                }
            }
        }
        $this->view->id = $id;
        $this->view->messages = $Messages;
        $this->view->fields = $request;
    }
    public function removeAction ()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNeverRender(true);
        $id = $this->_getParam('id');
        $data = array();
        if ($id) {
            $banner = new Backend_Model_DbTable_Banner();
            if ($banner->remove($id)) {
                $data['Message'] = 'Banner Removido com sucesso !';
            } else {
                $data['Message'] = 'Erro ao tentar Remover Banner, tente novamente !';
            }
        }
        
        echo Zend_Json::encode($data);
    }
}
?>