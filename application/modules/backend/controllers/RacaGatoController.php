<?php

class Backend_RacaGatoController extends Zend_Controller_Action {

    public function init() {

    }

    public function indexAction() {
        $tbRacaGato = new Backend_Model_DbTable_TbRacaGato();
        $this->view->racas = $tbRacaGato->findAll();
    }
    
    public function cadastroAction() {
        
        $tbRacaGato = new Backend_Model_DbTable_TbRacaGato();
        $this->view->headTitle("Backend > Raça de Gato > Cadastro");
        
        if($this->_getParam('id_raca_gato', false))
        {
            $this->view->formulario = $tbRacaGato->find(
                    $this->_getParam('id_raca_gato')
                    );
        }
            
        
        if($this->_request->isPost())
        {
            $id_raca = $tbRacaGato->cadastro(
                    $this->_getAllParams()
                    );
            
            $this->view->efetivado = true;
            
            $this->view->formulario = $tbRacaGato->find(
                    $id_raca
                    );
        }
        
        $this->view->relacionamentos = array(
            '1' => 'Otimo',
            '2' => 'Bom',
            '3' => 'Normal',
            '4' => 'Ruim'
        );
        $this->view->temperamentos = array(
            '1' => 'Porradeiro',
            '2' => 'Agressivo',
            '3' => 'Raivoso',
            '4' => 'Manso'
        );
        
    }
    
    public function deletarAction() {
         
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout()->disableLayout();
        $tbRacaGato = new Backend_Model_DbTable_TbRacaGato();
        $tbRacaGato->delete($this->_getParam('id_raca_gato', false));
        $this->_redirect("/backend/raca-gato"); 
    }

}