<?php

class Backend_ConteudoController extends Zend_Controller_Action {

    public function init() {
        
    }

    public function indexAction() {
        $tbConteudo = new Backend_Model_DbTable_TbConteudo();
        $this->view->conteudos = $tbConteudo->listConteudo();
    }
    
    public function cadastroAction() {
        
        
        $tbConteudo = new Backend_Model_DbTable_TbConteudo();
        $tbConteudoCategoria = new Backend_Model_DbTable_TbConteudoCategoria();
        $tbConteudoTag = new Backend_Model_DbTable_TbConteudoTag();
        $this->view->headTitle("Backend > Conteúdo > Cadastro");
        
        $this->view->categoriasSelect = $tbConteudoCategoria->getFormSelect();
         $this->view->categorias = $tbConteudoCategoria->findAll(); 
         
        
        if($this->_getParam('id_conteudo', false))
        {
            $this->view->formulario = $tbConteudo->find(
                    $this->_getParam('id_conteudo')
                    );
            
            $this->view->tags = $tbConteudoTag->findAll(
                "r.id_conteudo = " . $this->_getParam('id_conteudo')
                );
        }
            
        
        if($this->_request->isPost())
        {
            $id_conteudo = $tbConteudo->cadastro(
                    $this->_getAllParams()
                    );
            
            $this->view->efetivado = true;
            
            $this->view->formulario = $tbConteudo->find(
                    $id_conteudo
                    );
            
            $this->view->tags = $tbConteudoTag->findAll(
                "r.id_conteudo = " . $id_conteudo
                );
        }
        
    }

    public function removeAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout()->disableLayout();

        if ($this->getRequest()->isPost()) {
            $request = $this->_getAllParams();
            $conteudo = new Backend_Model_DbTable_TbConteudo();
            $conteudo->removeConteudo($request['id_conteudo']);
        }
    }

    /**
     * Função para cadastrar uma nova categoria
     * por ajax
     * 
     * @name adicionarCategoriaAction
     * @access public
     * @param void
     * @return void 
     */
    public function adicionarCategoriaAction() {
        
        $this->_helper->layout()->disableLayout();
        $tbConteudoCategoria = new Backend_Model_DbTable_TbConteudoCategoria();

        if ($this->_request->isPost()) {
            $tbConteudoCategoria->cadastroAjax(
                    $this->_getAllParams()
                    );
        }
        
        $this->view->id_conteudo = $this->_getParam('id_conteudo');
        $this->view->categorias = $tbConteudoCategoria->findAll();
    }
    
    public function adicionarTagAction() {
        
        $this->_helper->layout()->disableLayout();
        $tbConteudoTag = new Backend_Model_DbTable_TbConteudoTag();
        
        if ($this->_request->isPost()) {
            $tbConteudoTag->cadastroAjax(
                    $this->_getAllParams()
                    );
        }
        
        $this->view->tags = $tbConteudoTag->findAll(
                "r.id_conteudo = " . $this->_getParam('id_conteudo')
                );
    }
    
    public function removerTagAction() {
        
        $this->_helper->layout()->disableLayout();
        $tbConteudoTag = new Backend_Model_DbTable_TbConteudoTag();
        $tbConteudoRelacionadoTag = new Backend_Model_DbTable_TbConteudoRelacionadoTag();
        
        $tbConteudoRelacionadoTag->delete(
                $this->_getParam('id_conteudo_tag'),
                $this->_getParam('id_conteudo')
                );
        
        $this->view->tags = $tbConteudoTag->findAll(
                "r.id_conteudo = " . $this->_getParam('id_conteudo')
                );
        
        $this->render('adicionar-tag');
    }

    /**
     * Função par->a remover uma categoria
     * por ajax
     * 
     * @name removerCategoriaAction
     * @access public
     * @param void
     * @return json 
     */
    public function removerCategoriaAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ($this->_request->isPost()) {
            $conteudo = new Backend_Model_DbTable_TbConteudo();
            $id_categoria = $conteudo->removerCategoria($this->_request->getPost());
            if ($id_categoria) {
                echo json_encode($id_categoria);
            } else {
                echo json_encode(false);
            }
        }
    }

}

?>