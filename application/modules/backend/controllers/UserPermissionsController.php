<?php

class Backend_UserPermissionsController extends Zend_Controller_Action {
	
/* 
 * 	public function saveAction() {
		
		
		if ($this->getRequest ()->isXmlHttpRequest ()) {
			$this->_helper->layout ()->disableLayout ();
			$this->_helper->viewRenderer->setNoRender ( true );
			$db = new Zend_Db_Table ();
			$privilegesIds = explode ( ',', $this->_getParam ( 'ids' ) );
			
			if ('' != $privilegesIds) {
				$role = $db->getDefaultAdapter ()->insert ( 'admin_roles', array ('role_id' => new Zend_Db_Expr ( 'NULL' ), 'name' => $this->_getParam ( 'name' ), 'assert' => new Zend_Db_Expr ( 'NULL' ), 'created' => new Zend_Db_Expr ( 'CURRENT_TIMESTAMP' ) ) );
				$id = $db->getDefaultAdapter ()->lastInsertId ( 'admin_roles', 'role_id' );
				if ($role) {
					
					$data = array ();
					foreach ( $privilegesIds as $ids ) {
						$db->getDefaultAdapter ()->insert ( 'admin_roles_privileges', array ('id' => new Zend_Db_Expr ( 'NULL' ), 'role_id' => ( int ) $id, 'privilege_id' => ( int ) $ids, 'created' => new Zend_Db_Expr ( 'CURRENT_TIMESTAMP' ) ) );
					}
					$data ['message'] = 'Permissão cadastrada com sucesso';
					$data ['response'] = true;
				
				} else {
					$data ['message'] = 'Permissão cadastrada com sucesso';
					$data ['response'] = true;
				}
				
				echo Zend_Json::encode ( $data );
			}
		}else{
			
			
			
		}
	} 
*/
	
/* 	public function saveEditAction() {
		
		if ($this->getRequest ()->isXmlHttpRequest ()) {
			$this->_helper->layout ()->disableLayout ();
			$this->_helper->viewRenderer->setNoRender ( true );
			
			$db = new Zend_Db_Table ();
			$roles = new Backend_Model_DbTable_AdminUserRoles ();
			$name = $this->_getParam ( 'name' );
			
			$Role_id = $this->_getParam ( 'role_id' );
			
			$role = $roles->update ( array ('name' => $name ), array ('role_id = ?' => ( int ) $Role_id ) );
			
			if ($role) {
				
				$data ['message'] = 'Permissão cadastrada com sucesso';
				$data ['response'] = true;
			} else {
				$data ['message'] = 'Permissão cadastrada com sucesso';
				$data ['response'] = false;
			}
			
			if ('' != $this->_getParam ( 'ids' )) {
				$privilegesIds = explode ( ',', $this->_getParam ( 'ids' ) );
				
				 * $privileges = $db->getDefaultAdapter() ->select()
				 * ->from('admin_roles_privileges') ->where('role_id =
				 * ?',(int)$Role_id) ->query() ->fetchAll(); $privileges_id =
				 * array(); foreach ($privileges as $priv){ $privileges_id[] =
				 * $priv['privilege_id']; }
				 
				
				if ('' != $Role_id) {
					// if(count(array_diff($privileges, $privileges_id))==0){
					
					$db->getDefaultAdapter ()->delete ( 'admin_roles_privileges', array ('role_id = ?' => $Role_id ) );
					
					$data = array ();
					foreach ( $privilegesIds as $ids ) {
						$db->getDefaultAdapter ()->insert ( 'admin_roles_privileges', array ('id' => new Zend_Db_Expr ( 'NULL' ), 'role_id' => ( int ) $Role_id, 'privilege_id' => ( int ) $ids, 'created' => new Zend_Db_Expr ( 'CURRENT_TIMESTAMP' ) ) );
					
					}
					
					$data ['message'] = 'Permissão cadastrada com sucesso';
					$data ['response'] = true;
				
				} else {
					$data ['message'] = 'Permissão cadastrada com sucesso';
					$data ['response'] = false;
				}
				
				
			}
			
			echo Zend_Json::encode ( $data );
		}
	}
 */	
	public function addAction() {
			
		if ($this->getRequest ()->isXmlHttpRequest ()) {
			$this->_helper->layout ()->disableLayout ();
			$this->_helper->viewRenderer->setNoRender ( true );
			$db = new Zend_Db_Table ();
			
				
			if ('' != $this->_getParam ( 'ids' )) {
				$privilegesIds = explode ( ',', $this->_getParam ( 'ids' ) );
				$role = $db->getDefaultAdapter ()->insert ( 'admin_roles', array ('role_id' => new Zend_Db_Expr ( 'NULL' ), 'name' => $this->_getParam ( 'name' ), 'assert' => new Zend_Db_Expr ( 'NULL' ), 'created' => new Zend_Db_Expr ( 'CURRENT_TIMESTAMP' ) ) );
				$id = $db->getDefaultAdapter ()->lastInsertId ( 'admin_roles', 'role_id' );
				
				if ($role) {
						
					$data = array ();
					foreach ( $privilegesIds as $ids ) {
						$db->getDefaultAdapter ()
						   ->insert ( 'admin_roles_privileges', array (
						    		'id' => new Zend_Db_Expr ( 'NULL' ),
						    		'role_id' => ( int ) $id,
						    		'privilege_id' => ( int ) $ids, 
						    		'created' => new Zend_Db_Expr ( 'CURRENT_TIMESTAMP' ) ) );
					}
					
					$data ['message'] = 'Permissão cadastrada com sucesso';
					$data ['response'] = true;
		
				} else {
					$data ['message'] = 'Erro ao cadastrar permissão';
					$data ['response'] = false;
				}
		
			   }else{
				
				$data ['message'] = 'Selecione pelo menos um privilegio';
				$data ['response'] = false;
				
			 }
			
			echo Zend_Json::encode ( $data );
			
		}else{
				
			$db = new Backend_Model_DbTable_AdminUserResources ();
			
			$this->view->resources = $db->getDefaultAdapter ()->select ()->from ( array ('resource' => 'admin_resources' ) )->query ()->fetchAll ();
				
		}
	}
	
	public function listAction() {
		$db = new Backend_Model_DbTable_AdminUserRoles ();
		$this->view->permission = $db->select ()->from ( 'admin_roles', array (new Zend_Db_Expr ( "DATE_FORMAT(created,'%d/%c/%Y - %H:%i:%s') as createdFormatted" ), '*' ) )->query ()->fetchAll ();
	
	}
	
	public function editAction() {
		
		if ($this->getRequest ()->isXmlHttpRequest ()) {
			
			$this->_helper->layout ()->disableLayout ();
			$this->_helper->viewRenderer->setNoRender ( true );
				
			$db = new Zend_Db_Table ();
			$roles = new Backend_Model_DbTable_AdminUserRoles ();
			$name = $this->_getParam ( 'name' );
				
			$Role_id = $this->_getParam ( 'role_id' );
				
			$role = $roles->update ( array ('name' => $name ), array ('role_id = ?' => ( int ) $Role_id ) );
				
			if ($role) {
		
				$data ['message'] = 'Permissão cadastrada com sucesso';
				$data ['response'] = true;
			} else {
				$data ['message'] = 'Permissão cadastrada com sucesso';
				$data ['response'] = false;
			}
				
			if ('' != $this->_getParam ( 'ids' )) {
				$privilegesIds = explode ( ',', $this->_getParam ( 'ids' ) );
				
				/*
				 * $privileges = $db->getDefaultAdapter() ->select()
				* ->from('admin_roles_privileges') ->where('role_id =
						* ?',(int)$Role_id) ->query() ->fetchAll(); $privileges_id =
				* array(); foreach ($privileges as $priv){ $privileges_id[] =
				* $priv['privilege_id']; }
				*/
		
				if ('' != $Role_id) {
					// if(count(array_diff($privileges, $privileges_id))==0){
						
					$db->getDefaultAdapter ()->delete ( 'admin_roles_privileges', array ('role_id = ?' => $Role_id ) );
						
					$data = array ();
					foreach ( $privilegesIds as $ids ) {
						$db->getDefaultAdapter ()->insert ( 'admin_roles_privileges', array ('id' => new Zend_Db_Expr ( 'NULL' ), 'role_id' => ( int ) $Role_id, 'privilege_id' => ( int ) $ids, 'created' => new Zend_Db_Expr ( 'CURRENT_TIMESTAMP' ) ) );
							
					}
						
					$data ['message'] = 'Permissão cadastrada com sucesso';
					$data ['response'] = true;
		
				} else {
					$data ['message'] = 'Permissão cadastrada com sucesso';
					$data ['response'] = false;
				}
		
		
			}
				
			echo Zend_Json::encode ( $data );
		}else{
			
			$db = new Backend_Model_DbTable_AdminUserResources ();
			$role_id = $this->_getParam ( 'role-id' );
			$this->view->resources = $db->getDefaultAdapter ()->select ()->from ( array ('resource' => 'admin_resources' ) )->query ()->fetchAll ();
			$privileges = $db->getDefaultAdapter ()->select ()->from ( 'admin_roles_privileges' )->where ( 'role_id = ?', ( int ) $role_id )->query ()->fetchAll ();
			
			$privileges_id = array ();
			foreach ( $privileges as $priv ) {
				$privileges_id [] = $priv ['privilege_id'];
			}
			
			$this->view->privileges = $privileges_id;
			$this->view->permission = $db->getDefaultAdapter ()->select ()->from ( 'admin_roles', array ('role_id', 'name' ) )->where ( 'role_id = ? ', $role_id )->query ()->fetch ();
				
			
		}
		
	}
	
	public function removeAction() {
		
		$this->_helper->layout ()->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ( true );
		
		if ($this->getRequest ()->isXmlHttpRequest ()) {
			
			$db = new Zend_Db_Table ();
			$data = array ();
			if ($db->getDefaultAdapter ()->delete ( 'admin_roles', array ('role_id = ?' => ( int ) $this->_getParam ( 'id' ) ) )) {
				$data ['message'] = 'Permissão removida com sucesso';
				$data ['response'] = true;
			} else {
				$data ['message'] = 'Erro ao remover permissão.';
				$data ['response'] = false;
			
			}
			echo Zend_Json::encode ( $data );
		}
	}
}

?>