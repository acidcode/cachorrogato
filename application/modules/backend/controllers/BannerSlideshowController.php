<?php

/**
 * Controller do modulo de Administracao
 * @author @acidcode
 * @since 2012-06-01
 * @copyright GPO4  - www.cachorrogato.com.br -
 * andre.kernelpanic@gmail.com
 *
 */
class Backend_BannerSlideshowController extends Zend_Controller_Action
{
    private $_tbBannerSlideshow;
    
    public function init()
    {
        $this->_tbBannerSlideshow = new Backend_Model_DbTable_TbBannerSlideshow();
    }

    public function indexAction()
    {
        $this->view->headTitle("Banner Slideshows");
        $this->view->banner_slideshows = $this->_tbBannerSlideshow->fetchAll();
    }
    
    public function cadastroAction()
    {
        $this->view->headTitle('Banner Slideshows - Cadastro');
        $pk = $this->_getParam('id_banner_slideshow',false);
        
        if($this->_request->isPost()) {
            $pk = $this->_tbBannerSlideshow->cadastro(
                    $this->_getAllParams()
                    );
            $this->view->update = true;
        }

        if($pk) {
            $this->view->formulario = $this->_tbBannerSlideshow->fetchRow(
                    "id_banner_slideshow = $pk");
        }
    }

    public function deletarAction() {

        if($this->_getParam('id_banner_slideshow',false)) {
            $where = "id_banner_slideshow = " . $this->_getParam('id_banner_slideshow');
            $this->_tbBannerSlideshow->delete($where);
        }

        $this->_forward('index');
    }
}