<?php

class Backend_CategoriaController extends Zend_Controller_Action {

    public function init() {
        parent::init();
    }

    public function indexAction() {
        $categoria = new Backend_Model_DbTable_TbCategoria();
        $this->view->categoria = $categoria->fetchAll();
    }

    public function editAction() {
        
    }

    public function detalhesAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout()->disableLayout();

        $categoria = new Backend_Model_DbTable_TbCategoria();
        $retorno = $categoria->atualizarCategoria($this->getRequest()->getPost());

        echo Zend_Json_Encoder::encode($retorno);
    }

    public function buscarDetalhesAjaxAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout()->disableLayout();

        if ($this->getRequest()->isPost()) {
            $request = $this->getRequest()->getPost();
            $categoria = new Backend_Model_DbTable_TbCategoria();
            $detalhes = $categoria->find($request['id_categoria']);
            $array = $detalhes->toArray();

            if ($array[0]) {
                echo Zend_Json_Encoder::encode($array[0]);
            } else {
                echo Zend_Json_Encoder::encode(false);
            }
        }
    }

}

?>
