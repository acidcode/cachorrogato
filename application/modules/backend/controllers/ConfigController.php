<?php
/**
 * Controller de Configuracoes
 * @author @acidcode
 * @since 03-03-2011
 * @copyright GPO4  - www.cachorrogato.com.br - andre.kernelpanic@gmail.com
 */
class Backend_ConfigController extends Zend_Controller_Action {

    public function indexAction() {

        $config = new Backend_Model_Config();
        $ConfigContactus = $config->getConfig('config/email-contactus');
        $ConfigchatGoogleAnalytics = $config->getConfig('config/google-analytics');

        $this->view->configsContactus = Zend_Json::decode($ConfigContactus['config_data']);
        $this->view->configsGoogleAnalytics = Zend_Json::decode($ConfigchatGoogleAnalytics['config_data']);
    }

    public function saveAction() {
        $this->_helper->viewRenderer->setNoRender();

        $this->_helper->layout()->disableLayout();
        if ($this->_request->isXmlHttpRequest()) {
            $config = new Backend_Model_DbTable_Config();
            $Messages = array();

            $data_config = $this->_getAllParams();
            $path = 'config/' . $this->_getParam('path');
            unset($data_config['module']);
            unset($data_config['controller']);
            unset($data_config['action']);
            unset($data_config['path']);



            if ($config->getDefaultAdapter()->query('UPDATE config SET config_data = ? WHERE config_path = ? ;', array(
                        Zend_Json::encode($data_config),
                        $path
                    ))->execute()) {

                $Messages['Message'] = 'Configurações salva com sucesso.';
            } else {

                $Messages['Message'] = 'Erro ao salvar configurações.';
            }

            echo Zend_Json::encode($Messages);
        }
    }

}