<?php

class Backend_UserController extends Zend_Controller_Action {

    protected $_instance;

    public function init() {
        $this->_instance = new Backend_Model_DbTable_AdminUser ();
    }

    public function indexAction() {
        
    }

    public function loginAction() {
        $this->_helper->layout()->disableLayout();

        $Messages = array();
        $Params = $this->_getAllParams();

        if ($this->getRequest()->isPost()) {
            $this->_instance->setUsername($Params ['username'])->setPassword($Params ['password']);
            $this->_helper->viewRenderer->setNoRender(true);
            /* if (! Zend_Validate::is ( $this->_instance->getUsername (), 'NotEmpty' )) {
              $Messages ['response'] = false;
              $Messages ['Message'] = 'O campo do usuário não pode estar vazio.';
              } elseif (! Zend_Validate::is ( $this->_instance->getPassword (), 'NotEmpty' )) {
              $Messages ['response'] = false;
              $Messages ['Message'] = 'O campo da senha não pode estar vazio.';
              } else { */
            if (!$this->_instance->authUser()->isValid()) {
                $Messages ['response'] = false;
                $Messages ['Message'] = 'Usuário ou senha estou incorretos.';
            } else {
                $Messages ['response'] = true;
                // $Messages['Message'] = 'acesso realizado';
            }
            //}
            //echo Zend_Json::encode ( $Messages );
            $this->_redirect('/backend/index/');
        }
    }

    public function addAction() {
        $Params = $this->_getAllParams();
        $Messages = array();
        $db = new Backend_Model_DbTable_AdminUserRoles ();

        if ($this->getRequest()->isPost()) {
            echo '<pre>';
            print_r($Params);
            echo '</pre>';
            exit();
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            if ($this->_instance->setUsername($Params ['username'])->setPassword($Params ['password'])->setEmail($Params ['email'])->setFirstname($Params ['firstname'])->setSecondname($Params ['secondname'])->setIs_active($Params ['is_active'])->userExists()) {
                $Messages ['response'] = false;
                $Messages ['Message'] = 'Nome de Usuário já existe.';
            } elseif (Zend_Validate::is($this->_instance->getEmail(), 'EmailAddress')) {
                if ($this->_instance->AddUser()) {

                    $user_id = $this->_instance->getDefaultAdapter()->lastInsertId("admin_user", 'id');

                    $db->getDefaultAdapter()->insert('admin_user_role', array('id' => new Zend_Db_Expr('NULL'),
                        'role_id' => (int) $Params['role_id'],
                        'user_id' => (int) $user_id,
                        'created' => new Zend_Db_Expr('CURRENT_TIMESTAMP')));
                    $Messages ['response'] = true;
                    $Messages ['Message'] = 'Usuário cadastrado com sucesso.';
                } else {
                    $Messages ['response'] = false;
                    $Messages ['Message'] = 'Erro ao cadastrar usuário.';
                }
            } else {
                $Messages ['response'] = false;
                $Messages ['Message'] = 'Email inválido.';
            }
            echo Zend_Json::encode($Messages);
        }
        $this->view->permissions = $db->select()->from('admin_roles', array(new Zend_Db_Expr("DATE_FORMAT(created,'%d/%c/%Y - %H:%i:%s') as createdFormatted"), '*'))->query()->fetchAll();
    }

    public function recoverPasswordAction() {

        $this->_helper->layout()->setLayout('recover-password');
        $token = $this->_getParam('token');
        if ($token && $token != '') {
            $tokenExist = $this->_instance->getDefaultAdapter()->query('SELECT *,COUNT(*) AS count FROM admin_user WHERE rp_token = ? ', array($token))->fetch();
            if ($tokenExist ['count'] > 0) {
                $this->view->realUser = true;
                $this->view->userId = $tokenExist ['id'];
                $this->view->userName = $tokenExist ['firstname'] . ' ' . $tokenExist ['secondname'];
            }
        } else {
            $this->view->realUser = false;
        }
    }

    public function genTokenAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if ($this->_request->isXmlHttpRequest()) {
            $emailfield = $this->_getParam('email');
            $data = array();

            if ($emailfield == '') {
                $data ['message'] = 'O campo email não pode estar vazio';
                $data ['response'] = false;
            } else {
                $user = $this->_instance->fetchUserByEmail($emailfield);

                if ($user ['count'] <= 0) {
                    $data ['message'] = 'O Usuário não existe.';
                    $data ['response'] = false;
                } else {
                    $token = md5(time());
                    if ($this->_instance->getDefaultAdapter()->update("admin_user", array('rp_token' => $token, 'rp_token_created_at' => new Zend_Db_Expr('CURRENT_TIMESTAMP')), 'id = ' . $user ['id'])) {

                        $mail = new Backend_Model_Mail();
                        $config = new Backend_Model_Config();
                        $Config = $config->getConfig('config/email-resetpassword');
                        $mail->setOptions(Zend_Json::decode($Config['config_data']));
                        $mail->setForm('contactus')->setViewName('recover-password.phtml');

                        $email = $mail->getMail();
                        $option = $mail->getOptions();

                        $email->addTo($user['email'])->setReturnPath($option['username'])
                                ->setSubject('Recuperar senha');

                        $request = array('name' => $user ['firstname'] . ' ' . $user ['secondname'], 'token' => $token);

                        $view = $mail->getView();
                        $view->assign('request', $request);

                        if ($mail->send()) {

                            $data ['message'] = 'Um email foi enviado para ' . $emailfield . ' , com todas as instruções para criar uma nova senha. <div id="back-reset"  onclick="back();" style="color:green;text-decoration:underline;cursor:pointer;">Voltar</div>';
                            $data ['response'] = true;
                        } else {

                            $data ['message'] = 'Erro ao gerar token. <div id="back-reset" onclick="back();" style="color:green;text-decoration:underline;cursor:pointer;">Voltar</div>';
                            $data ['response'] = false;
                        }
                    } else {

                        $data ['message'] = 'Erro ao gerar token. <div id="back-reset" onclick="back();" style="color:green;text-decoration:underline;cursor:pointer;">Voltar</div>';
                        $data ['response'] = false;
                    }
                }
            }
            echo Zend_Json::encode($data);
        }
    }

    public function logoutAction() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $this->_instance->getAuth()->clearIdentity();

        $this->_redirect('/admin/');
    }

    public function removeAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $Params = $this->_getAllParams();
        $Messages = array();
        if ($this->getRequest()->isPost()) {
            if ($this->_instance->setId($Params ['id'])->removeUser()) {
                $Messages ['response'] = true;
                $Messages ['Message'] = 'Usuário removido  com sucesso.';
            } else {
                $Messages ['response'] = false;
                $Messages ['Message'] = 'Erro ao remover usuário.';
            }
            echo Zend_Json::encode($Messages);
        }
    }

    public function resetPasswordAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $Params = $this->_getAllParams();
        $Messages = array();
        if ($this->getRequest()->isPost()) {

            $this->_instance->setId($Params ['id'])->setPassword($Params ['password']);
            if (Zend_Validate::is($this->_instance->getPassword(), 'NotEmpty')) {

                if ($this->_instance->changePassword()) {

                    $this->_instance->getDefaultAdapter()->update("admin_user", array('rp_token' => new Zend_Db_Expr('NULL'), 'rp_token_created_at' => new Zend_Db_Expr('NULL')), array('id' => $Params ['id']));

                    $Messages ['response'] = true;
                    $Messages ['Message'] = 'Senha atualizada com sucesso.';
                } else {

                    $Messages ['response'] = false;
                    $Messages ['Message'] = 'Erro ao atualizar senha do usuário.';
                }
            } else {

                $Messages ['response'] = false;
                $Messages ['Message'] = 'O campo da senha não pode estar vazio.';
            }

            echo Zend_Json::encode($Messages);
        }
    }

    public function changePasswordAction() {
        $Params = $this->_getAllParams();
        $Messages = array();
        if ($this->getRequest()->isPost()) {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $this->_instance->setId($Params ['id'])->setPassword($Params ['password']);
            if (Zend_Validate::is($this->_instance->getPassword(), 'NotEmpty')) {

                if ($this->_instance->changePassword()) {

                    $Messages ['response'] = true;
                    $Messages ['Message'] = 'Senha atualizada com sucesso.';
                } else {

                    $Messages ['response'] = false;
                    $Messages ['Message'] = 'Erro ao atualizar senha do usuário.';
                }
            } else {

                $Messages ['response'] = false;
                $Messages ['Message'] = 'O campo da senha não pode estar vazio.';
            }

            echo Zend_Json::encode($Messages);
        }
    }

    public function editAction() {
        $Params = $this->_getAllParams();
        
        $Messages = array();
        $user = $this->_instance->setId($Params ['id'])->fetchUserByid();
        $db = new Backend_Model_DbTable_AdminUserRoles ();

        $this->view->user = $user [0];
        if ($this->getRequest()->isPost()) {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            $this->_instance->setId($Params ['id'])
                    ->setUsername($Params ['username'])
                    ->setPassword($Params ['password'])
                    ->setEmail($Params ['email'])
                    ->setFirstname($Params ['firstname'])
                    ->setSecondname($Params ['secondname'])
                    ->setIs_active($Params ['is_active'])
                    ->setabout($Params ['about'])
                    ->userExists($Params ['id']);
            $pk = $this->_instance->editUser();
            
            $this->_redirect("/backend/user/edit/id/" . $Params ['id']);
        }

        $this->view->permissions = $db->select()->from('admin_roles', array(new Zend_Db_Expr("DATE_FORMAT(created,'%d/%c/%Y - %H:%i:%s') as createdFormatted"), '*'))->query()->fetchAll();
        $this->view->permissionSelected = $db->getDefaultAdapter()->select()->from('admin_user_role')->where('user_id = ? ', $Params ['id'])->query()->fetch();
    }

    public function listAction() {
        $this->view->Users = $this->_instance->listAllUsers();
    }

}

?>