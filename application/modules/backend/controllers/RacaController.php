<?php

class Backend_RacaController extends Zend_Controller_Action {

    public function init() {
        parent::init();
    }

    public function indexAction() {
        $raca = new Backend_Model_DbTable_TbRaca();
        $this->view->raca = $raca->fetchAll();
    }

    public function addAction() {
        $request = $this->_request->getParams();
        $raca = new Backend_Model_DbTable_TbRaca();
        $this->view->conteudo = array();

        if ($this->_request->isPost()) {
            $raca->inserir($this->getRequest()->getPost());
            $this->view->efetivado = true;
        }

        if (!empty($request['id_raca'])) {
            $this->view->conteudo = $raca->find($request['id_raca']);
            $this->view->conteudo['submit'] = 'Atualizar Raça';
        } else {
            $this->view->conteudo['submit'] = 'Cadastrar Raça';
        }
    }

    public function removeAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout()->disableLayout();

        $request = $this->_request->getParam('id_raca');
        $id_raca = $request['id_raca'];

        if (!empty($id_raca) && (int) $id_raca) {
            $where = 'id_raca = ' . $id_raca;
            $raca = new Backend_Model_DbTable_TbRaca();
            $raca->delete($where);
        }

        $this->_redirect('/backend/raca/');
    }

}

?>
