<?php
class Backend_VideoController extends Zend_Controller_Action
{
    protected $_instance;
    public function init ()
    {
    	$this->_instance = new Backend_Model_DbTable_Videos();
    }
        
   public function addAction ()
    {
        $Params = $this->_getAllParams();
        $Messages = array();
        if ($this->getRequest()->isPost()) {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $this->_instance->setTitle($Params['title'])
                            ->setLink($Params['link'])
                            ->setDescription($Params['description'])
                            ->setStatus($Params['status']);
          
                if ($this->_instance->addVideo()) {
                    $Messages['response'] = true;
                    $Messages['Message'] = 'Video cadastrado com sucesso.';
                } else {
                    $Messages['response'] = false;
                    $Messages['Message'] = 'Erro ao cadastrar video.';
                }
            
            echo Zend_Json::encode($Messages);
        }
    
   }
    
 
  public function removeAction ()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $Params = $this->_getAllParams();
        $Messages = array();
        if ($this->getRequest()->isPost()) {
            if ($this->_instance->setId($Params['id'])->removeVideo()) {
                $Messages['response'] = true;
                $Messages['Message'] = 'Video removido  com sucesso.';
            } else {
                $Messages['response'] = false;
                $Messages['Message'] = 'Erro ao remover Video.';
            }
            echo Zend_Json::encode($Messages);
        }
    }
    
    public function editAction ()
    {
    	$Params = $this->_getAllParams();
    	$Messages = array();
    	$video = $this->_instance->setId($Params['id'])
    	                         ->fetchVideoByid();
    	
    	$this->view->video = $video[0];
    	
    	if ($this->getRequest()->isPost()) {
    		$this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
    		$this->_instance->setTitle($Params['title'])
    		                ->setLink($Params['link'])
    		                ->setId($Params['id'])
    		                ->setStatus($Params['status'])
    		                ->setDescription($Params['description']);
    		
    		
    			if ($this->_instance->editVideo()==1) {
    				$Messages['response'] = true;
    				$Messages['Message'] = 'Video Atualizado com sucesso.';
    			} else {
    				$Messages['response'] = false;
    				$Messages['Message'] = 'Erro ao Atualizar Video , tente novamente.';
    			}
    		
    		echo Zend_Json::encode($Messages);
    	}
    }
    public function listAction ()
    {
    	$this->view->Videos = $this->_instance->listAllVideos();
    }
    
    
}
?>