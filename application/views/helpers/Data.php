<?php

/*
 * Helper que retorna o uma data em vários formatos diferentes.
 * ex: echo $this->Data('2009-18-11')->extenso;
 * ex: echo $this->Data('2009-18-11')->pt_br;
 * ex: echo $this->Data('2009-18-11 09:11:00')->hora;
 * @author Nivaldo Arruda - nivaldo@gmail.com
 * @see www.nivaldoarruda.com.br
 * @version 1.0
 */

class Zend_View_Helper_Data extends Zend_View_Helper_Abstract {

    public $extenso;
    public $pt_br;
    public $hora;
    public $dataHora;
    public $dataExtenso;

    public function data($data) {
        //@acidcode
        //Se a data entrar vazio, nao retornar um erro de mktime
        if (empty($data)) {
            return false;
        }

        list($ano, $mes, $dia) = explode("-", substr($data, 0, 10));
        $this->extenso = $this->diasemana("$ano-$mes-$dia");
        $this->dataExtenso = $this->dataExtenso("$ano-$mes-$dia");
        $this->pt_br = "$dia/$mes/$ano";


        if (strlen($data) > 10) {
            list($hora, $minuto, $segundo) = explode(":", substr($data, 11, 8));
            $this->hora = "$hora:$minuto:$segundo";
            $this->dataHora = "$dia/$mes/$ano ás $hora:$minuto:$segundo";
            $this->dataExtenso = $this->dataExtenso("$ano-$mes-$dia");
        }

        return $this;
    }

    /**
     * Retorna o dia da semana, por extenso e em português, correspondente
     * a data informada por parametro (no padrão aaaa-mm-dd).
     *
     * @param Date $data
     * @return String
     */
    public function diasemana($data) {
        list($ano, $mes, $dia) = explode("-", $data);

        $diasemana = date("w", mktime(0, 0, 0, $mes, $dia, $ano));

        switch ($diasemana) {
            case 0: $diasemana = "Domingo";
                break;
            case 1: $diasemana = "Segunda-Feira";
                break;
            case 2: $diasemana = "Terça-Feira";
                break;
            case 3: $diasemana = "Quarta-Feira";
                break;
            case 4: $diasemana = "Quinta-Feira";
                break;
            case 5: $diasemana = "Sexta-Feira";
                break;
            case 6: $diasemana = "Sábado";
                break;
        }

        return $diasemana;
    }

    public function dataExtenso($data) {
        list($ano, $mes, $dia) = explode("-", $data);
        // leitura das datas
        $dia = date('d');
        $mes = date('m');
        $ano = date('Y');
        $semana = date("w", mktime(0, 0, 0, $mes, $dia, $ano));


        // configuração mes
        switch ($mes) {

            case 1: $mes = "Janeiro";
                break;
            case 2: $mes = "Fevereiro";
                break;
            case 3: $mes = "Março";
                break;
            case 4: $mes = "Abril";
                break;
            case 5: $mes = "Maio";
                break;
            case 6: $mes = "Junho";
                break;
            case 7: $mes = "Julho";
                break;
            case 8: $mes = "Agosto";
                break;
            case 9: $mes = "Setembro";
                break;
            case 10: $mes = "Outubro";
                break;
            case 11: $mes = "Novembro";
                break;
            case 12: $mes = "Dezembro";
                break;
        }


        // configuração semana

        switch ($semana) {

            case 0: $semana = "Domingo";
                break;
            case 1: $semana = "Segunda-Feira";
                break;
            case 2: $semana = "Terça-Feira";
                break;
            case 3: $semana = "Quarta-Feira";
                break;
            case 4: $semana = "Quinta-Feira";
                break;
            case 5: $semana = "Sexta-Feira";
                break;
            case 6: $semana = "Sábado";
                break;
        }
        //Agora basta imprimir na tela...
        return ("$semana, $dia de $mes de $ano");
    }

    public function dataToMysql($data) {

        $data = explode("/", $data);

        return $data[2] . "-" . $data[1] . "-" . $data[0];
    }

}

?>