<?php

/**
 * helper para mascara de dinheiro
 * @author @acidcode
 * @since 20/09/2010
 */
class Zend_View_Helper_Dinheiro extends Zend_View_Helper_Abstract {

    /**
     * Retorna valor formatado
     * @param double $valor Valor a ser tratado pela função
     * @param boolean $showEmpty Retornar valor "" caso o valor de saída for 0,00
     * @return string
     */
    public function dinheiro($valor = null, $showEmpty = false) {

        //Retorna valor em branco caso for 0, isso axilia na digitação dos valores.
        if ((float) $valor == 0 && $showEmpty) {
            return "";
        }

        return number_format($valor, 2, ",", ".");
    }

}