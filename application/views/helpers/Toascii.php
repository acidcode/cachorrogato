<?php

/**
 * String to ascii puro
 * @author #desconhecido
 * @author @acidcode
 * @since 07-05-2012
 */
class Zend_View_Helper_Toascii extends Zend_View_Helper_Abstract {

    function toascii($str, $replace = array(), $delimiter = '-') {
        if (!empty($replace)) {
            $str = str_replace((array) $replace, ' ', $str);
        }

        $str = $this->alpha($str);

        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

        return $clean;
    }

    function alpha($str) {
        $tofind = array(
            'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'à', 'á', 'â', 'ã', 'ä', 'å',
            'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø',
            'È', 'É', 'Ê', 'Ë', 'è', 'é', 'ê', 'ë', 'Ç', 'ç',
            'Ì', 'Í', 'Î', 'Ï', 'ì', 'í', 'î', 'ï',
            'Ù', 'Ú', 'Û', 'Ü', 'ù', 'ú', 'û', 'ü',
            'ÿ', 'Ñ', 'ñ', 'º', '°', '!', '@', '#', '$', '%', '?', ''
        );

        $replac = array(
            'A', 'A', 'A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a', 'a',
            'O', 'O', 'O', 'O', 'O', 'O', 'o', 'o', 'o', 'o', 'o', 'o',
            'E', 'E', 'E', 'E', 'e', 'e', 'e', 'e', 'C', 'c',
            'I', 'I', 'I', 'I', 'i', 'i', 'i', 'i',
            'U', 'U', 'U', 'U', 'u', 'u', 'u', 'u',
            'y', 'N', 'n', '', '', '', '', '', '', '', '', ''
        );

        $alfa = strtoupper(str_replace($tofind, $replac, $str));

        return $alfa;
    }

}
