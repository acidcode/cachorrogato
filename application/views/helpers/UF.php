<?php

/**
 * helper para criar select de Estados
 * @author @acidcode
 * @since 06/09/2010
 */
class Zend_View_Helper_UF extends Zend_View_Helper_Abstract {

    public function UF($todos = false) {


        $estados = array("SP" => "São Paulo",
            "AC" => "Acre",
            "AL" => "Alagoas",
            "AM" => "Amazonas",
            "AP" => "Amapá",
            "BA" => "Bahia",
            "CE" => "Ceará",
            "DF" => "Distrito Federal",
            "ES" => "Espirito Santo",
            "GO" => "Goiás",
            "MA" => "Maranhão",
            "MT" => "Mato Grosso",
            "MS" => "Mato Grosso do Sul",
            "MG" => "Minas Gerais",
            "PA" => "Pará",
            "PB" => "Paraíba",
            "PR" => "Paraná",
            "PE" => "Pernambuco",
            "PI" => "Piauí",
            "RJ" => "Rio de Janeiro",
            "RN" => "Rio Grande do Norte",
            "RO" => "Rondônia",
            "RS" => "Rio Grande do Sul",
            "RR" => "Roraima",
            "SC" => "Santa Catarina",
            "SE" => "Sergipe",
            "TO" => "Tocantins"
        );

        if ($todos) {
            $estados['TD'] = "Todos";
        }

        return $estados;
    }

}